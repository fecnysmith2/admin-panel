process.env.NODE_ENV = "dev";
const path = require('path');
const express = require('express');
const history = require('connect-history-api-fallback');
const nodemailer = require('nodemailer');
const logger = require('morgan'); // https://github.com/expressjs/morgan
const xhr = require('xhr');
const app = express();
const bodyParser = require('body-parser');
const jsonfile = require('jsonfile');
const fs = require('fs');
const crypto = require('crypto'),
    algorithm = 'aes256',
    password = '0cb029d';
const querystring = require('querystring');
const request = require('request');

var PORT;
switch (process.env.NODE_ENV) {
    case 'production':
        PORT = 35999;
        break;
    case 'test':
        PORT = 35999;
        break;
    case 'dev':
        PORT = 35999;
        break;
    default:
        PORT = 35999;
        break;
}


app.use(history());
if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'test') {
    console.log('START DEV');
    const webpackDevMiddleware = require('webpack-dev-middleware');
    const webpackHotMiddleware = require('webpack-hot-middleware');
    const webpack = require('webpack');
    const config = require('./webpack.config.js');
    const compiler = webpack(config);

    app.use(webpackDevMiddleware(compiler, {noInfo: true, publicPath: config.output.publicPath}));
    app.use(webpackHotMiddleware(compiler));
    app.use(logger('dev'));
} else {
    console.log('START PROD');
}

app.use('/', express.static('js'));

app.use('/scripts', express.static('scripts'));
app.use('/asset', express.static('asset'));
app.use('/css', express.static('css'));
app.use('/images', express.static('images'));
app.use('/font', express.static('font'));
app.use('/fonts', express.static('fonts'));

const urlencodedParser = bodyParser.urlencoded({extended: false});

app.listen(PORT, function (error) {
    if (error) {
        console.error(error);
    } else {
        console.info("Listening on port %s. Visit http://localhost:%s/ in your browser.", PORT, PORT);
    }
});
