const environment = 'prod';

module.exports = {
  environment, backend, baseUrl, webshopUrl, simple, simpleToken, maintenance, FB_TOKEN
};
