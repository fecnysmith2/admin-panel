const environment = 'prod';

// Backend variánsok
const backend = '//api.onlinetrainer.hu/OTSystem';

const baseUrl = '//onlinetrainer.hu';
const webshopUrl = '//webshop.onlinetrainer.hu';
const simple = 'https://secure.simplepay.hu/payment/order/lu.php';
const simpleToken = "https://secure.simplepay.hu/payment/order/tokens/";
const maintenance = false;
const FB_TOKEN = '275444269532489';

module.exports = {
  environment, backend, baseUrl, webshopUrl, simple, simpleToken, maintenance, FB_TOKEN
};
