/* ----------------------------------------------------
 *  Wizard
 *
 *  JavaScript file for the forms.wizard.html.
 *
 *  Description: Contains codes from jQuery Steps & bootstrapWizard
 *  Offical Site: http://www.jquery-steps.com/
 *                https://github.com/VinceG/twitter-bootstrap-wizard
 * ---------------------------------------------------- */
 
/*global
    jQuery
*/ 


'use strict';

var Wizard = function($) {


    /* ------------------------- 
        Begin jQuery Steps
     ------------------------- */

    var Steps = function() {
        // Store the form element
        var form = $('#product-edit-form').show();
    

        // Initiate the jQuery Steps Wizard
        form.steps({
            headerTag: 'h3',
            bodyTag: 'fieldset',
            transitionEffect: 'fade',
            enablePagination: false,
            enableAllSteps: true,
            titleTemplate: '#title#',
            loadingTemplate: '<div class="loading"></div>'
        });
    };

    /* ------------------------- 
        End jQuery Steps
     ------------------------- */


    /* ------------------------- 
        Begin Boostrap Wizard
     ------------------------- */ 

    var BootstrapWizard = function() {

        // Initiate bootstrap wizard
        $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#rootwizard').find('.bar').css({width:$percent+'%'});
            
            // If it's the last tab then hide the last button and show the finish instead
            if($current >= $total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
            
        }});
    };

    /* ------------------------- 
        End Bootstrap Wizard
     ------------------------- */


    // Return all the function under Init
    return {
        Init: function() {
            Steps();
            BootstrapWizard();
        }
    };
}(jQuery);
$(document).ready(function() {
    $('.summernote-editor').summernote({height:230});
    $('.summernote-airmode').summernote({
        airMode: true
    });
});
// Initiate the Wizard
