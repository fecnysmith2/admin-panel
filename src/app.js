import React, {Component} from 'react';
import bindAll from 'lodash.bindall';
import {withRouter} from 'react-router';
import Joyride from 'react-joyride';
import Header from './views/header';
import Footer from './views/footer';
import {environment, maintenance, TITLE_BASE} from '../config';
import {isLoginSessionActive} from "./utils/db";
import makeRequest from './utils/request-queue-handler';
import Navigation from "./views/navigation";
import Loading from 'react-loading-bar'
import 'react-loading-bar/dist/index.css'
import SideOverlay from "./views/SideOverlay";

class App extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'getFullPage'
        );

        this.state = {
            showLoading: true,
            hasConnectionError: false
        };
    }


    loadStoreSettings(){
        let that = this;
        makeRequest("loadStoreSettings", {}, function(ret){
            that.setState({loading:false});
        }, function(){
            that.setState({hasConnectionError: true});
        });
    }

    getFullPage() {
        const {location, children} = this.props;
        const loggedIn = isLoginSessionActive();

        window.log("isLoginSessionActive", isLoginSessionActive())
        let that = this;
        const childrenWithProps = React.Children.map(children, function (child) {
            return React.cloneElement(child);
        });

        let contentWrapperClass = "content-wrapper";
        if(window.location.pathname.includes('mails')){
            contentWrapperClass += " mail";
        }

        let component_SideOverlay, component_Navigation, component_Header, component_Main, component_Footer;

        if(loggedIn){ component_SideOverlay = <SideOverlay/>;}
        if(loggedIn){ component_Navigation = <Navigation/>;}
        if(loggedIn){ component_Header = <Header/>;}
        if(loggedIn){ component_Main = (<main id="main-container">
            <div className="content">
                {childrenWithProps}
            </div>
        </main>);}
        if(loggedIn){ component_Footer = <Footer/>;}


        return (
            <div id="page-container" className={loggedIn ? "sidebar-o side-scroll page-header-modern main-content-boxed side-trans-enabled sidebar-inverse" : "main-content-boxed side-trans-enabled"}>
            {this.state.hasConnectionError ? (<div id="webpack-hot-middleware-clientOverlay" className="connectionError text-center">
                <div style={{"margin-bottom": "26px"}}>
                    <h3 className="text-center">Megszakadt a kapcsolat a kiszolgálóval...</h3>
                </div>
            </div>) : ""}
                {loggedIn ? <SideOverlay/> : ""}
                {loggedIn ? <Navigation/> : ""}
                {loggedIn ? <Header/> : ""}
                {loggedIn ? <main id="main-container">
                        {childrenWithProps}
                </main> :
                    childrenWithProps}
                {loggedIn ? <Footer/> : ""}

            </div>
        );
    }

    render() {
        return this.getFullPage();
    }
}

App.propTypes = {};
App.defaultProps = {};

export default withRouter(App);
