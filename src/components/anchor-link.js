import React, {
   Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import is from 'is_js';

class AnchorLink extends Component {
   constructor(props) {
       super(props);
       bindAll(this,
          'onClick',
      );
   }

  onClick() {
    var that = this;
    if (is.firefox()) {
      document.getElementById(that.props.to + '-link').click();
    }
    this.props.onClick();
  }

  render () {
    return (
      <a id={this.props.to + '-link'} onClick={this.onClick} href={'#' + this.props.to} className={this.props.className}>
        {this.props.children}
      </a>
    )
  }
}

AnchorLink.propTypes = {};
AnchorLink.defaultProps = {
    to : '',
    className : '',
    onClick   : function(){}
};

export default AnchorLink;