import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {withRouter} from 'react-router';
import {getOauthTokenObject} from '../utils/db';
import {getUserObject} from '../utils/user-util';

export default function withAuthCheck(WrappedComponent, groups = [], shouldRedirect = true) {
    class WithAuthCheck extends Component {

        constructor(props) {
            super(props);
            // bindAll(this,
            //     '',
            // );
            let canAccess = groups.length > 0 ? false : true;
            const userObject = getUserObject();
            groups.forEach(function (group) {
                if (userObject != null && userObject != '') {
                    userObject.userAssigns.forEach(function (userAssign) {
                        if (group.userType == userAssign.userType && group.userSubtype == userAssign.internalUserData.userSubtype) {
                            canAccess = true;
                        }
                    });
                }
            });
            this.state = {
                canAccess: canAccess
            };
        }

        componentWillMount() {
            const {router, route: {path}} = this.props;
            const {canAccess} = this.state;
            window.log('PATH', path);
            window.log('PATH WHAT', canAccess + ' - ' + shouldRedirect);
            if (shouldRedirect) {
                let redirect = '';
                if (path != '/') {
                    redirect = btoa(path);
                    redirect = '/' + redirect;
                }
                if (!getOauthTokenObject()) {
                    router.push('/login' + redirect);
                    window.log('PATH', redirect);
                } else if (!canAccess) {
                    window.log('PATH', redirect);
                    router.push('/accessdenied');
                }
            }
        }

        render() {
            const { canAccess } = this.state;
            if (!getOauthTokenObject() || !canAccess) {
                return null;
            }
            return (
                <WrappedComponent {...this.props}/>
            );
        }
    }
    WithAuthCheck.propTypes = {};
    WithAuthCheck.defaultProps = {};
    return withRouter(WithAuthCheck);
}

