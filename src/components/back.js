import React, {
   Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import { withRouter, Link } from 'react-router';

class Back extends Component {
   constructor(props) {
       super(props);
       bindAll(this,
          'goBack',
      );
   }

    goBack(event) {
      this.props.router.goBack()
    }

    render() {
        return (
            <div>
              <input type='submit' onClick={this.goBack} value='Vissza'/>
            </div>
        );
    }
}

Back.propTypes = {};
Back.defaultProps = {};

export default withRouter(Back);