import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {withRouter} from "react-router";
import {getUserObjectProperty} from "../utils/user-util";

class DemoContainer extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'isDemoUser', 'isDependentPartnerUser', 'isInDemoUserGroups',
        );

        this.state = {
            isRenderable: false
        };
    }

    componentWillMount() {
        const { isDemoUser } = this;
        this.setState({
            isRenderable: isDemoUser()
        });
    }

    isDependentPartnerUser(dependentPartner) {
        return dependentPartner != null && dependentPartner.login.toLowerCase().includes('budapestbank');
    }

    isInDemoUserGroups(userAssign) {
        return (
            userAssign.userType == 'A' &&
            userAssign.internalUserData.userSubtype != null &&
            (
                userAssign.internalUserData.userSubtype == 9 ||
                userAssign.internalUserData.userSubtype == 3
            )
        );
    }

    isDemoUser() {
        const dependentPartner = getUserObjectProperty('dependentPartner');
        const userAssigns = getUserObjectProperty('userAssigns');
        if (userAssigns != null) {
            for (let i = 0; i < userAssigns.length; i++) {
                if (this.isInDemoUserGroups(userAssigns[i])) {
                    return true;
                }
            }
        }
        return false;
    }

    render() {
        const { notDemoContent, children } = this.props;
        const { isRenderable } = this.state;
        if (!isRenderable) {
            return notDemoContent ? notDemoContent : null;
        }
        return children ? children : null;
    }
}

DemoContainer.propTypes = {};
DemoContainer.defaultProps = {
    notDemoContent: ''
};

export default withRouter(DemoContainer);
