import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {environment} from '../../config'

class DevelopmentContainer extends Component {

    constructor(props) {
        super(props);
        // bindAll(this,
        //     '',
        // );

        this.state = {
            data: null
        };
    }

    render() {
        if (environment != 'prod') {
            return this.props.children;
        }
        return null;
    }
}

DevelopmentContainer.propTypes = {};
DevelopmentContainer.defaultProps = {};

export default DevelopmentContainer;
