import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import is from 'is_js';
import Input from './input';
import makeRequest from '../utils/request-queue-handler';
import {getFrontendComponentGraphVertexQuery} from '../utils/queries/queries';

class FrontendPage extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'processQuery', 'onDataReceived', 'onError',
            'handleSubmit', 'resetForm',
        );

        this.state = {
            callName: '',
            queryParamName: '',
            dataObjectConnections: []
        };
    }

    processQuery(data) {
        this.setState({
            callName: data.callName,
            queryParamName: data.queryParamName,
            dataObjectConnections: data.dataObjectConnections
        });
    }

    onDataReceived(response) {
        let data = response.data.FrontendComponentGraphVertex[0].component;
        this.processQuery(data);
    }

    onError(error) {
        window.error('sendQuery error', error);
    }

    componentWillMount() {
        const {queryName} = this.props;
        makeRequest(getFrontendComponentGraphVertexQuery(queryName), this.onDataReceived, this.onError);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

    componentDidUpdate(prevProps, prevState, prevContext) {
        const { clearFields } = this.props;
        if (clearFields) {
            this.resetForm();
        }
    }

    handleSubmit(event) {
        window.log('HANDLE SUBMIT');
        /*
         * Safari doesn't support required attribute on inputs, so we need to handle it with this alert dialog
         */
        if (is.safari()) {
            if (!event.target.checkValidity()) {
                event.preventDefault();
                alert('Kérlek tölts ki minden kötelező mezőt');
                return;
            }
        }
        this.props.handleSubmit(event);
    }

    resetForm() {
        const {queryParamName} = this.state;
        document.getElementById(queryParamName + 'Form').reset();
    }

    render() {
        const {queryParamName, dataObjectConnections, callName} = this.state;
        const {loading, params, errors, formIdPrefix} = this.props;
        if (!dataObjectConnections.length) {
            return (
                <i className='loader-item'></i>
            )
        }
        const that = this;
        let containsRequiredField = false;
        const dataObjects = dataObjectConnections.map(function (dataObjectConnection, index) {
            if (dataObjectConnection.required) {
                containsRequiredField = true;
            }
            return (
                <Input
                    errors={errors}
                    key={index}
                    dataObjectConnection={dataObjectConnection}
                    callName={callName}
                    loading={loading}
                    params={params}/>
            );
        });
        window.log('dataObjects: ', dataObjects);
        const requiredInfo = (<p className='required'>A *-gal jelölt mezők kitöltése kötelező!</p>);
        return (
            <form id={formIdPrefix + queryParamName + 'Form'} data-queryParamName={queryParamName} onSubmit={this.handleSubmit}>
		    	<div className="frontend-page">
					{containsRequiredField ? requiredInfo : ''}
                    {dataObjects}
				</div>
			</form>
        );
    }
}

FrontendPage.propTypes = {};
FrontendPage.defaultProps = {
    clearFields: false,
    formIdPrefix: ''
};

export default FrontendPage;
