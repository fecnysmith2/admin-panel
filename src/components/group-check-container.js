import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {getUserObject} from '../utils/user-util';

class GroupCheckContainer extends Component {

    constructor(props) {
        super(props);
        // bindAll(this,
        //     '',
        // );

        let canAccess = props.groups.length > 0 ? false : true;
        props.groups.forEach(function (group) {
            getUserObject().userAssigns.forEach(function (userAssign) {
                if (group.userType == userAssign.userType && group.userSubtype == userAssign.internalUserData.userSubtype) {
                    canAccess = true;
                }
            });
        });
        this.state = {
            canAccess: canAccess
        };
    }

    render() {
        const {canAccess} = this.state;
        if (!canAccess) {
            return null;
        } else {
            return this.props.children;
        }
    }
}

GroupCheckContainer.propTypes = {};
GroupCheckContainer.defaultProps = {
    groups: []
};

export default GroupCheckContainer;
