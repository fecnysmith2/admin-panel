import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {getTextFromHybridMemo} from '../utils/helpers'

class HealthTestInput extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'getContent', 'createRadioInputs', 'createCheckBoxInputs',
            'createSelectInput', 'createSelectOptionInputs', 'createSingleAnswer',
            'createMultiAnswer', 'createComplexInput', 'createMultiComplexInput',
        );

    }

    getContent() {
        const {item} = this.props;
        item.question.answers.sort(function (answer, otherAnswer) {
            return parseInt(answer.ordernum) - parseInt(otherAnswer.ordernum);
        });

        if (item.question.multiAnswer) {
            return this.createMultiAnswer();
        } else if (item.question.addValueMin != undefined && item.question.addValueMax != undefined) {
            return this.createComplexInput();
        } else {
            return this.createSingleAnswer();
        }
    }

    createRadioInputs() {
        const {item} = this.props;
        const questionRecnum = item.question.recnum;
        return item.question.answers.map(function (answer, index) {
            const answerRecnum = answer.recnum;
            const answerLetter = answer.letter;
            let input = '';
            if (item.previouslyChosenValue) {
                if (answerLetter == item.previouslyChosenValue.letter) {
                    return (
                        <div key={index}>
							<input
                                className='cursor-pointer'
                                type="radio"
                                name={questionRecnum}
                                id={'radioInput' + answerRecnum + index}
                                data-recnum={answerRecnum}
                                data-questionrecnum={questionRecnum}
                                value={answerLetter} checked/>
							<label className='cursor-pointer' htmlFor={'radioInput' + answerRecnum + index}>
								<span></span> {getTextFromHybridMemo(answer.text)}
							</label>
						</div>
                    );
                }
            }

            return (
                <div>
					<input
                        className='cursor-pointer'
                        type="radio"
                        id={'radioInput' + answerRecnum + index}
                        name={questionRecnum}
                        data-recnum={answerRecnum}
                        data-questionrecnum={questionRecnum}
                        value={answerLetter}/>
					<label className='cursor-pointer' htmlFor={'radioInput' + answerRecnum + index}>
						<span></span> {getTextFromHybridMemo(answer.text)}
					</label>
				</div>
            );
        });
    }

    createCheckBoxInputs(withAddValues = false) {
        const {item} = this.props;
        const questionRecnum = item.question.recnum;
        return item.question.answers.map(function (answer, index) {
            const answerRecnum = answer.recnum;
            const answerLetter = answer.letter;
            const onAddValueChange = function () {
                const value = document.getElementById('addValueInput' + questionRecnum + index).value;
                document.getElementById('checkboxInput' + questionRecnum + index).checked = (value != '' && value > 0);
            };
            const addValueInput = [
                <input
                    id={'addValueInput' + questionRecnum + index}
                    className='brutal-input-addValue'
                    type='number'
                    data-recnum={answerRecnum}
                    min={item.question.addValueMin}
                    max={item.question.addValueMax}
                    onChange={onAddValueChange}/>,
                (<p className="brutal-input-unit">
					{item.question.localizedUnitType != null ? item.question.localizedUnitType : ''}
				</p>)
            ];
            const elementClassName = 'brutal-input-element';
            let input = '';
            if (item.previouslyChosenValue) {
                if (answerLetter == item.previouslyChosenValue.letter) {
                    if (withAddValues) {
                        input = (<input
                            className="brutal-input-checkbox cursor-pointer"
                            type="checkbox"
                            id={'checkboxInput' + questionRecnum + index}
                            style={{display: "none"}}
                            name={questionRecnum}
                            data-recnum={answerRecnum}
                            data-questionrecnum={questionRecnum}
                            value={answerLetter} checked/>);
                    } else {
                        input = (<input
                            className="brutal-input-checkbox cursor-pointer"
                            type="checkbox"
                            id={'checkboxInput' + questionRecnum + index}
                            name={questionRecnum}
                            data-recnum={answerRecnum}
                            data-questionrecnum={questionRecnum}
                            value={answerLetter} checked/>);
                    }
                    return (
                        <div key={index} className={elementClassName}>
							{input}
                            <label
                                className="brutal-input-label cursor-pointer"
                                htmlFor={'checkboxInput' + questionRecnum + index}>
								<span></span> {getTextFromHybridMemo(answer.text)}
							</label>
                            {withAddValues ? addValueInput : ''}
						</div>
                    );
                }
            }

            if (withAddValues) {
                input = (<input
                    className="brutal-input-checkbox cursor-pointer"
                    type="checkbox"
                    id={'checkboxInput' + questionRecnum + index}
                    style={{display: "none"}}
                    name={questionRecnum}
                    data-recnum={answerRecnum}
                    data-questionrecnum={questionRecnum}
                    value={answerLetter}/>);
            } else {
                input = (<input
                    className="brutal-input-checkbox cursor-pointer"
                    type="checkbox"
                    id={'checkboxInput' + questionRecnum + index}
                    name={questionRecnum}
                    data-recnum={answerRecnum}
                    data-questionrecnum={questionRecnum}
                    value={answerLetter}/>);
            }
            return (
                <div key={index} className={elementClassName}>
					{input}
                    <label
                        className="brutal-input-label cursor-pointer"
                        htmlFor={'checkboxInput' + questionRecnum + index}>
						<span></span> {getTextFromHybridMemo(answer.text)}
					</label>
                    {withAddValues ? addValueInput : ''}
				</div>
            );
        });
    }

    createSelectInput() {
        const {item} = this.props;
        const questionRecnum = item.question.recnum;
        return (
            <div>
				<select
                    className='cursor-pointer'
                    name={questionRecnum}
                    data-questionrecnum={questionRecnum}>
					{this.createSelectOptionInputs()}
				</select>
			</div>
        );
    }

    createSelectOptionInputs() {
        const {item} = this.props;
        const questionRecnum = item.question.recnum;
        return item.question.answers.map(function (answer, index) {
            return (
                <option
                    data-recnum={answer.recnum}
                    data-questionrecnum={questionRecnum}
                    key={index}
                    value={answer.letter}>
					{getTextFromHybridMemo(answer.text)}
				</option>
            );
        });
    }

    createSingleAnswer() {
        const {item} = this.props;
        const answers = item.question.answers;
        if (answers.length > 5) {
            return this.createSelectInput();
        } else {
            return this.createRadioInputs();
        }
    }

    createMultiAnswer() {
        const {item} = this.props;
        if (item.question.addValueMin != undefined && item.question.addValueMax != undefined) {
            return this.createMultiComplexInput();
        } else {
            return this.createCheckBoxInputs();
        }
    }

    createComplexInput() {
        const {item} = this.props;
        const questionRecnum = item.question.recnum;
        return (
            <div>
				{this.createSelectInput()}
                <span>{item.question.localizedUnitType != null ? ' mértékegység (' + item.question.localizedUnitType + ')' : ''}</span>
                <span>Pontosan: </span>
				<input
                    name={'addValue-'+ questionRecnum}
                    type='number'
                    data-questionrecnum={'addValue-'+ questionRecnum}
                    min={item.question.addValueMin}
                    max={item.question.addValueMax}/>
			</div>
        );
    }

    createMultiComplexInput() {
        const {item} = this.props;
        return (
            <div className='brutal-input'>
				{this.createCheckBoxInputs(true)}
			</div>
        );
    }

    render() {
        const {item} = this.props;
        window.log('PROPS', item.recnum);
        return (
            <div>
				<h3>{getTextFromHybridMemo(item.question.questionText)}</h3>
                {this.getContent()}
			</div>
        );
    }
}

HealthTestInput.propTypes = {};
HealthTestInput.defaultProps = {};

export default HealthTestInput;
