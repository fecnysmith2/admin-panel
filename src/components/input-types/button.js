import React, {
   Component,
} from 'react';
import PropTypes from 'prop-types';

class Button extends Component {

   constructor(props) {
       super(props);
   }

	render() {
   		const {loading, name, attributes} = this.props;
		if(loading) {
			return (
				<i className='loader-item'></i>
			)
		}
		return (
			<div className='input-button'>
				<input type='submit' id={name} value={attributes.dataObject.captionText} />
			</div>
		);
	}
}

Button.propTypes = {};
Button.defaultProps = {};

export default Button;