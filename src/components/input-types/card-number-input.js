import React, {
   Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';

class CardNumberInput extends Component {

   constructor(props) {
       super(props);
       bindAll(this,
           'setCardValue', 'getSimpleCardNumberInput', 'checkInput',
           'getRequiredCardNumberInput',
       );

       this.state = {
           cardNumber: []
       }
   }

	setCardValue(event) {
		let cardNumber = this.state.cardNumber;

		switch(event.target.name) {
			case 'input-card-number-first':
				cardNumber[0] = event.target.value;
				this.setState({
					cardNumber: cardNumber,
				}); break;
			case 'input-card-number-second':
				cardNumber[1] = event.target.value;
				this.setState({
					cardNumber: cardNumber,
				}); break;
			case 'input-card-number-third':
				cardNumber[2] = event.target.value;
				this.setState({
					cardNumber: cardNumber,
				}); break;
			case 'input-card-number-fourth':
				cardNumber[3] = event.target.value;
				this.setState({
					cardNumber: cardNumber,
				}); break;
		}
	}

	getSimpleCardNumberInput() {
		const input1Id = 'cardInput1';
		const input2Id = 'cardInput2';
		const input3Id = 'cardInput3';
		const input4Id = 'cardInput4';
		const that = this;
		const cardInput1Handler = function() { that.checkInput(1); };
		const cardInput2Handler = function() { that.checkInput(2); };
		const cardInput3Handler = function() { that.checkInput(3); };
		const cardInput4Handler = function() { that.checkInput(4); };
		return (
			<div>
				<div id='cardContainer1' className='col-card-input'>
					<input
						id={input1Id}
						onKeyUp={cardInput1Handler}
						placeholder='0000'
					  pattern='[0-9]{4}'
						type='text'
						name='input-card-number-first'
						onChange={this.setCardValue} />
				</div>
				<div id='cardContainer2' className='col-card-input'>
					<input
						id={input2Id}
						onKeyUp={cardInput2Handler}
						placeholder='0000'
						pattern='[0-9]{4}'
						type='text'
						name='input-card-number-second'
						onChange={this.setCardValue} />
				</div>
				<div id='cardContainer3' className='col-card-input'>
					<input
						id={input3Id}
						onKeyUp={cardInput3Handler}
						placeholder='0000'
						pattern='[0-9]{4}'
						type='text'
						name='input-card-number-third'
						onChange={this.setCardValue} />
				</div>
				<div id='cardContainer4' className='col-card-input'>
					<input
						id={input4Id}
						onKeyUp={cardInput4Handler}
						placeholder='0000'
						pattern='[0-9]{4}'
						type='text'
						name='input-card-number-fourth'
						onChange={this.setCardValue} />
				</div>
			</div>
		);
	}

	checkInput(index) {
		const input = document.getElementById('cardInput'+index);
		if (input) {
			if (index < 4) {
				if (input.value.length == 4){
					document.getElementById('cardInput'+(index+1)).focus();
				}
				if (input.value.length > 4) {
					input.value = input.value.substring(0, 4);
					document.getElementById('cardInput'+(index+1)).focus();
				}
			} else {
				if (input.value.length > 4) {
					input.value = input.value.substring(0, 4);
				}
			}
			if (isNaN(input.value)) {
				document.getElementById('cardContainer'+(index)).className += ' error';
			} else {
				document.getElementById('cardContainer'+(index)).className = 'col-card-input';
			}
		}
	}

	getRequiredCardNumberInput() {
		const input1Id = 'cardInput1';
		const input2Id = 'cardInput2';
		const input3Id = 'cardInput3';
		const input4Id = 'cardInput4';
		const that = this;
		const cardInput1Handler = function() { that.checkInput(1); };
        const cardInput2Handler = function() { that.checkInput(2); };
        const cardInput3Handler = function() { that.checkInput(3); };
        const cardInput4Handler = function() { that.checkInput(4); };
		return (
			<div>
				<div id='cardContainer1' className='col-card-input'>
					<input
						id={input1Id}
						onKeyUp={cardInput1Handler}
						placeholder='0000'
					  pattern='[0-9]{4}'
						type='text'
						name='input-card-number-first'
						onChange={this.setCardValue}
						required />
				</div>
				<div id='cardContainer2' className='col-card-input'>
					<input
						id={input2Id}
						onKeyUp={cardInput2Handler}
						placeholder='0000'
						pattern='[0-9]{4}'
						type='text'
						name='input-card-number-second'
						onChange={this.setCardValue}
						required />
				</div>
				<div id='cardContainer3' className='col-card-input'>
					<input
						id={input3Id}
						onKeyUp={cardInput3Handler}
						placeholder='0000'
						pattern='[0-9]{4}'
						type='text'
						name='input-card-number-third'
						onChange={this.setCardValue}
						required />
				</div>
				<div id='cardContainer4' className='col-card-input'>
					<input
						id={input4Id}
						onKeyUp={cardInput4Handler}
						placeholder='0000'
						pattern='[0-9]{4}'
						type='text'
						name='input-card-number-fourth'
						onChange={this.setCardValue}
						required />
				</div>
			</div>
		);
	}

	render() {
		const cardNumberInputs = this.props.required ? this.getRequiredCardNumberInput() : this.getSimpleCardNumberInput();
		const cardNumber = this.state.cardNumber.join('');
		const requiredSign = (<span className='required' >*</span>);
		return(
			<div className='input-card-number'>
				<label htmlFor={this.props.name} >
					{this.props.captionText + ' '}
				</label>
				{this.props.required ? requiredSign : ''}
				{cardNumberInputs}
				<input
					type='hidden'
					name={this.props.name}
					data-location={this.props.location}
					data-fieldType={this.props.fieldType}
					value={cardNumber} />
			</div>
		);
	}
}

CardNumberInput.propTypes = {};
CardNumberInput.defaultProps = {};

export default CardNumberInput;