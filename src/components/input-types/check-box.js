import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';
import bindAll from 'lodash.bindall';

class CheckBox extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'getRequiredInput', 'getSimpleInput',
        );
        this.state = {
            data: null
        };
    }

    getRequiredInput() {
        const {required, fieldType, name, location} = this.props;
        return (
            <input
                type='checkbox'
                name={name}
                data-location={location}
                data-fieldType={fieldType}
                data-required={required}
                required/>
        );
    }

    getSimpleInput() {
        const {required, fieldType, name, location} = this.props;
        return (
            <input
                type='checkbox'
                name={name}
                data-location={location}
                data-fieldType={fieldType}
                data-required={required}/>
        );
    }

    render() {
        const {required, captionText, linked, name} = this.props;
        const input = required ? this.getRequiredInput() : this.getSimpleInput();
        let caption = captionText;
        if (linked) {
            caption = (
                <p>{captionText.replace('Felhasználási feltételeket', '')}
                    <Link to='/aszf' target='_blank'>Felhasználási feltételeket</Link>
				</p>
            );

        }
        const requiredSign = (<span className='required'>{' *'}</span>);
        return (
            <div className='input-checkbox'>
				{input}
                &nbsp;
                <label htmlFor={name}>
					{caption}
				</label>
                {required ? requiredSign : ''}
			</div>
        );
    }
}

CheckBox.propTypes = {};
CheckBox.defaultProps = {};

export default CheckBox;