import React, {
   Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import moment from 'moment';

class DateInput extends Component {
    constructor(props) {
        super(props);
        bindAll(this,
            'setDateValue', 'getSimpleDateInput', 'getRequiredDateInput',
        );
        let d = new Date();

		this.state = {
			date: moment(d).format('YYYY-MM-DD')
		}
    }

	setDateValue(event) {
		var date = this.state.date.split('-');

		switch(event.target.name) {
			case 'input-date-year':
				date[0] = event.target.value;
				this.setState({
					date: date.join('-'),
				}); break;
			case 'input-date-month':
				date[1] = event.target.value;
				this.setState({
					date: date.join('-'),
				}); break;
			case 'input-date-day':
				date[2] = event.target.value;
				this.setState({
					date: date.join('-'),
				}); break;
		}
	}

	getSimpleDateInput() {
		return (
			<div>
				<div className='col-birth-input'>
					<input
							placeholder='Év'
							type='number'
							name='input-date-year'
							min='1900'
							max={moment(new Date()).format('YYYY')}
							onChange={this.setDateValue} />
				</div>
				<div className='col-birth-input'>
					<input
							placeholder='Hónap'
							type='number'
							name='input-date-month'
							min='1'
							max='12'
							onChange={this.setDateValue} />
				</div>
				<div className='col-birth-input'>
					<input
							placeholder='Nap'
							type='number'
							name='input-date-day'
							min='1'
							max='31'
							onChange={this.setDateValue} />
				</div>
			</div>
		);
	}

	getRequiredDateInput() {
		return (
			<div>
				<div className='col-birth-input'>
					<input
							placeholder='Év'
							type='number'
							name='input-date-year'
							min='1900'
							max={moment(new Date()).format('YYYY')}
							onChange={this.setDateValue}
							required />
				</div>
				<div className='col-birth-input'>
					<input
							placeholder='Hónap'
							type='number'
							name='input-date-month'
							min='1'
							max='12'
							onChange={this.setDateValue}
							required />
				</div>
				<div className='col-birth-input'>
					<input
							placeholder='Nap'
							type='number'
							name='input-date-day'
							min='1'
							max='31'
							onChange={this.setDateValue}
							required />
				</div>
			</div>
		);
	}

	render() {
		var dateInputs = this.props.required ? this.getRequiredDateInput() : this.getSimpleDateInput();
		var requiredSign = (<span className='required' >*</span>);
		return(
			<div className='input-date'>
				<label htmlFor={this.props.name} >
					{this.props.captionText + ' '}
				</label>
				{this.props.required ? requiredSign : ''}
				{dateInputs}
				<input
					type='hidden'
					name={this.props.name}
					data-location={this.props.location}
					data-fieldType={this.props.fieldType}
					value={this.state.date} />
			</div>
		);
	}
}

DateInput.propTypes = {};
DateInput.defaultProps = {};

export default DateInput;