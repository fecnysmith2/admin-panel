import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import {getObjectFromLocalStore} from '../../utils/db';
import Select from './select';
import bindAll from 'lodash.bindall';

class EmailInput extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'getRequiredInput', 'getSimpleInput', 'checkPartnerEmail',
            'getGroupSelect', 'setUpVeryfiedEmail',

        );
        this.state = {
            bbEmail: false
        };
    }

    getRequiredInput(confirmTag = '', confirmNeededParam = false) {
        const {name, confirmNeeded, placeholder, location, fieldType, required} = this.props;
        const nameLabel = confirmNeededParam ? name : name + confirmTag;

        const ref = confirmNeeded && confirmNeededParam ? 'emailInputRequiredConfirm' : 'emailInputRequired';
        window.log('EMAIL REF', ref);
        return (
            <input
                ref={ref}
                key={ref}
                type="text"
                name={nameLabel}
                placeholder={placeholder}
                data-confirmNeeded={confirmNeeded}
                data-confirmTag={confirmTag}
                data-location={location}
                data-fieldType={fieldType}
                data-required={required}
                onKeyUp={this.checkPartnerEmail}
                required/>
        );
    }

    getSimpleInput(confirmTag = '', confirmNeededParam = false) {
        const {name, confirmNeeded, placeholder, location, fieldType, required} = this.props;
        const nameLabel = confirmNeededParam ? name : name + confirmTag;
        const ref = confirmNeeded  && confirmNeededParam ? 'emailInputConfirm' : 'emailInput';
        return (
            <input
                ref={ref}
                key={ref}
                type="text"
                name={nameLabel}
                placeholder={placeholder}
                data-confirmNeeded={confirmNeededParam}
                data-confirmTag={confirmTag}
                data-location={location}
                data-fieldType={fieldType}
                data-required={required}/>
        );
    }

    /*
     Ha bb landingen megadta az email címét akkor itt automatikusan beállítjuk
     neki, hogy ne keljen úra megadnia regisztrációkor
     */
    componentDidUpdate(prevProps, prevState) {
        this.setUpVeryfiedEmail();
    }

    checkPartnerEmail() {
        const { emailInputRequiredConfirm, emailInputConfirm} = this.refs;
        let email = ReactDOM.findDOMNode(emailInputConfirm);
        if (!email) {
            email = ReactDOM.findDOMNode(emailInputRequiredConfirm);
            if (email) {
                email = ReactDOM.findDOMNode(emailInputRequiredConfirm).value;
            }
        }
        if (email) {
            window.log('email', email);
            let visibleList = [];
            if (email.includes('budapestbank.hu') || email.includes('lillafitt.hu')) {
                this.setState({
                    bbEmail: true
                });
            } else {
                this.setState({
                    bbEmail: false
                });
            }
        }
    }

    getGroupSelect() {
        //@TODO ezt általánosítani kell mert ez így most beégetett. Ennek az input nak is adatb-ből kell jönnie
        return (
            <Select
                id='partnerGroupInput'
                ref='partnerGroupInput'
                key='partnerGroupInput'
                name='partnerGroup'
                location=''
                fieldType=''
                required={true}
                captionText='Kérjük add meg hol dolgozol'
                attributes=''
                params=''/>
        );
    }

    setUpVeryfiedEmail() {
        const {bbEmail} = this.state;
        const {confirmNeeded, confirmTag} = this.props;
        const {emailInput, emailInputRequired, emailInputConfirm, emailInputRequiredConfirm} = this.refs;
        if (confirmNeeded) {
            let email = getObjectFromLocalStore('email-verified');
            window.log('VERIFIED EMAIL', email);
            window.log('confirmtag', confirmTag);
            if (email && confirmTag === 'register') {
                email = email.replace(/\"/g, '');
            } else {
                email = '';
            }

            let emailInput = ReactDOM.findDOMNode(emailInput);
            window.log('EMAIL AFTER input', emailInput);
            window.log('EMAIL AFTER refs', this.refs);
            if (!emailInput) {
                emailInput = ReactDOM.findDOMNode(emailInputRequired);
            }

            let emailInputConfirm = ReactDOM.findDOMNode(emailInputConfirm);
            if (!emailInputConfirm) {
                emailInputConfirm = ReactDOM.findDOMNode(emailInputRequiredConfirm);
            }
            window.log('EMAIL AFTER', email);
            window.log('EMAIL AFTER input', emailInput);
            window.log('EMAIL AFTER input confirm', emailInputConfirm);
            if (emailInput && emailInput !== null && emailInputConfirm && emailInputConfirm !== null) {
                window.log('EMAIL AFTER input value', emailInput.value);
                window.log('EMAIL AFTER input confirm value', emailInputConfirm.value);
                if (emailInput.value === '' && emailInputConfirm.value === '') {
                    emailInput.value = email;
                    emailInputConfirm.value = email;
                }
            }
        }
    }

    componentDidMount() {
        this.setUpVeryfiedEmail();
    }

    render() {
        const {bbEmail} = this.state;
        const {confirmNeeded, required, confirmTag, captionText, name} = this.props;
        const requiredSign = (<span key='confirmEmailLRequiredSign' className='required'>*</span>);
        let input = '';
        let confirmInput = [];
        if (confirmNeeded) {
            input = required ? this.getRequiredInput(confirmTag, confirmNeeded) : this.getSimpleInput(confirmTag, confirmNeeded);
            confirmInput.push(<label key='confirmEmailLAbel'
                                     htmlFor={name}>{captionText + ' ismét '}</label>);
            confirmInput.push(required ? requiredSign : '');
            confirmInput.push(required ? this.getRequiredInput(confirmTag) : this.getSimpleInput(confirmTag));
        } else {
            input = required ? this.getRequiredInput() : this.getSimpleInput();
        }
        return (
            <div className='input-text'>
                <label htmlFor={name}>
                    {captionText + ' '}
                </label>
                {required ? requiredSign : ''}
                {input}
                {confirmInput}
                {bbEmail ? this.getGroupSelect() : ''}
            </div>
        );
    }
}

EmailInput.propTypes = {};
EmailInput.defaultProps = {};

export default EmailInput;