import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import makeRequest from '../../utils/request-queue-handler.js';

class HealthFundSelect extends Component {
    constructor(props) {
        super(props);
        bindAll(this,
            'queries', 'onDataReceived', 'onError',
            'componentWillMount',
        );

        this.state = {
            options: []
        }
    }

    queries(queryName) {
        var queries = {
            fundId: {
                queryName: 'healthFund',
                query: `query getHealthFund {
          HealthFund {
        	  recnum
        	  fullname
        	}
        }`,
                variables: '',
                mutation: false,
                cacheEnabled: true
            }
        };

        return queries[queryName];
    }

    onDataReceived(response) {
        let data = response.data;
        /* mivel nem tudhatjuk pontosan milyen megnevezése van a query eredmény property-ének ezért így nyerem ki*/
        for (var propertyName in data) {
            data = data[propertyName];
            break;
        }
        this.setState({options: data});
    }

    onError(error) {
        window.error('sendQuery error', error);
    }

    componentWillMount() {
        window.log('HealthFund select mount', this.props.name);
        makeRequest(this.queries(this.props.name), this.onDataReceived, this.onError);
    }

    render() {
        var options;
        if (!this.state.options.length) {
            return (
                <i className='loader-item'></i>
            )
        }
        options = this.state.options.map(function (option, index) {
            return (
                <option key={index} value={option.recnum}>
          {option.fullname}
        </option>
            );
        });
        var requiredSign = (<span className='required'>*</span>);
        return (
            <div className='input-select'>
        <label htmlFor={this.props.name}>
          {this.props.captionText + ' '}
        </label>
                {this.props.required ? requiredSign : ''}
                <div className='select'>
          <select
              ref='select'
              name={this.props.name}
              data-location={this.props.location}
              data-fieldType={this.props.fieldType}
              data-required={this.props.required}>
            {options}
          </select>
        </div>
      </div>
        );
    }
}

HealthFundSelect.propTypes = {};
HealthFundSelect.defaultProps = {};

export default HealthFundSelect;