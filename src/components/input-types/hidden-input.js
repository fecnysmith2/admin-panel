import React, {
    Component,
} from 'react';
import bindAll from 'lodash.bindall';

class HiddenInput extends Component {
    constructor(props) {
        super(props);
        bindAll(this,
            'getHiddenValue',
        );

    }

    componentDidMount() {
        const{name} = this.props;
        if (name !== null) {
            document.querySelector('input[name="' + name + '"]').value = this.getHiddenValue();
        }        
    }

    getHiddenValue() {
        const{clientSideFieldType, attributes} = this.props;
        if (clientSideFieldType) {
            return attributes.dataObject.clientSideFieldType.replace('hidden_', '');
        }
        return '';
    }

    render() {
        const{name, location, fieldType} = this.props;
        return (
            <div className='input-hidden'>
            <input type='hidden'
                   name={name}
                   data-location={location}
                   data-fieldType={fieldType} />
            </div>
        );
    }
}

HiddenInput.propTypes = {};
HiddenInput.defaultProps = {};

export default HiddenInput;