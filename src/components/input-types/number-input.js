import React, {
   Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';

class NumberInput extends Component {
    constructor(props) {
       super(props);
        bindAll(this,
            'getRequiredInput', 'getSimpleInput',
        );
    }

	getRequiredInput() {
        const {required, fieldType, name, location, min, max, placeholder} = this.props;
		return(
			<input
				type="number"
				name={name}
				placeholder={placeholder}
				data-location={location}
				data-fieldType={fieldType}
				data-required={required}
				min={min}
				max={max}
				required />
		);
	}

	getSimpleInput() {
        const {required, fieldType, name, location, min, max, placeholder} = this.props;
		return(
			<input
				type="number"
				name={name}
				placeholder={placeholder}
				data-location={location}
				data-fieldType={fieldType}
				min={min}
				max={max}
				data-required={required} />
		);
	}

	render() {
        const {required, name, captionText} = this.props;
		const input = required ? this.getRequiredInput() : this.getSimpleInput();
		const requiredSign = (<span className='required' >*</span>);
		return(
			<div className="input-number">
				<label htmlFor={name} >{captionText + ' '}</label>
				{required ? requiredSign : ''}
				{input}
			</div>
		);
	}
}

NumberInput.propTypes = {};
NumberInput.defaultProps = {};

export default NumberInput;