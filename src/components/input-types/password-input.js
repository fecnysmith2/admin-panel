import React, {Component, PropTypes}  from 'react';
import bindAll from 'lodash.bindall';

export default class PasswordInput extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'getRequiredInput', 'getSimpleInput',
        );
    }

    getRequiredInput(confirmTag = '', confirmNeededParam = false) {
        const {required, name, location, fieldType, id, confirmNeeded}= this.props;
        const nameText = confirmNeededParam ? name + confirmTag : name;
        return (
            <input
                key={'passwordConfirm'+name}
                type="password"
                name={nameText}
                id={id}
                data-confirmNeeded={confirmNeeded}
                data-confirmTag={confirmTag}
                data-location={location}
                data-fieldType={fieldType}
                data-required={required}
                required/>
        );
    }

    getSimpleInput(confirmTag = '', confirmNeededParam = false) {
        const {required, name, location, fieldType, id, confirmNeeded}= this.props;
        const nameText = confirmNeededParam ? name + confirmTag : name;
        return (
            <input
                key={'passwordSimple'+name}
                type="password"
                name={nameText}
                id={id}
                data-confirmNeeded={confirmNeeded}
                data-confirmTag={confirmTag}
                data-location={location}
                data-fieldType={fieldType}
                data-required={required}/>
        );
    }

    render() {
        const requiredSign = (<span className='required'>*</span>);
        const {confirmNeeded, required, confirmTag, captionText, name}= this.props;
        let input = '';
        var confirmInput = [];
        if (confirmNeeded) {
            input = required ? this.getRequiredInput(confirmTag, confirmNeeded) : this.getSimpleInput(confirmTag, confirmNeeded);
            confirmInput.push(<label key={'passwordLabel'+name} htmlFor={name}>{captionText + ' ismét '}</label>);
            confirmInput.push(required ? requiredSign : '');
            confirmInput.push(required ? this.getRequiredInput(confirmTag) : this.getSimpleInput(confirmTag));
        } else {
            input = required ? this.getRequiredInput() : this.getSimpleInput();
        }
        return (
            <div className="input-password">
                <label htmlFor={name}>{this.props.captionText + ' '}</label>
                {required ? requiredSign : ''}
                {input}
                {confirmInput}
            </div>
        );
    }
}
