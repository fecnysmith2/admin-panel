import React, {
   Component,
} from 'react';
import PropTypes from 'prop-types';
import makeRequest from '../../utils/request-queue-handler.js';
import bindAll from 'lodash.bindall';

class RadioButtonGroup extends Component {
    constructor(props) {
        super(props);
        bindAll(this,
            'queries', 'onDataReceived', 'onError',
        );

        this.state = {
            radios: []
        };
    }

	queries(queryName) {

		const queries = {
			sex: {
				queryName: 'sex',
				query: `query sex {
							DataValues_getDataValueByName(variableGroupName: "sex", orderBy: "Description ASC")
						}`,
				variables: '',
				cacheEnabled: true,
				mutation: false
			}
		};

		return queries[queryName];
	}

	onDataReceived(response) {
        let data = JSON.parse(response.data.DataValues_getDataValueByName).data;
        window.log('SELECT RESPONSE', data);
		this.setState({ radios: data });
	}

	onError(error) {
		window.error('sendQuery', error);
	}

	componentWillMount() {
		var name = this.props.name;
		makeRequest(this.queries(name), this.onDataReceived, this.onError);
	}

	render() {
        const {required, fieldType, name, location, min, max, placeholder, captionText} = this.props;
        const {radios} = this.state;
		var radiosButtons;
		if(!radios.length) {
			return (
				<i className='loader-item'></i>
			)
		} else {
			const that = this;
            radiosButtons = radios.map(function(radio, index) {
				if (required) {
					return (
						<div key={index}>
							<input
								id={name + '-' + index}
								type="radio"
								name={name}
								value={radio.key}
								data-location={location}
								data-required={required} required/>
							<label htmlFor={name + '-' + index}>
								<span></span> {radio.Description}
							</label>
						</div>
					);
				} else {
					return (
						<div key={index}>
							<input
								id={name + '-' + index}
								type="radio"
								name={name}
								value={radio.key}
								data-location={location}
								data-required={required} />
							<label htmlFor={name + '-' + index}>
								<span></span> {radio.Description}
							</label>
						</div>
					);
				}
			});
		}

		const requiredSign = (<span className='required' >*</span>);
		return (
			<div className="radio-group">
				<label>
					{captionText + ' '}
				</label>
				{required ? requiredSign : ''}
				{radiosButtons}
			</div>
		);
	}
}

RadioButtonGroup.propTypes = {};
RadioButtonGroup.defaultProps = {};

export default RadioButtonGroup;