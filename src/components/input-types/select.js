import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import ReactDOM from 'react-dom';
import makeRequest from '../../utils/request-queue-handler.js';
import {getTextFromHybridMemo} from '../../utils/helpers';
import { queries } from '../../utils/queries/select-queries.js';

class Select extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'onDataReceived', 'onError', 'getDefaultValue',
            'getRequiredInput', 'getSimpleInput',
        );

        this.state = {
            options: []
        };
    }

    onDataReceived(response) {
        let data;
        if (response.data.DataValues_getDataValueByName) {
            data = JSON.parse(response.data.DataValues_getDataValueByName).data;
        } else {
            const object = response.data;
            // window.log('SELECT RESPONSE', JSON.parse(data));
            /* mivel nem tudhatjuk pontosan milyen megnevezése van a query eredmény property-ének ezért így nyerem ki*/
            for (let propertyName in object) {
                data = object[propertyName];
                break;
            }
        }
        window.log('SELECT RESPONSE', data);
        this.setState({options: data});
    }

    onError(error) {
        window.error('sendQuery error', error);
    }

    componentWillMount() {
        const {name} = this.props;
        makeRequest(queries[name], this.onDataReceived, this.onError);
    }

    componentDidUpdate() {
        const {name} = this.props;
        const {select} = this.refs;
        const defaultValue = this.getDefaultValue();
        if (name == 'addressType' && (defaultValue == 'BA' || defaultValue == 'LA' || defaultValue == 'PA')) {
            ReactDOM.findDOMNode(select).value = defaultValue;
            ReactDOM.findDOMNode(select).disabled = true;
        }
        if (defaultValue != '' && defaultValue != undefined) {
            ReactDOM.findDOMNode(select).value = defaultValue;
        }
    }

    getDefaultValue() {
        const {name, params, attributes} = this.props;
        window.log('DEFAULT VALUE', attributes);
        // if (params && params.redirectURL && name == 'addressType') {
        //     const parameters = atob(params.redirectURL).split('/');
        //     const defaultAddressType = parameters.length == 2 && parameters[0].includes('webshop') ? parameters[1] : '';
        //     if (name == 'addressType' && defaultAddressType != '') {
        //         return defaultAddressType;
        //     }
        // }
        for (let i = 0; i < attributes.length; i++) {
            if (attributes[i].type == 'def_value') {
                return attributes[i].value;
            }
        }
        return '';
    }

    getRequiredInput(options) {
        const {name, location, fieldType, required, index} = this.props;
        return (
            <select
                style={{maxWidth: '70%'}}
                ref='select'
                id={name+index}
                name={name}
                data-location={location}
                data-fieldType={fieldType}
                data-required={required}
                required>
                {options}
            </select>
        );
    }

    getSimpleInput(options) {
        const {name, location, fieldType, required, index} = this.props;
        return (
            <select
                ref='select'
                id={name+index}
                name={name}
                data-location={location}
                data-fieldType={fieldType}
                data-required={required}>
                {options}
            </select>
        );
    }

    render() {
        const {captionText, name, required, index} = this.props;
        const {options} = this.state;

        if (!options.length) {
            return (
                <i className='loader-item'></i>
            )
        }

        const that = this;
        let optionList = [];
        if (name == 'parentGroup') {
            optionList.push(
                <option key={-1} value={'n'}>
                    nincs
                </option>
            );
        }

        optionList = optionList.concat(options.map(function (option, index) {
            if (name == 'parentGroup') {
                return (
                    <option key={index} value={option.recnum}>
                        {option.groupNameText != null ? option.groupNameText : option.dedicatedName != null ? option.dedicatedName : 'nincs név'}
                    </option>
                );
            }
            if (name == 'partnerGroup') {
                return (
                    <option key={index} value={option.recnum}>
                        {option.hybridMemo != null ? getTextFromHybridMemo(option.hybridMemo) : 'hiba'}
                    </option>
                );
            }
            if (name == 'country') {
                return (
                    <option key={index} value={option.key}>
                        {option.CountryName != null ? option.CountryName : 'hiba'}
                    </option>
                );
            }
            if (name == 'hint' || name == 'coupled') {
                window.log('TEXT', option);
                return (
                    <option key={index} value={option.recnum}>
                        {option.textItems.length > 0 ? option.textItems[0].textHeaderText : 'hiba'}
                    </option>
                );
            }
            if (name == 'dataType') {
                window.log('TEXT', option);
                return (
                    <option key={index} value={option.Type}>
                        {option.Description}
                    </option>
                );
            }
            return (
                <option key={index} value={option.key}>
                    {option.Description}
                </option>
            );
        }));

        if (name == 'partnerGroup') {
            optionList.unshift(
                <option value=''>
                    Kérlek válassz!
                </option>
            );
        } else if (name == 'hint' || name == 'coupled') {
            optionList.unshift(
                <option value=''>

                </option>
            );
        }
        const requiredSign = (<span className='required'>*</span>);
        const label = captionText != '' ? <label htmlFor={name}>{captionText + ' '}</label> : '';
        return (
            <div id={'container-' + name + index}
                 className='input-select'>
                {label}
                {required ? requiredSign : ''}
                <div className='select'>
                    {required ? this.getRequiredInput(optionList) : this.getSimpleInput(optionList)}
                </div>
            </div>
        );
    }
}

Select.propTypes = {};
Select.defaultProps = {
    attributes: [],
    name: '',
    index: '',
    location: '',
    fieldType: '',
    required: false,
    captionText: ''
};

export default Select;
