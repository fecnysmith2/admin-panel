import React, {
   Component,
} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import bindAll from 'lodash.bindall';

class ShortDateInput extends Component {

    constructor(props) {
       super(props);
        bindAll(this,
            'setDateValue', 'getSimpleShortDateInput', 'getRequiredShortDateInput',
            'checkInput',
        );
        this.state = {
            shortDate: []
        }
    }

	setDateValue(event) {
		var shortDate = this.state.shortDate;

		switch(event.target.name) {
			case 'input-short-date-month':
				shortDate[0] = event.target.value;
				this.setState({
					shortDate: shortDate,
				}); break;
        case 'input-short-date-year':
				shortDate[1] = event.target.value;
				this.setState({
					shortDate: shortDate,
				}); break;
		}
	}

	getSimpleShortDateInput() {
		const input1Id = 'dateInput1';
		const input2Id = 'dateInput2';
		var that = this;
		var dateInput1Handler = function() { that.checkInput(1); };
		var dateInput2Handler = function() { that.checkInput(2); };
		return (
			<div>
				<div id='dateContainer1' className='col-short-date-input'>
					<input
						id={input1Id}
						onKeyUp={dateInput1Handler}
						placeholder='01'
					  pattern='^(0?[1-9]|1[012])$'
						type='text'
						name='input-short-date-month'
						onChange={this.setDateValue} />
				</div>
				<div id='dateContainer2' className='col-short-date-input'>
					<input
						id={input2Id}
						onKeyUp={dateInput2Handler}
						placeholder='16'
						pattern='[0-9]{2}'
						type='text'
						name='input-short-date-year'
						onChange={this.setDateValue} />
				</div>
			</div>
		);
	}

	getRequiredShortDateInput() {
		const input1Id = 'dateInput1';
		const input2Id = 'dateInput2';
		var that = this;
		var dateInput1Handler = function() { that.checkInput(1); };
		var dateInput2Handler = function() { that.checkInput(2); };
		return (
      <div>
        <div id='dateContainer1' className='col-short-date-input'>
          <input
						id={input1Id}
						onKeyUp={dateInput1Handler}
            placeholder='HH'
            pattern='^(0?[1-9]|1[012])$'
            type='text'
            name='input-short-date-month'
            onChange={this.setDateValue} />
        </div>
        <div id='dateContainer2' className='col-short-date-input'>
          <input
						id={input2Id}
						onKeyUp={dateInput2Handler}
            placeholder='ÉÉ'
            pattern='[0-9]{2}'
            type='text'
            name='input-short-date-year'
            onChange={this.setDateValue} />
        </div>
      </div>
		);
	}

	checkInput(index) {
		var input = document.getElementById('dateInput'+index);
		window.log('INPUT',input);
		if (input) {
			if (index < 2) {
				if (input.value.length == 2){
					document.getElementById('dateInput'+(index+1)).focus();
				}
				if (input.value.length > 2) {
					input.value = input.value.substring(0, 2);
					document.getElementById('dateInput'+(index+1)).focus();
				}
			} else {
				if (input.value.length > 2) {
					input.value = input.value.substring(0, 2);
				}
			}
			if (isNaN(input.value)) {
				document.getElementById('dateContainer'+(index)).className += ' error';
			} else {
				document.getElementById('dateContainer'+(index)).className = 'col-short-date-input';
			}
		}
	}

	render() {
		var dateInputs = this.props.required ? this.getRequiredShortDateInput() : this.getSimpleShortDateInput();
        var shortDate = this.state.shortDate.join('');
		var requiredSign = (<span className='required' >*</span>);
		return(
			<div className='input-short-date'>
				<label htmlFor={this.props.name} >
					{this.props.captionText + ' '}
				</label>
				{this.props.required ? requiredSign : ''}
				{dateInputs}
				<input
					type='hidden'
					name={this.props.name}
					data-location={this.props.location}
					data-fieldType={this.props.fieldType}
					value={shortDate} />
			</div>
		);
	}
}

ShortDateInput.propTypes = {};
ShortDateInput.defaultProps = {};

export default ShortDateInput;