import React, {
   Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';

class TextInput extends Component {

    constructor(props) {
       super(props);
        bindAll(this,
            'getRequiredInput', 'getSimpleInput',
        );
    }

	getRequiredInput() {
        const {required, fieldType, name, location, placeholder, id} = this.props;
		return(
			<input
				type="text"
				name={name}
				id={id}
				placeholder={placeholder}
				data-location={location}
				data-fieldType={fieldType}
				data-required={required}
				required />
		);
	}

	getSimpleInput() {
        const {required, fieldType, name, location, placeholder} = this.props;
		return(
			<input
				type="text"
				name={name}
				id={name}
				placeholder={placeholder}
				data-location={location}
				data-fieldType={fieldType}
				data-required={required} />
		);
	}

	render() {
        const {required, name, className, captionText} = this.props;
		const input = required ? this.getRequiredInput() : this.getSimpleInput();
		const classNameVar = "input-text " + className;
		const requiredSign = (<span className='required' >*</span>);
		return (
			<div className="input-text">
				<label htmlFor={name} >
					{captionText + ' '}
				</label>
				{required ? requiredSign : ''}
				{input}
			</div>
		);
	}
}

TextInput.propTypes = {};
TextInput.defaultProps = {};

export default TextInput;