import React, {
    Component,
} from 'react';
import bindAll from 'lodash.bindall';
import Button from './input-types/button';
import CheckBox from './input-types/check-box';
import DateInput from './input-types/date-input';
import NumberInput from './input-types/number-input';
import PasswordInput from './input-types/password-input';
import RadioButtonGroup from './input-types/radio-button-group';
import Select from './input-types/select';
import HealthFundSelect from './input-types/healt-fund-select';
import TextInput from './input-types/text-input';
import EmailInput from './input-types/email-input';
import HiddenInput from './input-types/hidden-input';
import Message from '../components/message';
import CardNumberInput from './input-types/card-number-input';
import ShortDateInput from './input-types/short-date-input';

class Input extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'processProperties',
            'createHidden',
            'createButton',
            'createTextInput',
            'createCardNumberInput',
            'createShortDateInput',
            'createNumberInput',
            'createPasswordInput',
            'createDateInput',
            'createCheckBox',
            'createLinkedCheckBox',
            'createSelect',
            'createHealthFundSelect',
            'createEmailInput',
            'createRadioInputs'
        );
    }

    processProperties(props) {
        let selected, type;

        type = props.dataObjectConnection.dataObject.clientSideFieldType;
        switch (type) {
            case 'text' :
                selected = this.createTextInput();
                break;
            case 'date'    :
                selected = this.createDateInput();
                break;
            case 'email':
                selected = this.createEmailInput();
                break;
            case 'password':
                selected = this.createPasswordInput();
                break;
            case 'radio':
                selected = this.createRadioInputs();
                break;
            case 'select':
                selected = this.createSelect();
                break;
            case 'accept_0':
                selected = this.createLinkedCheckBox();
                break;
            case 'checkbox':
                selected = this.createCheckBox();
                break;
            case 'number' :
                selected = this.createNumberInput(0, 99999);
                break;
            case 'localized_length':
                selected = this.createNumberInput();
                break;
            case 'localized_weight':
                selected = this.createNumberInput();
                break;
            case 'button':
                selected = this.createButton();
                break;
            case 'healthFund':
                selected = this.createHealthFundSelect();
                break;
            case 'cardNum':
                selected = this.createCardNumberInput();
                break;
            case 'shortDate':
                selected = this.createShortDateInput();
                break;
            default:
                selected = this.createHidden();
                break;
        }

        if (this.props.name === 'acceptedContracts') {
            selected = this.createLinkedCheckBox();
        }

        return selected;
    }

    createHidden() {
        const {dataObjectConnection} = this.props;
        return (
            <HiddenInput
                name={dataObjectConnection.dataObject.clientSideFieldName}
                location={dataObjectConnection.location}
                fieldType={dataObjectConnection.fieldType}
                required={dataObjectConnection.required}
                captionText={dataObjectConnection.dataObject.captionText}
                attributes={dataObjectConnection} />
        );
    }

    createButton() {
        const {dataObjectConnection, loading} = this.props;
        return (
            <Button
                loading={loading}
                name={dataObjectConnection.dataObject.clientSideFieldName}
                location={dataObjectConnection.location}
                fieldType={dataObjectConnection.fieldType}
                required={dataObjectConnection.required}
                captionText={dataObjectConnection.dataObject.captionText}
                attributes={dataObjectConnection} />
        );
    }

    createTextInput() {
        const {dataObjectConnection, callName} = this.props;
        return (
            <TextInput
                name={dataObjectConnection.dataObject.clientSideFieldName}
                id={callName + this.capitalizeFirstLetter(dataObjectConnection.dataObject.clientSideFieldName)}
                location={dataObjectConnection.location}
                fieldType={dataObjectConnection.fieldType}
                required={dataObjectConnection.required}
                captionText={dataObjectConnection.dataObject.captionText}
                attributes={dataObjectConnection} />
        );
    }

    createCardNumberInput() {
        const {dataObjectConnection} = this.props;
        return (
            <CardNumberInput
                name={dataObjectConnection.dataObject.clientSideFieldName}
                location={dataObjectConnection.location}
                fieldType={dataObjectConnection.fieldType}
                required={dataObjectConnection.required}
                visible={dataObjectConnection.visible}
                captionText={dataObjectConnection.dataObject.captionText}
                attributes={dataObjectConnection} />
        );
    }

    createShortDateInput() {
        const {dataObjectConnection} = this.props;
        return (
            <ShortDateInput
                name={dataObjectConnection.dataObject.clientSideFieldName}
                location={dataObjectConnection.location}
                fieldType={dataObjectConnection.fieldType}
                required={dataObjectConnection.required}
                captionText={dataObjectConnection.dataObject.captionText}
                attributes={dataObjectConnection} />
        );
    }

    createNumberInput(min = null, max = null) {
        const {dataObjectConnection} = this.props;
        if (min !== null && max !== null) {
            return (
                <NumberInput
                    name={dataObjectConnection.dataObject.clientSideFieldName}
                    location={dataObjectConnection.location}
                    fieldType={dataObjectConnection.fieldType}
                    required={dataObjectConnection.required}
                    min={min}
                    max={max}
                    captionText={dataObjectConnection.dataObject.captionText}
                    attributes={dataObjectConnection} />
            );
        }
        return (
            <NumberInput
                name={dataObjectConnection.dataObject.clientSideFieldName}
                location={dataObjectConnection.location}
                fieldType={dataObjectConnection.fieldType}
                required={dataObjectConnection.required}
                captionText={dataObjectConnection.dataObject.captionText}
                attributes={dataObjectConnection} />
        );
    }

    capitalizeFirstLetter(string) {
        if (string !== null) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
        return '';
    }

    createPasswordInput() {
        const {dataObjectConnection, callName} = this.props;
        if (dataObjectConnection.dataObject.clientSideFieldName === 'password_confirm') {
            return null;
        }
        let confirmNeeded = false;
        let confirmTag = '';
        if (callName === 'register' || callName === 'reset_password' || callName === 'change_password') {
            confirmNeeded = true;
            confirmTag = callName;
        }
        return (
            <PasswordInput
                name={dataObjectConnection.dataObject.clientSideFieldName}
                id={callName + this.capitalizeFirstLetter(dataObjectConnection.dataObject.clientSideFieldName)}
                confirmTag={confirmTag}
                confirmNeeded={confirmNeeded}
                location={dataObjectConnection.location}
                fieldType={dataObjectConnection.fieldType}
                required={dataObjectConnection.required}
                captionText={dataObjectConnection.dataObject.captionText}
                attributes={dataObjectConnection} />
        );
    }

    createDateInput() {
        const {dataObjectConnection} = this.props;
        return (
            <DateInput
                name={dataObjectConnection.dataObject.clientSideFieldName}
                location={dataObjectConnection.location}
                fieldType={dataObjectConnection.fieldType}
                required={dataObjectConnection.required}
                captionText={dataObjectConnection.dataObject.captionText}
                attributes={dataObjectConnection} />
        );
    }

    createCheckBox() {
        const {dataObjectConnection} = this.props;
        return (
            <CheckBox
                name={dataObjectConnection.dataObject.clientSideFieldName}
                location={dataObjectConnection.location}
                fieldType={dataObjectConnection.fieldType}
                required={dataObjectConnection.required}
                captionText={dataObjectConnection.dataObject.captionText}
                attributes={dataObjectConnection} />
        );
    }

    createLinkedCheckBox() {
        const {dataObjectConnection} = this.props;
        return (
            <CheckBox
                name={dataObjectConnection.dataObject.clientSideFieldName}
                location={dataObjectConnection.location}
                fieldType={dataObjectConnection.fieldType}
                required={dataObjectConnection.required}
                captionText={dataObjectConnection.dataObject.captionText}
                attributes={dataObjectConnection}
                linked={true}
                linkText={'Felhasználási feltételeket'} />
        );
    }

    createSelect() {
        const {dataObjectConnection, params} = this.props;
        return (
            <Select
                name={dataObjectConnection.dataObject.clientSideFieldName}
                location={dataObjectConnection.location}
                fieldType={dataObjectConnection.fieldType}
                required={dataObjectConnection.required}
                captionText={dataObjectConnection.dataObject.captionText}
                attributes={dataObjectConnection.dataObject.attributes}
                params={params} />
        );
    }

    createHealthFundSelect() {
        const {dataObjectConnection} = this.props;
        return (
            <HealthFundSelect
                name={dataObjectConnection.dataObject.clientSideFieldName}
                location={dataObjectConnection.location}
                fieldType={dataObjectConnection.fieldType}
                required={dataObjectConnection.required}
                captionText={dataObjectConnection.dataObject.captionText}
                attributes={dataObjectConnection.dataObject.attributes} />
        );
    }

    createEmailInput() {
        const {dataObjectConnection, callName} = this.props;
        let confirmNeeded = false;
        let confirmTag = '';
        if (callName === 'register') {
            confirmNeeded = true;
            confirmTag = callName;
        }
        return (
            <EmailInput
                name={dataObjectConnection.dataObject.clientSideFieldName}
                location={dataObjectConnection.location}
                confirmTag={confirmTag}
                confirmNeeded={confirmNeeded}
                fieldType={dataObjectConnection.fieldType}
                required={dataObjectConnection.required}
                captionText={dataObjectConnection.dataObject.captionText}
                attributes={dataObjectConnection} />
        );
    }

    createRadioInputs() {
        const {dataObjectConnection} = this.props;
        return (
            <RadioButtonGroup
                name={dataObjectConnection.dataObject.clientSideFieldName}
                location={dataObjectConnection.location}
                fieldType={dataObjectConnection.fieldType}
                required={dataObjectConnection.required}
                captionText={dataObjectConnection.dataObject.captionText}
                attributes={dataObjectConnection} />
        );
    }

    render() {
        const {dataObjectConnection, errors} = this.props;
        let parsedErrors = [];
        if (errors) {
            parsedErrors  = errors.map(function (error) {
                if (dataObjectConnection.location) {
                    window.log(`PARSED ERROR ${dataObjectConnection.location}`, error);
                    if (error.location === dataObjectConnection.location) {
                        return error.validationError.localizedMessage + '\n';
                    }
                }
            }).filter(function (val) {
                return val !== undefined && val !== null && val !== '';
            });
        }
        window.log(`PARSED ERRORs ${dataObjectConnection.location}`, parsedErrors);
        return (
            <div className={(!dataObjectConnection.visible ? 'hide ' : '') + (parsedErrors.length > 0 ? 'error' : '') } >
                <Message
                    type='danger'
                    message={parsedErrors.filter(function (val) {
                        return val !== null && val !== null && val !== ''
                    })} />
                {this.processProperties(this.props)}
            </div>
        );
    }
}

Input.propTypes = {};
Input.defaultProps = {};

export default Input;