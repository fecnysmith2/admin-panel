import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';

class Loading extends Component {

    constructor(props) {
        super(props);
        // bindAll(this,
        //     '',
        // );

        this.state = {
            data: null
        };
    }

    render() {
        return (
            <i className='loader-item'></i>
        );
    }
}

Loading.propTypes = {};
Loading.defaultProps = {};

export default Loading;
