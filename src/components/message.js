import React, {Component} from "react";
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {unique} from "../utils/helpers.js";
import {Alert} from "react-bootstrap";

/*
 * types: success, info, warning, error, danger
 */
export default class Message extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'closeMessage', 'processMessages',
        );

        let messages = [];
        if (props.message && props.message.length > 0) {
            messages = props.message;
        }
        this.state = {
            messages: messages,
            closed: false
        }
    }

    closeMessage(index) {
        const {messages} = this.state;
        if (index > -1) {
            messages.splice(index, 1);
            this.setState({
                messages: messages
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        let messages = [];
        window.log('MESSAGE NEXT PROPS', nextProps);
        if (nextProps.message && (Array.isArray(nextProps.message) || (nextProps.message.type && nextProps.message.type === 'ul'))) {
            if (nextProps.message.length > 0) {
                messages = nextProps.message;
            }
            this.setState({
                messages: messages
            });
        }
    }

    processMessages() {
        const {type, closable} = this.props;
        const {messages} = this.state;
        const that = this;
        // TODO: disable closable alert
        const messagesArray = unique(messages).map(function (message, index) {
            const closeFunction = function () {
                that.closeMessage(index);
            };
            window.log('MESSAGE',typeof message);
            if (message === undefined || message === null) {
                return null;
            } else {
                return (
                    <Alert bsStyle={type}
                           className={["uk-alert"]}
                           key={'message' + index }
                           onDismiss={closeFunction}>
                        {message}
                    </Alert>
                );
            }
        });
        return messagesArray;
    }

    render() {
        const {messages} = this.state;
        const {containerClassName} = this.props;
        if (messages && messages.length > 0 && (typeof messages[0] === 'string' || (messages[0] && messages[0].type && messages[0].type === 'ul'))) {
            return (
                <div className={containerClassName} >
                    {this.processMessages()}
                </div>
            );
        } else {
            return null;
        }
    }

}

Message.propTypes = {
    type: PropTypes.oneOf(['success', 'info', 'warning', 'danger']),
    containerClassName: PropTypes.string,
    closable: PropTypes.bool,
    message: PropTypes.array
};

Message.defaultProps = {
    type: 'success',
    containerClassName: '',
    closable: false,
    message: []
};