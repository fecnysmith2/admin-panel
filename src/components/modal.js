import React, {Component} from 'react';
// import PropTypes from 'prop-types';
import {Modal, ModalHeader, ModalTitle, ModalClose, ModalBody} from 'react-modal-bootstrap';
// import bindAll from 'lodash.bindall';

class ModalDialog extends Component {

    constructor(props) {
        super(props);
        // bindAll(this,
        //     '',
        // );

        this.state = {
            data: null
        };
    }

    render() {
        const {open, close, size, enableClose, title, children, fixWidth} = this.props;
        return (
            <Modal isOpen={open} size={size}
                   onRequestHide={close} backdrop={enableClose} keyboard={enableClose}>
                <ModalHeader>
                    {title ? <ModalTitle>{title}</ModalTitle> : null}
                    {enableClose ? <ModalClose onClick={close}/> : null}
                </ModalHeader>
                <ModalBody>
                    <div style={fixWidth}>
                        {children}
                    </div>
                </ModalBody>
            </Modal>
        );
    }
}

ModalDialog.propTypes = {};
ModalDialog.defaultProps = {
    size: 'modal-lg',
    fixWidth: {width: 'auto'}
};

export default ModalDialog;

