import React, {Component} from 'react';

export default class PageIntro extends Component {

    randomClass() {
        return 'page-intro-' + Math.ceil(Math.random() * 4);
    }

    render() {
        const {children, text, className, title} = this.props;

        const content = children ? children : <p>{text}</p>;
        const classes = ['page-intro', this.randomClass()];
        if (className) {
            classes.push(className);
        }

        return (<section className="uk-padding">
                <div className="uk-container uk-container-center">
                    <div className={classes.join(' ')}>
                        <div className='container'>
                            <h1 className='h1'>{title}</h1>
                            {content}
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
