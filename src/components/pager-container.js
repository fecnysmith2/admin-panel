import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import ProgressBar from '../components/progress-bar.js';

class PagerContainer extends Component {
    constructor(props) {
        super(props);
        bindAll(this,
            'moveNextPage',
            'movePreviousPage'
        );
        this.state = {
            actualPageNumber: props.actualPageNumber ? parseInt(props.actualPageNumber) : 0,
        };
    }

    componentDidUpdate(prevProps, prevState, prevContext) {
        scroll(0, 0);
    }

    moveNextPage() {
        const {onNext} = this.props;
        const {actualPageNumber} = this.state;
        const newPagenumber = actualPageNumber + 1;
        this.setState({
            actualPageNumber: newPagenumber,
        });
        onNext(newPagenumber);
    }

    movePreviousPage() {
        const {onPrevious} = this.props;
        const newPagenumber = this.state.actualPageNumber - 1;
        this.setState({
            actualPageNumber: newPagenumber,
        });
        onPrevious(newPagenumber);
    }

    render() {
        const {children, firstPage, lastPage, totalPagesNumber} = this.props;
        const {actualPageNumber} = this.state;
        const that = this;
        window.log('ACTUAL_PAGE_NUMBER_PAGER_CONTAINER', actualPageNumber);
        window.log('TOTAL_PAGE_NUMBER_PAGER_CONTAINER', totalPagesNumber);

        const content = function () {
            if (actualPageNumber === 0) {
                return firstPage(that.moveNextPage);
            } else if (actualPageNumber === totalPagesNumber + 1) {
                return lastPage();
            } else {
                return children;
            }
        };
        return (
            <div>
                <ProgressBar
                    actualPageNumber={actualPageNumber}
                    totalPagesNumber={totalPagesNumber} />
                {content()}
                <div className='clearfix' >
                    <div className='pull-left col-sm-6 nopadding' >
                        <button
                            className={actualPageNumber === 0 ? 'button-tag-prev hidden' : 'button-tag-prev'}
                            onClick={this.movePreviousPage} >
                            Vissza
                        </button>
                    </div>
                    <div className='pull-right text-right col-sm-6 nopadding' >
                        <button
                            className={actualPageNumber === 0 || actualPageNumber === totalPagesNumber + 1 ? 'button-tag-next hidden' : 'button-tag-next'}
                            onClick={this.moveNextPage} >
                            Tovább
                        </button>
                    </div>
                </div>
            </div>
        );
    }

}

PagerContainer.propTypes = {};
PagerContainer.defaultProps = {};

export default PagerContainer;