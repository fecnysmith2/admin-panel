import React, {Component} from 'react';
import {ProgressBar as Progress} from 'react-bootstrap';

class ProgressBar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {actualPageNumber, totalPagesNumber} = this.props;
        const now = (((actualPageNumber-1) / totalPagesNumber) * 100).toFixed(0);
        return (
            <div>
                <Progress now={now}
                          bsClass='progress progress-bar'
                          label={`${now}%`} />
            </div>
        );
    }

}

ProgressBar.propTypes = {};
ProgressBar.defaultProps = {
    totalPagesNumber: 1,
    actualPageNumber: 1
};

export default ProgressBar;