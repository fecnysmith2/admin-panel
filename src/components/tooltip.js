import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import ToolTip from 'react-portal-tooltip';
import bindAll from 'lodash.bindall';

class Tooltip extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'showTooltip', 'hideTooltip',
        );

        this.state = {
            isTooltipActive: false
        };
    }

    showTooltip() {
        this.setState({isTooltipActive: true});
    }

    hideTooltip() {
        this.setState({isTooltipActive: false});
    }

    render() {
        const {id, style, tipClass, tipBaseClass, children, tooltipContent, position, arrow, tipOverClass} = this.props;
        const {isTooltipActive} = this.state;
        return (
            <div className={tipBaseClass}>
                <div id={id} className={tipClass} onMouseEnter={this.showTooltip} onMouseLeave={this.hideTooltip}>
                    {children}
                </div>
                <ToolTip active={isTooltipActive}
                         position={position}
                         arrow={arrow}
                         parent={'#' + id}
                         className={tipClass}
                         style={style}>
                    <div className={tipOverClass}>
                        {tooltipContent}
                    </div>
                </ToolTip>
            </div>
        );
    }
}

Tooltip.propTypes = {};
Tooltip.defaultProps = {
    arrow: "center",
    position: "right",
    tooltipContent: '',
    tipBaseClass: 'tooltip-base',
    tipClass: '',
    tipOverClass: 'tooltip-over',
    style: {
        style: {},
        arrowStyle: {}
    }
};

export default Tooltip;
