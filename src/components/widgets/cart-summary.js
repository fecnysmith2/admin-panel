import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {Link, withRouter} from "react-router";
import {priceFormat, getNetPrice, getVat} from "../../utils/number";
import Cart from "../../views/cart";

class CartSummary extends Component {

    constructor(props) {
        super(props);
        this.state = {
            'cartSummaryData': {'cartSum': Cart.getCartSum(), 'cartVat': getVat(Cart.getCartSum()), 'cartNet': getNetPrice(Cart.getCartSum())}
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({cartSummaryData: {'cartSum': Cart.getCartSum(), 'cartVat': getVat(Cart.getCartSum()), 'cartNet': getNetPrice(Cart.getCartSum())}})
    }


    render() {
        const {cartSummaryData} = this.state;
        window.log("cartSummary data", cartSummaryData);
        return (<div className="uk-grid uk-margin-large-bottom">
            <div className="uk-width-1-2 uk-width-medium-7-10 uk-width-small-1-1 uk-container-center">
                <div className="uk-panel-box idz-panel grey uk-text-contrast">
                    <h4 className="uk-text-contrast">Kosár végösszege</h4>
                    <table id="cart-table-total" className="uk-table">
                        <tbody>
                        <tr>
                            <th>Kosár tartalma</th>
                            <td>{priceFormat(cartSummaryData.cartSum)} Ft</td>
                        </tr>
                        <tr>
                            <th>Áfa</th>
                            <td>{priceFormat(cartSummaryData.cartVat)} Ft</td>
                        </tr>
                        <tr>
                            <th>Nettó összeg</th>
                            <td>{priceFormat(cartSummaryData.cartNet)} Ft</td>
                        </tr>
                        <tr>
                            <th>Összesen</th>
                            <td>{priceFormat(cartSummaryData.cartSum)} Ft</td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="/checkout" className="uk-button uk-button-small idz-button-white uk-width-1-1">Tovább a megrendeléshez</a>
                </div>
            </div>
        </div>);
    }
}

CartSummary.propTypes = {};
CartSummary.defaultProps = {};

export default CartSummary;
