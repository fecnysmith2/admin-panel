import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {Link, withRouter} from "react-router";
import makeRequest from '../../../utils/request-queue-handler'

class CategoryList extends Component {

    constructor(props) {
        super(props);

        bindAll(this,
            'loadCategories'
        );

        this.state = {
            loading:true,
            subCategories: {}
        };
        this.loadCategories();
    }

    loadCategories(){
        let that = this;
        const params = {
            "ObjectID": this.props.currentCategory,
        };
        makeRequest("getCategoryWithChildren", params, function(ret){
            window.log("loadChildCategories return", ret);
            that.setState({loading:false, subCategories: ret.children});
        });
    }

    render() {
        let that = this;
        if(this.state.loading){
            return (<aside>
                <h4 className="uk-margin-medium-bottom"><span className="line-accent">Kategóriák</span></h4>
                <span className="loading"/>
                <div className="uk-clearfix"/>
            </aside>);
        }else {
            window.log("this.state.subCategories",this.state.subCategories);
            return (<aside>
                <h4 className="uk-margin-medium-bottom"><span className="line-accent">Kategóriák</span></h4>
                <ul className="uk-list uk-list-line">
                    {Object.keys(this.state.subCategories).map(function(categoryDataKey){
                        const categoryData = that.state.subCategories[categoryDataKey];
                        const categoryURL = "/category/"+categoryDataKey;
                        return <li><a href={categoryURL}>{categoryData.properties.name.hu}</a></li>
                    })}
                </ul>
            </aside>);
        }
    }
}

CategoryList.propTypes = {};
CategoryList.defaultProps = {};

export default CategoryList;
