import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {Link, withRouter} from "react-router";

class FeaturedProducts extends Component {

    constructor(props) {
        super(props);
    }

    productRow(){
        return (<li>
            <a href="product-page.html">
                <img src="images/product-images/product_thumb5.jpg" alt=""/>
                <h6><span>Curved Swivel Chair</span></h6>
            </a>
            <span>$1,440</span><span className="strikethrough-price">$2,155</span>
        </li>);
    }

    render() {
        return (<aside>
            <h4 className="uk-margin-medium-bottom"><span className="line-accent">Kiemelt termékek</span></h4>
            <ul className="uk-list uk-list-line idz-product-widget">
                <li>Nem található kiemelt termék!</li>
            </ul>
        </aside>);
    }
}

FeaturedProducts.propTypes = {};
FeaturedProducts.defaultProps = {};

export default FeaturedProducts;
