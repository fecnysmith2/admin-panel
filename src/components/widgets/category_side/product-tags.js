import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {Link, withRouter} from "react-router";

class ProductTags extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (<aside>
                <h4 className="uk-margin-medium-bottom"><span className="line-accent">Termék cimkék</span></h4>
                <div className="idz-tag-cloud">
                </div>
            </aside>);
    }
}

ProductTags.propTypes = {};
ProductTags.defaultProps = {};

export default ProductTags;
