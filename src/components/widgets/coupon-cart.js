import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {Link, withRouter} from "react-router";

class CouponCart extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'checkCoupon'
        );
        this.state = {
            couponSent: false
        };
    }

    checkCoupon(){
        this.setState({couponSent: true});
    }

    render() {
        let buttonContainer = "Kupon beváltása";
        return (
            <table id="cart-table-bottom" className="uk-table">
                <tbody><tr>
                    <th>
                        <form className="uk-form apply-coupon">
                            <fieldset>
                                <input type="text" placeholder="Kuponkód" disabled={this.state.couponSent}/>
                                <a onClick={this.checkCoupon} className="uk-button uk-button-small idz-button-grey uk-margin-small-top">
                                    {buttonContainer}
                                </a>
                                {this.state.couponSent ? <span className="loading pull-right" style={{"float":"unset", "font-size": "unset"}}/> : ""}
                            </fieldset>
                        </form>
                    </th>
                </tr>
                </tbody>
            </table>);
    }
}

CouponCart.propTypes = {};
CouponCart.defaultProps = {};

export default CouponCart;
