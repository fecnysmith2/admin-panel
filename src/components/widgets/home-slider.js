import React, {
    Component,
} from 'react';
import makeRequest from '../../utils/request-queue-handler'
import {bindAll} from "lodash";

class HomeSlider extends Component {

    constructor(props) {
        super(props);
        // bindAll(this,
        //     ''
        // );

        this.state = {
            'loading': false,
            'menus': null
        }
    }
    
    render() {
        if(this.state.loading){
            return (<div id="secondary-info"><span className="loading"/></div>);
        }else {
            return (<section id="slideshow-container">
                <div id="homepage-slide" className="uk-slidenav-position">
                    <ul className="uk-slideshow uk-overlay-active" data-uk-slideshow="{autoplay:true}">
                        <li>
                            <img src="/images/slideshow/sld1.jpg" alt=""/>
                            <div className="uk-overlay-panel uk-overlay-background uk-overlay-fade uk-flex uk-flex-center uk-flex-middle">
                                <div className="uk-container uk-container-center">
                                    <div className="slide-content1">
                                        <h1>CSAK MA</h1>
                                        <h1 className="discount-text2">40%</h1>
                                        <h4 style={{"marginTop":"157px"}}>kedvezmény minden termékre</h4>
                                        <div className="uk-clearfix"/>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <a href="" className="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous">&nbsp;</a>
                    <a href="" className="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next">&nbsp;</a>
                </div>
            </section>);
        }
    }
}

HomeSlider.propTypes = {};
HomeSlider.defaultProps = {};

export default HomeSlider;
