import React, {
    Component,
} from 'react';
import makeRequest from '../../utils/request-queue-handler'
import {bindAll} from "lodash";

class MainMenu extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'loadMenus'
        );

        this.state = {
            'loading': true,
            'menus': null
        }
    }

    componentWillMount(){
        this.loadMenus();
    }

    loadMenus(){
        let that = this;
        makeRequest("getMenu", {"MenuName":"MainMenu"}, function(ret){
            that.setState({loading:false, menus:ret});
        });
    }

    render() {
        if(this.state.loading){
            return (<div id="secondary-info"><span className="loading"/></div>);
        }else {
            return (<div id="secondary-info">
                <nav id="mainmenu" className="uk-navbar">
                    <ul className="uk-navbar-nav uk-hidden-small">
                        {this.state.menus.content.map(function(menuData, index){
                            let dropdownList;
                            if(menuData.hasOwnProperty("childrenMenus") && menuData.childrenMenus.length > 0){
                                dropdownList = (<div className="uk-dropdown uk-dropdown-navbar">
                                                    <ul className="uk-nav uk-navbar">
                                                        {menuData.childrenMenus.map(function(childMenu){
                                                            return (<li><a href={childMenu.url}>{childMenu.name}</a></li>);
                                                        })}
                                                    </ul>
                                                </div>);
                            }

                            return (
                                <li key={index} className="uk-parent" data-uk-dropdown>
                                    <a href={menuData.url}>{menuData.name}</a>
                                    {dropdownList}
                                </li>);
                        })}
                    </ul>
                    <a href="#mobile-nav" className="uk-navbar-toggle uk-visible-small" data-uk-offcanvas/>
                </nav>
                <div id="mobile-nav" className="uk-offcanvas">
                    <div className="uk-offcanvas-bar">
                        <ul className="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav>
                            {this.state.menus.content.map(function(menuData, index){
                                return (<li key={index}><a href={menuData.url}>{menuData.name}</a></li>);
                            })}
                        </ul>
                    </div>
                </div>
            </div>);
        }
    }
}

MainMenu.propTypes = {};
MainMenu.defaultProps = {};

export default MainMenu;
