import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {Link, withRouter} from "react-router";
import Cart from "../../views/cart";
import {priceFormat} from "../../utils/number";

class MiniCart extends Component {

    constructor(props) {
        super(props);

        bindAll(this,
            'loadCartData'
        );

        this.state = {
            'cartData': {'cartNum': 0, 'cartSum': 0},
            'cartDataLoading': true,
            'cartItems': []
        };
    }

    componentWillMount() {
        setInterval(this.loadCartData, 200);
    }

    loadCartData(){
        let cartData = {'cartNum': Cart.getCartNum(), 'cartSum': Cart.getCartSum()};
        this.setState({cartDataLoading: false, cartData: cartData, cartItems: Cart.getCartData()});
    }

    getMiniCartBody(){
        let that = this;
        if(Object.keys(this.state.cartItems).length > 0){
            return (<div className="uk-dropdown uk-dropdown-center"><ul className="uk-nav uk-nav-dropdown idz-product-widget">
                {Object.keys(this.state.cartItems).map(function(cartIndex){
                    const cartItem = that.state.cartItems[cartIndex];
                    const productURL = "/product/"+cartItem.product._id;
                    return (<li className="uk-text-truncate">
                        <a href={productURL}>
                            <img src={cartItem.product.default_media.url} alt=""/>
                            <p>{cartItem.product.properties.name.hu}</p>
                        </a>
                        <span>{cartItem.qty} x {priceFormat(cartItem.product.prices.huf.actionprice ? cartItem.product.prices.huf.actionprice : cartItem.product.prices.huf.price)} Ft</span>
                    </li>);
                })}
                <li className="subtotal-price">
                    <h6>Összesen : {priceFormat(this.state.cartData.cartSum)} Ft</h6>
                </li>
                <li>
                    <a href="/cart" style={{"width":"100%"}} className="uk-button uk-button-mini idz-button-white uk-width-1-1">Kosár megtekintése</a>
                </li>
            </ul></div>);
        }
    }

    render(){
        let that = this;
        if(this.state.cartDataLoading){
            return (<span className="loading"/>);
        }else{
            return (<div className="uk-button-dropdown" data-uk-dropdown>
                        <span className="uk-icon-button uk-icon-shopping-cart">&nbsp;</span>
                            <div className="uk-badge uk-badge-notification uk-badge-danger">{this.state.cartData.cartNum}</div>
                            {priceFormat(this.state.cartData.cartSum)} Ft

                        {this.getMiniCartBody()}
                    </div>);
        }
    }

}

MiniCart.propTypes = {
};
MiniCart.defaultProps = {
};

export default MiniCart;
