import React, {
    Component,
} from 'react';
import bindAll from 'lodash.bindall';
import {getPostFromWP} from '../../utils/news-utils';
import {registerObservable} from '../../utils/observables';

class NewsFeed extends Component {
    constructor(props) {
        super(props);
        bindAll(this,
            'setNews', 'setMaxHeight',
        );
        this.state = {
            news: [],
            errors: [],
            maxHeight: '0px'
        }
    }

    componentWillMount() {
        registerObservable('newsFeed', 'dashboardMaxHeight', this.setMaxHeight);
        getPostFromWP(this.props.type, this.setNews, this.props.subPage);
    }

    componentDidUpdate(prevProps, prevState, prevContext) {
        this.setMaxHeight();
    }

    setMaxHeight() {
        const {maxHeight} = this.state;
        const dashboardHMax =document.getElementById('dashboard').clientHeight;
        const sidebarHMax = document.getElementById('userSideBar').clientHeight;
        const newHeight = `${(dashboardHMax > sidebarHMax ? dashboardHMax : sidebarHMax)}px`;
        if (newHeight !== maxHeight) {
            this.setState({
                maxHeight: newHeight
            });
        }
    }

    setNews(news) {
        this.setState({
            news: news
        });
    }

    render() {
        const {news, maxHeight} = this.state;
        const style = {
            maxHeight: maxHeight,
            height: maxHeight
        };
        return (
            <div className='newsfeed col-md-3'
                 style={style} >
                {news}
            </div>
        );
    }
}

NewsFeed.propTypes = {};
NewsFeed.defaultProps = {
    type: 'dashboard',
    subPage: ''
};

export default NewsFeed;
