import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {Link, withRouter} from "react-router";
import makeRequest from '../../utils/request-queue-handler'
import button from "../input-types/button";
import {validateElement} from "../../utils/validation";

class NewsletterSignup extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'signupNewsletter', 'clearError'
        );

        this.state = {
            loading: false,
            signedUp: false,
            signupError: false,
            errorMessage: "Ez az email cím már szerepel az adatbázisunkban!"
        };
    }

    signupNewsletter(event) {
        event.preventDefault();
        // funcname newsletterSignup
        let formData = $('#footer-newsletter').serializeObject();
        let that = this;
        this.setState({loading: true});

        if(!validateElement("Email",formData.email)){
            that.setState({signupError: true, loading: false, errorMessage: "Nem megfelelő a megadott email cím formátuma, próbáld újra!"});
            // clear error message after x sec
            setTimeout(function () {
                that.setState({signupError: false});
            }, 3000);
        }else {
            makeRequest("newsletterSignup", formData, function (retObject) {
                window.log("newsletterSignup return", retObject);
                if (retObject.insertedId) {
                    that.setState({signedUp: true, loading: false});
                } else {
                    that.setState({signupError: true, loading: false});
                    // clear error message after x sec
                    setTimeout(function () {
                        that.setState({signupError: false});
                    }, 3000);
                }
            });
        }
        return false;
    }

    clearError(){
        this.setState({signupError: false});
    }

    render() {
        return this.state.signupError ? (<div className="uk-alert uk-alert-warning" data-uk-alert="">
            <a onClick={this.clearError} className="uk-alert-close"/>
            <i className="uk-icon-warning"/>
            <p className="uk-margin-small-top">{this.state.errorMessage}</p>
        </div>) : (this.state.signedUp ? (<div className="uk-alert uk-alert-success" data-uk-alert="">
            <i className="uk-icon-check"/>
            <p className="uk-margin-small-top">Sikeresen feliratkoztál a hírleveleinkre!</p>
        </div>) : (<div>
                <h4>Hírlevél feliratkozás</h4>
                <form id="footer-newsletter" className="uk-form" onSubmit={this.signupNewsletter}>
                    <fieldset>
                        <input type="text" name="email" placeholder="add meg az e-mail címedet..."/>
                        <a onClick={this.signupNewsletter} className="uk-button" style={{"paddingTop": "6px"}}> {!this.state.loading ?
                            <i className="uk-icon-envelope-o"/> :
                            <span className="loading" style={{"margin": "unset"}}/>}</a>
                    </fieldset>
                </form>
            </div>));
    }
}

NewsletterSignup.propTypes = {};
NewsletterSignup.defaultProps = {};

export default NewsletterSignup;
