import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {Link, withRouter} from "react-router";

class Pagination extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'getPagesArray'
        );
        this.state = {
            'pageArray': [],
            'loading': true
        };
        this.getPagesArray();
    }

    getPagesArray(){
        const productsPerPage = parseInt(this.props.productsPerPage);
        const allProducts = parseInt(this.props.productsFound);
        const pagesCount = Math.ceil(allProducts / productsPerPage);
        const currentPage = this.props.currentPage;

        function pagination(c, m) {
            let delta = 2,
                range = [],
                rangeWithDots = [],
                l;

            range.push(1);
            for (let i = c - delta; i <= c + delta; i++) {
                if (i < m && i > 1) {
                    range.push(i);
                }
            }
            range.push(m);

            for (let i of range) {
                if (l) {
                    if (i - l === 2) {
                        rangeWithDots.push(l + 1);
                    } else if (i - l !== 1) {
                        rangeWithDots.push('...');
                    }
                }
                rangeWithDots.push(i);
                l = i;
            }

            return rangeWithDots;
        }
        return pagination(currentPage, pagesCount);
    }

    getPagesRow(){
        let that = this;
        window.log("paginationArray",this.getPagesArray());

        return this.getPagesArray().map(function(page){
            const changePage = function(){
                that.props.onPagination(page);
                return false;
            };
            let className = "page-item ";
            let innerContent = (<a onClick={changePage}>{page}</a>);

            if(page === that.props.currentPage){
                className = className + "active";
                innerContent = (<a className="page-link" href="javascript:void(0)">{page}</a>);
            }

            if(page === "..."){
                className = className + "disabled";
                innerContent = (<span className={"page-link"} style={{"background":"transparent"}}>{page}</span>);
            }
            return <li className={className} >{innerContent}</li>
        });
    }

    render() {
        return (<nav aria-label="Lapozó">
            <ul className="pagination pagination-sm">
                    {this.getPagesRow()}
            </ul></nav>);
    }
}

Pagination.propTypes = {};
Pagination.defaultProps = {};

export default Pagination;
