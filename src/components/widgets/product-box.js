import React, {
    Component,
} from 'react';
import bindAll from 'lodash.bindall';
import {Link, withRouter} from "react-router";
import makeRequest from '../../utils/request-queue-handler'

class ProductBox extends Component {

    constructor(props) {
        super(props);

        bindAll(this,
            'loadProductData'
        );
        this.state = {
            'productAddedToCart': false,
            ProductData: null,
            loading: true
        };
    }


    componentWillReceiveProps(nextProps, nextContext) {
        this.loadProductData();
    }

    componentWillMount() {
        this.loadProductData();
    }


    loadProductData(){
        let that = this;
        that.setState({loading:true});
        window.log("ProductBox loadProductData", {"ObjectID":this.props.productID});
        makeRequest("getProductData", {"ObjectID":this.props.productID}, function(ret){
            window.log("loadProductData return", ret);
            that.setState({loading:false, ProductData: ret});
        });
    }

    render() {
        const {ProductData} = this.state;
        let that = this;
        return this.state.loading ? (<span className="Loading__spinner___11Pm4"></span> ) : (<div className="profile-widget-compact">
            <div className="profile-widget-upper" style={{"backgroundColor": "#F7FBFC"}}>
                <div className="profile-image">
                    <img src={ProductData.default_media.url} alt="Avatar"/>
                </div>
                <div className="profile-info">
                    <p>{ProductData.properties.name.hu}</p>
                    <ul className="list-inline">
                        <li><a href="javascript:void(0)" className="text-grey"><i class="fa fa-facebook fa-fw"></i></a></li>
                        <li><a href="javascript:void(0)" className="text-grey"><i class="fa fa-twitter fa-fw"></i></a></li>
                        <li><a href="javascript:void(0)" className="text-grey"><i class="fa fa-google-plus fa-fw"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>);
    }
}

ProductBox.propTypes = {};
ProductBox.defaultProps = {};

export default ProductBox;
