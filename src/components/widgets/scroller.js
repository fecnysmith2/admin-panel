import React, {
   Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {Link} from 'react-router';
import Slider from 'react-slick';

var divStyle = {
  backgroundImage: 'url(img/scroller/dash-scroller-photo.jpg)',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat'
};

var divStyleBB = {
  backgroundImage: 'url(img/scroller/scroller-eletmodvalto-program.jpg)',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat'
};

class SimpleSlider extends Component {
   constructor(props) {
       super(props);
   }

  render() {
    var settings = {
      autoplay: true,
      autoplaySpeed: 2700,
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      lazyLoad: true,
      variableWidth: false,
      nextArrow: null,
      prevArrow: null
    };
    return (
      <Slider {...settings}>
        {this.props.children}
      </Slider>
    );
  }
}

class Scroller extends Component {
   constructor(props) {
       super(props);
   }

  render() {
      return (
        <SimpleSlider>
          <div>
            <a href="https://webshop.onlinetrainer.hu/gerinc-csomag" target="_blank">
              <p className="text">Gerinc csomag</p>
              <img src="/img/scroller/gerinc.jpg" alt=""/>
            </a>
          </div>
          <div>
            <a href="http://webshop.onlinetrainer.hu/gerinc-csomag-extra" target="_blank">
              <p className="text">Gerinc csomag Extra</p>
              <img src="/img/scroller/gerinc-extra.jpg" alt=""/>
            </a>
          </div>
          <div>
            <a href="http://webshop.onlinetrainer.hu/eletmodvalto-csomag" target="_blank">
              <p className="text">Életmódváltó csomag</p>
              <img src="/img/scroller/eletmodvalto.jpg" alt=""/>
            </a>
          </div>
          <div>
            <a href="http://webshop.onlinetrainer.hu/eletmodvalto-csomag-extra" target="_blank">
              <p className="text">Életmódváltó csomag Extra</p>
              <img src="/img/scroller/eletmodvalto-extra.jpg" alt=""/>
            </a>
          </div>
          <div>
            <a href="http://start.onlinetrainer.hu/10-legnagyobb-tevhit-az-edzessel-kapcsolatban/" target="_blank">
              <p className="text">A 10 legnagyobb tévhit az edzéssel kapcsolatban</p>
              <img src="/img/scroller/dash-scroller-photo.jpg" alt=""/>
            </a>
          </div>
        </SimpleSlider>
      );
  }
}

Scroller.propTypes = {};
Scroller.defaultProps = {};

export default Scroller;