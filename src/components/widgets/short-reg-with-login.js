import React, {
    Component
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import Message from '../message';
import FacebookRegisterButton from './facebook/facebook-register-button';
import FrontendPage from '../frontend-page';
import {Link} from 'react-router';
import {getRegisterUserMutation} from '../../utils/queries/queries';
import {saveOauthTokenObject} from '../../utils/db';
import {saveUserObject} from '../../utils/user-util';
import makeRequest from '../../utils/request-queue-handler';
import getObjectFromForm from '../../utils/object-from-form-generator';
import Login from '../../views/login';

class ShortRegWithLogin extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'handleSubmit', 'onQueryError', 'getRegisterUserMutation',
            'handleFacebookLogin', 'handleFacebookRegSuccess',
        );

        this.state = {
            errors: [],
            loading: false,
        };
    }

    componentDidUpdate(prevProps, prevState, prevContext) {
        const {loading} = this.props;
        if (loading !== null && prevProps.loading !== loading) {
            this.setState({
                loading: loading
            });
        }
    }


    onQueryError(errors) {
        window.error('REG', errors);
        let errorsArray = [];
        if (errors) {
            errorsArray = errors[0].exception.validationResult.errors.map(function (error, index) {
                return error;
            });
        } else {
            errorsArray.push('Valami hiba történt! Kérlek próbálkozz később!');
        }
        this.setState({
            errors: errorsArray,
            loading: false
        });
    }

    handleFacebookRegSuccess(userWrapper) {
        const {onLoggedIn} = this.props;
        window.log('FB USER WRAPPER', userWrapper);
        onLoggedIn();
        this.setState({
            errors: [],
            loading: false
        });
    }

    handleFacebookLogin(facebookUserData) {
        window.log('FB USER DATA', facebookUserData);
    }

    getRegisterUserMutation(event) {
        let userData;
        try {
            userData = getObjectFromForm(event.target.getAttribute('data-queryParamName'), event.target.elements);
        } catch (err) {
            window.error('VALIDATE_REGISTER_FORM_FAILED', err);
            return false;
        }

        return getRegisterUserMutation(userData);
    }

    handleSubmit(event) {
        event.preventDefault();
        const {onLoggedIn} = this.props;
        const {errors} = this.state;

        this.setState({
            loading: true,
            errors: []
        });

        const that = this;
        const registerUserMutation = this.getRegisterUserMutation(event);
        window.log('LOG REGISTER', registerUserMutation);
        if (registerUserMutation) {
            makeRequest(registerUserMutation, function (response) {
                const userData = response.data.User_register;
                window.log('LOG REGISTER USER', userData);
                saveOauthTokenObject(userData.initialOAuth2Token);
                saveUserObject(userData.user);
                that.setState({
                    errors: [],
                    loading: false
                });
                onLoggedIn(true);
            }, function (error) {
                that.onQueryError(error);
            });
        } else {
            errors.push('Hibás adatok!');
            this.setState({
                errors: errors,
                loading: false
            });
        }
    }

    render() {
        const {title, onLoggedIn, customLoginButtonLabel} = this.props;
        const {errors, loading} = this.state;
        let loginButton = null;
        if (document.referrer) {
            window.log('IFRA', document.referrer.includes('kupon-igenyles'))
            loginButton = document.referrer.includes('kupon-igenyles') ? 'Bejelentkezek és kérem a kupont' : null;
        }

        if (loginButton === null) {
            loginButton = customLoginButtonLabel;
        }
        return (
            <div className='col-sm-12 coupon-page2' >
                <div className='col-sm-6 col-xs-12 nopadding' >
                    <p><strong className="negate">Regisztrált tagként kattints ide:</strong></p>
                    <img src='/img/kupon/nyil-gorbe.svg' alt='' />
                    <Login showFacebook={false}
                           customLoginButtonLabel={loginButton}
                           onLogin={function () {
                               onLoggedIn();
                           }}/>
                </div>
                <div className='col-sm-6 col-xs-12 nopadding' >
                    <p><strong className="negate">Amennyiben még nem regisztráltál, tedd meg pillanatok alatt, itt:</strong></p>
                    <img src='/img/kupon/nyil-gorbe2.svg' alt='' />
                    <Message type='danger'
                             message={errors} />
                    <h1 className='h1' >Regisztráció</h1>
                    <FacebookRegisterButton onRegisterError={this.onQueryError}
                                            onRegisterSuccess={this.handleFacebookRegSuccess}
                                            onFaceBookLogin={this.handleFacebookLogin}/>
                    <FrontendPage errors={errors}
                                  handleSubmit={this.handleSubmit}
                                  queryName='short_register'
                                  loading={loading} />
                    <p className='text-center'>
                        A regisztrációval elfogadod a
                        <a href='/aszf'
                           style={{color: '#577ebe'}} > Felhasználási feltételeket.
                        </a>
                    </p>
                </div>
                <div>
                  <small>* A kupon nem átruházható, készpénzre nem váltható be. A kupon felhasználási ideje a beváltástól számítva 31 naptári nap, legkésőbb augusztus 31-ig beváltható.</small>
                </div>
            </div>
        );
    }
}

ShortRegWithLogin.propTypes = {
    title: PropTypes.node,
    loading: PropTypes.bool,
    onLoggedIn: PropTypes.func,
    customLoginButtonLabel: PropTypes.string
};
ShortRegWithLogin.defaultProps = {
    title: <h2 className='text-center'>A folytatáshoz regisztrálj, vagy jelentkezz be!</h2>,
    customLoginButtonLabel: null,
    loading: null,
    onLoggedIn: function () {
        window.log('LOGGED IN');
    }
};

export default ShortRegWithLogin;
