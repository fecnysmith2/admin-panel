import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {Link, withRouter} from "react-router";

class StayConnected extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (<div>
            <div id="footer-social" className="idz-social uk-align-right">
                <ul>
                    <li>
                        <a href="#" className="uk-icon-button uk-icon-facebook">&nbsp;</a>
                    </li>
                    <li>
                        <a href="#" className="uk-icon-button uk-icon-twitter">&nbsp;</a>
                    </li>
                    <li>
                        <a href="#" className="uk-icon-button uk-icon-instagram">&nbsp;</a>
                    </li>
                    <li>
                        <a href="#" className="uk-icon-button uk-icon-google-plus">&nbsp;</a>
                    </li>
                    <li>
                        <a href="#" className="uk-icon-button uk-icon-pinterest-p">&nbsp;</a>
                    </li>
                </ul>
            </div>
            <h4>Maradj kapcsolatban</h4></div>);
    }
}

StayConnected.propTypes = {};
StayConnected.defaultProps = {};

export default StayConnected;
