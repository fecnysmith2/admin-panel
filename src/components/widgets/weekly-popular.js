import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {Link, withRouter} from "react-router";
import makeRequest from '../../utils/request-queue-handler'
import ProductBox from "./product-box";

class WeeklyPopular extends Component {

    constructor(props) {
        super(props);

        bindAll(this,
            'loadPopular'
        );

        this.state = {
            loading: true,
            products: {}
        };

        this.loadPopular();
    }

    loadPopular(){
        let that = this;
        makeRequest("getPopular", {}, function(ret){
            window.log("getPopular return", ret);
            that.setState({loading:false, products: ret});
        });
    }

    render() {
        let that = this;
        return (<div><h2 className="uk-text-center uk-margin-medium-bottom"><span className="cross-accent">Heti népszerű</span></h2>
            <div id="product-slide" className="uk-slidenav-position" data-uk-slider="{infinite: false}">
                <div className="uk-slider-container">
                    {this.state.loading ? (<span className="loading"/>) :
                        (<ul className="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-3">
                            {Object.keys(this.state.products).map(function(rowData){
                                const productData = that.state.products[rowData].product;
                                window.log("weeklyProductData", productData);
                                return <ProductBox ProductData={productData}/>
                            })}
                        </ul>)
                    }

                    <a href="" className="uk-slidenav uk-slidenav-previous" data-uk-slider-item="previous">&nbsp;</a>
                    <a href="" className="uk-slidenav uk-slidenav-next" data-uk-slider-item="next">&nbsp;</a>
                </div>
            </div>
        </div>);
    }
    }

    WeeklyPopular.propTypes = {};
    WeeklyPopular.defaultProps = {};

    export default WeeklyPopular;
