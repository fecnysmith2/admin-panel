import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

i18n
    .use(LanguageDetector)
    .init({
        fallbackLng: 'hu',

        // have a common namespace used around the full app
        ns: ['header'],
        defaultNS: 'header',

        debug: true,

        interpolation: {
            escapeValue: false, // not needed for react!!
            formatSeparator: ',',
            format: function(value, format, lng) {
                if (format === 'uppercase') return value.toUpperCase();
                return value;
            }
        }
    });

export default i18n;