import React from 'react';
import {render} from 'react-dom';
import {Router, browserHistory} from 'react-router';
import {initLoki, isLoginSessionActive} from './utils/db';
import routes from './routes';
import {initPolyfills} from './utils/polyfills';
import '../scss/styles.scss';
import {environment, TITLE_BASE} from '../config';
import $ from 'jquery';
import { I18nextProvider } from 'react-i18next'; // as we build ourself via webpack
import i18n from './i18n'; // initialized i18next instance


/*Logoláshoz a window.log helyett ez használandó*/
window.log = function (title = '', object = '') {
    if (environment != 'prod') {
        console.log(title, object);
    }
};
/*Error logoláshoz a window.log helyett ez használandó*/
window.error = function (title = '', object = '') {
    if (environment != 'prod') {
        console.error(title, object);
    }
};



    $.fn.serializeObject = function(){

        let self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };


        this.build = function(base, key, value){
            base[key] = value;
            return base;
        };

        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function(){

            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            let k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }

                // named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };

// Main Notification Object
var Notification = function(element, options) {
    var self = this;

    self.ele = element;
    self.$ele = $(element);


    // Extend or Overwrite the defaults options
    self.options = $.fn.extend({}, $.fn.notify.defaults, options);


    // Check if the notification container for the specific position is available
    // if so use it else create a new one
    if($('.notify-container[data-position="' + self.options.position + '"]').length) {
        self.container = self.$ele.find('.notify-container[data-position="' + self.options.position + '"]');
    } else {
        self.container = $('<div class="notify-container" ' +
            'data-position="' + self.options.position + '">' +
            '</div>');

        self.$ele
            .append(self.container);
    }


    // If the type of the notification is thumb or thumb circle
    // returnt he html without the stype attribute
    // otherwise return the html with the style attribute
    if(self.options.type == 'thumb' || self.options.type == 'thumb-circle') {
        self.wrapper = $('<div class="notify-wrapper" ' +
            'data-type="' + self.options.type + '">' +
            '</div>');
    } else {
        self.wrapper = $('<div class="notify-wrapper" ' +
            'data-style="' + self.options.style + '" ' +
            'data-type="' + self.options.type + '">' +
            '</div>');
    }


    // Store the close buttons
    self.closeButton = $('<i class="fa fa-times fa-2x notify-close-button"></i>');


    // Append the close button
    self.wrapper
        .append(self.closeButton);

    // Initiate the notification
    self.init();
};


// Init function for the notification
Notification.prototype.init = function() {
    var self = this;


    // Return new notification based on the
    // type selected or given
    switch(self.options.type) {
        case 'thumb' :
            new NotifyThumb();
            break;
        case 'thumb-circle' :
            new NotifyThumbCircle();
            break;
        case 'alert' :
            new NotifyAlert();
            break;
        case 'simple' :
            new NotifySimple();
            break;
        case 'flip' :
            new NotifyFlip();
            break;
        case 'bar' :
            new NotifyBar();
            break;
    }


    // Thumb Notification
    function NotifyThumb() {

        var thumbContainer = $('<div class="thumb--container"></div>');
        var thumbImage = $('<div class="thumb--image">' +
            '<img src="' + self.options.imgSource + '">' +
            '</div>');
        var thumbMessage = $('<div class="thumb--message">' +
            '<span>' + self.options.message + '</span>' +
            '</div>');

        thumbContainer
            .append(thumbImage)
            .append(thumbMessage)
            .prependTo(self.wrapper);

        self.wrapper.addClass('notify-open');

    };


    // Thumb Circle Notification
    function NotifyThumbCircle() {

        var thumbCircleContainer = $('<div class="thumb-circle--container"></div>');
        var thumbCircleImage = $('<div class="thumb-circle--image">' +
            '<img src="' + self.options.imgSource + '">' +
            '</div>');
        var thumbCircleMessage = $('<div class="thumb-circle--message">' +
            '<span>' + self.options.message + '</span>' +
            '</div>');

        thumbCircleContainer
            .append(thumbCircleImage)
            .append(thumbCircleMessage)
            .prependTo(self.wrapper);

        self.wrapper.addClass('notify-open');

    };


    // Alert Notification
    function NotifyAlert() {
        var alertIcon = '';

        if(self.options.style == 'info') {
            alertIcon = 'info-circle';
        } else if(self.options.style == 'warning') {
            alertIcon = 'exclamation-circle';
        } else if(self.options.style == 'danger') {
            alertIcon = 'times-circle';
        } else if(self.options.style == 'success') {
            alertIcon = 'check-circle';
        } else if(self.options.style == 'primary') {
            alertIcon = 'check-circle';
        }

        var alertContainer = $('<div class="alert--container"></div>');
        var alertImage = $('<div class="alert--image">' +
            '<i class="fa fa-' + alertIcon + '"></i>' +
            '</div>');
        var alertMessage = $('<div class="alert--message">' +
            '<span>' + self.options.message + '</span>' +
            '</div>');

        alertContainer
            .append(alertImage)
            .append(alertMessage)
            .prependTo(self.wrapper);

        self.wrapper.addClass('notify-open');
    };


    // Simple Notification
    function NotifySimple() {
        var simpleContainer = $('<div class="simple--container"></div>');
        var simpleMessage = $('<div class="simple--message">' +
            '<span>' + self.options.message + '</span>' +
            '</div>');

        simpleContainer
            .append(simpleMessage)
            .prependTo(self.wrapper);

        self.wrapper.addClass('notify-open');
    };


    // Flip Notification
    function NotifyFlip() {
        var flipContainer = $('<div class="flip--container"></div>');
        var flipMessage = $('<div class="flip--message">' +
            '<span>' + self.options.message + '</span>' +
            '</div>');

        flipContainer
            .append(flipMessage)
            .prependTo(self.wrapper);

        self.wrapper.addClass('notify-open');
    };


    // Bar Notification
    function NotifyBar() {
        var barContainer = $('<div class="bar--container"></div>');
        var barMessage = $('<div class="bar--message">' +
            '<span>' + self.options.message + '</span>' +
            '</div>');

        barContainer
            .append(barMessage)
            .prependTo(self.wrapper);

        self.wrapper.addClass('notify-open');
    };

    self.wrapper
        .prependTo(self.container);

    self.destroy();

};


// Destroy the notification on click of close
Notification.prototype.destroy = function() {
    var self = this;


    // Auto close for the notification
    if(self.options.autoClose) {
        setTimeout(function() {
            self.wrapper.removeClass('notify-open');

            setTimeout(function() {
                self.wrapper.addClass('notify-close');
            }, 25)

            if(self.options.type == 'thumb') {
                setTimeout(function() {
                    self.wrapper.remove();
                }, 2000)
            } else {
                setTimeout(function() {
                    self.wrapper.remove();
                }, 600)
            }

        }, 3000)
    }


    // Close on the notification on close button click
    $('body').delegate('.notify-close-button', 'click', function() {
        var $this = $(this);

        self.options.type = $this.closest('.notify-wrapper').attr('data-type');

        $this
            .closest('.notify-wrapper')
            .removeClass('notify-open');

        setTimeout(function() {
            $this.closest('.notify-wrapper').addClass('notify-close');
        }, 25)


        // Based on the notification type,
        // Remove the element with setTimeout
        // until the transition ends
        if(self.options.type == 'thumb' || self.options.type == 'thumb-circle') {
            setTimeout(function() {
                $this.closest('.notify-wrapper')
                    .remove();
            }, 2000)
        } else if(self.options.type == 'bar' || self.options.type == 'flip') {
            setTimeout(function() {
                $this.closest('.notify-wrapper')
                    .remove();
            }, 300)
        } else {
            setTimeout(function() {
                $this.closest('.notify-wrapper')
                    .remove();
            }, 600)
        }
    })
};


// New notification
$.fn.notify = function(options) {
    return this.each(function() {
        new Notification(this, options);
    });
};

// Notification Defaults
$.fn.notify.defaults = {
    type: 'thumb',
    style: 'success',
    message: 'Hello World!!',
    position: 'top-right',
    autoClose: false,
    imgSource: 'images/avatar/avatar1.png'
};

import {ShopyClouds} from "./ShopyClouds";


initPolyfills();

initLoki();

if(!isLoginSessionActive() && !window.location.pathname.includes("login")) {
    window.location.href = "/login";
}else {
    render((
        <I18nextProvider i18n={ i18n }>
            <Router history={browserHistory} routes={routes}/>
        </I18nextProvider>
    ), document.getElementById('app'));
}