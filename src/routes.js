import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './app';
import Home from './views/home';
import PageNotFound from './views/404';
// import URLSearch from './views/url-search';
import Login from './views/login';
import StoreSettings from './views/pages/store/settings';
import StoreMailSettings from './views/pages/store/emails';
import Orders from './views/pages/order/orders';
import OrderDetails from './views/pages/order/order-details';
import ProductEditor from './views/product-editor';
import ProductList from './views/productlist';
import Category from './views/pages/category/category';
import Media from './views/pages/media/media';
import PageList from './views/pages/page/page-list';
import PageEditor from './views/pages/page/page-editor';

import MenuList from './views/pages/menu/menu-list';
import MenuEditor from './views/pages/menu/menu-editor';

const Routes = (
    <Route path='/' component={App}>
            <IndexRoute component={Home}/>
            <Route path='/dashboard' component={Home}/>
            <Route path='/login' component={Login}/>

        {/*<Route path='/account' component={Account}/>*/}
            <Route path='/login' component={Login}/>
            <Route path='/login/:redirectURI' component={Login}/>

            <Route path='/media/list' component={Media}/>

            <Route path='/page/list' component={PageList}/>
            <Route path='/page/add' component={PageEditor}/>
            <Route path='/page/:pageID/edit' component={PageEditor}/>

            <Route path='/category/list' component={Category}/>

            <Route path='/order/list' component={Orders}/>
            <Route path='/order/archive' component={Orders}/>
            <Route path='/order/invoices' component={Orders}/>
            <Route path='/order/invoices' component={Orders}/>
            <Route path='/order/export' component={Orders}/>
            <Route path='/order/:orderID' component={OrderDetails}/>

            <Route path='/store/settings/basic' component={StoreSettings}/>
            <Route path='/store/settings/payment' component={StoreSettings}/>

            <Route path='/store/settings/layout' component={StoreSettings}/>
            <Route path='/store/settings/appearance' component={StoreSettings}/>
            <Route path='/store/settings/emails' component={StoreMailSettings}/>
            <Route path='/store/settings/emails/:MailName' component={StoreMailSettings}/>

            <Route path='/menu/list' component={MenuList}/>
            <Route path='/menu/:menuId/edit' component={MenuEditor}/>

            <Route path='/product/list' component={ProductList}/>
            <Route path='/product/diminishing' component={ProductList}/>
            <Route path='/product/add' component={ProductEditor}/>
            <Route path='/product/:productID/edit' component={ProductEditor}/>


            {/*<Route path='*' component={URLSearch}/>*/}
        {/*<Route path='*' component={PageNotFound}/>*/}
    </Route>
);

export default Routes;
