/**
 * Created by Fecny on 2017.06.19..
 */
const path = require('path');
const fs = require('fs');
const crypto = require('crypto'),
    algorithm = 'aes256',
    password = '0cb029d1bb4bb163ed63d8cdaf171e285567e661e575da722d85f7dc2e6c75d1d88fbc3ec';
module.exports = function(app, request, backendGraphQLUri) {
    function decrypt(text) {
        let decipher = crypto.createDecipher(algorithm, password);
        let dec = decipher.update(text, 'hex', 'utf8');
        dec += decipher.final('utf8');
        return dec;
    }

    function General_setStateAndSendEmailForCurrentAssign(jsonData) {
        const DataObject = JSON.parse(jsonData);
        let UserAssignId = "";
        let Authorization = "";
        if (DataObject.hasOwnProperty("header")) {
            if (DataObject.header.hasOwnProperty("User-Assign-Id")) {
                UserAssignId = DataObject.header["User-Assign-Id"];
            }
            if (DataObject.header.hasOwnProperty("Authorization")) {
                Authorization = DataObject.header["Authorization"];
            }
        }
        const mutation = {
            queryName: 'General_setStateAndSendEmailForCurrentAssign',
            query: `mutation General_setStateAndSendEmailForCurrentAssign {General_setStateAndSendEmailForCurrentAssign}`,
            cacheEnabled: false,
            mutation: true
        };
        const query = {
            url: backendGraphQLUri,
            json: {
                query: mutation.query
            },
            headers: {
                'User-Agent': 'nodejs',
                'User-Assign-Id': UserAssignId,
                'Content-Type': 'application/json',
                'Accept-Language': 'hu;q=0.9',
                'Authorization': Authorization,
                'Validation-Error-Format': 'flat'
            }
        };

        request.post(query, function (error, response, body) {
            console.log('error', error);
            console.log('body', body);
        });
    }

    app.get('/filedownload/:fileHash', function (req, res, next) {
        const jsonString = decrypt(req.params.fileHash);
        const downloadObject = JSON.parse(jsonString);
        const downloadFile = downloadObject.fileName;
        const justFileName = path.basename(downloadFile);
        const filestream = fs.createReadStream(downloadFile);

        // console.log(filestream);
        // set file download header
        res.setHeader('Content-disposition', 'attachment; filename=' + justFileName);

        filestream.pipe(res);
        filestream.on('end', function () {
            if (downloadObject.hasOwnProperty('afterSuccess')) {
                // if has a callback mutation
                if (downloadObject.afterSuccess.hasOwnProperty("callMutation")) {
                    if (eval('typeof ' + downloadObject.afterSuccess.callMutation) === "function") {
                        console.log("Call mutation", downloadObject.afterSuccess.callMutation + "('" + jsonString + "');");
                        eval(downloadObject.afterSuccess.callMutation + "('" + jsonString + "');");
                    } else {
                        console.log("Failed to call mutation ", eval('typeof ' + downloadObject.afterSuccess.callMutation))
                    }
                }
            }
        });
    });
};