var jsonfile = require('jsonfile');
var request = require('request');
var backend = require('../../config').backend;

const backendGraphQLUri = backend + '/api/graphql';

const getTextHeaderAndHybridMemoListQuery = function (textIds = [], memoIds = []) {
    console.log('TEXTHEADERS3',textIds);
    console.log('MEMOS3',memoIds);
    return `query TextHeader_getListByTextIds {
            TextHeader_getListByTextIds(textIds: ${JSON.stringify(JSON.stringify(textIds))}) {
                version
                textKey
                hintText
                category
                coupledText
                textItems {
                    text
                    language
                    recnum
                    region
                    consoleType
                }
                recnum
            }
            HybridMemo_getListByMemoIds(memoIds: ${JSON.stringify(JSON.stringify(memoIds))}) {
                recnum
                translation
                hybridMemoItems {
                  textblock
                  language
                  recnum
                }
            }
        }`
};

const loadLanguageTexts = function (name, textIds = [], memoIds = []) {
    console.log('TEXTHEADERS2',textIds);
    console.log('MEMOS2',memoIds);
    console.log('URL', backendGraphQLUri);
    var query = {
        url: 'http:'+backendGraphQLUri,
        json: {
            query: getTextHeaderAndHybridMemoListQuery(textIds, memoIds)
        },
        headers: {
            'User-Agent': 'nodejs',
            'User-Assign-Id': '',
            'Content-Type': 'application/json',
            'Accept-Language': 'hu;q=0.9',
            'Authorization': '',
            'Validation-Error-Format': 'flat'
        }
    };
    console.log('QUERY',JSON.stringify(query.json.query));
    request.post(query, function (error, response, body) {
        console.log('responseJSON',JSON.stringify(response));
        var resultJSON = {
            textHeaders : response && response.body && response.body.data && response.body.data.HybridMemo_getListByMemoIds ? response.body.data.TextHeader_getListByTextIds : [],
            hybridMemos : response && response.body && response.body.data && response.body.data.HybridMemo_getListByMemoIds ? response.body.data.HybridMemo_getListByMemoIds : []
        };

        saveLocalizationFile(name, resultJSON);
    });
};

const convertJSONFileToArrays = function (name, fileName) {
    var result = {
        textHeaders: [],
        hybridMemos: []
    };
    var file = './src/i18n/' + fileName;
    jsonfile.readFile(file, function(err, obj) {
        console.dir(obj);
        if (!err){
            console.log('received data: ' + JSON.stringify(obj));
            for (var propertyName in obj) {
                var data = obj[propertyName];
                console.log('PROPERTY', propertyName);
                console.log('PROPERTY VALUE', data);
                if (data.indexOf('$') !== -1) {
                    result.hybridMemos.push(data.replace('$',''));
                } else {
                    result.textHeaders.push(data);
                }
            }
            console.log('TEXTHEADERS',result.textHeaders);
            console.log('MEMOS',result.hybridMemos);
            loadLanguageTexts(name,  result.textHeaders, result.hybridMemos);
        }else{
            console.log(err);
        }
    });
};

const saveLocalizationFile = function (fileName, json) {
    console.log('response',JSON.stringify(json));
    var file = './i18/' + fileName + '.json';

    jsonfile.writeFile(file, json, function (err) {
        if (err) {
            console.error(err)
        }
    });
};

const loadLocalizationFiles = function () {
    var fs = require('fs');
    var path = require('path');
    var dirPath = './src/i18n';  //directory path
    var fileType = '.json'; //file extension
    var files = [];
    fs.readdir(dirPath, function(err,list){
        if(err) throw err;
        for(var i=0; i<list.length; i++)
        {
            if(path.extname(list[i])===fileType)
            {
                console.log('FILE',path.parse((list[i])).name); //print the file
                var file = {
                    name : path.parse((list[i])).name,
                    filename: list[i]
                };
                convertJSONFileToArrays(file.name, file.filename);
                files.push(file);
            }
        }
    });

    console.log('FILES',files);
};

module.exports = {
    convertJSONFileToArrays,
    saveLocalizationFile,
    loadLocalizationFiles
};