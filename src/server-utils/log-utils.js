const fs = require('fs');

module.exports = function(app, urlencodedParser) {
    app.post('/log', urlencodedParser, function (req, res) {
        const logFileName = './error.log';
        console.log('LOG',JSON.stringify(req.body));
        fs.exists(logFileName, function (exists) {
            const body = req.body;
            const logLine = `${JSON.stringify(body.time)} :: ${JSON.stringify(body.logData)}\n`;
            if(!exists){
                fs.writeFile(logFileName, logLine);
            } else {
                fs.appendFile(logFileName, logLine, function (err) {
                    if (err) {
                        console.log('LOG ERROR', err);
                    }
                });
            }
            res.end();
        });

    });
};