import makeRequest from '../request-queue-handler';
import {
  getVariableGroupsQuery,
  getVariableGroupByRecnumQuery,
  getVariableGroupsMutation } from '../queries/admin/data-value-queries';

const getVariableGroups = function(callback = function(response){}){

};

const getVariableGroupByRecnum = function(recnum, onSuccess = function(response){},onError = function(response){}){
  makeRequest(getVariableGroupByRecnumQuery(recnum), function(response) {
    onSuccess(response.data.VariableGroup);
  }, function(error) {
    window.log('variable-group-error', error);
    onError(error);
  });
};

const saveVariableGroups = function(variableGroupMutation, onSuccess = function(response){},onError = function(response){}){
  makeRequest(variableGroupMutation, function(response) {
    onSuccess(response);
    window.log('SAVE VARIABLE GROUP',response);
  }, function(error) {
    window.log('SAVE variable-group-error', error);
    onError(error);
  });
};

module.exports = {
  getVariableGroups,
  getVariableGroupByRecnum,
  saveVariableGroups
};
