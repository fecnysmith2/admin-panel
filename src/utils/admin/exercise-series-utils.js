import makeRequest from "../request-queue-handler";
import {
    getExerciseSeriesMutation,
    getExerciseSeriesByRecnumQuery,
    getExerciseSeriesByTrainingElementQuery} from "../queries/admin/exercise-queries";

const saveExerciseSeries = function (exerciseSeries, onSuccess = function (response) {
                                     }, onError = function (error) {
                                     }) {
    makeRequest(getExerciseSeriesMutation(exerciseSeries), function (response) {
        window.log('EXERCISESERIES SAVE', response);
        onSuccess(response);
    }, function (error) {
        window.error('EXERCISESERIES SAVE', error);
        onError(error);
    });
};

const loadExerciseSeries = function (rootExerciseId, onSuccess = function (response) {
                                     }, onError = function (error) {
                                     }) {
    makeRequest(getExerciseSeriesByRecnumQuery(rootExerciseId), function (response) {
        window.log('EXERCISESERIES GET', response);
        onSuccess(response);
    }, function (error) {
        window.error('EXERCISESERIES GET', error);
        onError(error);
    });
};

const loadExerciseSeriesByTrainingElement = function (trainingElemtRecnum, onSuccess = function (response) {
                                                      }, onError = function (error) {
                                                      }) {
    makeRequest(getExerciseSeriesByTrainingElementQuery(trainingElemtRecnum), function (response) {
        window.log('EXERCISESERIES GET', response);
        onSuccess(response);
    }, function (error) {
        window.error('EXERCISESERIES GET', error);
        onError(error);
    });
};

const sortExercises = function sortExercisesRecursive(items) {
    let result = items;
    result.sort(function(a, b) {
        let ordernumFirst = 0;
        let ordernumSecond = 0;
        if (a.trainingExerciseItem != null) {
            ordernumFirst = a.trainingExerciseItem.ordernum;
        } else {
            ordernumFirst = a.exerciseItem.ordernum;
        }
        if (b.trainingExerciseItem != null) {
            ordernumSecond = b.trainingExerciseItem.ordernum;
        } else {
            ordernumSecond = b.exerciseItem.ordernum;
        }
        return ordernumFirst - ordernumSecond;
    });

    result = result.map(function (item) {
        if (item.children.length > 0){
            item.children = sortExercisesRecursive(item.children);
        }
        return item;
    });
    return result;
};

const processchildren = function processRecursive(childArray) {
    return childArray.map(function (item) {
        delete item.parendId;
        delete item.id;
        delete item.text;
        delete item.typeName;
        delete item.type;
        item.children = processchildren(item.children);
        return item;
    });
};

const getRootExerciseObject = function (isPersonalized = false, trainingElement = -1, exerciseItem = null) {
    return {
        id: 1,
        parentId: 0,
        exercise: {
            recnum: -1,
            exerciseType: "R"
        },
        trainingExerciseItem: isPersonalized ? getTrainingExerciseItemObject(trainingElement, exerciseItem, -1, 1, null, 0, false) : null,
        exerciseItem: isPersonalized ? null : getExerciseItemObject(-1, 1, null, 0, false),
        removeFlag: false,
        children: []
    };
};

const getPyramidExerciseObject = function (exercise, id = [-1, -1, -1], ordernum = 0, parentId = '', isPersonalized = false, trainingElement = -1, exerciseItem = null) {
    return {
        id: id[0],
        parentId: parentId,
        exercise: {
            recnum: -1,
            exerciseType: "P"
        },
        trainingExerciseItem: isPersonalized ? getTrainingExerciseItemObject(trainingElement, exerciseItem, -1, 1, null, 0, false) : null,
        exerciseItem: isPersonalized ? null : getExerciseItemObject(-1, 1, null, 0, false),
        removeFlag: false,
        children: [
            {
                id: id[1],
                parentId: id[0],
                exercise: {
                    recnum: -1,
                    exerciseType: "C"
                },
                trainingExerciseItem: isPersonalized ? getTrainingExerciseItemObject(trainingElement, exerciseItem, -1, 1, null, 0, false) : null,
                exerciseItem: isPersonalized ? null : getExerciseItemObject(-1, 1, null, 0, false),
                children: [
                    {
                        id: id[2],
                        parentId: id[1],
                        removeFlag: false,
                        exercise: exercise,
                        trainingExerciseItem: isPersonalized ? getTrainingExerciseItemObject(trainingElement, exerciseItem, -1, 10, null, 15, false) : null,
                        exerciseItem: isPersonalized ? null : getExerciseItemObject(-1, 10, null, 15, false),
                    }
                ]
            }
        ]
    };
};

const getLinearArrayExerciseObject = function (exercise, id = ['', '', ''], ordernum = 0, parentId = '', isPersonalized = false, trainingElement = -1, exerciseItem = null) {
    return {
        id: id[0],
        parentId: parentId,
        exercise: {
            recnum: -1,
            exerciseType: "L"
        },
        trainingExerciseItem: isPersonalized ? getTrainingExerciseItemObject(trainingElement, exerciseItem, -1, 1, null, 0, false) : null,
        exerciseItem: isPersonalized ? null : getExerciseItemObject(-1, 1, null, 0, false),
        removeFlag: false,
        children: [
            {
                id: id[1],
                parentId: id[0],
                exercise: {
                    recnum: -1,
                    exerciseType: "N"
                },
                trainingExerciseItem: isPersonalized ? getTrainingExerciseItemObject(trainingElement, exerciseItem, -1, 1, null, 0, false) : null,
                exerciseItem: isPersonalized ? null : getExerciseItemObject(-1, 1, null, 0, false),
                children: [
                    {
                        id: id[2],
                        parentId: id[1],
                        removeFlag: false,
                        exercise: exercise,
                        trainingExerciseItem: isPersonalized ? getTrainingExerciseItemObject(trainingElement, exerciseItem, -1, 10, null, 15, false) : null,
                        exerciseItem: isPersonalized ? null : getExerciseItemObject(-1, 10, null, 15, false),
                    }
                ]
            }
        ]
    };
};

const getCircleExerciseObject = function (id = ['', ''], ordernum = 0, parentId = '', isPersonalized = false, trainingElement = -1, exerciseItem = null) {
    return {
        id: id[0],
        parentId: parentId,
        exercise: {
            recnum: -1,
            exerciseType: "Q"
        },
        trainingExerciseItem: isPersonalized ? getTrainingExerciseItemObject(trainingElement, exerciseItem) : null,
        exerciseItem: isPersonalized ? null : getExerciseItemObject(),
        removeFlag: false,
        children: [
            {
                id: id[1],
                parentId: id[0],
                exercise: {
                    recnum: -1,
                    exerciseType: "C"
                },
                trainingExerciseItem: isPersonalized ? getTrainingExerciseItemObject(trainingElement, exerciseItem, -1, 1, null, 0, false) : null,
                exerciseItem: isPersonalized ? null : getExerciseItemObject(-1, 1, null, 0, false),
                children: []
            }
        ]
    };
};

const getCombinatedExerciseObject = function (id = ['', ''], ordernum = 0, parentId = '', isPersonalized = false, trainingElement = -1, exerciseItem = null) {
    return {
        id: id[0],
        parentId: parentId,
        exercise: {
            recnum: -1,
            exerciseType: "S"
        },
        trainingExerciseItem: isPersonalized ? getTrainingExerciseItemObject(trainingElement, exerciseItem) : null,
        exerciseItem: isPersonalized ? null : getExerciseItemObject(),
        removeFlag: false,
        children: [
            {
                id: id[1],
                parentId: id[0],
                exercise: {
                    recnum: -1,
                    exerciseType: "C"
                },
                trainingExerciseItem: isPersonalized ? getTrainingExerciseItemObject(trainingElement, exerciseItem, -1, 1, null, 0, false) : null,
                exerciseItem: isPersonalized ? null : getExerciseItemObject(-1, 1, null, 0, false),
                children: []
            }
        ]
    };
};

const getExerciseItemObject = function (recnum = -1, repeat = 1, ordernum = null, restingTime = 15, repeatTime = false) {
    return {
        recnum: recnum,
        repeat: repeat,
        ordernum: ordernum,
        restingTime: restingTime,
        repeatTime: repeatTime
    }
};

const getTrainingExerciseItemObject = function (trainingElement, exerciseItem = null, recnum = -1, repeat = 1, ordernum = null, restingTime = 15, repeatTime = false, type = 'I') {
    let trainingExerciseItem = getExerciseItemObject(recnum, repeat, ordernum, restingTime, repeatTime);
    trainingExerciseItem.opType = type;
    trainingExerciseItem.trainingElement = trainingElement;
    trainingExerciseItem.exerciseItem = exerciseItem;
    return trainingExerciseItem;
};

module.exports = {
    saveExerciseSeries,
    loadExerciseSeries,
    loadExerciseSeriesByTrainingElement,
    sortExercises,
    getPyramidExerciseObject,
    getLinearArrayExerciseObject,
    getCombinatedExerciseObject,
    getCircleExerciseObject,
    getRootExerciseObject,
    getTrainingExerciseItemObject
};
