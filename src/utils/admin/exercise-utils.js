import makeRequest from '../request-queue-handler';
import {saveObjectToLocalStore, getObjectFromLocalStore} from '../db';
import {
    getExerciseQuery,
    getExercisePageQuery,
    getDeleteExerciseByObjectMutation,
    getDeleteElementaryExerciseByObjectMutation,
    getExercisePhaseByExerciseRecnumQuery, getExerciseMutation, getExercisePhasesSaveMutation
} from '../queries/admin/exercise-queries';

const getExercises = function (onSuccess = function (response) {}, onError = function (error) {}, types = '') {
    const that = this;
    makeRequest(getExerciseQuery(types), function (response) {
        window.log('EXERCISES', response);
        onSuccess(response);
    }, function (error) {
        window.error('EXERCISES', error);
        onError(error);
    });
};

const getExercisePage = function (onSuccess = function (response) {}, onError = function (error) {}, type = '', page = 1, size = 5) {
    const that = this;
    makeRequest(getExercisePageQuery(type, page, size), function (response) {
        window.log('EXERCISES', response);
        onSuccess(response);
    }, function (error) {
        window.error('EXERCISES', error);
        onError(error);
    });
};

const deleteExercise = function (exercise, onSuccess = function (response) {}, onError = function (error) {}) {
    delete exercise.phases;
    makeRequest(getDeleteExerciseByObjectMutation(exercise), function (response) {
        window.log('EXERCISE DELETE', response);
        onSuccess(response);
    }, function (error) {
        window.error('EXERCISES DELETE', error);
        onError(error);
    });
};

const deleteElementaryExercise = function (exercise, onSuccess = function (response) {}, onError = function (error) {}) {
    delete exercise.phases;
    makeRequest(getDeleteElementaryExerciseByObjectMutation(exercise), function (response) {
        window.log('EXERCISE DELETE', response);
        onSuccess(response);
    }, function (error) {
        window.error('EXERCISES DELETE', error);
        onError(error);
    });
};

const loadExercisesPhases = function (exerciseRecnum, onSuccess = function (response) {}, onError = function (error) {}) {
    makeRequest(getExercisePhaseByExerciseRecnumQuery(exerciseRecnum), function (response) {
        window.log('EXERCISE PHASE', response);
        onSuccess(response);
    }, function (error) {
        window.error('EXERCISE PHASE', error);
        onError(error);
    });
};

const saveExercise = function (exercise, onSuccess = function (response) {}, onError = function (error) {}) {
    makeRequest(getExerciseMutation(exercise), function (response) {
        window.log('NEW EXERCISE response', response);
        onSuccess(response);
    }, function (error) {
        window.error('NEW EXERCISE response', error);
        onError(error)
    });
};

const saveExercisePhases = function (exercisePhases, onSuccess = function (response) {}, onError = function (error) {}) {
    makeRequest(getExercisePhasesSaveMutation(exercisePhases), function (response) {
        window.log('NEW EXERCISE  PHASE response', response);
        onSuccess(response);
    }, function (error) {
        window.error('NEW EXERCISE  PHASE response', error);
        onError(error);
    });
};

const saveLocalModifications = function (exercise) {
    saveObjectToLocalStore(`exerciseMod${exercise.recnum}`,exercise);
};

const loadLocalModifications = function (recnum) {
    return getObjectFromLocalStore(`exerciseMod${recnum}`);
};


module.exports = {
    deleteExercise,
    getExercises,
    loadExercisesPhases,
    getExercisePage,
    deleteElementaryExercise,
    saveExercise,
    saveExercisePhases,
    saveLocalModifications,
    loadLocalModifications
};
