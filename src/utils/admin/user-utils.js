import makeRequest from '../request-queue-handler';
import { getUserAnswerListQuery, getUserAnswerForATestQuery } from '../queries/admin/user-queries';

const getUserAnswersByUserRecnum = function(recnum, onSuccess = function(response){},onError = function(response){}){
  makeRequest(getUserAnswerListQuery(recnum), function(response) {
    onSuccess(response.data.UserAnswer_getForDifferentUser);
  }, function(error) {
    window.log('user-list-error', error);
    onError(error);
  });
};

const getUserAnswerForATestByRecnum = function(recnum, onSuccess = function(response){},onError = function(response){}){
  makeRequest(getUserAnswerForATestQuery(recnum), function(response) {
    onSuccess(response.data.UserAnswer);
  }, function(error) {
    window.log('user-test-answer-error', error);
    onError(error);
  });
};

module.exports = {
  getUserAnswersByUserRecnum,
  getUserAnswerForATestByRecnum
};
