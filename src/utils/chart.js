const context = {
    getColor: function (i) {
        switch (i.label) {
            case'Január':
                return "#017260";
                break;
            case'Február':
                return "#0a3e89";
                break;
            case'Március':
                return "#0071bd";
                break;
            case'Április':
                return "#71513c";
                break;
            case'Május':
                return "#231e20";
                break;
            case'Június':
                return "#a6aaad";
                break;
            case'Július':
                return "#d71440";
                break;
            case'Augusztus':
                return "#fede00";
                break;
            case'Szeptember':
                return "#f89791";
                break;
            case'Október':
                return "#f7933d";
                break;
            case'November':
                return "#5c2d8f";
                break;
            case'December':
                return "#11965f";
                break;
        }
    }
};
const getMonthName = function(monthNum){
    switch(monthNum){
        case "1": return "Január"; break;
        case "2": return "Február"; break;
        case "3": return "Március"; break;
        case "4": return "Április"; break;
        case "5": return "Május"; break;
        case "6": return "Június"; break;
        case "7": return "Július"; break;
        case "8": return "Augusztus"; break;
        case "9": return "Szeptember"; break;
        case "10": return "Október"; break;
        case "11": return "November"; break;
        case "12": return "December"; break;
    }
};

module.exports = {
    context,
    getMonthName
};