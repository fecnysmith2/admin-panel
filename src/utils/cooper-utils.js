import React from 'react'

const getIconForMoveType = function(type) {
    switch (type) {
        case 1:
            return 'ikon-sport futas';
        case 2:
            return 'ikon-sport kerekpar';
        case 3:
            return 'ikon-sport szobakerekpar';
        case 6:
            return 'ikon-sport futopad';
        case 7:
            return 'ikon-sport egyeb';
        case 8:
            return 'ikon-sport nordic-walking';
        case 9:
            return 'ikon-sport turazas';
        case 10:
            return 'ikon-sport roller';
        case 11:
            return 'ikon-sport gorkorcsolya';
        default:
            return 'ikon-sport futas';
    }
};

const getHeartRate = function (fitPointer) {
    const heartRate = [<span className='ikon-rate'>.</span>];
    for (let index = 1; index < fitPointer; index++) {
        heartRate.push(<span className='ikon-rate'>.</span>);
    }
    return heartRate;
}

module.exports = {
    getIconForMoveType,
    getHeartRate
}