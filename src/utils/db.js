import Loki from 'lokijs';
import CryptoJS from 'crypto-js';
import {environment} from '../../config';

const cacheTimeOut = 60 * 60 * 1000; // millisecond-ben megadva

const frontendVersion = '2.2.9';

const localStorageFrontendVersionKey = 'frontendVersion';

const localStorageQueriesKey = 'queries';

const localStorageViewsKey = 'views';

const localStorageUserKey = 'user';

const localStorageUserObject = 'u-object';

const localStorageOauthTokenKey = 'encodedOauthTokenObject';

const localStorageUserLoginTicket = 'login_ticket';

const dbName = 'db';

const devEnvironment = 'dev';

const testEnvironment = 'test';

const prodEnvironment = 'prod';

const localEnvironment = 'local';

let db = new Loki(dbName);

const secretKey = 'klKOkKIedyXfmvk21lsbvIMSd';

let views = db.addCollection(localStorageViewsKey);

let queries = db.addCollection(localStorageQueriesKey);

let user = db.addCollection(localStorageUserKey);

let isLocalStorageSupported = false;

const initLoki = function () {
    /*environment függő beállítások*/
    if (environment !== prodEnvironment) {
        window.db = db;
    } else {

    }

    // Ellenőrizzük, hogy támogat-e a böngésző localStorage-et
    if (typeof(Storage) !== 'undefined') {
        isLoginSessionActive();
        if (localStorage.getItem(localStorageFrontendVersionKey) !== frontendVersion) {
            localStorage.removeItem(localStorageQueriesKey);
            localStorage.removeItem(localStorageUserLoginTicket);
            localStorage.removeItem('posts');
            localStorage.removeItem('email-verified');
            localStorage.setItem(localStorageFrontendVersionKey, frontendVersion);
        }

        isLocalStorageSupported = true;

        db.addCollection(localStorageQueriesKey);
        var queries = eval(localStorage.getItem(localStorageQueriesKey));

        if (queries != null) {
            queries.forEach(function (item, index) {
                addQueryToCache(item.name, item.result, item.cacheTimeOut);
            });
        }
    } else {
        isLocalStorageSupported = false;
    }
};

const addQueryToCache = function (queryName, queryResult, cacheTime = '') {
    // A cache lejárati ideje a mostani idő plusz a beállított érték
    let timeOut = new Date();
    if (cacheTime != null && cacheTime != '') {
        timeOut.setTime(cacheTime);
    } else {
        var newTime = timeOut.getTime() + cacheTimeOut;
        timeOut.setTime(newTime);
    }
    const newQuery = {name: queryName, result: queryResult, cacheTimeOut: timeOut.getTime()};
    queries.insert(newQuery);
    if (isLocalStorageSupported) {
        localStorage.setItem(localStorageQueriesKey, JSON.stringify(queries.data));
    }
};

const findQueryInCache = function (queryName) {
    let query = queries.findOne({'name': queryName});

    if (query != null) {
        if (new Date(query.cacheTimeOut).getTime() > Date.now()) {
            return query.result;
        } else {
            queries.remove(query);
            return '';
        }
    } else {
        return '';
    }
};

const addObjectToCache = function (objectName, object, cacheTimeOut = '') {
    // A cache lejárati ideje a mostani idő plusz a beállított érték
    const timeOut = new Date();
    if (cacheTimeOut !== null && cacheTimeOut !== '') {
        timeOut.setTime(cacheTimeOut);
    } else {
        const newTime = timeOut.getTime() + cacheTimeOut;
        timeOut.setTime(newTime);
    }
    const newObject = {name: objectName, result: object, cacheTimeOut: timeOut.getTime()};
    if (isLocalStorageSupported) {
        localStorage.setItem(objectName, JSON.stringify(newObject));
    }
};

const findObjectInCache = function (objectName) {
    var object;
    if (isLocalStorageSupported) {
        object = localStorage.getItem(objectName);
    }
    if (object) {
        if (new Date(object.cacheTimeOut).getTime() > Date.now()) {
            return object.result;
        } else {
            localStorage.removeItem(objectName);
            return '';
        }
    } else {
        return '';
    }
};

const saveOauthTokenObject = function (oauthTokenObject) {
    var oauthObject = Object.assign({}, oauthTokenObject);
    var validityDate = Date.now() + oauthTokenObject.expires_in;
    oauthObject.expires_in = validityDate;
    encodeAndSaveObjectToLocalStore(localStorageOauthTokenKey, oauthObject)
};

const getOauthTokenObject = function () {
    return getObjectFromLocalStore("oAuth_token");
};

const encodeAndSaveObjectToLocalStore = function (name, object, secret = '') {
    if (secret == '') {
        secret = secretKey;
    }
    var cipherObject = CryptoJS.AES.encrypt(JSON.stringify(object), secret);
    localStorage.setItem(name, cipherObject);
};

const getEncodedObjectFromLocalStorage = function (name, secret = '') {
    var data = localStorage.getItem(name);
    if (secret === '') {
        secret = secretKey;
    }
    if (data !== undefined && data !== null && data !== '') {
        const bytes = CryptoJS.AES.decrypt(data.toString(), secret);
        const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        return decryptedData
    }
    return '';
};

const deleteOauthTokenObject = function () {
    localStorage.removeItem(localStorageOauthTokenKey);
    localStorage.removeItem(localStorageUserLoginTicket);
    localStorage.removeItem(localStorageUserObject)
};

const isLoginSessionActive = function () {
    if(!getOauthTokenObject()){ return false; }
    else if(getOauthTokenObject() === null){ return false; }
    else if(getOauthTokenObject() === ""){ return false; }
    else return getOauthTokenObject().length > 0;
};

const saveObjectToLocalStore = function (name, object) {
    if (isLocalStorageSupported) {
        localStorage.setItem(name, JSON.stringify(object));
    }
};

const saveOauthToken = function (name, oAuth_token) {
    if (isLocalStorageSupported) {
        localStorage.setItem(name, oAuth_token);
    }
};

const getObjectFromLocalStore = function (name) {
    if (isLocalStorageSupported) {
        return localStorage.getItem(name);
    }
    return '';
};

const removeObjectFromLocalStore = function (name) {
    if (isLocalStorageSupported) {
        localStorage.removeItem(name);
    }
    return false;
};

const addCollectionToLoki = function (collectionName) {
    return db.addCollection(collectionName);
};

const saveHealthTestAnswerToLoki = function (collectionName, object) {
    var collection = getCollectionFromLoki(collectionName);
    var tempObject;
    for (var i = 0; i < collection.data.length; i++) {
        tempObject = collection.data[i];
        if (tempObject != undefined &&
            tempObject.question.recnum == object.question.recnum &&
            tempObject.answer.recnum == object.answer.recnum) {
            for (var propertyName in object) {
                tempObject[propertyName] = object[propertyName];
            }
            collection.update(tempObject);
            return;
        }
    }
    collection.insert(object);
};

const getCollectionFromLoki = function (collectionName) {
    var collection = db.getCollection(collectionName);
    return collection != null ? collection : addCollectionToLoki(collectionName);
};

const getObjectFromLoki = function (collectionName, filterObject) {
    var collection = getCollectionFromLoki(collectionName);
    return collection.findOne(filterObject);
};

/*
 Mivel a loki sok más propertit is hozzáfűz az objektekhez, ezért
 szükséges egy egy prototypeObject-et átadnunk, hogy rendesen szerializálhatóak legyenek az
 objektek. Pl:
 var prototypeObject = {
 value: '',
 type: ''
 };
 */
const getAllConvertedObjectFromCollectionLoki = function (collectionName, prototypeObject) {
    const collectionItems = getCollectionFromLoki(collectionName).data;
    let resultList = [];
    if (collectionItems.length > 0) {
        for (let index = 0; index < collectionItems.length; index++) {
            const collectionItem = collectionItems[index];
            let tempObject = {};
            for (var propertyName in prototypeObject) {
                tempObject[propertyName] = collectionItem[propertyName];
            }
            resultList.push(tempObject);
        }
    }
    window.log('CONVERT', resultList);
    return resultList;
};

const getAllObjectFromCollectionLoki = function (collectionName) {
    return getCollectionFromLoki(collectionName).data;
};

module.exports = {
    addQueryToCache,
    findQueryInCache,
    initLoki,
    saveOauthTokenObject,
    getOauthTokenObject,
    isLoginSessionActive,
    addCollectionToLoki,
    saveHealthTestAnswerToLoki,
    getCollectionFromLoki,
    getObjectFromLoki,
    getAllObjectFromCollectionLoki,
    getAllConvertedObjectFromCollectionLoki,
    deleteOauthTokenObject,
    saveObjectToLocalStore,
    getObjectFromLocalStore,
    removeObjectFromLocalStore,
    encodeAndSaveObjectToLocalStore,
    getEncodedObjectFromLocalStorage,
    addObjectToCache,
    findObjectInCache,
    saveOauthToken
};
