import {
    saveHealthTestAnswerToLoki,
    getAllObjectFromCollectionLoki,
    getAllConvertedObjectFromCollectionLoki
} from '../utils/db';
import {getUserObjectProperty} from '../utils/user-util';

const getUserAnswerObjectWithQuestionObject = function (questionRecnum, answerRecnum, addValue = null) {
    window.log('WITHQUESTION OBJECT', questionRecnum + ' : ' + answerRecnum + ' : ' + addValue);
    return {
        addValue: addValue != null ? parseInt(addValue) : null,
        question: {
            recnum: parseInt(questionRecnum)
        },
        answer: {
            recnum: parseInt(answerRecnum)
        }
    }
};

const getHealtTestMutation = function (healthTestCollectionName, questionGroupRecnum = 2) {
    const answer = {
        questionGroup: {
            recnum: questionGroupRecnum
        },
        userAnswerItems: getAllConvertedObjectFromCollectionLoki(healthTestCollectionName, getUserAnswerObjectWithQuestionObject('', '', ''))
    };

    return {
        queryName: 'submitAnswer',
        query: `mutation submitAnswer($answer: String) {
            UserAnswer_submitForCurrentUser(answer: $answer) {
                evaluationDate
                    recnum
                    evaluationSend
                    version
                    fillDate
                }
        }`,
        variables: JSON.stringify({
            answer: JSON.stringify(answer)
        }),
        mutation: true,
        cacheEnabled: false
    }
};

const getUserUpdateMutation = function (weight = '', height = '') {
    let variable = {
        recnum: parseInt(getUserObjectProperty('recnum')),
    };

    if (weight !== '') {
        variable.bodyWeight = {
            unit: 'KGM',
            value: parseInt(weight)
        };
    }

    if (height !== '') {
        variable.height = {
            unit: 'CMT',
            value: parseInt(height)
        };
    }

    return {
        queryName: 'submitUserUpdate',
        query: `mutation submitUserUpdate($json: String!) {
          User_update(json: $json) {
            recnum
          }
        }`,
        variables: JSON.stringify({
            json: JSON.stringify(variable)
        }),
        mutation: true,
        cacheEnabled: false
    }
};

const saveAnswers = function (healthTestCollectionName, form = null) {
    let questionsWithAnswers = [];
    if (form != null) {
        window.log('form elements: ', form.elements);
        for (let index = 0; index < form.elements.length; index++) {
            const element = form.elements[index];
            let answerObject = {};
            const questionRecnum = element.dataset['questionrecnum'];
            let numberInput = null;
            if (element.checked) {

                for (let secondIndex = 0; secondIndex < form.elements.length; secondIndex++) {
                    if (form.elements[secondIndex].type == 'number' && form.elements[secondIndex].dataset['recnum'] == element.dataset['recnum']) {
                        numberInput = form.elements[secondIndex].value;
                    }
                }

                // numberInput = document.getElementsByName('addValue-'+ questionRecnum);
                window.log('ADD VALUE', numberInput);
                window.log('DATASET', element.dataset);
                answerObject = getUserAnswerObjectWithQuestionObject(questionRecnum, element.dataset['recnum'], numberInput);
                saveHealthTestAnswerToLoki(healthTestCollectionName, answerObject);
                questionsWithAnswers.push(questionRecnum);
            } else if (element.type != 'number' && element.type != 'checkbox' && element.type != 'radio') {
                const answer = element.options[element.options.selectedIndex];

                for (let secondIndex = 0; secondIndex < form.elements.length; secondIndex++) {
                    if (form.elements[secondIndex].type == 'number' && form.elements[secondIndex].dataset['questionrecnum'] == questionRecnum) {
                        numberInput = form.elements[secondIndex].value;
                    }
                }
                let addValue = null;
                numberInput = document.getElementsByName('addValue-' + questionRecnum);
                if (numberInput.length > 0) {
                    addValue = numberInput[0].value;
                }
                window.log('ADD VALUE', addValue);
                window.log('DATASET', element.dataset);
                answerObject = getUserAnswerObjectWithQuestionObject(questionRecnum, answer.dataset['recnum'], addValue);
                window.log('ANSWER OBJECT', answerObject);
                saveHealthTestAnswerToLoki(healthTestCollectionName, answerObject);
                questionsWithAnswers.push(questionRecnum);
            }
            answerObject = {};
        }
        form.reset();
    }
};

const loadAnswers = function (healthTestCollectionName, form = null) {
    const savedAnswers = getAllObjectFromCollectionLoki(healthTestCollectionName);
    if (form != null) {
        form.reset();
        for (let index = 0; index < form.elements.length; index++) {
            let element = form.elements[index];
            for (let secondIndex = 0; secondIndex < savedAnswers.length; secondIndex++) {
                if (element.type != 'number' && element.type != 'checkbox' && element.type != 'radio') {
                    const answers = element.options;
                    for (let thirdIndex = 0; thirdIndex < answers.length; thirdIndex++) {
                        if (answers[thirdIndex].dataset['recnum'] == savedAnswers[secondIndex].answer.recnum) {
                            element.value = answers[thirdIndex].value;
                        }
                    }
                    window.log('ANSWER OBJECT LOAD', savedAnswers[secondIndex].addValue);
                    const addValueInput = document.getElementsByName('addValue-'+ savedAnswers[secondIndex].question.recnum);
                    if (savedAnswers[secondIndex].addValue != null && addValueInput.length > 0) {
                        addValueInput[0].value = savedAnswers[secondIndex].addValue;
                    }
                } else if (element.type == 'checkbox' || element.type == 'radio') {

                    if (element.dataset['recnum'] == savedAnswers[secondIndex].answer.recnum) {
                        element.checked = true;
                        for (let thirdIndex = 0; thirdIndex < form.elements.length; thirdIndex++) {
                            let input = form.elements[thirdIndex];
                            if (input.type == 'number' && input.dataset['recnum'] == savedAnswers[secondIndex].answer.recnum) {
                                input.value = savedAnswers[secondIndex].addValue;
                            }
                        }
                    }
                }
            }
        }
    }
};

module.exports = {
    saveAnswers,
    loadAnswers,
    getHealtTestMutation,
    getUserUpdateMutation
};
