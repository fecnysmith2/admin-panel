import xhr from 'xhr';
import {baseUrl} from '../../config';
import makeRequest from '../utils/request-queue-handler';
import {getHybridMemoMutation} from '../utils/queries/admin/dictionary-queries';
import {getUserObjectProperty} from './user-util';
import {getObjectFromLocalStore} from "./db";

const fillDataObject = function (object, findInput = "") {
    Object.keys(object).forEach(function (thisIndex) {
        const thisValue = object[thisIndex];
        const newIndex = (findInput && findInput + ".") + thisIndex;
        if (typeof thisValue === 'object') {
            fillDataObject(thisValue, newIndex);
        } else {
            window.log("Attach element", newIndex);
            if (newIndex.indexOf('content') > -1) {
                that.setState({'contentOfPage': thisValue});
            } else {
                if (document.getElementById(newIndex)) {
                    document.getElementById(newIndex).value = thisValue;
                }
            }
        }
    });
};

const unique = function (array) {
    let i;
    let a = [];
    for (i = 0; i < array.length; i++) {
        let current = array[i];
        if (a.indexOf(current) < 0) a.push(current);
    }

    array.length = 0;
    for (i = 0; i < a.length; i++) {
        array.push(a[i]);
    }

    return array;
};

const validateConfirmFormInputs = function (formElements) {
    let errorHappened = false;
    for (let i = 0; i < formElements.length; i++) {
        if (formElements[i].dataset.confirmneeded && formElements[i].dataset.confirmneeded == 'true') {
            let input = formElements[i];
            let confirmInput = formElements[formElements[i].name + formElements[i].dataset.confirmtag];
            if (confirmInput) {
                if (input.value != confirmInput.value) {
                    input.parentElement.className = input.parentElement.className + ' error';
                    confirmInput.parentElement.className = input.parentElement.className + ' error';
                    errorHappened = true;
                } else {
                    input.parentElement.className = input.parentElement.className.replace('error', '');
                    confirmInput.parentElement.className = input.parentElement.className.replace('error', '');
                }
            }
        }
    }

    return !errorHappened;
};

const sendEmail = function (event, mailTo = 'ugyfelszolgalat@onlinetrainer.hu', onSuccess = function () {
}, onError = function () {
}, offer = false, sendConfirm = true, isFormSubmit = true, subjectTag = '') {
    let phoneSubject = '', name = '', email = '', message, offerData, subjectData;
    const userAssign = getUserObjectProperty('userAssigns')[0];
    if (!isFormSubmit) {
        phoneSubject = event.phone ? ' telefon:' + event.phone : ''; // Subject line
        if (!event.name) {
            name = userAssign.firstName + ' ' + userAssign.lastName;
        } else {
            name = event.name;
        }
        if (!event.email) {
            email = getUserObjectProperty('emails')[0].email;
        } else {
            email = event.email;
        }
        message = event.message;
    } else {
        phoneSubject = event.target.phone ? ' telefon:' + event.target.phone.value : ''; // Subject line
        if (!event.target.name) {
            name = userAssign.firstName + ' ' + userAssign.lastName;
        } else {
            name = event.target.name.value;
        }
        if (!event.target.email) {
            email = getUserObjectProperty('emails')[0].email;
        } else {
            email = event.target.email.value
        }
        message = event.target.message.value;
    }

    if (offer) {
        offerData =
            '\r\n' +
            'Cégnév: ' + event.target.companyName.value + '\r\n' +
            'Kapcsolattartó neve: ' + event.target.name.value + '\r\n' +
            'Email: ' + event.target.email.value + '\r\n' +
            'Telefon: ' + event.target.phone.value + '\r\n\r\n';
        subjectData = 'Ajánlatkérés';
    } else {
        subjectData = 'Kapcsolat: ' + name + (phoneSubject);
    }
    let mailOptions = {
            from: email, // sender address
            to: mailTo, // list of receivers
            subject: subjectTag + subjectData,
            text: (offer ? offerData : '') + message // plaintext body
        },
        confirmMailOptions = {
            from: 'ugyfelszolgalat@onlinetrainer.hu', // sender address
            to: email, // list of receivers
            subject: subjectTag + 'Visszajelzés kapcsolatfelvételről', // Subject line
            text: `Kedves ${name}!
                Ügyfélszolgálatunkra az alábbi üzeneted megérkezett:
                
                " ${message} "
                
                A válasz megérkezéséig türelmedet kérjük.
                Üdvözlettel:
                az Online Trainer csapata`
        };
    getMailXHR(JSON.stringify(mailOptions)).then(function (object) {
        onSuccess(object.body);
        if (email !== 'info@onlinetrainer.hu' && sendConfirm) {
            getMailXHR(JSON.stringify(confirmMailOptions)).then(function (object) {
                window.log('confirm message sent', object);
            }).catch(function (object) {
                window.error('confirm message error', object);
            });
        }
    }).catch(function (object) {
        onError(object);
    });
};

const getMailXHR = function (data) {
    let uri = baseUrl,
        encodedData = encodeURI(data); //needed for utf-8 characters
    return new Promise((resolve, reject) => {
        xhr({
            method: 'POST',
            uri: uri + '/sendMail/' + btoa(btoa(encodedData)),
        }, (error, response, body) => {
            if (response.statusCode !== 200) {
                reject({error, response, body});
            } else {
                resolve(response);
            }
        });
    });
};

const sendTokenPayment = function (paymentData, onSuccess = function () {
}, onError = function () {
}) {
    getPaymentXHR(JSON.stringify(paymentData)).then(function (object) {
        onSuccess(object.body);
    }).catch(function (object) {
        onError(object);
    });
};

const getPaymentXHR = function (data) {
    let uri = baseUrl;
    return new Promise((resolve, reject) => {
        xhr({
            method: 'POST',
            uri: uri + '/simple/token-pay/' + b64EncodeUnicode(data),
        }, (error, response, body) => {
            if (response.statusCode !== 200) {
                reject({error, response, body});
            } else {
                resolve(response);
            }
        });
    });
};

const b64EncodeUnicode = function (str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
        return String.fromCharCode('0x' + p1);
    }));
};

const b64DecodeUnicode = function (str) {
    return decodeURIComponent(Array.prototype.map.call(atob(str), function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
};

const generatePdfFromHTML = function (containerToRender, fileName = 'generated.pdf', specialElementHandlers = {}) {
    window.log(jsPDF);
    let doc = new jsPDF(),
        html;
    html = document.getElementById(containerToRender);
    window.log(html);
    // All units are in the set measurement for the document
    // This can be changed to "pt" (points), "mm" (Default), "cm", "in"
    doc.fromHTML(html, function () {
    });
    doc.save(fileName);
};

const animatedScrollTo = function (elem, style, unit, from, to, time, prop) {
    if (!elem) return;
    let start = new Date().getTime(),
        timer = setInterval(function () {
            let step = Math.min(1, (new Date().getTime() - start) / time);
            if (prop) {
                elem[style] = (from + step * (to - from)) + unit;
            } else {
                elem.style[style] = (from + step * (to - from)) + unit;
            }
            if (step === 1) {
                clearInterval(timer);
            }

        }, 25);
    elem.style[style] = from + unit;
};

const isPartnerDemoUser = function () {
    return true;
};

/**
 * Check if demo user logged in.
 * @return boolean
 */
const isDemoUser = function () {
    const dependentPartner = getUserObjectProperty('dependentPartner');
    const userAssigns = getUserObjectProperty('userAssigns');

    function isInDemoUserGroups(userAssign) {
        return (
            userAssign.userType === 'A' &&
            userAssign.internalUserData.userSubtype !== null &&
            (
                userAssign.internalUserData.userSubtype === 9 ||
                userAssign.internalUserData.userSubtype === 3
            )
        );
    }

    if (userAssigns !== null) {
        for (let i = 0; i < userAssigns.length; i++) {
            if (isInDemoUserGroups(userAssigns[i])) {
                return true;
            }
        }
    }
    return false;
};

/**
 * Check if test user logged in.
 * @return boolean
 */
const isTestUser = function () {
    const dependentPartner = getUserObjectProperty('dependentPartner');
    const userAssigns = getUserObjectProperty('userAssigns');

    function isInDemoUserGroups(userAssign) {
        return (
            userAssign.userType === 'P' &&
            userAssign.internalUserData.userSubtype !== null &&
            userAssign.internalUserData.userSubtype === 2
        );
    }

    if (userAssigns !== undefined) {
        for (let i = 0; i < userAssigns.length; i++) {
            if (isInDemoUserGroups(userAssigns[i])) {
                return true;
            }
        }
    }
    return false;
};

const getTextFromHybridMemo = function (hybridMemo = null, defaultValue = '', language = 'HU') {
    if (hybridMemo && hybridMemo !== null) {
        if (hybridMemo.hybridMemoItems) {
            for (let i = 0; i < hybridMemo.hybridMemoItems.length; i++) {
                const memoItem = hybridMemo.hybridMemoItems[i];
                if (memoItem.language === language) {
                    return memoItem.textblock;
                }
            }
        }
    }
    return defaultValue;
};

const saveHybridMemo = function (hybridMemo, callBackSuccess = function (hybridMemo) {
}, callBackError = function (error) {
}) {
    makeRequest(getHybridMemoMutation(hybridMemo), function (response) {
        window.log('save hybridmemo', response);
        callBackSuccess(response.data.HybridMemo_save);
    }, function (error) {
        window.log('save hybridmemo-error', error);
        callBackError(error);
    });
};

const calculateDuration = function (timeInMillis) {
    function addZ(n) {
        return (n < 10 ? '0' : '') + n;
    }

    const milliseconds = timeInMillis % 1000;
    timeInMillis = (timeInMillis - milliseconds) / 1000;
    const seconds = timeInMillis % 60;
    timeInMillis = (timeInMillis - seconds) / 60;
    const minutes = timeInMillis % 60;
    const hours = (timeInMillis - minutes) / 60;
    return addZ(hours) + ':' + addZ(minutes) + ':' + addZ(seconds);
};

const stringComparator = function (a, b) {
    const textA = a.unAccent().toUpperCase();
    const textB = b.unAccent().toUpperCase();
    window.log('ATEXT', textA);
    window.log('BTEXT', textB);
    window.log('TEXT COMP', (textA < textB) ? -1 : (textA > textB) ? 1 : 0);
    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
};

const isNullOrUndefined = function (variable) {
    return variable === undefined || variable === null;
};

const getLoginInfo = function () {
    const ip = getObjectFromLocalStore('userIp');
    const fingerPrint = getObjectFromLocalStore('UUID');
    const ipParam = ip !== null && ip !== '' ? ip.replace(/"/g, '') : '';
    return {
        clientId: 'ot_web',
        deviceId: fingerPrint.replace(/"/g, '') + ipParam,
        userIp: ipParam
    };
};

const storeEvents = {
    'ACCOUNT_CREATE': {
        "name": "ACCOUNT_CREATE",
        "properties": {
            "name": {
                "hu": "Regisztráció"
            }
            ,
            "description": {
                "hu": "Regisztráció után küldött visszaigazoló e-mail"
            }
        }
    }
    ,
    'EMAIL_CONFIRMATION': {
        "name": "EMAIL_CONFIRMATION",
        "properties": {
            "name": {
                "hu": "Regisztráció visszaigazolás (Opcionális)"
            }
            ,
            "description": {
                "hu": "A regisztráció megerősítését követő e-mail"
            }
        }
    }
    ,
    'INVALID_LOGIN': {
        "name": "INVALID_LOGIN",
        "properties": {
            "name": {
                "hu": "Sikertelen bejelentkezés (Opcionális)"
            }
            ,
            "description": {
                "hu": "Hibás bejelentkezéskor küldött üzenet"
            }
        }
    }
    ,
    'CHECKOUT_CONFIRMATION_1': {
        "name": "CHECKOUT_CONFIRMATION_1",
        "properties": {
            "name": {
                "hu": "Vásárlás visszaigazolása #1"
            }
            ,
            "description": {
                "hu": "Vásárlás visszaigazolára szolgáló e-mail"
            }
        }
    }
    ,
    'CHECKOUT_CONFIRMATION_2': {
        "name": "CHECKOUT_CONFIRMATION_2",
        "properties": {
            "name": {
                "hu": "Vásárlás visszaigazolása #2"
            }
            ,
            "description": {
                "hu": "Vásárlás visszaigazolára szolgáló másodlagos üzenet. Használható például ügyfélszolgálati értesítésre."
            }
        }
    }
    ,
    'NEWSLETTER_SIGNUP': {
        "name": "NEWSLETTER_SIGNUP",
        "properties": {
            "name": {
                "hu": "Hirlevél feliratkozás"
            }
            ,
            "description": {
                "hu": "Hirlevélre való feliratkozást követő üdvözlő e-mail"
            }
        }
    }
};

module.exports = {
    unique,
    validateConfirmFormInputs,
    generatePdfFromHTML,
    sendEmail,
    sendTokenPayment,
    b64EncodeUnicode,
    b64DecodeUnicode,
    animatedScrollTo,
    isDemoUser,
    isTestUser,
    saveHybridMemo,
    getTextFromHybridMemo,
    calculateDuration,
    stringComparator,
    isNullOrUndefined,
    getLoginInfo,
    fillDataObject,
    storeEvents
};
