import {sendPostRequest} from '../utils/xhr-request';
import {addQueryToCache, findQueryInCache} from '../utils/db';
import {getTextFromHybridMemo} from '../utils/helpers';
import {baseUrl} from '../../config';

const loadLanguagesForComponent = function (i18n, componentName, componentJson) {
    let textHeaderList = [];
    let textMemoList = [];
    let map = new Map();
    let result = {
        en: {},
        hu: {},
        de: {}
    };
    Object.keys(componentJson).map(function(objectKey, index) {
        let value = componentJson[objectKey];
        if ((value + '').includes('$')) {
            window.log('ADD MEMOID',value);
            textMemoList.push(value.replace('$', ''));
        } else {
            window.log('ADD TEXTID',value);
            textHeaderList.push(value);
        }
        map.set(value, objectKey);
    });
    window.log('TEXTHEADERS',textHeaderList);
    window.log('TEXTMEMOS',textMemoList);
    window.log('TEXTMAP',map);
    const cache = findQueryInCache('localization-' + componentName);
    if (cache != '') {
        loadLanguageToi18(componentName, cache, result, map, i18n);
    } else {
        sendPostRequest(baseUrl + '/i18/' + componentName).then(function (object) {
            loadLanguageToi18(componentName, JSON.parse(object.body), result, map, i18n);
        }).catch(function (object) {
            console.error('POST', object);
        });
    }
};

const loadLanguageToi18 = function (componentName, textJSON, result, map, i18n ) {
    if (textJSON.event && textJSON.event == 'error') {
        window.error('LANGUAGE FILE NOT FOUND', textJSON.fileName);
        return;
    }
    window.log('TEXTHEADERS RESPONSE', textJSON);
    textJSON.textHeaders.forEach(function (item) {
        if (map.has(item.recnum.toString())) {
            window.log('TEXTITEM', item);
            item.textItems.forEach(function (textItem) {
                window.log('TEXTITEM2', textItem);
                switch (textItem.language) {
                    case 'HU':
                        result.hu[map.get(item.recnum.toString())] = textItem.text;
                        break;
                    case 'EN':
                        result.en[map.get(item.recnum.toString())] = textItem.text;
                        break;
                    case 'DE':
                        result.de[map.get(item.recnum.toString())] = textItem.text;
                        break;
                    default:

                }
            });
        }
    });
    textJSON.hybridMemos.forEach(function (item) {
        if (map.has('$' + item.recnum)) {
            window.log('MEMO', item);
            for (let i = 0; i < item.hybridMemoItems.length; i++) {
                const hybridMemoItem = item.hybridMemoItems[i];
                const text = getTextFromHybridMemo(item, '', hybridMemoItem.language);
                switch (hybridMemoItem.language) {
                    case 'HU':
                        result.hu[map.get('$' + item.recnum)] = text;
                        break;
                    case 'EN':
                        result.en[map.get('$' + item.recnum)] = text;
                        break;
                    case 'DE':
                        result.de[map.get('$' + item.recnum)] = text;
                        break;
                    default:

                }
            }
        }
    });
    window.log('LANGUAGE TEXT RESULT', result);
    i18n.addResources('hu-HU', componentName, result.hu);
    i18n.addResources('hu', componentName, result.hu);
    i18n.addResources('en-EN', componentName, result.en);
    i18n.addResources('en', componentName, result.en);
    i18n.addResources('de-DE', componentName, result.de);
    i18n.addResources('de', componentName, result.de);
    window.log('RESULT FROM POST', result);
    addQueryToCache('localization-' + componentName, textJSON);
};

module.exports = {
    loadLanguagesForComponent
};