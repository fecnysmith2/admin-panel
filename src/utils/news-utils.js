import React from "react";
import htmlToText from "html-to-text";
import {sendAjaxRequest} from "./xhr-request";
import {addObjectToCache, findObjectInCache} from "./db";

const getPostFromWP = function (type = 'dashboard', callBack = function (a) {
}, subPage = '') {
    // Get posts from WP site
    var requestObject = {
        uri: '//start.onlinetrainer.hu/wp-json/wp/v2/posts?per_page=5'
    };

    var subUrl = '';

    switch (subPage) {
        case 'basic_dashboard':
            subUrl = '&filter[category_name]=kezdolapom';
            break;
        case 'good_to_know':
            subUrl = '&filter[category_name]=jotudni';
            break;
        default:
            subUrl = '';
            break;
    }

    requestObject.uri += subUrl;

    // Posts are in memory
    var json = findObjectInCache('posts');
    if (json != '' && json != null) {
        if (type == 'dashboard') {
            callBack(getPostsOneColumn(JSON.parse(json)));
        } else {
            callBack(getPostsGrid(JSON.parse(json)));
        }
        window.log('JSON MEMORY OBJECT', json);
        return;
    }

    var that = this;
    sendAjaxRequest(requestObject).then(function (object) {
        var parsed = parsePosts(object.body);
        const cacheTimeOut = 60 * 60 * 1000;
        addObjectToCache('posts', parsed, cacheTimeOut);
        if (type == 'dashboard') {
            callBack(getPostsOneColumn(parsed));
        } else {
            callBack(getPostsGrid(parsed));
        }
    }).catch(function (object) {
        window.log(object);
    });
};

const extractPostImage = function (input) {
    var output = "",
        m = [],
        urls = [],
        rex = /src="([^"]+)"/g;

    // Get first image
    if (m = rex.exec(input)) {
        output = m[1];
    }
    // var result = [];
    // while (result = rex.exec(input)) {
    //    m.push(result[0]);
    // }
    // output = decodeURIComponent(m[0]);
    return output;
};

const parsePosts = function (input = '') {
    var data = JSON.parse(input),
        output = [],
        len = data.length;

    for (let i = 0; i < len; i++) {
        var content = data[i]['content']['rendered'],
            imageUrl = extractPostImage(content),
            cleanExcerpt = htmlToText.fromString(data[i]['excerpt']['rendered']);
        output[i] = [
            data[i]['link'],
            data[i]['title'].rendered,
            cleanExcerpt.toString(),
            imageUrl,
            data[i]['date']
        ];
    }
    return output;
};

const getPostsOneColumn = function (posts) {
    var articles = posts.map(function (article, index) {
        return (
            <article className="article-normal" key={index}>
                <a href={article[0]} target="_blank">
                    <div>
                        <h3 className="h3 text-center">{article[1]}</h3>
                        <div className="text-center">
                            {/*a hír dátuma <h3 className="h3">{article[4]}</h3>*/}
                            <img src={article[3]} className='img-responsive' alt=""/>
                        </div>
                        <p className="article-margin-top">{article[2]}</p>
                    </div>
                </a>
            </article>
        );
    });
    return (
        <div>
            <h3 className="h3 newsfeed-title">Hírfolyam</h3>
            {articles}
        </div>
    );
};

const getPostsGrid = function (posts) {
    var articles = [];
    for (let i = 0; i < 4; i += 2) {
        let post = posts[i];
        let next = posts[i + 1];
        articles.push(
            <div className="row articles grid-articles" key={i}>
                <article className='col-xs-12 col-sm-6'>
                    <div className="text-center">
                        <a href={post[0]} target="_blank">
                            <img src={post[3]} alt='Hír kép'/>
                        </a>
                        <a href={post[0]} target="_blank">
                            <h3 className='h3'>{post[1]}</h3>
                        </a>
                    </div>
                    <p><a href={post[0]} target="_blank">{post[2]} <strong>[tovább]</strong></a></p>
                </article>
                <article className='col-xs-12 col-sm-6'>
                    <div className="text-center">
                        <a href={next[0]} target="_blank">
                            <img src={next[3]} alt='Hír kép'/>
                        </a>
                        <a href={next[0]} target="_blank">
                            <h3 className='h3'>{next[1]}</h3>
                        </a>
                    </div>
                    <p><a href={next[0]} target="_blank">{next[2]} <strong>[tovább]</strong></a></p>
                </article>
            </div>
        );
    }
    return (
        <div>
            {articles}
        </div>
    );
};

module.exports = {
    getPostFromWP
};
