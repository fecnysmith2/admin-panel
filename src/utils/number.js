/**
 * Created by Törpedínó on 2017. 07. 03..
 */

Number.prototype.formatMoney = function(c, d, t){
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

const getVat = function(price){
    return price * 0.27;
};

const getNetPrice = function(price){
    const vat = getVat(price);
    return price - vat;
};

const priceFormat = function(price){
    // return Math.round(price).formatMoney(0,'', ' ')
    return price.formatMoney(0,'', ' ')
};

module.exports = {priceFormat,getNetPrice,getVat};