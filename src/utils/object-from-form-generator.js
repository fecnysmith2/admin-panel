const getObjectFromForm = function (queryParamName, objects) {
    let formObject = {};
    formObject[queryParamName] = {};
    window.log('OBJECT GENERATOR queryParamName', queryParamName);
    for (var i = 0; i < objects.length; i++) {
        if (objects[i].dataset.location != '' && objects[i].dataset.location != null) {
            if (objects[i].type == 'radio') {
                eval(getRadio(objects[i]));
            } else {
                if (objects[i].dataset.location.indexOf('[') > -1) {
                    window.log('OBJECT GENERATOR objects[i].dataset.location.indexOf([) > -1', objects[i].dataset.location.indexOf('['));
                    if (eval('formObject.' + objects[i].dataset.location.substring(0, objects[i].dataset.location.indexOf('['))) == undefined) {
                        window.log('OBJECT GENERATOR formObject 1', formObject);
                        eval('formObject.' + objects[i].dataset.location.substring(0, objects[i].dataset.location.indexOf('[')) + ' = []');
                    }
                    if (objects[i].dataset.location.indexOf('userType') < 0 && objects[i].dataset.location.indexOf('internalUserData') < 0) {
                        window.log('OBJECT GENERATOR formObject 2', getArray(objects[i], formObject));
                        eval(getArray(objects[i], formObject));
                    }
                } else {
                    window.log('LOCATION LONG ', objects[i].dataset.location.split('.'));
                    if (objects[i].dataset.location.indexOf('height') > -1) {
                        window.log('OBJECT GENERATOR formObject 3', formObject);
                        eval('formObject.' + objects[i].dataset.location + ' = { "value" : ' + objects[i].value + ', "unit" : "CMT"};');
                    } else if (objects[i].dataset.location.split('.').length > 1) {
                        getNestedInput(objects[i], formObject);
                    } else {
                        window.log('OBJECT GENERATOR formObject 4', getSimpleInput(objects[i]));
                        eval(getSimpleInput(objects[i]));
                    }
                }
            }
        }
    }
    for (var propertyName in formObject[queryParamName]) {
        if (formObject[queryParamName][propertyName] != false && formObject[queryParamName][propertyName] == '') {
            delete formObject[queryParamName][propertyName];
        }
    }
    return eval('formObject.' + queryParamName);
};

/*
 * Generate Array from input type with array signed location
 */
const getArray = function (form, formObject) {
    const childElement = form.dataset.location.substring(form.dataset.location.indexOf(']') + 2, form.dataset.location.length);
    if (childElement.length > 0) {
        let resultJsString = '';
        let isAlreadyDefined = false;
        eval('if (formObject.' + form.dataset.location.substring(0, form.dataset.location.indexOf(']') + 1) + ' != undefined) { isAlreadyDefined = true; }');
        if (childElement.indexOf('.') > -1) {
            resultJsString = 'var ';
            resultJsString = resultJsString.concat(childElement.substring(0, childElement.indexOf('.')) + ' = {};');
            resultJsString = resultJsString.concat(childElement + ' = "' + form.value + '"; ');
            resultJsString = resultJsString.concat(' formObject.' + form.dataset.location.substring(0, form.dataset.location.indexOf(']') + 1) + ' = ' + childElement.substring(0, childElement.indexOf('.')) + ';');
        }
        if (!isAlreadyDefined) {
            resultJsString = resultJsString.concat('formObject.' + form.dataset.location.substring(0, form.dataset.location.indexOf(']') + 1) + ' = {};');
        }
        return resultJsString + ' formObject.' + form.dataset.location.substring(0, form.dataset.location.indexOf(']') + 1) + '.' + childElement + ' = "' + form.value + '";';
    } else {
        return getContractValue(form);
    }
};


const getContractValue = function (form) {
    /*@TODO remove this with a generic solution*/
    if (form.value == 'on') {
        return 'formObject.' + form.dataset.location.substring(0, form.dataset.location.indexOf('[')) + '.push({"contractType": "0"});';
    } else {
        return '';
    }
};


const getRadio = function (form) {
    var selectedRadio;
    window.log('SELECT RADIO', form);
    if (document.querySelector('input[name="' + form.name + '"]:checked')) {
        selectedRadio = document.querySelector('input[name="' + form.name + '"]:checked').value;
        window.log('SELECT RADIO VALUE', selectedRadio);
        return 'formObject.' + form.dataset.location + ' = "' + selectedRadio + '";';
    }
    return '';
};

const getCheckBoxValue = function (form) {
    window.log('CHECK', form.checked);
    return form.checked;
};

/*
 *  This function used to parse nested objects from forms with complex location attribute like: json.data.name.recnum
 */
const getNestedInput = function (form, formObject) {
    /*
     * if the form input is empty we shouldn't iterate over the location string,
     * because it's better to avoid empty field sending to backend
     */
    if (form.value == '') {
        return formObject;
    }
    var resultObject = formObject;
    try {
        window.log('FORM', form);
        window.log('FORM VALUE', form.value);
        var locationArray = form.dataset.location.split('.');
        window.log('locationArray', locationArray);
        var objectFullString = '';
        for (var i = 0; i < locationArray.length; i++) {
            if (i > 0) {
                objectFullString += '.' + locationArray[i];
            } else {
                objectFullString += locationArray[i];
            }
            window.log('index ' + i + 'NESTED OBJECT for', 'resultObject' + objectFullString);
            window.log('index ' + i + 'NESTED OBJECT for 2', resultObject);
            if (!eval(resultObject[objectFullString])) {
                if (i == locationArray.length - 1) {
                    window.log('index ' + i + 'NESTED OBJECT value', 'resultObject.' + objectFullString + form.value);
                    if (form.type == 'checkbox') {
                        var value = getCheckBoxValue(form);
                        window.log('index ' + i + 'checkbox', 'resultObject.' + objectFullString + value);
                        eval('resultObject.' + objectFullString + ' = value');
                        window.log('index ' + i + 'checkbox', resultObject);
                    } else {
                        eval('resultObject.' + objectFullString + ' = ' + 'form.value');
                    }
                    window.log('index ' + i + 'NESTED OBJECT VALUE result resultObject.' + objectFullString, resultObject);
                } else {
                    window.log('index ' + i + 'NESTED OBJECT {}', 'resultObject.' + objectFullString + '= {}');
                    eval('resultObject.' + objectFullString + '= {}');
                    window.log('index ' + i + 'NESTED OBJECT {} result resultObject.' + objectFullString, resultObject);
                }
            }
        }
        window.log('index ' + i + 'NESTED OBJECT', resultObject);
    } catch (error) {
        window.error('ERROR', error);
    }
    return resultObject;
};

const getSimpleInput = function (form) {
    switch (form.type) {
        case 'number':
            return 'formObject.' + form.dataset.location + ' = "' + form.value + '";';
            break;
        default:
            return 'formObject.' + form.dataset.location + ' = "' + form.value + '";';
    }
};

module.exports = getObjectFromForm;
