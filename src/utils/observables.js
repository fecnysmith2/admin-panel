const observablesArray = [];

const registerObservable = function (name, eventName, callback = function () {}) {
    if (name && name !== '') {
        const observableObject = {
            name: name,
            callback: callback
        };
        let observableIndex = -1;
        for (let i = 0 ; i < observablesArray.length; i++) {
            if (observablesArray[i].eventName === eventName) {
                observableIndex = i;
                break;
            }
        }
        if (observableIndex > -1) {
            observablesArray[observableIndex].observables.push(observableObject);
        } else {
            observablesArray.push({eventName: eventName, observables: [observableObject]});
        }
    }
};

const removeObservable = function (name, eventName) {
    for (let i = 0 ; i < observablesArray.length; i++){
        const observableEvent = observablesArray[i];
        if (observableEvent.eventName === eventName) {
            for (let y = 0 ; y < observableEvent.observables.length; y++) {
                const observable = observableEvent.observables[y];
                if (observable.name === name) {
                    observableEvent.observables.splice(y, 1);
                    break;
                }
            }
            break;
        }
    }
};

const observ = function (eventName) {
    window.log('OBSRVS', observablesArray);
    for (let i = 0 ; i < observablesArray.length; i++){
        const observableEvent = observablesArray[i];
        if (observableEvent.eventName === eventName) {
            for (let y = 0 ; y < observableEvent.observables.length; y++) {
                observableEvent.observables[y].callback();
            }
            break;
        }
    }
};

module.exports = {
    registerObservable,
    removeObservable,
    observ,
};