const initPolyfills = function () {
    stringIncludesFunctionCheck();
    arrayIncludes();
    stringReplaceAt();
    stringUnAccent();
};

const stringReplaceAt = function () {
    String.prototype.replaceAt=function(index, character) {
        return this.substr(0, index) + character + this.substr(index+character.length);
    }
};

const stringUnAccent = function () {
    String.prototype.unAccent=function() {
        let string = this.replace(/á/g,'a');
        string = string.replace(/é/g,'e');
        string = string.replace(/í/g,'i');
        string = string.replace(/ó/g,'o');
        string = string.replace(/ö/g,'o');
        string = string.replace(/ő/g,'o');
        string = string.replace(/ú/g,'u');
        string = string.replace(/ü/g,'u');
        string = string.replace(/ű/g,'u');
        string = string.replace(/Á/g,'A');
        string = string.replace(/É/g,'E');
        string = string.replace(/Í/g,'I');
        string = string.replace(/Ó/g,'O');
        string = string.replace(/Ö/g,'O');
        string = string.replace(/Ő/g,'O');
        string = string.replace(/Ú/g,'U');
        string = string.replace(/Ü/g,'U');
        string = string.replace(/Ű/g,'U');
        window.log('UNACCENT', string);
        return string;
    }
};

const stringIncludesFunctionCheck = function () {
    if (!String.prototype.includes) {
        window.log('lefut');
        String.prototype.includes = function (search, start) {
            'use strict';
            if (typeof start !== 'number') {
                start = 0;
            }

            if (start + search.length > this.length) {
                return false;
            } else {
                return this.indexOf(search, start) !== -1;
            }
        };
    }
};

const arrayIncludes = function () {
    if (!Array.prototype.includes) {
        Object.defineProperty(Array.prototype, 'includes', {
            value: function (searchElement, fromIndex) {

                // 1. Let O be ? ToObject(this value).
                if (this == null) {
                    throw new TypeError('"this" is null or not defined');
                }

                var o = Object(this);

                // 2. Let len be ? ToLength(? Get(O, "length")).
                var len = o.length >>> 0;

                // 3. If len is 0, return false.
                if (len === 0) {
                    return false;
                }

                // 4. Let n be ? ToInteger(fromIndex).
                //    (If fromIndex is undefined, this step produces the value 0.)
                var n = fromIndex | 0;

                // 5. If n ≥ 0, then
                //  a. Let k be n.
                // 6. Else n < 0,
                //  a. Let k be len + n.
                //  b. If k < 0, let k be 0.
                var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

                // 7. Repeat, while k < len
                while (k < len) {
                    // a. Let elementK be the result of ? Get(O, ! ToString(k)).
                    // b. If SameValueZero(searchElement, elementK) is true, return true.
                    // c. Increase k by 1.
                    // NOTE: === provides the correct "SameValueZero" comparison needed here.
                    if (o[k] === searchElement) {
                        return true;
                    }
                    k++;
                }

                // 8. Return false
                return false;
            }
        });
    }
};

module.exports = {initPolyfills};
