const getVariableGroupsQuery = {
    queryName: 'VariableGroup',
    query: `query VariableGroup {
  	VariableGroup {
  	  recnum
      parentGroup {
        recnum
      }
  	  consoleType
      groupName {
        coupledText
        recnum
        version
        category
        textKey
        maxLength
        hintText
      }
  	  groupNameText
  	  version
  	  orderNum
  	  region
      dedicatedName
  	}
  }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};

const getVariableGroupByRecnumQuery = function (recnum) {
    return {
        queryName: 'VariableGroup',
        query: `query VariableGroup {
    	VariableGroup(search: "recnum==${recnum}") {
    	  recnum
        parentGroup {
          recnum
        }
    	  consoleType
        groupName {
          coupledText
          recnum
          version
          category
          textKey
          maxLength
          hintText
        }
    	  groupNameText
    	  version
    	  orderNum
    	  region
        dedicatedName
    	}
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    }
};

const getVariableGroupByGroupIdQuery = function (recnum) {
    return {
        queryName: 'VariableGroup',
        dependencies: ['textHeader'],
        query: `query VariableGroup {
    	VariableGroup(search: "recnum==${recnum}") {
                recnum
                parentGroup {
                  recnum
                }
                consoleType
                groupName {
                  ...textHeader
                }
                orderNum
                region
                dedicatedName
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    }
};

const getVariableGroupsMutation = function (parentGroup = '', orderNum = '', region = '', dedicatedName = '', console = '', groupName = '', recnum = '') {
    return {
        queryName: 'getVariableGroupsMutation',
        query: `mutation getVariableGroupsMutation($recnum: String, $parentGroup: Srting, $orderNum: String, $region: String, $dedicatedName: String, $console: String,  $groupName: String) {
          VariableGroup_save(recnum: $recnum, parentGroup: $parentGroup, orderNum: $orderNum, region: $region, dedicatedName: $dedicatedName, console: $console,  groupName: $groupName) {
            recnum
          }
        }`,
        variables: JSON.stringify({
            recnum: recnum,
            parentGroup: parentGroup,
            orderNum: orderNum,
            region: region,
            dedicatedName: dedicatedName,
            console: console,
            groupName: groupName
        }),
        cacheEnabled: false,
        mutation: true
    }
};

const getDataDescriptsByGroupId = function (groupId) {
    return {
        queryName: 'getDataDescriptsByGroupId',
        dependencies: ['textHeader'],
        query: `query getDataDescriptsByGroupId {
            DataDescript {
                group(search: "recnum==${groupId}") {
                    groupName{
                        ...textHeader
                    }
                    recnum
                    dedicatedName
                }
                languageLocation
                keyField
                nullEnabled
                textKey
                inputMode
                verifyName
                dataType
                dataLength
                recnum
                defaultValue {
                    valueBint
                    valueInt
                    valueNum
                    valueChar
                    valueVchar
                    valueDate
                    valueTime
                    valueStamp
                    valueBool
                }
                inputPrompt {
                    ...textHeader
                }
                orderNum
            }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

const getDataDescriptMutation = function (dataDescript, defaultValue = '', name = '') {
    const defaultParam = defaultValue != '' ? `, defaultValue: "${defaultValue}"` : '';
    const nameParam = name != '' ? `, name: "${name}"` : '';
    return {
        queryName: 'DataDescript_createOrEdit',
        dependencies: ['textHeader'],
        query: `mutation DataDescript_createOrEdit {
           DataDescript_createOrEdit(dataDescript: ${JSON.stringify(JSON.stringify(dataDescript))} ${defaultValue} ${name}) {
                group {
                    groupId
                    recnum
                    groupName {
                      ...textHeader
                    }
                    dedicatedName
                }
                languageLocation
                keyField
                nullEnabled
                textKey
                inputMode
                verifyName
                dataType
                dataLength
                recnum
                defaultValue {
                    valueBint
                    valueInt
                    valueNum
                    valueChar
                    valueVchar
                    valueDate
                    valueTime
                    valueStamp
                    valueBool
                }
                inputPrompt {
                    ...textHeader
                }
                orderNum
            }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

const getDataDescriptDeleteMutation = function (dataDescript, defaultValue = '') {
    const defaultParam = defaultValue !== '' ? `, defaultValue: "${defaultValue}"` : '';
    return {
        queryName: 'DataDescript_createOrEdit',
        dependencies: ['textHeader'],
        query: `mutation DataDescript_createOrEdit {
           DataDescript_createOrEdit(dataDescript: ${JSON.stringify(JSON.stringify(dataDescript))}) {
                group {
                    recnum
                    groupName {
                      ...textHeader
                    }
                    dedicatedName
                }
                languageLocation
                keyField
                nullEnabled
                textKey
                inputMode
                verifyName
                dataType
                dataLength
                recnum
                defaultValue {
                    valueBint
                    valueInt
                    valueNum
                    valueChar
                    valueVchar
                    valueDate
                    valueTime
                    valueStamp
                    valueBool
                }
                inputPrompt {
                    ...textHeader
                }
                orderNum
            }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

module.exports = {
    getVariableGroupsQuery,
    getVariableGroupByRecnumQuery,
    getVariableGroupByGroupIdQuery,
    getVariableGroupsMutation,
    getDataDescriptsByGroupId,
    getDataDescriptMutation,
    getDataDescriptDeleteMutation
};
