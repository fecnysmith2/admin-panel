const getTextHeaderMutation = function (textHeader) {
    const param = JSON.stringify(JSON.stringify(textHeader));
    return {
        queryName: 'textHeaderMutation',
        dependencies: ['textHeader'],
        query: `mutation textHeader {
            TextHeader(json: ${param}) {
              ...textHeader
            }
        }`,
        mutation: true
    }
};

const getTextItemMutation = function (textItem) {
    const param = JSON.stringify(JSON.stringify(textItem));
    return {
        queryName: 'textItemMutation',
        dependencies: ['textHeader','textItem'],
        query: `mutation textItem {
            TextItem_add(textItem: ${param}) {
              textHeader {
                ...textHeader
              }
              textItem {
                ...textItem
              }
            }
        }`,
        mutation: true
    }
};

const getHybridMemoMutation = function (hybridMemo) {
    const param = JSON.stringify(JSON.stringify(hybridMemo));
    return {
        queryName: 'HybridMemo_save',
        dependencies: ['hybridMemo'],
        query: `mutation HybridMemo_save{
            HybridMemo_save(hybridMemo: ${param}) {
                ...hybridMemo
            }
        }`,
        mutation: true
    }
};

module.exports = {
    getTextHeaderMutation,
    getTextItemMutation,
    getHybridMemoMutation
};
