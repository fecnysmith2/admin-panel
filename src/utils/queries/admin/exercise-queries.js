const getExerciseSeriesMutation = function (exercise) {
    delete exercise.lastHandler;
    const param = JSON.stringify(JSON.stringify(exercise));
    return {
        queryName: 'exerciseSeriesMutation',
        query: `mutation exerciseSeriesMutation{
            ExerciseSeries_save(exerciseSeriesJson: ${param})
        }`,
        mutation: true
    }
};

const getExerciseSeriesById = function (recnum) {
    return {
        queryName: 'exerciseSeriesQueryById',
        query: `query exerciseSeriesQuery{
            ExerciseSeries_getById(recnum: "${recnum}") {
              recnum
            }
        }`,
        mutation: false
    }
};

const getExerciseMutation = function (exercise) {
    delete exercise.lastHandler;
    return {
        queryName: 'exerciseMutation',
        dependencies: ['hybridMemo'],
        query: `mutation exerciseMutation{
        Exercise_save(exercise: ${JSON.stringify(JSON.stringify(exercise))}) {
          recnum
          name {
            ...hybridMemo
          }
          timeRequired
          mainPhase {
            recnum
          }
          attachmentVideoMobile {
            recnum
          }
          attachmentVideo {
            recnum
          }
          creator {
            recnum
            lastName
            middleName
            userType
            userTitle
            firstName
            version
          }
          restingTime
          profClass
          repeatCount
          version
          free
          stretch
          exerciseType
        }
        }`,
        mutation: true
    }
};

const getExercisePhasesSaveMutation = function (exercisePhases) {
    exercisePhases = exercisePhases.map(function (exercisePhase) {
        delete exercisePhase.lastHandler;
        delete exercisePhase.operationReference;
        return exercisePhase;
    });
    return {
        queryName: 'ExercisePhase_saveByArray',
        dependencies: ['hybridMemo'],
        query: `mutation ExercisePhase_saveByArray{
        ExercisePhase_saveByArray(exercisePhases: ${JSON.stringify(JSON.stringify(exercisePhases))}) {
          ordernum
          exercise {
            recnum
          }
          attachment {
            recnum
          }
          text {
            ...hybridMemo
          }
          recnum
          version
        }
        }`,
        mutation: true
    }
};

const getExercisePhaseMutation = function (exercisePhase) {
    delete exercisePhase.lastHandler;
    delete exercisePhase.operationReference;
    return {
        queryName: 'exercisePhaseMutation',
        dependencies: ['exercisePhase'],
        query: `mutation exercisePhaseMutation{
            ExercisePhase_save(exercisePhase: ${JSON.stringify(JSON.stringify(exercisePhase))}) {
                ...exercisePhase
            }
        }`,
        mutation: true
    }
};

const getExerciseQuery = function (types = '') {
    return {
        queryName: 'exerciseQuery',
        dependencies: ['exercise'],
        query: `query exerciseQuery{
            Exercise_getByType(types: ${JSON.stringify(types)}) {
                ...exercise
            }
        }`,
        mutation: false
    }
};

const getExercisePageQuery = function (type = '', page = 1, size = 5) {
    const typeSearchString = type === '' ? '' : '(search: "exerciseType=='+ type +'")';
    return {
        queryName: 'exerciseQuery',
        dependencies: ['hybridMemo'],
        query: `query exerciseQuery{
            ExercisePage(paginationRequest: {page: ${page}, size: ${size}}) {
                totalPages
                totalElements
                content${typeSearchString} {
                  recnum
                  name {
                    ...hybridMemo
                  }
                  mainPhase {
                    ...exercisePhase
                  }
                  attachmentVideoMobile {
                    ...attachment
                  }
                  attachmentVideo {
                    ...attachment
                  }
                  creator {
                    recnum
                    lastName
                    middleName
                    userType
                    userTitle
                    firstName
                    version
                  }
                  restingTime
                  profClass
                  repeatCount
                  version
                  free
                  stretch
                  exerciseType
                }
            }
        }
        fragment exercisePhase on ExercisePhase {
            ordernum
            recnum
            attachment {
              ...attachment
            }
            text {
              ...hybridMemo
            }
        }
        fragment attachment on Attachment {
          origFilename
          mimeType
          filesize
          path
          duration
          version
          width
          height
          regName
          recnum
        }`,
        mutation: false
    }
};

const getExerciseByRecnumQuery = function (recnum) {
    return {
        queryName: 'getExerciseByRecnumQuery',
        dependencies: ['exercise'],
        query: `query getExerciseByRecnumQuery{
            Exercise(search: "recnum==${recnum}") {
                ...exercise
            }
        }`,
        mutation: false
    }
};

const getDeleteExerciseByObjectMutation = function (exercise) {
    delete exercise.lastHandler;
    window.log('MUTATION', exercise);
    return {
        queryName: 'getDeleteExerciseByObjectMutation',
        dependencies: ['exercise'],
        query: `mutation getDeleteExerciseByObjectMutation{
            Exercise_deleteByObject(exercise: ${JSON.stringify(JSON.stringify(exercise))}) {
                ...exercise
            }
        }`,
        mutation: true
    }
};

const getDeleteElementaryExerciseByObjectMutation = function (exercise) {
    delete exercise.lastHandler;
    window.log('MUTATION', exercise);
    const param = JSON.stringify(JSON.stringify(exercise));
    return {
        queryName: 'getDeleteElementaryExerciseByObjectMutation',
        dependencies: ['exercise'],
        query: `mutation getDeleteElementaryExerciseByObjectMutation {
            Exercise_deleteElementaryExercise(exercise: ${param}) {
                ...exercise
            }
        }`,
        mutation: true
    }
};

const getDeleteExercisePhaseByObjectMutation = function (exercisePhase) {
    delete exercisePhase.lastHandler;
    delete exercisePhase.operator;
    return {
        queryName: 'getDeleteExercisePhaseByObjectMutation',
        dependencies: ['exercisePhase'],
        query: `mutation getDeleteExercisePhaseByObjectMutation{
            ExercisePhase_deleteByObject(exercisePhase: ${JSON.stringify(JSON.stringify(exercisePhase))}) {
                ...exercisePhase
            }
        }`,
        mutation: true
    }
};

const getExercisePhaseByExerciseRecnumQuery = function (recnum) {
    return {
        queryName: 'getExercisePhaseByExerciseRecnumQuery',
        dependencies: ['exercisePhase'],
        query: `query getExercisePhaseByExerciseRecnumQuery{
            ExercisePhase_getByExerciseId(recnum: "${recnum}") {
                ...exercisePhase
            }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

const getExerciseSeriesByRecnumQuery = function (recnum) {
    return {
        queryName: 'getExerciseSeriesByRecnumQuery',
        dependencies: ['exerciseSeries'],
        query: `query getExerciseSeriesByRecnumQuery {
            ExerciseSeries_getById(recnum: "${recnum}") {
                ...exerciseSeries
            }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

const getExerciseSeriesByTrainingElementQuery = function (recnum) {
    return {
        queryName: 'TrainingElement_getExerciseSeriesByTrainingElement',
        dependencies: ['exerciseSeries'],
        query: `query TrainingElement_getExerciseSeriesByTrainingElement {
            TrainingElement_getExerciseSeriesByTrainingElement(recnum: "${recnum}") {
                ...exerciseSeries
            }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

module.exports = {
    getExerciseSeriesMutation,
    getExerciseSeriesById,
    getExerciseMutation,
    getExercisePhaseMutation,
    getExercisePhasesSaveMutation,
    getExerciseQuery,
    getExerciseByRecnumQuery,
    getDeleteExerciseByObjectMutation,
    getDeleteExercisePhaseByObjectMutation,
    getDeleteElementaryExerciseByObjectMutation,
    getExercisePhaseByExerciseRecnumQuery,
    getExerciseSeriesByRecnumQuery,
    getExerciseSeriesByTrainingElementQuery,
    getExercisePageQuery
};
