const getTrainingProgsQuery = {
    queryName: `TrainingProgramList`,
    dependencies: ['trainingProgs'],
    query: `query TrainingProgram {
        TrainingProgs {
            ...trainingProgs
        }
    }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};

const getUserTrainingProgramQuery = {
    queryName: `UserTrainingProgram`,
    dependencies: ['userAssignProg'],
    query: `query UserTrainingProgram {
        UserAssignProg_getForCurrentUser {
            ...userAssignProg
        }
    }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};

const getTrainingProgsByRecnumQuery = function (recnum) {
    return {
        queryName: `TrainingProgram`,
        dependencies: ['trainingProgs'],
        query: `query TrainingProgram {
            TrainingProgs(search: "recnum==${recnum}") {
                ...trainingProgs
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    }
};

const getTrainingProgsMutation = function (trainingMutation) {
    return {
        queryName: `TrainingProgramMutation`,
        dependencies: ['trainingProgs'],
        query: `mutation TrainingProgram {
            TrainingProgs(json: ${JSON.stringify(JSON.stringify(trainingMutation))}) {
                ...trainingProgs
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingProgsGraphByTrainingProgramQuery = function (recnum) {
    return {
        queryName: `TrainingProgsGraf`,
        dependencies: ['hybridMemo'],
        query: `query TrainingProgsGraf {
          TrainingProgsGraf {
            recnum
            stepletter
            step(orderBy: ASC)
            memoName {
              ...hybridMemo
            }
            trainingPlanTemplate {
              message {
                ...hybridMemo
              }
              note {
                ...hybridMemo
              }
              recnum
              validFrom
              name {
                ...hybridMemo
              }
              repeat
            }
            trainingProgs(search: "recnum==${recnum}") {
              recnum
            }
          }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    }
};

const getTrainingProgsGraphMutation = function (trainingProgGraph) {
    return {
        queryName: `TrainingProgsGraf`,
        dependencies: ['hybridMemo'],
        query: `mutation TrainingProgsGraf {
          TrainingProgsGraf_save(trainingProgsGraf: ${JSON.stringify(JSON.stringify(trainingProgGraph))}) {
            recnum
            stepletter
            step(orderBy: ASC)
            memoName {
              ...hybridMemo
            }
            trainingPlanTemplate {
              message {
                ...hybridMemo
              }
              note {
                ...hybridMemo
              }
              recnum
              validFrom
              name {
                ...hybridMemo
              }
              repeat
            }
            trainingProgs {
              recnum
            }
          }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingProgsGraphDeleteMutation = function (recnum) {
    return {
        queryName: `TrainingProgsGraf`,
        dependencies: ['trainingProgsGraf'],
        query: `mutation TrainingProgsGraf_delete {
            TrainingProgsGraf_remove(recnum: "${recnum}") {
                ...trainingProgsGraf
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingProgsEventQuery = function (recnum) {
    return {
        queryName: `TrainingProgsEvent`,
        dependencies: ['hybridMemo'],
        query: `query TrainingProgsEvent {
          TrainingProgsEvent {
            trainingProgs(search: "recnum==${recnum}") {
              recnum
            }
            progsEvent
            messageKind
            message {
              ...hybridMemo
            }
            emailRepeat
            correlateMode
            obligate
            expiration
            messagePush {
              ...hybridMemo
            }
            recnum
            reference
            eventValue
            setValue
            emailId {
              subject {
                ...hybridMemo
              }
              recnum
              name {
                ...hybridMemo
              }
              senderName {
                ...hybridMemo
              }
            }
          }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    }
};

const getTrainingProgsEventMutation = function (trainingProgsEvent) {
    return {
        queryName: `TrainingProgsEvent`,
        dependencies: ['trainingProgsEvent'],
        query: `mutation TrainingProgsEvent {
            TrainingProgsEvent(json: ${JSON.stringify(JSON.stringify(trainingProgsEvent))}) {
                ...trainingProgsEvent
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingProgsEventDeleteMutation = function (recnum) {
    return {
        queryName: `TrainingProgsEvent_delete`,
        dependencies: ['trainingProgsEvent'],
        query: `mutation TrainingProgsEvent_delete {
            TrainingProgsEvent_delete(recnum: "${recnum}") {
                ...trainingProgsEvent
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingProgsTypeQuery = {
    queryName: `TrainingProgsType`,
    query: `query TrainingProgsType {
            DataValues_getDataValueByGroupId(variableGroupId: "135")
    }`,
    variables: '',
    cacheEnabled: true,
    mutation: false
};

const getTrainingProgsEventMessageTypeQuery = {
    queryName: `TrainingProgsEventMessageType`,
    query: `query TrainingProgsEventMessageType {
            DataValues_getDataValueByGroupId(variableGroupId: "174")
    }`,
    variables: '',
    cacheEnabled: true,
    mutation: false
};

const getTrainingProgsEventEmailtemplateTypeQuery = {
    queryName: `TrainingProgsEventEmailtemplateType`,
    dependencies: ['emailTemplate'],
    query: `query EmailTemplate {
        EmailTemplate {
            ...emailTemplate
        }
    }`,
    variables: '',
    cacheEnabled: true,
    mutation: false
};

const getTrainingProgsEventTypeQuery = {
    queryName: `TrainingProgsEventType`,
    query: `query TrainingProgsEventType {
            DataValues_getDataValueByGroupId(variableGroupId: "167")
    }`,
    variables: '',
    cacheEnabled: true,
    mutation: false
};

const getTrainingProgsCorrelateModeQuery = {
    queryName: `TrainingProgsEventCorrelateMode`,
    query: `query TrainingProgsEventCorrelateMode {
            DataValues_getDataValueByGroupId(variableGroupId: "169")
    }`,
    variables: '',
    cacheEnabled: true,
    mutation: false
};

const getTrainingProgsStepchangeVariablesQuery = {
    queryName: `TrainingProgsStepchangeVariables`,
    query: `query TrainingProgsStepchangeVariables {
            DataValues_getDataValueByGroupId(variableGroupId: "170")
    }`,
    variables: '',
    cacheEnabled: true,
    mutation: false
};

const getTrainingProgsStepchangeByProgsRecnumQuery = function (recnum) {
    return {
        queryName: `TrainingProgsStepchange`,
        dependencies: ['trainingProgsStepchange'],
        query: `query TrainingProgsStepchange {
            TrainingProgsStepchange {
                trainingProgs(search: "recnum==${recnum}") {
                  recnum
                }
                ...trainingProgsStepchange
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    }
};

const getTrainingProgsStepchangeDeleteMutation = function (recnum) {
    return {
        queryName: `TrainingProgsStepchange_delete`,
        dependencies: ['trainingProgsStepchange'],
        query: `mutation TrainingProgsStepchange_delete {
            TrainingProgsStepchange_delete(recnum: "${recnum}") {
                ...trainingProgsStepchange
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingProgsStepchangeMutation = function (trainingProgsStepchange) {
    return {
        queryName: `TrainingProgsStepchange`,
        dependencies: ['trainingProgsStepchange'],
        query: `mutation TrainingProgsStepchange {
            TrainingProgsStepchange(json: ${JSON.stringify(JSON.stringify(trainingProgsStepchange))}) {
                ...trainingProgsStepchange
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingProgsCondspecByProgsRecnumQuery = function (recnum) {
    return {
        queryName: `TrainingProgsCondspec`,
        query: `query TrainingProgsCondspec {
            TrainingProgsCondspec {
                levelValue
                trainingProgs(search: "recnum==${recnum}") {
                    recnum
                }
                trainingProgsGraf {
                  recnum
                  step
                  stepletter
                }
                levelMin
                recnum
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    }
};

const getTrainingProgsCondspecMutation = function (trainingProgsCondspec) {
    return {
        queryName: `TrainingProgsCondspec`,
        dependencies: ['trainingProgsCondspec'],
        query: `mutation TrainingProgsCondspec {
            TrainingProgsCondspec(json: ${JSON.stringify(JSON.stringify(trainingProgsCondspec))}) {
                ...trainingProgsCondspec
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingProgsCondspecDeleteMutation = function (recnum) {
    return {
        queryName: `TrainingProgsCondspec_delete`,
        dependencies: ['trainingProgsCondspec'],
        query: `mutation TrainingProgsCondspec_delete {
            TrainingProgsCondspec_delete(recnum: "${recnum}") {
                ...trainingProgsCondspec
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingProgsEventPrecondByProgsRecnumQuery = function (recnum) {
    return {
        queryName: `TrainingProgsEventPrecond`,
        query: `query TrainingProgsEventPrecond {
          TrainingProgsEventPrecond {
            logicalGroup
            trainingProgsEventCond {
              ...trainingProgsEvent
            }
            trainingProgsEvent {
              ...trainingProgsEvent
            }
            version
            recnum
          }
        }
        fragment trainingProgsEvent on TrainingProgsEvent {
          recnum
          expiration
          messageKind
          messagePushMemo
          obligate
          additionValue
          correlateMode
          emailRepeat
          messageMemo
          setValue
          version
          eventValue
          progsEvent
          trainingProgs(search: "recnum==${recnum}") {
            recnum
          }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    }
};

const getTrainingProgsEventPrecondMutation = function (trainingProgsPrecond) {
    return {
        queryName: `TrainingProgsEventPrecond`,
        dependencies: ['trainingProgsEventPrecond'],
        query: `mutation TrainingProgsEventPrecond {
            TrainingProgsEventPrecond(json: ${JSON.stringify(JSON.stringify(trainingProgsPrecond))}) {
                ...trainingProgsEventPrecond
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingProgsEventPrecondDeleteMutation = function (recnum) {
    return {
        queryName: `TrainingProgsEventPrecond_delete`,
        dependencies: ['trainingProgsEventPrecond'],
        query: `mutation TrainingProgsEventPrecond_delete {
            TrainingProgsEventPrecond_delete(recnum: "${recnum}") {
                ...trainingProgsEventPrecond
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingProgsGraphJunctionByProgsRecnumQuery = function (recnum) {
    return {
        queryName: `TrainingProgsGrafJunction`,
        dependencies: ['trainingProgsGraf'],
        query: `query TrainingProgsGrafJunction {
            TrainingProgsGrafJunction {
                logicalGroup
                junctionValue
                recnum
                junctionMode
                trainingProgs(search: "recnum==${recnum}") {
                    recnum
                }
                trainingProgsGrafNextStep {
                    ...trainingProgsGraf
                }
                trainingProgsGraf {
                    ...trainingProgsGraf
                }
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    }
};

const getTrainingProgsGraphJunctionMutation = function (trainingProgsPrecond) {
    return {
        queryName: `TrainingProgsGrafJunction`,
        dependencies: ['trainingProgsGrafJunction'],
        query: `mutation TrainingProgsGrafJunction {
            TrainingProgsGrafJunction(json: ${JSON.stringify(JSON.stringify(trainingProgsPrecond))}) {
                ...trainingProgsGrafJunction
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingProgsGraphJunctionDeleteMutation = function (recnum) {
    return {
        queryName: `TrainingProgsGrafJunction_delete`,
        dependencies: ['trainingProgsGrafJunction'],
        query: `mutation TrainingProgsGrafJunction_delete {
            TrainingProgsGrafJunction_delete(recnum: "${recnum}") {
                ...trainingProgsGrafJunction
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingProgsGraphJunctionModeQuery = {
    queryName: `TrainingProgsGrafJunction_delete`,
    query: `query q {
        DataValues_getDataValueByName(variableGroupName: "junction_mode")
    }`,
    variables: '',
    cacheEnabled: false,
    mutation: true
};

const getTrainingQuery = {
    queryName: `Training`,
    dependencies: ['training'],
    query: `query Training {
        Training {
            ...training
        }
    }`,
    variables: '',
    cacheEnabled: false,
    mutation: true
};

module.exports = {
    getTrainingProgsQuery,
    getUserTrainingProgramQuery,
    getTrainingProgsByRecnumQuery,
    getTrainingProgsMutation,
    getTrainingProgsEventEmailtemplateTypeQuery,
    getTrainingProgsGraphByTrainingProgramQuery,
    getTrainingProgsGraphMutation,
    getTrainingProgsGraphDeleteMutation,
    getTrainingProgsEventQuery,
    getTrainingProgsEventMutation,
    getTrainingProgsEventDeleteMutation,
    getTrainingProgsTypeQuery,
    getTrainingProgsEventMessageTypeQuery,
    getTrainingProgsEventTypeQuery,
    getTrainingProgsCorrelateModeQuery,
    getTrainingProgsStepchangeVariablesQuery,
    getTrainingProgsStepchangeByProgsRecnumQuery,
    getTrainingProgsStepchangeDeleteMutation,
    getTrainingProgsStepchangeMutation,
    getTrainingProgsCondspecByProgsRecnumQuery,
    getTrainingProgsCondspecMutation,
    getTrainingProgsCondspecDeleteMutation,
    getTrainingProgsEventPrecondByProgsRecnumQuery,
    getTrainingProgsEventPrecondMutation,
    getTrainingProgsEventPrecondDeleteMutation,
    getTrainingProgsGraphJunctionByProgsRecnumQuery,
    getTrainingProgsGraphJunctionMutation,
    getTrainingProgsGraphJunctionDeleteMutation,
    getTrainingProgsGraphJunctionModeQuery,
    getTrainingQuery
};