const getUserListQuery = {
    queryName: 'UserListWrapper_getUserListWrapperDataForTrainingPlan',
    query: `query UserListWrapper_getUserListWrapperDataForTrainingPlan {
        UserListWrapper_getUserListWrapperDataForTrainingPlan {
            user {
              recnum
              login
              lastOperation
              userAssigns {
                recnum
                firstName
                middleName
                lastName
                mainContact {
                  recnum
                  extension
                  phoneType
                  phoneNumber
                }
              }
              emails {
                recnum
                email
              }
              birthDate
              sex
            }
            trainingCount
            unprizedTrainingCount
            trainingProgs {
              recnum
              maxWeek
              programEnd
              registEnd
              trainingProg
              programStart
              registStart
            }
          }
      }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};

const getUserAnswerListQuery = function (userId) {
    return {
        queryName: `UserAnswerList_${userId}`,
        dependencies: ['hybridMemo'],
        query: `query UserAnswerList {
      UserAnswer_getForDifferentUser(userId: "${userId}"){
           questionGroup {
             desc {
                ...hybridMemo
             }
             needsEvaluation
             recnum
             title {
                ...hybridMemo
             }
           }
           fillDate
           recnum
           evaluationDate
           evaluationSend
         }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    }
};

const getUserAnswerQuery = function (userAnswerId) {
    return {
        queryName: `UserAnswer_${userAnswerId}`,
        query: `query UserAnswer {
      UserAnswer_getForDifferentUser(userId: "${userAnswerId}"){
          version
          fillDate
          recnum
          evaluationDate
          evaluationSend
        }
    }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    }
};

const getUserInviteMutation = function (email) {
    return {
        queryName: `User_sendInviteRequest_${email}`,
        query: `
          mutation User_sendInviteRequest {
            User_sendInviteRequest(email: "${email}"){
                status
            }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

const getUserAnswerForATestQuery = function (userAnswerId) {
    return {
        queryName: `UserAnswer_${userAnswerId}`,
        dependencies: ['hybridMemo'],
        query: `query UserAnswer {
            UserAnswer(search: "recnum==${userAnswerId}") {
            fillDate
            recnum
            userAnswerItems {
             addValue
             answer {
               question {
                 recnum
                 questionText {
                    ...hybridMemo
                 }
                 unitType
               }
               ordernum
               recnum
               letter
               note {
                   ...hybridMemo
               }
               text {
                ...hybridMemo
               }
             }
            }
            evaluationDate
            evaluationSend
            evaluationSuccess
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    }
};

const getBusinessPartnerIdentificationForCurrentUserQuery = {
    queryName: `BusinessPartnerIdentification_getForCurrentUser`,
    query: `query BusinessPartnerIdentification_getForCurrentUser {
     BusinessPartnerIdentification_getForCurrentUser {
        value
      }
   }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};

const getUserByRecnumQuery = function (recnum) {
    return {
        queryName: `UserByRecnum`,
        dependencies: ['user'],
        query:`query UserByRecnum {
            User(search: "recnum==${recnum}") {
                ...user
            }
        }`,
        variables : '',
        cacheEnabled : false,
        mutation : false
    }
};

const getUserAssignProg = {
    queryName: 'UserAssignProg',
    query: `query UserAssignProg {
              UserAssignProg (ownedObjectsOnly: true) {
                finishDate
                startDate
                version
                recnum
                pulseZoneGroup
              }
            }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};

const getTrainingProgramQuery =  {
      queryName: `TrainingProgram`,
      query:`query TrainingProgram {
               TrainingProgs {
                  registEnd
                  memoNameMemo
                  trainingProg
                  programStart
                  programEnd
                  recnum
                  resident
                  maxWeek
                  memoNoteMemo
                  registStart
                  version
                }
              }`,
      variables : '',
      cacheEnabled : false,
      mutation : false
};

module.exports = {
    getUserListQuery,
    getUserAnswerListQuery,
    getUserAnswerForATestQuery,
    getBusinessPartnerIdentificationForCurrentUserQuery,
    getUserByRecnumQuery,
    getUserInviteMutation,
    getUserAssignProg,
    getTrainingProgramQuery
};
