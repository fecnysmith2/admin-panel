const exercise = {
    name: 'exercise',
    dependencies: ['exercisePhase','attachment','hybridMemo'],
    fragment: `
        fragment exercise on Exercise {
            timeRequired
            repeatCount
            fixFirst
            phase
            difficulty
            pattern
            relativeDifficulty
            level3Min
            sex
            level2Min
            repeatTimeIsInSeconds
            cooper
            condtest
            restingTime
            recnum
            exerciseType
            free
            profClass
            level5Min
            stretch
            outdoor
            level4Min
            mainPhase {
                ...exercisePhase
            }
            attachmentVideo {
                ...attachment
            }
            attachmentVideoMobile {
                ...attachment
            }
            name {
                ...hybridMemo
            }
        }
    `
};

const exercisePhase = {
    name: 'exercisePhase',
    dependencies: ['hybridMemo','attachment'],
    fragment: `
        fragment exercisePhase on ExercisePhase {
            attachment {
                ...attachment
            }
            exercise {
                recnum
            }
            ordernum
            recnum
            text {
                ...hybridMemo
            }
        }
    `
};

const exerciseItem = {
    name: 'exerciseItem',
    dependencies: [],
    fragment: `
        fragment exerciseItem on ExerciseItem {
            repeatTime
            repeat
            recnum
            ordernum
            restingTime
        }
    `
};

const exerciseSeries = {
    name: 'exerciseSeries',
    dependencies: ['exercise','exerciseItem','trainingExerciseItem'],
    fragment: `
        fragment exerciseSeries on ExerciseSeriesWrapper {
            id
            parentId
            exerciseItem {
                ...exerciseItem
            }
            trainingExerciseItem {
                ...trainingExerciseItem
            }
            removeFlag
            exercise {
                ...exercise
            }
            children {
                ...exerciseSeriesChildren
            }
        }
        
        fragment exerciseSeriesChildren on ExerciseSeriesWrapper {
            id
            parentId
            exerciseItem {
                ...exerciseItem
            }
            trainingExerciseItem {
                ...trainingExerciseItem
            }
            removeFlag
            exercise {
                ...exercise
            }
            children {
                ...exerciseSeriesSecondChildren
            }
        }
        
        fragment exerciseSeriesSecondChildren on ExerciseSeriesWrapper {
            id
            parentId
            exerciseItem {
                ...exerciseItem
            }
            trainingExerciseItem {
                ...trainingExerciseItem
            }
            removeFlag
            exercise {
                ...exercise
            }
            children {
                id
                parentId
                exerciseItem {
                    ...exerciseItem
                }
                removeFlag
                exercise {
                    ...exercise
                }
                children {
                    id
                }
            }
        }
    `
};

const trainingExerciseItem = {
    name: 'trainingExerciseItem',
    dependencies: ['exercise','exerciseItem'],
    fragment: `
        fragment trainingExerciseItem on TrainingExerciseItem {
            trainingElement {
                ordernum
                recnum
                rest
                repeat
            }
            exerciseItem {
                ...exerciseItem
            }
            parentExercise {
                ...exercise
            }
            childExercise {
                ...exercise
            }
            recnum
            opType
            ordernum
            repeatTime
            restingTime
            repeat
        }
    `
};

const exerciseFragments = function () {
    let fragmentsArray = [];
    fragmentsArray['exercise'] = exercise;
    fragmentsArray['exercisePhase'] = exercisePhase;
    fragmentsArray['exerciseItem'] = exerciseItem;
    fragmentsArray['exerciseSeries'] = exerciseSeries;
    fragmentsArray['trainingExerciseItem'] = trainingExerciseItem;
    return fragmentsArray;
};

module.exports = {
    exerciseFragments
};