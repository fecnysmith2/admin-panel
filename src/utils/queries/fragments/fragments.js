import {paymentFragments} from './payment-fragments';
import {userFragments} from './user-fragments';
import {exerciseFragments} from './exercise-fragments';
import {trainingFragments} from './training-fragments';
import {generalFragments} from './general-fragments';
import {testFragments} from './test-fragments';

const fragments = function () {
    let fragmentsArray = [];
    fragmentsArray = concatFragmentArray(fragmentsArray,userFragments());
    fragmentsArray = concatFragmentArray(fragmentsArray,exerciseFragments());
    fragmentsArray = concatFragmentArray(fragmentsArray,trainingFragments());
    fragmentsArray = concatFragmentArray(fragmentsArray,generalFragments());
    fragmentsArray = concatFragmentArray(fragmentsArray,paymentFragments());
    fragmentsArray = concatFragmentArray(fragmentsArray,testFragments());
    return fragmentsArray;
};

const concatFragmentArray = function (originalArray, dependencies) {
    for (let key in dependencies) {
        if (dependencies.hasOwnProperty(key)) {
            originalArray[key] = dependencies[key];
        }
    }
    return originalArray;
};


module.exports = {
    fragments
};