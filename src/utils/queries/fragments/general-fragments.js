const hybridMemoItems = {
    name: 'hybridMemoItems',
    dependencies: [],
    fragment: `
        fragment hybridMemoItems on HybridMemoItems {
            language
            recnum
            textblock
        }
    `
};

const hybridMemo = {
    name: 'hybridMemo',
    dependencies: ['hybridMemoItems'],
    fragment: `
        fragment hybridMemo on HybridMemo {
            hybridMemoItems {
                ...hybridMemoItems
            }
            recnum
            translation
        }
    `
};

const textItem = {
    name: 'textItem',
    dependencies: [],
    fragment: `
        fragment textItem on TextItem {
            consoleType
            language
            recnum
            region
            text
        }
    `
};

const textHeader = {
    name: 'textHeader',
    dependencies: ['textItem'],
    fragment: `
        fragment textHeader on TextHeader {
            category
            coupledText
            hintText
            maxLength
            recnum
            textId
            textItems {
                ...textItem
            }
            textKey
        }
    `
};

const localizedLength = {
    name: 'localizedLength',
    dependencies: [],
    fragment: `
        fragment localizedLength on LocalizedLength {
            unit
            value
        }
    `
};

const localizedWeight = {
    name: 'localizedWeight',
    dependencies: [],
    fragment: `
        fragment localizedWeight on LocalizedWeight {
            unit
            value
        }
    `
};

const attachment = {
    name: 'attachment',
    dependencies: ['hybridMemo'],
    fragment: `
        fragment attachment on Attachment {
            duration
            filesize
            height
            memo {
                ...hybridMemo
            }
            mimeType
            origFilename
            path
            recnum
            regName
            width
        }
    `
};

const oAuth2Token = {
    name: 'oAuth2Token',
    dependencies: [],
    fragment: `
        fragment oAuth2Token on OAuth2Token {
            access_token
            token_type
            refresh_token
            expires_in
            scope
        }
    `
};

const product = {
    name: 'product',
    dependencies: [],
    fragment: `
        fragment product on Product {
            productCode
            recnum
        }
    `
};

const emailTemplate = {
    name: 'emailTemplate',
    dependencies: [],
    fragment: `
        fragment emailTemplate on EmailTemplate {
            subject {
            ...hybridMemo
            }
            senderName {
            ...hybridMemo
            }
            name {
            ...hybridMemo
            }
            recnum
        }
    `
};

const generalFragments = function () {
    let fragmentsArray = [];
    fragmentsArray['hybridMemoItems'] = hybridMemoItems;
    fragmentsArray['hybridMemo'] = hybridMemo;
    fragmentsArray['textItem'] = textItem;
    fragmentsArray['textHeader'] = textHeader;
    fragmentsArray['localizedLength'] = localizedLength;
    fragmentsArray['localizedWeight'] = localizedWeight;
    fragmentsArray['attachment'] = attachment;
    fragmentsArray['oAuth2Token'] = oAuth2Token;
    fragmentsArray['product'] = product;
    fragmentsArray['emailTemplate'] = emailTemplate;
    return fragmentsArray;
};

module.exports = {
    generalFragments
};