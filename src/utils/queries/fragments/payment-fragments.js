const creditCardProcess = {
    name: 'creditCardProcess',
    dependencies: ['userSubscription'],
    fragment: `
        fragment creditCardProcess on CreditCardProcess {
            subscriptionId
            token
            endDate
            simpleUserid
            recurringAmount
            recnum
            subscriptionDate
            transactionCode
            currency
            firstAmount
            subId {
                ...userSubscription
            }
        }
    `
};

const creditCardProcessEvent = {
    name: 'creditCardProcessEvent',
    dependencies: ['creditCardProcess','exerciseItem'],
    fragment: `
        fragment creditCardProcessEvent on CreditCardProcessEvent {
            referenceNumber
            tokenType
            sendDate
            recnum
            referenceCode
            referenceDate
            process {
                ...creditCardProcess
            }
        }
    `
};

const userSubscription = {
    name: 'userSubscription',
    dependencies: ['product'],
    fragment: `
        fragment userSubscription on UserSubscription {
            deactivatedPlanDate
            nextPayment
            activate
            recnum
            sales
            subCycle
            version
            deactivatedDate
            prolongation
            product {
                ...product
            }
        }
    `
};

const product = {
    name: 'product',
    dependencies: [],
    fragment: `
        fragment product on Product {
            recnum
            version
            productCode
        }
    `
};

const paymentFragments = function () {
    let fragmentsArray = [];
    fragmentsArray['creditCardProcess'] = creditCardProcess;
    fragmentsArray['creditCardProcessEvent'] = creditCardProcessEvent;
    fragmentsArray['userSubscription'] = userSubscription;
    fragmentsArray['product'] = product;
    return fragmentsArray;
};

module.exports = {
    paymentFragments
};