const answer = {
    name: 'answer',
    dependencies: ['hybridMemo'],
    fragment: `
        fragment answer on Answer {
            add_text_enabled
            add_value_max_percent
            add_value_min_percent
            blocking
            letter
            ordernum
            note {
                ...hybridMemo
            }
            recnum
            sexFilter
            text {
                ...hybridMemo
            }
        }
    `
};

const questionGroup = {
    name: 'questionGroup',
    dependencies: ['hybridMemo','questionGroupItem'],
    fragment: `
        fragment questionGroup on QuestionGroup {
            title {
                ...hybridMemo
            }
            desc {
                ...hybridMemo
            }
            items {
                ...questionGroupItem
            }
        }
    `
};

const questionGroupItem = {
    name: 'questionGroupItem',
    dependencies: ['answer','hybridMemo','question'],
    fragment: `
        fragment questionGroupItem on QuestionGroupItem {
            ordernum
            nullEnabled
            clasped
            recnum
            previouslyChosenValue {
                ...answer
            }
            question {
                ...question
            }
        }
    `
};

const question = {
    name: 'question',
    dependencies: ['answer','hybridMemo'],
    fragment: `
        fragment question on Question {
            unitType
            recnum
            note {
                ...hybridMemo
            }
            multiAnswer
            localizedUnitType
            questionText {
                ...hybridMemo
            }
            addValueMax
            addValueMin
            answers {
                ...answer
            }
        }
    `
};

const userAnswer = {
    name: 'userAnswer',
    dependencies: ['questionGroup','userAnswerItem'],
    fragment: `
        fragment userAnswer on UserAnswer {
            evaluationCateg
            evaluationSend
            recnum
            evaluationSuccess
            evaluationPublic
            evaluationLine
            evaluationDate
            fillDate
            questionGroup {
                ...questionGroup
            }
            userAnswerItems {
                ...userAnswerItem
            }
        }
    `
};

const userAnswerItem = {
    name: 'userAnswerItem',
    dependencies: ['answer','question'],
    fragment: `
        fragment userAnswerItem on UserAnswerItem {
            addValue
            addText
            recnum
            version
            answer {
                ...answer
            }
            question {
                ...question
            }
        }
    `
};

const testFragments = function () {
    let fragmentsArray = [];
    fragmentsArray['answer'] = answer;
    fragmentsArray['questionGroup'] = questionGroup;
    fragmentsArray['questionGroupItem'] = questionGroupItem;
    fragmentsArray['question'] = question;
    fragmentsArray['userAnswer'] = userAnswer;
    fragmentsArray['userAnswerItem'] = userAnswerItem;

    return fragmentsArray;
};

module.exports = {
    testFragments
};