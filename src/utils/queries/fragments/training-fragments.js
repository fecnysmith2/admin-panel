const trainingPlanTemplateItem = {
    name: 'trainingPlanTemplateItem',
    dependencies: ['training'],
    fragment: `
        fragment trainingPlanTemplateItem on TrainingPlanTemplateItem {
            recnum
            timing
            training {
                ...training
            }
            weekmask
        }
    `
};

const trainingPlanTemplate = {
    name: 'trainingPlanTemplate',
    dependencies: ['hybridMemo','trainingPlanTemplateItem'],
    fragment: `
        fragment trainingPlanTemplate on TrainingPlanTemplate {
            trainingEventCount
            message {
            ...hybridMemo
            }
            recnum
            repeat
            trainerAssign {
                recnum
            }
            userAssign {
                recnum
            }
            validFrom
            name {
                ...hybridMemo
            }
            note {
                ...hybridMemo
            }
            trainingPlanTemplateItems {
                ...trainingPlanTemplateItem
            }
            parentPlanTemplate {
                name {
                    ...hybridMemo
                }
                recnum
                repeat
                note {
                    ...hybridMemo
                }
            }
        }
    `
};

const training = {
    name: 'training',
    dependencies: ['hybridMemo','trainingElement'],
    fragment: `
        fragment training on Training {
            cooper
            condtest
            name {
                ...hybridMemo
            }
            pulseZoneGroup
            recnum
            requireOrder
            trainingElements {
                ...trainingElement
            }
            trainingProg
        }
    `
};

const trainingElement = {
    name: 'trainingElement',
    dependencies: ['exercise'],
    fragment: `
        fragment trainingElement on TrainingElement {
            rest
            repeat
            recnum
            version
            ordernum
            exercise {
                ...exercise
            }
        }
    `
};

const trainingProgs = {
    name: 'trainingProgs',
    dependencies: ['hybridMemo','product','attachment'],
    fragment: `
        fragment trainingProgs on TrainingProgs {
            version
            programStart
            maxWeek
            memoName {
                ...hybridMemo
            }
            memoNote {
                ...hybridMemo
            }
            recnum
            registEnd
            registStart
            trainingProg
            programEnd
            product {
                ...product
            }
            resident
            attachment {
                ...attachment
            }
        }
    `
};

const trainingProgsEvent = {
    name: 'trainingProgsEvent',
    dependencies: ['hybridMemo','emailTemplate'],
    fragment: `
        fragment trainingProgsEvent on TrainingProgsEvent {
            additionValue
            trainingProgs {
                recnum
            }
            progsEvent
            messageKind
            message {
                ...hybridMemo
            }
            emailRepeat
            reference
            correlateMode
            obligate
            expiration
            messagePush {
                ...hybridMemo
            }
            recnum
            eventValue
            setValue
            emailId {
                ...emailTemplate
            }
        }
    `
};

const trainingProgsEventPrecond = {
    name: 'trainingProgsEventPrecond',
    dependencies: ['trainingProgsEvent'],
    fragment: `
        fragment trainingProgsEventPrecond on TrainingProgsEventPrecond {
            logicalGroup
            trainingProgsEventCond {
                ...trainingProgsEvent
            }
            trainingProgsEvent {
                ...trainingProgsEvent
            }
            recnum
        }
    `
};

const trainingProgsGraf = {
    name: 'trainingProgsGraf',
    dependencies: ['hybridMemo'],
    fragment: `
        fragment trainingProgsGraf on TrainingProgsGraf {
            rowNumber
            stepletter
            step
            lineNumber
            recnum
            memoName {
                ...hybridMemo
            }
        }
    `
};

const trainingProgsGrafJunction = {
    name: 'trainingProgsGrafJunction',
    dependencies: ['trainingProgsGraf'],
    fragment: `
        fragment trainingProgsGrafJunction on TrainingProgsGrafJunction {
            logicalGroup
            junctionValue
            recnum
            junctionMode
            trainingProgs {
                recnum
            }
            trainingProgsGraf {
                ...trainingProgsGraf
            }
        }
    `
};

const trainingProgsCondspec = {
    name: 'trainingProgsCondspec',
    dependencies: [],
    fragment: `
        fragment trainingProgsCondspec on TrainingProgsCondspec {
            levelValue
            trainingProgs {
                recnum
            }
            trainingProgsGraf {
                recnum
                step
                stepletter
            }
            levelMin
            recnum
        }
    `
};

const trainingProgsStepchange = {
    name: 'trainingProgsStepchange',
    dependencies: [],
    fragment: `
        fragment trainingProgsStepchange on TrainingProgsStepchange {
            stepchangeVariables
            recnum
            valueNumber
            orderNum
        }
    `
};

const executedTrainingShort = {
    name: 'executedTrainingShort',
    dependencies: [],
    fragment: `
        fragment executedTrainingShort on ExecutedTraining {
            recnum
            execStart(orderBy: ASC)
            execEnd
            nettoTime
            trainingSource
        }
    `
};

const executedTraining = {
    name: 'executedTraining',
    dependencies: ['hybridMemo','exerciseItem'],
    fragment: `
        fragment executedTraining on ExecutedTraining {
            recnum
            execStart
            execEnd
            nettoTime
            memoFeedback {
                ...hybridMemo
            }
            memo {
                ...hybridMemo
            }
            averageSpeed
            outdoor
            trainingSource
            difficulty
            executedCardios {
                intensivePercent
                length
                recnum
                numberOfChanges
            }
            pulseData {
                trainingId
                latitude
                pulse
                anonymousTrainingId
                yymm
                deltaT
                longitude
                altitude
                estimated
                steps
            }
            memoLocation {
                ...hybridMemo
            }
            trainingMovetype
            executedItems {
                recnum
                repeat
                exerciseItem {
                    ...exerciseItem
                }
            }
            groupTraining {
                recnum
                comment {
                    ...hybridMemo
                }
                room
                start
                end
                name {
                    ...hybridMemo
                }
                maxPersonCount
            }
            training {
                recnum
                name {
                    ...hybridMemo
                }
                trainingProg
                cooper
            }
            repeat
            caloriesBurnt
            goalType
            goalTypeLocalized
            goalValue
            groupTrJoin
            distance
            migratedAnonymousTrainingId
        }
    `
};

const trainingFragments = function () {
    let fragmentsArray = [];
    fragmentsArray['trainingPlanTemplateItem'] = trainingPlanTemplateItem;
    fragmentsArray['trainingPlanTemplate'] = trainingPlanTemplate;
    fragmentsArray['training'] = training;
    fragmentsArray['trainingElement'] = trainingElement;
    fragmentsArray['trainingProgs'] = trainingProgs;
    fragmentsArray['trainingProgsEvent'] = trainingProgsEvent;
    fragmentsArray['trainingProgsEventPrecond'] = trainingProgsEventPrecond;
    fragmentsArray['trainingProgsGraf'] = trainingProgsGraf;
    fragmentsArray['trainingProgsGrafJunction'] = trainingProgsGrafJunction;
    fragmentsArray['trainingProgsCondspec'] = trainingProgsCondspec;
    fragmentsArray['trainingProgsStepchange'] = trainingProgsStepchange;
    fragmentsArray['executedTrainingShort'] = executedTrainingShort;
    fragmentsArray['executedTraining'] = executedTraining;

    return fragmentsArray;
};

module.exports = {
    trainingFragments
};