const user = {
    name: 'user',
    dependencies: ['localizedWeight','localizedLength','userEmail','attachment','userAssign','userContact', 'userAddress'],
    fragment: `
        fragment user on User {
            abbrev
            addresses {
                    ...userAddress
            }
            birthDate
            bmi
            bodyWeight {
                ...localizedWeight
            }
            bondBalance
            country
            contacts {
                ...userContact
            }
            dateFormat
            defaultLengthUnit
            defaultWeightUnit
            dependentPartner {
                recnum
                login
                test
                recnum
                country
                operator
                dateFormat
                defaultLengthUnit
                infType
                defaultWeightUnit
                language
                sex
                bondBalance
                nickName
                maxPromoEmailsPerWeek
                birthDate
            }
            emails {
                ...userEmail
            }
            fbAccessToken
            fbUserId
            fbTokenExpire
            height {
                ...localizedLength
            }
            infType
            language
            login
            maxPromoEmailsPerWeek
            nickName
            operator
            pulseZoneGroup
            recnum
            simpleUserid
            sex
            test
            userAssigns {
                ...userAssign
            }
            waistline
        }
    `
};

const calendarEntry = {
    name: 'calendarEntry',
    dependencies: ['hybridMemo','trainingPlanTemplateItem'],
    fragment: `
        fragment calendarEntry on CalendarEntry {
            calendarId
            calendarType
            finish
            memo {
                ...hybridMemo
            }
            recnum
            reference
            room
            start
            trainingKind
            trainingPlanTemplateItem {
                ...trainingPlanTemplateItem
            }
        }
    `
};

const userAssign = {
    name: 'userAssign',
    dependencies: ['attachment','userAddress','userAssignInside','userEmail'],
    fragment: `
        fragment userAssign on UserAssign {
            internalUserData {
                ...userAssignInside
            }
            userTitle
            middleName
            recnum
            firstName
            userType
            version
            lastName
            attachment {
                ...attachment
            }
            mainAddress {
                ...userAddress
            }
            mainEmail {
                ...userEmail
            }
        }
    `
};

const userAssignInside = {
    name: 'userAssignInside',
    dependencies: [],
    fragment: `
        fragment userAssignInside on UserAssignInside {
            userSubtype
            bornFirstName
            bornLastName
            recnum
            finishAmbassador
            transparent
            startAmbassador
        }
    `
};

const userAddress = {
    name: 'userAddress',
    dependencies: ['hybridMemo'],
    fragment: `
        fragment userAddress on UserAddress {
            active
            address
            addressType
            addrNumber
            building
            city
            country
            directionName
            door
            floor
            lotNumber
            memo {
                ...hybridMemo
            }
            user {
                recnum
            }
            placeType
            recnum
            state
            township
            zip
        }
    `
};

const userEmail = {
    name: 'userEmail',
    dependencies: [],
    fragment: `
        fragment userEmail on UserEmail {
            disabledStart
            email
            emailNote
            emailSendDate
            enabledStart
            promotionEnabled
            recnum
        }
    `
};

const userContact = {
    name: 'userContact',
    dependencies: [],
    fragment: `
        fragment userContact on UserContact {
            email
            emailNote
            extension
            firstName
            lastName
            namePrefix
            phoneNote
            phoneNumber
            phoneType
            position
            promotionEnabled
            recnum
            sex
        }
    `
};

const userHealthDataWrapper = {
    name: 'userHealthDataWrapper',
    dependencies: [],
    fragment: `
        fragment userHealthDataWrapper on UserHealthDataWrapper {
          antropCateg
          healthCateg
          date
          value
          isFirstData
          validValue
        }
    `
};

const userTokenWrapper = {
    name: 'userTokenWrapper',
    dependencies: ['user','oAuth2Token'],
    fragment: `
        fragment userTokenWrapper on UserTokenWrapper {
            user {
                ...user
            }
            initialOAuth2Token {
                ...oAuth2Token
            }
        }
    `
};

const userHealthData = {
    name: 'userHealthData',
    dependencies: [],
    fragment: `
        fragment userHealthData on UserHealthData {
            start
            value
            recnum
            healthCateg
        }
    `
};

const userSubscription = {
    name: 'userSubscription',
    dependencies: ['product'],
    fragment: `
        fragment userSubscription on UserSubscription {
            deactivatedPlanDate
            version
            sales
            subCycle
            recnum
            deactivatedDate
            activate
            prolongation
            product {
                ...product
            }
        }
    `
};

const userAssignProg = {
    name: 'userAssignProg',
    dependencies: ['trainingProgs'],
    fragment: `
        fragment userAssignProg on UserAssignProg {
            abandonmentDate
            blocked
            closeDate
            finishDate
            pause
            permitted
            pulseZoneGroup
            recnum
            registDate
            startDate
            trainingProgs {
                ...trainingProgs
            }
            userAssign {
                recnum
                firstName
                lastName
            }
        }
    `
};

const userFragments = function () {
    let fragmentsArray = [];
    fragmentsArray['user'] = user;
    fragmentsArray['calendarEntry'] = calendarEntry;
    fragmentsArray['userAssign'] = userAssign;
    fragmentsArray['userAssignInside'] = userAssignInside;
    fragmentsArray['userAddress'] = userAddress;
    fragmentsArray['userEmail'] = userEmail;
    fragmentsArray['userContact'] = userContact;
    fragmentsArray['userHealthDataWrapper'] = userHealthDataWrapper;
    fragmentsArray['userTokenWrapper'] = userTokenWrapper;
    fragmentsArray['userHealthData'] = userHealthData;
    fragmentsArray['userSubscription'] = userSubscription;
    fragmentsArray['userAssignProg'] = userAssignProg;

    return fragmentsArray;
};

module.exports = {
    userFragments
};