const getToplistQuery = {
    queryName: 'Toplist',
    query: `
        query Toplist {
            Toplist : General_getScoreToplist {
                place
                winners
                score
            }
        }
    `,
    cacheEnabled: false,
    mutation: false
};


const unsubscribeNewsletter = function (userEmail) {
    return {
        queryName: 'unsubscribeNewsletter',
        query: `mutation m {
                  UserEmail_unsubscribeNewsletter(email:"${userEmail}")
                }`,
        cacheEnabled: false,
        mutation: true
    }
};

const getSumExecutedQuery = {
    queryName: 'SumExecuted',
    query: `
        query SumExecuted {
            SumExecuted : General_getSummaExecuted {
                distanceRunAndOther
                distanceBicycle
                caloriesBurnt
                timeDays
                timeHours
                timeMinutes
            }
        }
    `,
    cacheEnabled: false,
    mutation: false
};

const getSetStateMutation = {
    queryName: 'General_setStateAndSendEmailForCurrentAssign',
    query: `
        mutation General_setStateAndSendEmailForCurrentAssign {
            General_setStateAndSendEmailForCurrentAssign
        }
    `,
    cacheEnabled: false,
    mutation: true
};

const getBonusCouponDataQuery = {
    queryName: 'CouponData',
    query: `
        query CouponData {
            Coupons_getBonusCouponCount(happeningId:"410257")
            Coupons_getBonusCouponMaxCount(happeningId:"410257")
        }
    `,
    cacheEnabled: false,
    mutation: false
};

const generateCouponMutation = {
    queryName: 'Coupons_makeBonusCouponForCurrentAssign',
    query: `
        mutation Coupons_makeBonusCouponForCurrentAssign {
         Coupon : Coupons_makeBonusCouponForCurrentAssign(happeningId:"410257")
        }
    `,
    cacheEnabled: false,
    mutation: false
};

const alreadyAskedForCouponQuery = {
    queryName: 'Coupons_isRequestBonusCoupon',
    query: `
        query  Coupons_isRequestBonusCoupon {
            Coupons_isRequestBonusCoupon(happeningId:"410257")
        }
    `,
    cacheEnabled: false,
    mutation: false
};

const sendCouponMutation = function (couponCode) {
    return {
        queryName: 'Coupons_setCouponRedemption',
        query: `
            mutation Coupons_setCouponRedemption {
                Coupons_setCouponRedemption(couponCode: "${couponCode}") {
                    message
                    errorMessage
                }
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

module.exports = {
    getToplistQuery,
    getSumExecutedQuery,
    getSetStateMutation,
    getBonusCouponDataQuery,
    generateCouponMutation,
    alreadyAskedForCouponQuery,
    sendCouponMutation,
    unsubscribeNewsletter
};