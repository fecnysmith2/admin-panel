import generator from 'generate-password';
import {getLoginInfo} from "../helpers";

const getUserQuery = {
    queryName: 'getCurrentUser',
    dependencies: ['user'],
    query: `query getCurrentUser {
        User: User_getCurrent {
            ...user
        }
    }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};

const getLoginTicketQuery = {
    queryName: 'loginTicket',
    query: `mutation loginTicket {
        LoginTicket_generateForCurrentUser {
            validUntil
            ticketKey
            recnum
        }
    }`,
    variables: '',
    cacheEnabled: false,
    mutation: true
};

const getCalendarEntryArrayMutation = function (calendarEntries = []) {
    return {
        queryName: 'CalendarEntry_saveForCurrentUser',
        dependencies: ['hybridMemo', 'calendarEntry'],
        query: `mutation CalendarEntry_saveForCurrentUser {
            CalendarEntry_saveForCurrentUser(calendarEntries: ${JSON.stringify(JSON.stringify(calendarEntries))}) {
                ...calendarEntry
            }
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

const getCalendarEntryQuery = {
    queryName: 'CalendarEntry',
    dependencies: ['calendarEntry'],
    query: `query CalendarEntry {
        CalendarEntry_getForCurrentUser {
            ...calendarEntry
        }
    }`,
    cacheEnabled: false,
    mutation: false
};

const getUserTestIdByCalendarEntryQuery = function (recnum) {
    return {
        queryName: 'getUserTestIdByCalendarEntryQuery',
        query: `query getUserTestIdByCalendarEntryQuery {
            UserAnswerWrapper_getUserAnswerFromCalendarEntry(calendarRecnum: "${recnum}") {
                userAnswerRecnum
                description
            }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

const fillMissingFieldsForUser = function (user) {
    if (user.emails) {
        if (user.login === undefined) {
            const name = user.emails[0].email.substring(0, user.emails[0].email.lastIndexOf('@')).replace(/\.|\+/gi, '');
            const nameSuffix = generator.generate({
                length: 24 - name.length,
                numbers: true,
                strict: true
            });
            user.login = name + nameSuffix;
        }

        if (user.passwords === undefined) {
            const password = generator.generate({
                length: 12,
                numbers: true,
                strict: true
            });

            user.passwords = [{
                password: password
            }];
        }

        user.userAssigns[0].userType = 'P';

        user.acceptedContracts = [{
            contractType: 0
        }];
    }
    return user;
};

const getRegisterUserMutation = function (userData, groupsId = '-1', inviteKey = '') {
    userData = fillMissingFieldsForUser(userData);
    return {
        queryName: 'registerPage',
        dependencies: ['userTokenWrapper'],
        query: `mutation registerUser($user: String, $reCaptchaResponse: String, $backURL: String, $groupsId:Long, $inviteKey: String, $loginInfo: String) {
            User_register(uData: $user, reCaptchaResponse: $reCaptchaResponse, backURL: $backURL, groupsId: $groupsId, inviteKey: $inviteKey, loginInfo: $loginInfo) {
                ...userTokenWrapper
            }
        }`,
        variables: JSON.stringify({
            user: JSON.stringify(userData),
            reCaptchaResponse: '',
            groupsId: groupsId,
            inviteKey: inviteKey,
            backURL: '/',
            loginInfo: JSON.stringify(getLoginInfo())
        }),
        mutation: true
    }
};

const getCheckFacebookUserIdAndEmailQuery = function (email, fbUserId, fbAccessToken, fbTokenExpire) {
    return {
        queryName: 'User_connectFacebookDataToUser',
        dependencies: ['userTokenWrapper'],
        query: `query User_checkFacebookUserIdAndEmail {
            UserTokenWrapper : User_checkFacebookUserIdAndEmail(email: "${email}", fbUserId: "${fbUserId}", fbAccessToken: "${fbAccessToken}", fbTokenExpire: "${fbTokenExpire}", loginInfo: ${JSON.stringify(JSON.stringify(getLoginInfo()))}) {
                ...userTokenWrapper
            }
        }`,
        mutation: true
    }
};

const getDisconnectFacebookDataToUserMutation = {
    queryName: 'User_connectFacebookDataToUser',
    dependencies: ['user'],
    query: `mutation User_connectFacebookDataToUser {
        User : User_disconnectFacebookDataToUser {
            ...user
        }
    }`,
    mutation: true
};

const getConnectFacebookDataToUserMutation = function (fbUserId, fbAccessToken, fbTokenExpire) {
    return {
        queryName: 'User_connectFacebookDataToUser',
        dependencies: ['user'],
        query: `mutation User_connectFacebookDataToUser {
            User : User_connectFacebookDataToUser(fbUserId: "${fbUserId}", fbAccessToken: "${fbAccessToken}", fbTokenExpire: "${fbTokenExpire}") {
                ...user
            }
        }`,
        mutation: true
    }
};

const getAuthenticateUserByFacebookMutation = function (userData, groupsId = '-1', inviteKey = '') {
    userData = fillMissingFieldsForUser(userData);
    return {
        queryName: 'User_authenticateByFacebook',
        dependencies: ['user', 'oAuth2Token'],
        query: `mutation User_authenticateByFacebook($user: String, $reCaptchaResponse: String, $backURL: String, $groupsId:Long, $inviteKey: String, $loginInfo: String) {
            User_authenticateByFacebook(user: $user, reCaptchaResponse: $reCaptchaResponse, backURL: $backURL, groupsId: $groupsId, inviteKey: $inviteKey, loginInfo: $loginInfo) {
                user {
                    ...user
                }
                initialOAuth2Token {
                    ...oAuth2Token
                }
            }
        }`,
        variables: JSON.stringify({
            user: JSON.stringify(userData),
            reCaptchaResponse: '',
            groupsId: groupsId,
            inviteKey: inviteKey,
            backURL: '/',
            loginInfo: JSON.stringify(getLoginInfo())
    }),
    mutation: true
}
};

const getPersonalDataQuery = {
    queryName: 'personalData',
    dependencies: ['userAssign'],
    query: `query getPersonalData {
        DashboardData_getForCurrentUser {
            regDate
            caloriesBurnt
            totalDistance
            coinBalance
            spendableCoinBalance
            numberOfTrainings
            totalWorkoutTimeSec
            score
        }
        UserAssign_getCurrentUserAssign {
            ...userAssign
        }
    }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};

const getHealthTestIdQuery = {
    queryName: 'healthTestId',
    dependencies: ['userAnswer'],
    query: `query healthTestId {
        UserAnswerEvaluationText(ownedObjectsOnly: true) {
            userAnswer {
                ...userAnswer
            }
        }
    }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};

const getHealthTestFilledQuery = {
    queryName: 'healthTestFilled',
    dependencies: ['userAnswer'],
    query: `query healthTestFilled {
        UserAnswer(ownedObjectsOnly: true) {
            ...userAnswer
        }
    }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};

const getEvaluationsQuery = {
    queryName: 'userAnswerEvaluation',
    dependencies: ['userAnswer'],
    query: `query userAnswerEvaluation {
        UserAnswerEvaluationText(ownedObjectsOnly: true) {
            userAnswer {
                ...userAnswer
            }
        }
    }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};

const getHealthCategoryQuery = {
    queryName: 'healthCateg',
    query: `query healthCateg {
        DataValues_getDataValueByName(variableGroupName: "health_categ")
    }`,
    variables: '',
    cacheEnabled: true,
    mutation: false
};

const getUserHealthDataMutation = function (data) {
    return {
        queryName: 'userHealthDataMutation',
        dependencies: ['userHealthData'],
        query: `mutation userHealthDataMutation($data: String) {
            UserHealthData_submitForCurrentUser(data: $data) {
                ...userHealthData
            }
        }`,
        variables: JSON.stringify({
            data: JSON.stringify(data)
        }),
        mutation: true
    }
};

const getUserHealthDataForDifferentUserMutation = function (userRecnum, data) {
    return {
        queryName: 'userHealthDataMutation',
        dependencies: ['userHealthData'],
        query: `mutation userHealthDataMutation($data: String) {
            UserHealthData_submitForDifferentUser(userId: "${userRecnum}", data: $data) {
                ...userHealthData
            }
        }`,
        variables: JSON.stringify({
            data: JSON.stringify(data)
        }),
        mutation: true
    }
};

const getHealthCategsQuery = function () {
    return {
        queryName: 'healthCategs',
        query: `query healthCategs {
            DataValues_getDataValueByGroupId(variableGroupId: "121")
        }`,
        cacheEnabled: false,
        mutation: false
    }
};


const getUserHealthDataQuery = function (columnNumber = 10, offset = 1, antropCateg = 1, healthCategs = "null", enableNull = false) {
    return {
        queryName: 'userHealthData',
        dependencies: ['userHealthDataWrapper'],
        query: `query userHealthData {
            UserHealthData_getForCurrentUser(columnNumber: ${columnNumber}, offset: ${offset}, antropCateg: ${antropCateg}, healthCateg: ${JSON.stringify(healthCategs)}, enableNull: ${enableNull}) {
                ...userHealthDataWrapper
            }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

const getUserHealthDataForDifferentQuery = function (userRecnum, columnNumber = 10, offset = 1, antropCateg = 1, healthCategs = "null", enableNull = false) {
    return {
        queryName: 'userHealthData',
        dependencies: ['userHealthDataWrapper'],
        query: `query userHealthData {
            UserHealthData_getForDifferentUser(userRecnum: "${userRecnum}", columnNumber: ${columnNumber}, offset: ${offset}, antropCateg: ${antropCateg}, healthCateg: ${JSON.stringify(healthCategs)}, enableNull: ${enableNull}) {
                ...userHealthDataWrapper
            }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};
const getUserHealthTabs = function () {
    return {
        queryName: 'UserHealthTabs',
        query: `query q {
             DataValues_getDataValueByGroupId(variableGroupId: "160")
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

const getHealtTestPageQuery = function (pageNumber) {
    return {
        queryName: 'getHealtTestPage',
        dependencies: ['questionGroup'],
        query: `query getHealtTestPage {
            QuestionGroup_getForCurrentUser(questionGroupId: "2", pageNum: ${pageNumber}) {
                questionGroup {
                    ...questionGroup
                }
                totalPages
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    };
};

const getFrontendComponentGraphVertexQuery = function (callName) {
    return {
        queryName: `frontendComponentGraphVertex_${callName}`,
        dependencies: ['hybridMemo'],
        query: `query frontendComponentGraphVertex_${callName} {
          FrontendComponentGraphVertex {
            component(search: "callName==${callName}") {
              ...comp
            }
            children {
              many
              component {
                ...comp
              }
            }
          }
        }
    
        fragment comp on FrontendComponent {
            queryName
          callName
          queryParamName
          memo {
            ...hybridMemo
          }
          recnum
          nameText
          dataObjectConnections {
            recnum
            ordernum
            location
            required
            visible
            dataObject {
              captionText
              memo {
                ...hybridMemo
              }
              fieldType
              clientSideFieldType
              dataValueType
              clientSideFieldName
              attributes {
                value
                type
              }
            }
          }
        }`,
        variables: '',
        cacheEnabled: true,
        mutation: false
    }
};

const getNewsLetterSubscribeMutation = function (name = '', email, type = '') {
    return {
        queryName: 'userHealthDataMutation',
        query: `mutation userHealthDataMutation {
            User_subscribeToNewsLetter(name: "${name}", email: "${email}", type: "${type}")
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getCompanyNewsLetterSubscribeMutation = function (company, name, email, phone, type) {
    return {
        queryName: 'userHealthDataMutation',
        query: `mutation userHealthDataMutation {
            User_subscribeCompanyToNewsLetter(company: "${company}", name: "${name}", email: "${email}", phone: "${phone}", type: "${type}")
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: true
    }
};

const getTestResults = function (testRecnum) {
    return {
        queryName: 'getTestResults',
        query: `query getTestResults {
          UserAnswerEvaluationText {
            evaluationText {
              ordernum
              type
              text
              emptyLine
            }
            userAnswer(search: "recnum==${testRecnum}") {
              recnum
            }
          }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    }
};

const getCreditCardProcessMutation = function (paymentData) {
    return {
        queryName: 'creditCardProcess',
        dependencies: ['creditCardProcess'],
        query: `mutation creditCardProcess($creditCardProcess: String) {
            CreditCardProcess_add(creditCardProcess: $creditCardProcess) {
                ...creditCardProcess
            }
        }`,
        variables: JSON.stringify({
            creditCardProcess: JSON.stringify(paymentData)
        }),
        mutation: true
    }
};

const getCreditCardProcessEventByPayReferenceNumberQuery = function (referenceNumber) {
    return {
        queryName: 'CreditCardProcessEvent_getByPayReferenceNumber',
        dependencies: ['creditCardProcessEvent'],
        query: `query CreditCardProcessEvent_getByPayReferenceNumber {
            CreditCardProcessEvent_getByPayReferenceNumber(referenceNumber: "${referenceNumber}") {
                ...creditCardProcessEvent
            }
        }`,
        mutation: false,
        cacheEnabled: false
    }
};

const getCreditCardProcessEventMutation = function (paymentData) {
    return {
        queryName: 'creditCardProcessEvent',
        query: `mutation creditCardProcessEvent($creditCardProcessEvent: String, $token: String) {
            CreditCardProcessEvent_add(creditCardProcessEvent: $creditCardProcessEvent, token: $token) {
                ...creditCardProcessEvent
            }
        }`,
        variables: JSON.stringify({
            creditCardProcessEvent: JSON.stringify(paymentData)
        }),
        mutation: true
    }
};

const getUserSubscription = {
    queryName: 'getUserSubscription',
    dependencies: ['userSubscription'],
    query: `query getUserSubscription {
        UserSubscription_getCurrentUserSubscription {
            ...userSubscription
        }
    }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};

const getTrainingProgsForUserByProgId = function (progId) {

    return {
        queryName: 'getUserSubscription',
        query: `query TrainingProgs_getForCurrentUser {
            TrainingProgs_getForCurrentUser(trainingProgId: ${progId}) {
                productId
                trainingProgs {
                    registEnd
                    memoNameMemo
                    version
                    programStart
                    memoNoteMemo
                    resident
                    trainingProg
                    maxWeek
                    registStart
                    recnum
                    programEnd
                }
            }
        }`,
        variables: '',
        cacheEnabled: false,
        mutation: false
    }
};

const getUserAssignProgsEvents = {
    queryName: 'getUserAssignProgsEvents',
    query: `query getUserAssignProgsEvents {
      UserAssignProgsEvents_getForCurrentAssign {
        trainingProgId
        trainingProgName
        errorMessage
      } 
    }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};

const getUserSubscriptionMutation = function (subscription) {
    return {
        queryName: 'creditCardProcessEvent',
        dependencies: ['userSubscription'],
        query: `mutation addUserSubscription($userSubscription: String) {
            UserSubscription_add(userSubscription: $userSubscription) {
                ...userSubscription
            }
        }`,
        variables: JSON.stringify({
            userSubscription: JSON.stringify(subscription)
        }),
        mutation: true
    }
};

const getCreditCardProcessInvalidateMutation = function (paymentData) {
    return {
        queryName: 'creditCardProcessInvalidate',
        dependencies: ['creditCardProcess'],
        query: `mutation creditCardProcessInvalidate($creditCardProcess: String) {
            CreditCardProcess_invalidate(creditCardProcess: $creditCardProcess) {
                ...creditCardProcess
            }
        }`,
        variables: JSON.stringify({
            creditCardProcess: JSON.stringify(paymentData)
        }),
        mutation: true
    }
};

const getCreditCardProcessQuery = {
    queryName: 'creditCardProcessQuery',
    dependencies: ['creditCardProcess'],
    query: `query creditCardProcess {
        CreditCardProcess {
          ...creditCardProcess
        }
    }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};


const getProductQuery = {
    queryName: 'productQuery',
    dependencies: ['product'],
    query: `query product{
        Product_getAll {
            ...product
      	}
      }`,
    variables: '',
    cacheEnabled: false,
    mutation: false
};

const getHybridMemoByMemoIdQuery = function (recnum) {
    return {
        queryName: 'HybridMemo',
        dependencies: ['hybridMemo'],
        query: `query HybridMemo {
            HybridMemo(search: "recnum==${recnum}") {
                ...hybridMemo
            }
        }`,
        variables: '',
        cacheEnabled: true,
        mutation: false
    }
};

const getHybridMemoListByMemoIdListQuery = function (memoIds = [], textListKey = 'Index') {
    return {
        queryName: 'HybridMemo_getListByMemoIds' + textListKey,
        dependencies: ['hybridMemo'],
        query: `
        query HybridMemo_getListByMemoIds {
            HybridMemo_getListByMemoIds(memoIds: ${JSON.stringify(JSON.stringify(memoIds))}) {
                ...hybridMemo
            }
        }`,
        cacheEnabled: true,
        mutation: false
    }
};

const getTextHeaderListByTextIdListQuery = function (textIds = [], textListKey = 'Index') {
    return {
        queryName: 'TextHeader_getListByTextIds' + textListKey,
        dependencies: ['textHeader'],
        query: `query TextHeader_getListByTextIds {
            TextHeader_getListByTextIds(textIds: ${JSON.stringify(JSON.stringify(textIds))}) {
                ...textHeader
            }
        }`,
        cacheEnabled: true,
        mutation: false
    }
};


const getUserFromInviteKeyQuery = function (inviteKey = "") {
    return {
        queryName: 'User_getUserAssignFromInviteKey' + inviteKey,
        query: `
            query User_getUserFromInviteKey {
                UserInviteWrapper_getUserFromInviteKey(inviteKey: "${inviteKey}") {
                    trainer {
                        sex
                        nickName
                        userAssigns {
                          attachment {
                            recnum
                          }
                          firstName
                          lastName
                        }
                    }
                    userEmail
                }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

const loadUnits = function () {
    return {
        queryName: 'Load_Units',
        query: `query Unit_Types {
            DataValues_getDataValueByName(variableGroupName: "unit_type")
        }`,
        cacheEnabled: true,
        mutation: false
    }
};

const getMissingInformations = function () {
    return {
        queryName: 'User_getMissingRegDateForCurrentUser',
        query: `query q {
            User_getMissingRegDateForCurrentUser {
                tableName
                fieldName
                text
                keyfieldName
            }
        }`,
        cacheEnabled: true,
        mutation: false
    }
};

const cloneExerciseByPhase = function (recnum, type) {
    return {
        queryName: 'User_getMissingRegDateForCurrentUser',
        query: `mutation m {
                  Exercise_cloneExerciseByPhase(exerciseId:"${recnum}",phase:"${type}")
                }`,
        cacheEnabled: false,
        mutation: true
    }
};

module.exports = {
    getUserQuery,
    getCalendarEntryQuery,
    getCalendarEntryArrayMutation,
    getRegisterUserMutation,
    getAuthenticateUserByFacebookMutation,
    getDisconnectFacebookDataToUserMutation,
    getConnectFacebookDataToUserMutation,
    getCheckFacebookUserIdAndEmailQuery,
    getLoginTicketQuery,
    getPersonalDataQuery,
    getHealthTestIdQuery,
    getEvaluationsQuery,
    getHealthCategoryQuery,
    getUserHealthDataMutation,
    getUserHealthDataQuery,
    getFrontendComponentGraphVertexQuery,
    getNewsLetterSubscribeMutation,
    getCompanyNewsLetterSubscribeMutation,
    getHealtTestPageQuery,
    getTestResults,
    getCreditCardProcessMutation,
    getCreditCardProcessEventByPayReferenceNumberQuery,
    getProductQuery,
    getUserSubscriptionMutation,
    getTrainingProgsForUserByProgId,
    getCreditCardProcessEventMutation,
    getCreditCardProcessInvalidateMutation,
    getCreditCardProcessQuery,
    getHybridMemoByMemoIdQuery,
    getHealthTestFilledQuery,
    getUserTestIdByCalendarEntryQuery,
    getHybridMemoListByMemoIdListQuery,
    getTextHeaderListByTextIdListQuery,
    getUserHealthDataForDifferentQuery,
    getUserHealthDataForDifferentUserMutation,
    getUserFromInviteKeyQuery,
    getUserSubscription,
    getUserAssignProgsEvents,
    getHealthCategsQuery,
    getUserHealthTabs,
    loadUnits,
    getMissingInformations,
    cloneExerciseByPhase
};
