const getPhoneTypesQuery = {
    queryName: 'phoneTypes',
    query: `query phoneTypes {
        DataValues_getDataValueByName(variableGroupName: "phone_type", orderBy: "Description ASC")
    }`,
    cacheEnabled: false,
    mutation: false
};

const getLanguagesQuery = {
    queryName: 'language',
    query: `query getLanguage {
                DataValues_getDataValueByName(variableGroupName: "language", orderBy: "Description ASC")
            }`,
    mutation: false,
    cacheEnabled: true
};

const getParentGroupQuery = {
    queryName: 'variablegroup',
    query: `query getVariableGroups {
          VariableGroup {
            recnum
            dedicatedName
            groupNameText
          }
        }`,
    variables: '',
    mutation: false,
    cacheEnabled: true
};

const getInfTypeQuery = {
    queryName: 'infType',
    query: `query getInftype {
          DataValues_getDataValueByName(variableGroupName: "inf_type", orderBy: "Description ASC")
        }`,
    variables: '',
    mutation: false,
    cacheEnabled: true
};

const getPartnerGroupQuery = {
    queryName: 'partnerGroup',
    dependencies: ['hybridMemo'],
    query: `query getPartnerGroups {
        User_getGroupByUserId(userId: "1000747") {
          recnum
          hybridMemo {
            ...hybridMemo
          }
        }
    }`,
    variables: '',
    mutation: false,
    cacheEnabled: false
};

const getCountryQuery = {
    queryName: 'country',
    query: `query getCountry {
          DataValues_getDataValueByName(variableGroupName: "country", orderBy: "CountryName ASC")
        }`,
    variables: '',
    mutation: false,
    cacheEnabled: true
};

const getAddressTypeQuery = {
    queryName: 'addressType',
    query: `query getAddressType {
          DataValues_getDataValueByName(variableGroupName: "address_type", orderBy: "Description ASC")
        }`,
    variables: '',
    mutation: false,
    cacheEnabled: true
};

const getPlaceTypeQuery = {
    queryName: 'placeType',
    query: `query getPlaceType {
          DataValues_getDataValueByName(variableGroupName: "place_type", orderBy: "Description ASC")
        }`,
    variables: '',
    mutation: false,
    cacheEnabled: true
};

const getHintQuery = {
    queryName: 'textHeader',
    dependencies: ['textHeader'],
    query: `query getTextHeader {
        TextHeader {
            ...textHeader
        }
    }`,
    variables: '',
    mutation: false,
    cacheEnabled: true,
    cacheTime: 5000
};

const getCoupledQuery = {
    queryName: 'textHeader',
    query: `query getTextHeader {
        TextHeader {
            ...textHeader
        }
    }`,
    variables: '',
    mutation: false,
    cacheEnabled: true,
    cacheTime: 5000
};

const getInputModeQuery = {
    queryName: 'inputMode',
    query: `query InputMode {
        DataValues_getDataValueByName(variableGroupName: "input_mode")
    }`,
    mutation: false,
    cacheEnabled: true,
    cacheTime: 1600000
};

const getDataTypeQuery = {
    queryName: 'dataType',
    query: `query DataType {
        DataValues_getDataValueByName(variableGroupName: "field_type")
    }`,
    mutation: false,
    cacheEnabled: true,
    cacheTime: 1600000
};

const getLanguageLocationQuery = {
    queryName: 'languageLocation',
    query: `query LanguageLocation {
        DataValues_getDataValueByName(variableGroupName: "language_loc")
    }`,
    mutation: false,
    cacheEnabled: true,
    cacheTime: 1600000
};

const queries = {
    language: getLanguagesQuery,
    parentGroup: getParentGroupQuery,
    infType: getInfTypeQuery,
    partnerGroup: getPartnerGroupQuery,
    country: getCountryQuery,
    addressType: getAddressTypeQuery,
    placeType: getPlaceTypeQuery,
    hint: getHintQuery,
    coupled: getCoupledQuery,
    phoneType: getPhoneTypesQuery,
    inputMode: getInputModeQuery,
    dataType: getDataTypeQuery,
    languageLocation: getLanguageLocationQuery,
};

module.exports = {
    getLanguagesQuery,
    getParentGroupQuery,
    getInfTypeQuery,
    getPartnerGroupQuery,
    getCountryQuery,
    getAddressTypeQuery,
    getPlaceTypeQuery,
    getHintQuery,
    getCoupledQuery,
    getPhoneTypesQuery,
    getInputModeQuery,
    getDataTypeQuery,
    getLanguageLocationQuery,
    queries
};