const getCurrentUserAddressesQuery = {
    queryName: 'userAddresses',
    dependencies: ['userAddress'],
    query: `query userAddresses {
        UserAssign(ownedObjectsOnly: true){
            mainAddress {
                ...userAddress
            }
        }
        UserAddress(ownedObjectsOnly: true) {
            ...userAddress
        }
    }`,
    cacheEnabled: false,
    mutation: false
};

const getUpdateUserAddressMutation = function (addressData, isMainAddress = false) {
    return {
        queryName: 'addressSetup',
        dependencies: ['userAddress'],
        query: `mutation userAddress($json: String, $isMainAddress: Boolean) {
            UserAddress_update(json: $json, isMainAddress: $isMainAddress) {
                ...userAddress
            }
        }`,
        variables: JSON.stringify({
            json: JSON.stringify(addressData),
            isMainAddress: isMainAddress
        }),
        mutation: true
    }
};

const deleteUserAddress = function (recnum) {
    return {
        queryName: 'addressDelete',
        dependencies: ['userAddress'],
        query: `mutation m {
            UserAddress_remove(recnum: "${recnum}") {
                ...userAddress
            }
        }`,
        mutation: true
    }
};

module.exports = {
    getCurrentUserAddressesQuery,
    getUpdateUserAddressMutation,
    deleteUserAddress
};
