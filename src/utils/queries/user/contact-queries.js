const getCurrentUserPhonesQuery = {
    queryName: 'currentUserPhones',
    dependencies: ['userContact'],
    query: `query CurrentUserPhones {
     	UserContact(ownedObjectsOnly: true) {
           ...userContact
	    }
    }`,
    cacheEnabled: false,
    mutation: false
};

const getUserContactSaveMutation = function (contact) {
    const param = JSON.stringify(JSON.stringify(contact));
    return {
        queryName: 'userContactMutation',
        dependencies: ['userContact'],
        query: `mutation userContactMutation{
            UserContact(json: ${param}) {
                ...userContact
            }
        }`,
        mutation: true
    }
};

const deleteUserContact = function (recnum) {
    return {
        queryName: 'deleteUserContact',
        dependencies: ['userContact'],
        query: `mutation m{
            UserContact_delete(recnum:"${recnum}") {
                ...userContact
            }
        }`,
        mutation: true
    }
};

module.exports = {
    getCurrentUserPhonesQuery,
    getUserContactSaveMutation,
    deleteUserContact
};
