import {getLoginInfo} from "../../helpers";
const getCurrentUserAddressesQuery = {
    queryName: 'userAddresses',
    dependencies: ['userAddress'],
    query: `query userAddresses {
        UserAssign(ownedObjectsOnly: true){
            mainAddress {
                ...userAddress
            }
        }
        UserAddress(ownedObjectsOnly: true) {
            ...userAddress
        }
    }`,
    cacheEnabled: false,
    mutation: false
};

const getUserEmailsMutation = function (email) {
    email = JSON.stringify(JSON.stringify(email));
    return {
        queryName: 'UserEmail',
        dependencies: ['userEmail'],
        query: `mutation UserEmail_addForCurrentUser {
            UserEmail : UserEmail_addForCurrentUser(userEmail: ${email}) {
                ...userEmail
            }
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

const getUserUpdateMutation = function (user) {
    user = JSON.stringify(JSON.stringify(user));
    return {
        queryName: 'User_update',
        dependencies: ['user'],
        query: `mutation User_update {
            User_update(json: ${user}) {
                ...user
            }
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

const getUserAssignUpdateMutation = function (userAssign) {
    userAssign = JSON.stringify(JSON.stringify(userAssign));
    return {
        queryName: 'UserAssign_update',
        dependencies: ['userAssign'],
        query: `mutation UserAssign_update {
            UserAssign_update(json: ${userAssign}) {
                ...userAssign
            }
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

const getPasswordResetMutation = function(passwordResetToken, newPassword) {
    const userPassword = {
        password: newPassword
    };
    return {
        queryName: 'UserPassword_executePasswordReset',
        dependencies: ['oAuth2Token'],
        query: `mutation UserPassword_executePasswordReset {
            UserPassword_executePasswordReset(passwordResetToken: "${passwordResetToken}", newPasswordObject: ${JSON.stringify(JSON.stringify(userPassword))}, loginInfo: ${JSON.stringify(JSON.stringify(getLoginInfo()))}) {
                initialOAuth2Token {
                    ...oAuth2Token
                }
            }
        }`,
        variables: null,
        cacheEnabled: false,
        mutation: true
    }
};

const getUserPasswordMutation = function(newPassword) {
    const userPassword = {
        password: newPassword
    };
    return {
        queryName: 'UserPassword_executePasswordChange',
        dependencies: ['user'],
        query: `mutation UserPassword_executePasswordChange {
            UserPassword_executePasswordChange(newPasswordObject: ${JSON.stringify(JSON.stringify(userPassword))}) {
                recnum
                start
                user(ownedObjectsOnly: true) {
                    ...user
                }
            }
        }`,
        variables: null,
        mutation: true
    }
};

const getUserProfileImageSetMutation = function (attachmentRecnum) {
    return {
        queryName: 'UserAssign_setProfileImageForCurrentUser',
        dependencies: ['userAssign'],
        query: `mutation UserAssign_setProfileImageForCurrentUser {
            UserAssign_setProfileImageForCurrentUser${attachmentRecnum !== null ? '(recnum: ' + JSON.stringify(attachmentRecnum) + ')' : ''} {
                ...userAssign
            }
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

const getUserSubscriptions = function () {
    return {
        queryName: 'UserSubscriptions_getList',
        dependencies: ['userSubscription'],
        query: `query SumExecuted {
            Subscription: UserSubscription_getCurrentUserSubscriptionWrappers {
                subscriptionName
                validFrom
                validTo
                userSubscription {
                    ...userSubscription
                }
            }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

const cancelUserSubscription = function (recnum) {
    return {
        queryName: 'UserSubscriptions_cancel',
        dependencies: ['userSubscription'],
        query: `mutation CancelSubscription {
            UserSubscription_cancelSubscriptionByRecnum(recnum:"${recnum}") {
                ...userSubscription
            }
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

const deleteUserEmail = function (recnum) {
    return {
        queryName: 'UserEmail_delete',
        dependencies: ['userEmail'],
        query: `mutation UserEmail {
            UserEmail_delete(recnum:"${recnum}") {
                ...userEmail
            }
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

module.exports = {
    getUserEmailsMutation,
    getCurrentUserAddressesQuery,
    getUserUpdateMutation,
    getUserAssignUpdateMutation,
    getPasswordResetMutation,
    getUserPasswordMutation,
    getUserProfileImageSetMutation,
    cancelUserSubscription,
    getUserSubscriptions,
    deleteUserEmail
};