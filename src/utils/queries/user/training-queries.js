const getExecutedTraining_getForCurrentUserCalendarQuery = {
    queryName: 'ExecutedTraining_getForCurrentUser',
    dependencies: ['executedTrainingShort'],
    query: `query ExecutedTraining_getForDifferentUser {
        ExecutedTraining(search: "trainingSource==2", ownedObjectsOnly: true) {
            ...executedTrainingShort
        }
    }`,
    cacheEnabled: false,
    mutation: false
};

const getExecutedTraining_getForUserByAssignCalendarQuery = function (userAssignRecnum) {
    return {
        queryName: 'ExecutedTraining_getForDifferentUser',
        dependencies: ['executedTrainingShort'],
        query: `query ExecutedTraining_getForDifferentUser {
            ExecutedTraining(search: "trainingSource==2") {
                ...executedTrainingShort
                userAssign(search: "recnum==${userAssignRecnum}") {
                    recnum
                }
            }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

const getExecutedTrainingWrapperByRecnum = function (recnum) {
    return {
        queryName: 'ExecutedTrainingWrapper_getForCurrentUserByRecnum' + recnum,
        dependencies: ['executedTraining'],
        query: `query ExecutedTrainingWrapper_getForCurrentUserByRecnum {
            ExecutedTrainingWrapper_getForCurrentUserByRecnum(recnum: "${recnum}") {
                fitPointer
                executedTraining {
                    ...executedTraining
                }
            }
        }`,
        cacheEnabled: true,
        mutation: false
    }
};

const getExecutedTrainingWrapperForDifferentUserByRecnum = function (userAssign, recnum) {
    return {
        queryName: 'ExecutedTrainingWrapper_getForDifferentUserByRecnum' + recnum,
        dependencies: ['hybridMemo','training'],
        query: `query ExecutedTrainingWrapper_getForDifferentUserByRecnum {
            ExecutedTrainingWrapper_getForDifferentUserByRecnum(userAssign: ${JSON.stringify(JSON.stringify(userAssign))} recnum: "${recnum}") {
                fitPointer
                executedTraining {
                    ...executedTraining
                }
            }
        }`,
        cacheEnabled: true,
        mutation: false
    }
};

const getTrainingWrapperByRecnum = function (recnum) {
    return {
        queryName: 'TrainingWrapper_getForCurrentUserByRecnum' + recnum,
        dependencies: ['trainingElement','training'],
        query: `query TrainingWrapper_getTraining {
            TrainingWrapper_getTraining(recnum:"${recnum}"){
                trainingElement {
                    ...trainingElement
                }
                training {
                    ...training
                } 
            }
        }`,
        cacheEnabled: true,
        mutation: false
    }
};

const getTrainingPlanTemplateForUserAssign = function (userAssign) {
    return {
        queryName: 'TrainingPlanTemplate_getForDifferentUser',
        dependencies: ['trainingPlanTemplate'],
        query: `query TrainingPlanTemplate_getForDifferentUser {
            TrainingPlanTemplate_getForDifferentUser(userAssign: ${JSON.stringify(JSON.stringify(userAssign))}) {
                ...trainingPlanTemplate
            }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

const getTrainingPlanTemplateByRecnum = function (recnum) {
    return {
        queryName: 'TrainingPlanTemplateByRecnum',
        dependencies: ['trainingPlanTemplate'],
        query: `query TrainingPlanTemplateByRecnum {
            TrainingPlanTemplate(search: "recnum==${recnum}") {
                ...trainingPlanTemplate
            }
        }`,
        cacheEnabled: false,
        mutation: false
    }
};

const getTrainingPlanTemplateMutation = function (trainingtemplate) {
    return {
        queryName: 'TrainingPlanTemplate_save',
        dependencies: ['trainingPlanTemplate'],
        query: `mutation TrainingPlanTemplate_save {
            TrainingPlanTemplate_save(trainingPlanTemplate: ${JSON.stringify(JSON.stringify(trainingtemplate))}) {
                ...trainingPlanTemplate
            }
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingPlanTemplateCreateMutation = function (trainingtemplate) {
    return {
        queryName: 'TrainingPlanTemplate_update',
        dependencies: ['trainingPlanTemplate'],
        query: `mutation TrainingPlanTemplate_update {
            TrainingPlanTemplate(json: ${JSON.stringify(JSON.stringify(trainingtemplate))}) {
                ...trainingPlanTemplate
            }
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingPlanTemplateItemMutation = function (trainingTemplateItem, exercises = []) {
    return {
        queryName: 'TrainingPlanTemplateItem_create',
        dependencies: ['trainingPlanTemplateItem'],
        query: `mutation TrainingPlanTemplateItem_create {
            TrainingPlanTemplateItem_create(trainingPlanTemplateItem: ${JSON.stringify(JSON.stringify(trainingTemplateItem))}, exercises: ${JSON.stringify(JSON.stringify(exercises))} ){
                ...trainingPlanTemplateItem
            }
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingPlanTemplateItemSimpleMutation = function (trainingTemplateItem) {
    return {
        queryName: 'TrainingPlanTemplateItem',
        dependencies: ['trainingPlanTemplateItem'],
        query: `mutation TrainingPlanTemplateItem {
            TrainingPlanTemplateItem_update(trainingPlanTemplateItem: ${JSON.stringify(JSON.stringify(trainingTemplateItem))}){
                ...trainingPlanTemplateItem
            }
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingPlanTemplateItemDeleteMutation = function (trainingTemplateItem) {
    return {
        queryName: 'TrainingPlanTemplateItem',
        dependencies: ['trainingPlanTemplateItem'],
        query: `mutation TrainingPlanTemplateItem {
            TrainingPlanTemplateItem_delete(recnum: "${trainingTemplateItem.recnum}") {
                ...trainingPlanTemplateItem
            }
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

const getTrainingPlanTemplateAllocateMutation = function (trainingPlanTemplate, date) {
    return {
        queryName: 'TrainingPlanTemplate_allocate',
        query: `mutation TrainingPlanTemplate_allocate {
            TrainingPlanTemplate_allocate(trainingPlanTemplate: ${JSON.stringify(JSON.stringify(trainingPlanTemplate))}, date: ${JSON.stringify(date)})
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

const setTrainingProgToUser = function (trainingRecnum) {
    return {
        queryName: 'setTrainingProgToUser',
        dependencies: ['userAssignProg'],
        query: `mutation setTrainingProgToUser {
            UserAssignProg_setTrainingProgToUser(recnum: "${trainingRecnum}") {
                ...userAssignProg
            }
        }`,
        cacheEnabled: false,
        mutation: true
    }
};

module.exports = {
    getExecutedTrainingWrapperByRecnum,
    getExecutedTrainingWrapperForDifferentUserByRecnum,
    getExecutedTraining_getForCurrentUserCalendarQuery,
    getExecutedTraining_getForUserByAssignCalendarQuery,
    getTrainingWrapperByRecnum,
    getTrainingPlanTemplateByRecnum,
    getTrainingPlanTemplateForUserAssign,
    getTrainingPlanTemplateMutation,
    getTrainingPlanTemplateCreateMutation,
    getTrainingPlanTemplateItemMutation,
    getTrainingPlanTemplateItemSimpleMutation,
    getTrainingPlanTemplateItemDeleteMutation,
    getTrainingPlanTemplateAllocateMutation,
    setTrainingProgToUser
};
