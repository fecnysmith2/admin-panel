import {SHOP_KEY, SHOP_PASS, API_URL} from '../../config';
import React from 'react';
import {fragments} from './queries/fragments/fragments';
import {unique} from "./helpers";
import xhr from 'xhr';
import Header from "../views/header";
import {getOauthTokenObject} from "./db";
import {getSelectedStore} from "./user-util";
const crypto = require('crypto'),
    algorithm = 'aes-256-ctr';

function encrypt(text){
    var cipher = crypto.createCipher(algorithm,SHOP_PASS)
    var crypted = cipher.update(text,'utf8','hex')
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text){
    var decipher = crypto.createDecipher(algorithm,SHOP_PASS)
    var dec = decipher.update(text,'hex','utf8')
    dec += decipher.final('utf8');
    return dec;
}

const getAuthToken = function () {
    const oauthTokenObject = getOauthTokenObject();
    // return oauthTokenObject ? 'Bearer ' + oauthTokenObject : '';
    return oauthTokenObject ? oauthTokenObject : '';
};

const cacheAvailable = [];
// TODO: Add cache method with the same params to localStorage

const makeRequest = function (queryObject, params = {}, onReceived = function (result) {
}, onError = function (error) {
}) {
    window.log("unEncryptedDataRequest", encodeURIComponent(JSON.stringify(params)));
    let url = API_URL+"/api/request/"+queryObject+"/"+encrypt(encodeURIComponent(JSON.stringify(params)));
    window.log('callURL', url);
    xhr({
        method: 'GET',
        uri: url,
        headers: {
            'Shop-Token': getSelectedStore()["_id"],
            'Authorization': getAuthToken(),
        },
    }, (error, response, body) => {
        if (response.statusCode === 401) {
            localStorage.removeItem("oAuth_token");
            localStorage.removeItem("account_object");
            if(!window.location.pathname.includes("login")) {
                window.location.href = "/login";
            }
        }
        if (response.statusCode !== 200) {
            onError(error);
        }
        onReceived(JSON.parse(decrypt(response.body)));
    });
};

const addFragmentsToQuery = function (queryObject) {
    const start = Date.now();
    log(`QUERY BUILD START${queryObject.queryName}`,start);
    const fragmentsArray = fragments();
    let fragmentsToAdd = [];

    const getDependencies = function getDependencies(graphqlObject) {
        if (graphqlObject.dependencies !== undefined && graphqlObject.dependencies.length > 0) {
            const dependencies = graphqlObject.dependencies;
            if (dependencies !== undefined) {
                fragmentsToAdd = unique(fragmentsToAdd.concat(dependencies));
                dependencies.forEach(function (fragmentName) {
                    log('FRAGMENT NAME TO CHECK', fragmentName);
                    log('FRAGMENT TO CHECK', fragmentsArray[fragmentName]);
                    if (fragmentsArray[fragmentName] !== undefined) {
                        getDependencies(fragmentsArray[fragmentName]);
                    }
                });
            }
        }
    };

    getDependencies(queryObject);
    log('QUERY OBJECT:', queryObject.query);
    if (fragmentsToAdd.length > 0) {
        fragmentsToAdd.forEach(function (fragmentName) {
            if (!queryObject.query.includes(`fragment ${fragmentName} on`)) {
                if (fragmentsArray[fragmentName] !== undefined) {
                    const fragment = fragmentsArray[fragmentName];
                    if (fragment.fragment !== undefined) {
                        queryObject.query = queryObject.query.concat(fragment.fragment);
                    }
                }
            }
        });
        log('QUERY OBJECT AFTER:', queryObject.query);
    }
    log(`QUERY BUILD FINISH${queryObject.queryName}`,(Date.now() - start)/1000);
    return queryObject;
};

export default makeRequest;
