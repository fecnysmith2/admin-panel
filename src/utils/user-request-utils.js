import makeRequest from './request-queue-handler';
import {saveUserObject, registerObservable} from './user-util';
import {getUserQuery} from './queries/queries';

/*
 *  To use this function tha queryObject must contains query q { User: someQuery .... syntax or be null to
 * */
const handleUserRequestAndSaveUserObject = function(queryObject, name = '', callback = function (user) {}) {
    registerObservable(name,  callback);
    if (queryObject === undefined || queryObject === '') {
        queryObject = getUserQuery;
    }
    window.log('USER QUERY',queryObject)
    makeRequest(queryObject, function(response) {
        const user = response.data.User;
        if (user !== undefined) {
            saveUserObject(user);
        }
    }, function(error){
        window.log('USER QUERY/MUTATION ERROR', error);
    });
}

module.exports = {
    handleUserRequestAndSaveUserObject
};