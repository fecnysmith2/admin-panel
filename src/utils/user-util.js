import {encodeAndSaveObjectToLocalStore, getEncodedObjectFromLocalStorage, removeObjectFromLocalStore} from './db';

const userObjectSecret = 'Sjhui3Ied4klKOkKIejfmvk216bc4cc29522a138d0092aa8';

const userObjectTag = 'account_object';

const observablesArray = [];

const saveAccountObject = function (userObject) {
    encodeAndSaveObjectToLocalStore(userObjectTag, userObject, userObjectSecret);
    observ();
};

const saveAccountStores = function (stores) {
    encodeAndSaveObjectToLocalStore("account_stores", stores, userObjectSecret);
};
const getAccountStores = function () {
    return getEncodedObjectFromLocalStorage("account_stores", userObjectSecret);
};

const setSelectedStore = function (store) {
    encodeAndSaveObjectToLocalStore("selected_store", store, userObjectSecret);
    observ();
};
const getSelectedStore = function () {
    return getEncodedObjectFromLocalStorage("selected_store", userObjectSecret);
};


const clearAccountObject = function () {
    removeObjectFromLocalStore(userObjectTag);
    observ();
};

const getAccountObject = function () {
    return getEncodedObjectFromLocalStorage(userObjectTag, userObjectSecret);
};

const getAccountObjectProperty = function (propertyName) {
    const userObject = getAccountObject();
    return userObject[propertyName];
};

const setAccountObjectProperty = function (propertyName, propertyValue) {
    let userObject = getAccountObject();
    userObject[propertyName] = propertyValue;
    saveAccountObject(userObject);
};

const getAccountAssignId = function () {
    const userObject = getAccountObject();
    let userAssignId = '';
    if (userObject.userAssigns !== undefined) {
        userAssignId = userObject.userAssigns[0].recnum !== undefined ? userObject.userAssigns[0].recnum : '';
    }
    return userAssignId;
};

const saveAccountAssign = function (userAssign) {
    let userObject = getAccountObject();
    userObject.userAssigns = userObject.userAssigns.map(function (item) {
        if (item.recnum === userAssign.recnum) {
            return userAssign;
        } else {
            return item;
        }
    });
    saveAccountObject(userObject);
};

const registerObservable = function (name, callback = function (user) {}) {
    if (name && name !== '') {
        const observableObject = {
            name: name,
            callback: callback
        };
        observablesArray.push(observableObject);
    }
};

const removeObservable = function (name) {
    let removeIndex = -1;
    observablesArray.filter(function (item, index) {
        if (item.name === name) {
            removeIndex = index;
        }
    });
    observablesArray.splice(index, 1);
};

const observ = function () {
    window.log('OBSRVS', observablesArray);
    for(let i = 0; i < observablesArray.length; i++) {
        observablesArray[i].callback();
    }
};

const getAccountName = function () {
    const accountObject = getAccountObject();
    return accountObject.personalData.name.firstName + ' ' + accountObject.personalData.name.lastName;
};

const getAccountAddress = function (userAddress) {
    if (userAddress === null) {
        return '';
    }
    let floorDoor = '';
    if (userAddress.floor !== null && userAddress.floor !== '') {
        floorDoor =  userAddress.floor + '. emelet '  + userAddress.door + '. ajtó';
    }
    return userAddress.zip + ' ' + userAddress.city + ', ' + userAddress.address + getPlaceTypeName(userAddress) + ' ' + userAddress.addrNumber + '. ' + floorDoor;
};

const getPlaceTypeName = function(userAddress) {
    return '';
};

const getProfilePicture = function (user) {
    if (user !== null) {
        const defaultImage = user.sex === 'W' ? '../../../img/ikonok/profil-no.svg' : '../../../img/ikonok/profil-ferfi.svg';
        let profilePictureAttachment = null;
        if (user.userAssigns[0].attachment !== null) {
            profilePictureAttachment = user.userAssigns[0].attachment.recnum;
        }
        return{
            profilePicture: profilePictureAttachment,
            defaultImage: defaultImage
        };
    }
    return{
        profilePicture: null,
        defaultImage: null
    };
};

module.exports = {
    saveAccountObject,
    clearAccountObject,
    getAccountObject,
    setAccountObjectProperty,
    getAccountObjectProperty,
    saveAccountAssign,
    getAccountAssignId,
    registerObservable,
    removeObservable,
    getAccountName,
    getAccountAddress,
    getProfilePicture,
    saveAccountStores,
    getAccountStores,
    setSelectedStore,
    getSelectedStore
};
