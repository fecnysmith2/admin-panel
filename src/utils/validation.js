/**
 * Created by Törpedínó on 2017. 07. 03..
 */
function validateEmail(email){
    return /(.+)@(.+){2,}\.(.+){2,}/.test(email);
}
function validatePhone(phone){
    return /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(phone);
}

function validateElement(method, value){
    switch(method){
        case'Email':
            return validateEmail(value);
            break;
        case'Phone':
            return validatePhone(value);
            break;
    }
}
module.exports = {validateElement, validateEmail, validatePhone};