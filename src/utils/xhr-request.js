import xhr from 'xhr';
import is from 'is_js';
import {getOauthTokenObject, deleteOauthTokenObject, getObjectFromLocalStore} from './db';
import {getUserAssignId} from './user-util';
import {backend, baseUrl} from '../../config';
import P from 'promise-polyfill';
import moment from "moment";
import {SHOP_KEY, SHOP_PASS, API_URL} from '../../config';

if (is.ie()) {
    window.Promise = P;
}

const oauthURI = backend + '/oauth/token';
const oauthRevokeURI = backend + '/oauth/revoke';
const graphqlURI = backend + '/api/graphql';

// @TODO Bele a helpers .js-be
const getAuthToken = function () {
    const oauthTokenObject = getOauthTokenObject();
    // return oauthTokenObject ? 'Bearer ' + oauthTokenObject : '';
    return oauthTokenObject ? oauthTokenObject : '';
};

// @TODO: 401 azért jön, mert valaki a tokennel már kilépett egy másik számítógéppel, de én bent maradtam az adott (már kiléptetett) tokennel.
// Megoldás: ha 401-es Unauthorized message jön, azonnal törölni kell a tokent a localstorage-ból, és Authorization header nélkül elküledeni a
// requestet.
const sendMutation = function (query, variables) {
    return new Promise((resolve, reject) => {
        var req = xhr({
            method: 'POST',
            json: {
                query: query,
                variables: variables
            },
            uri: graphqlURI,
            headers: {
                'User-Assign-Id': getUserAssignId(),
                'Content-Type': 'application/json',
                'Accept-Language': 'hu;q=0.9',
                'Authorization': getAuthToken(),
                'Validation-Error-Format': 'flat'
            }
        }, (error, response, body) => {
            const logObject = {
                query: query,
                variables: variables,
                body: body
            };
            if (response.statusCode == 401 || response.statusCode == 400) {
                deleteOauthTokenObject();
                reject({error, response, body});
            } else if (response.statusCode == 200 && body.errors.length > 0) {
                sendLog(logObject);
                reject({error, response, body});
            } else if (response.statusCode !== 200) {
                sendLog(logObject);
                reject({error, response, body});
            }
            resolve({error, response, body});
        });
    });
};

const sendQuery = function (query, variables = null) {
    return new Promise((resolve, reject) => {
        var req = xhr({
            method: 'POST',
            json: {
                query: query,
                variables: variables
            },
            uri: graphqlURI,
            headers: {
                'User-Assign-Id': getUserAssignId(),
                'Content-Type': 'application/json',
                'Accept-Language': 'hu;q=0.9',
                'Authorization': getAuthToken()
            }
        }, (error, response, body) => {
            // window.log('error, response, body', error, response, body);
            // window.log('response.statusCode', response.statusCode);
            const logObject = {
                query: query,
                variables: variables,
                body: body
            };
            if (response.statusCode == 401 || response.statusCode == 400) {
                deleteOauthTokenObject();
                reject(body, response.statusCode);
            } else if (response.statusCode == 200 && body.errors.length > 0) {
                sendLog(logObject);
                reject({error, response, body});
            } else if (response.statusCode !== 200) {
                sendLog(logObject);
                reject({error, response, body});
            }
            resolve({error, response, body});
        });
    });
};

const sendLog = function(logObject) {
    sendPostToNodeRequest(baseUrl + '/log', logObject).then(function (object) {
        console.log('POST success', object);
    }).catch(function (object) {
        console.error('POST', object);
    });
};

const authenticateUser = function (data) {
    const ip = getObjectFromLocalStore('userIp');
    const fingerPrint = getObjectFromLocalStore('UUID');
    const ipParam = ip != null && ip != '' ? '&clientIp='+ip.replace(/"/g, '') : '';
    var clientId = 'ot_web';
    var clientSecret = 'mt1MqojrfMy3Os3rbJFz';
    var grant_type = 'password';
    var userName = encodeURIComponent(data.username);
    var body = 'grant_type=' + grant_type + '&username=' + userName + '&password=' + data.password + '&deviceId=' + fingerPrint.replace(/"/g, '') + ipParam;
    return new Promise((resolve, reject) => {
        var req = xhr({
            method: 'POST',
            uri: oauthURI,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Basic ' + btoa(clientId + ':' + clientSecret)
            },
            body: body
        }, (error, response, body) => {
            // window.log('error, response, body', error, response, body);
            if (response.statusCode !== 200) {
                reject({error, response, body});
            }
            resolve({error, response, body});
        });
    });
};

const revokeAuthentication = function () {
    return new Promise((resolve, reject) => {
        var req = xhr({
            method: 'GET',
            uri: oauthRevokeURI,
            headers: {
                'Authorization': getAuthToken()
            }
        }, (error, response, body) => {
            if (response.statusCode !== 200) {
                reject(error, response.statusCode);
            }
            resolve(response);
        });
    });
};

const sendAjaxRequest = function (requestObject) {
    return new Promise((resolve, reject) => {
        var req = xhr({
            method: 'GET',
            uri: requestObject.uri
        }, (error, response, body) => {
            if (response.statusCode !== 200) {
                reject(error);
            }
            resolve(response);
        });
    });
};

const sendPostRequest = function (uri = '', body = '') {
    return new Promise((resolve, reject) => {
        var req = xhr({
            method: 'POST',
            headers: {
                'Accept-Language': 'hu;q=0.9',
                'Shop-Token': SHOP_KEY,
                'Authorization': getAuthToken()
            },
            uri: uri,
            body: body
        }, (error, response, responseBody) => {
            if (response.statusCode !== 200) {
                reject(error);
            }
            resolve(response);
        });
    });
};

const sendPostToNodeRequest = function (uri = '', data = '') {
    const time = moment().format();
    return new Promise((resolve, reject) => {
        var req = xhr({
            method: 'POST',
            headers: {
                'content-type': 'application/x-www-form-urlencoded',
            },
            uri: uri,
            // data: {
            //     "logData" : `${JSON.stringify(data)}`,
            //     "time" : `${time}`
            // }
            data: `logData=${JSON.stringify(data)}&time=${time}`
        }, (error, response, responseBody) => {
            if (response.statusCode !== 200) {
                reject(error);
            }
            resolve(response);
        });
    });
};

const sendGetRequest = function (uri = '') {
    return new Promise((resolve, reject) => {
        var req = xhr({
            method: 'GET',
            headers: {
                'Content-Type': 'image/jpg;charset=UTF-8',
                'User-Assign-Id': getUserAssignId(),
                'Accept-Language': 'hu;q=0.9',
                'Authorization': getAuthToken()
            },
            uri: uri,
        }, (error, response, body) => {
            if (response.statusCode !== 200) {
                reject(error);
            }
            resolve(response);
        });
    });
};

const sendPostToWebshop = function (requestObject) {
    return new Promise((resolve, reject) => {
        var req = xhr({
            method: 'POST',
            uri: requestObject.uri,
            data: JSON.stringify(requestObject.data)
        }, (error, response, body) => {
            if (response.statusCode !== 200) {
                reject(error);
            }
            resolve(response);
        });
    });
};

module.exports = {
    getAuthToken,
    sendQuery,
    sendMutation,
    authenticateUser,
    revokeAuthentication,
    sendAjaxRequest,
    sendPostRequest,
    sendPostToNodeRequest,
    sendPostToWebshop,
    sendGetRequest
};
