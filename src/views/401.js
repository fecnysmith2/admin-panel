import React, { Component } from 'react';
import {Link} from 'react-router';
import PageIntro from '../components/page-intro';

export default class PageNotAuthorized extends Component {

	render() {
		var urlParam = '';
		if (this.props.params.encodedParam) {
			urlParam = '/' + this.props.params.encodedParam;
		}
		return (
			<PageIntro title='Belépés szükséges' >
				<p>
					Nincs jogosultságod a tartalom megtekintéséhez. A megjelenítéshez bejelentkezés szükséges,
					<Link to={ '/belepes' + urlParam } style={{color: 'white'}}>
					{ ' ' }<b><u>kérjük jelentkezz be!</u></b>
					</Link>
				</p>
			</PageIntro>
		);
	}
};
