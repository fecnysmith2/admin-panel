import React, {
    Component,
    PropTypes,
} from 'react';
import {Link} from 'react-router';
import PageIntro from '../components/page-intro';

class PageNoAccess extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <PageIntro title="Nem megfelelő jogosultsági szint" >
                <p>
                    Nincs jogosultságod a tartalom megtekintéséhez.
                </p>
            </PageIntro>
        );
    }
}

PageNoAccess.propTypes = {};
PageNoAccess.defaultProps = {};

export default PageNoAccess;