import React, {
   Component,
} from 'react';
import PageIntro from '../components/page-intro';

class PageNotFound extends Component {

   constructor(props) {
       super(props);
   }

	render() {
		return (<main id="page-content">
			<div className="error-container">
				<div className="error-message text-center">
					<h1 className="fs-96">404</h1>
					<p>Not found - We're sorry, the page you requested for is not found.</p>
					<a href="javascript:void(0)" className="text-info text-uppercase"><i className="fa fa-bug fa-fw"></i>Report</a> or <a href="index.html" className="text-info text-uppercase"><i className="fa fa-home fa-fw"></i>Home</a>

					<p className="m-xl-t p-xl-t text-center"><strong className="text-success ls-xs">X</strong><span className="ls-xs">ploit</span> <span className="app-version">1.0.0</span>  © - 2017</p>
				</div>
			</div>
		</main>);
	}
}

PageNotFound.propTypes = {};
PageNotFound.defaultProps = {};

export default PageNotFound;