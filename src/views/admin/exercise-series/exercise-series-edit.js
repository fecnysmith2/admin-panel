import React, {Component} from "react";
import bindAll from 'lodash.bindall';
import {withRouter} from 'react-router';
import {DragDropContext} from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";
import ExerciseSeriesEditContainer from "./exercise-series-edit-container";
import withAuthCheck from "../../../components/auth-check";
import {sortExercises, loadExerciseSeries, loadExerciseSeriesByTrainingElement, getRootExerciseObject} from "../../../utils/admin/exercise-series-utils";
import ExerciseSeriesStickyPanel from './exercise-series-sticky-panel';

class ExerciseSeriesEdit extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'loadExerciseSeries', 'setNew'
        );
        this.state = {
            exerciseSeries: getRootExerciseObject(),
        };
    }

    componentWillMount() {
        this.loadExerciseSeries();
    }

    componentDidUpdate(prevProps, prevState, prevContext) {
        const {params, trainingElement} = this.props;
        const prevParams = prevProps.params;
        const prevTrainingElement = prevProps.trainingElement;
        if ((prevParams && params && prevParams.recnum != params.recnum)
            || (prevTrainingElement != null && trainingElement != null && prevTrainingElement.recnum != trainingElement.recnum)) {
            this.loadExerciseSeries();
        }
    }

    loadExerciseSeries() {
        const {params, trainingElement, personal} = this.props;
        const that = this;
        if (personal && trainingElement != null) {
            loadExerciseSeriesByTrainingElement(trainingElement.recnum, function (response) {
                let exerciseSeries = response.data.TrainingElement_getExerciseSeriesByTrainingElement;
                exerciseSeries.children = sortExercises(exerciseSeries.children);
                that.setState({
                    exerciseSeries: exerciseSeries
                });
            });
        } else if (params && params.recnum) {
            loadExerciseSeries(params.recnum, function (response) {
                let exerciseSeries = response.data.ExerciseSeries_getById;
                exerciseSeries.children = sortExercises(exerciseSeries.children);
                that.setState({
                    exerciseSeries: exerciseSeries
                });
            });
        }
    }

    setNew() {
        this.state = {
            exerciseSeries: getRootExerciseObject(),
        };
    }

    render() {
        const {exerciseSeries} = this.state;
        const {router, personal, trainingElement} = this.props;
        return (
            <div className='page-container' >
                <h1 className='h1' >{personal ? 'Gyakorlatsor testreszabása' : 'Gyakorlatsor sablon szerkesztő' }</h1>
                {personal ? '' : <ExerciseSeriesStickyPanel   onNewClick={this.setNew}/> }
                <ExerciseSeriesEditContainer exerciseSeries={exerciseSeries}
                                             router={router}
                                             personal={personal}
                                             trainingElement={trainingElement}
                                             loadExerciseSeries={this.loadExerciseSeries} />
            </div>
        );
    }
}

ExerciseSeriesEdit.propTypes = {
};

ExerciseSeriesEdit.defaultProps = {
    personal: false,
    trainingElement: null
};

export default DragDropContext(HTML5Backend)(withRouter(ExerciseSeriesEdit));
