import React, {Component} from 'react';
import bindAll from 'lodash.bindall';
import makeRequest from '../../../utils/request-queue-handler';
import FileUpload from '../../../components/widgets/file-upload';
import AttachmentVideoDownload from '../../../components/widgets/attachment-video-download';
import {
    getExerciseByRecnumQuery,
    getExerciseMutation,
    getExercisePhaseByExerciseRecnumQuery,
    getExercisePhaseMutation,
    getDeleteExercisePhaseByObjectMutation
} from '../../../utils/queries/admin/exercise-queries';
import ExerciseHeaderPanel from './exercise-header-panel';
import withAuthCheck from '../../../components/auth-check';
import {getTextFromHybridMemo, isNullOrUndefined} from '../../../utils/helpers';
import ExercisePhase from './exercise-phase';
import {Button} from 'react-bootstrap';
import {
    loadLocalModifications, saveExercise, saveExercisePhases,
    saveLocalModifications
} from "../../../utils/admin/exercise-utils";

class ExerciseEditAdmin extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'loadExercisesPhases', 'saveExercise', 'getExercisesPhase',
            'deleteExercisePhase', 'addNewExercisePhase', 'saveExercisePhase',
            'getExercisePhase', 'getExercisePhases', 'onVideoUpload',
            'onMobileVideoUpload', 'clearState', 'getVideosComponent',
            'removeVideo', 'cancelRemoveVideo', 'saveExercisePhases',
        );

        this.state = {
            exercise: '',
            exerciseItems: [],
            itemLoadArray: [],
            loaded: false,
            mainPhaseLoaded: false,
            itemsLoaded: false,
            savingAction: false,
            cleared: false,
            oldAttachmentVideo: null,
            oldAttachmentVideoMobile: null
        };
    }

    componentWillMount() {
        const {params} = this.props;
        const that = this;
        if (params.recnum) {
            if (!isNaN(params.recnum)) {
                const exercise = loadLocalModifications(params.recnum);
                if (exercise !== '') {
                    makeRequest(getExerciseByRecnumQuery(params.recnum), function (response) {
                        window.log('EXERCISES', response);
                        that.setState({
                            exercise: response.data.Exercise[0],
                            savingAction: false
                        });
                    }, function (error) {
                        window.error('EXERCISES', error);
                    });
                    this.loadExercisesPhases(params.recnum);
                } else {
                    const phases = Array.isArray(exercise.phases) ? exercise.phases : [];
                    const loadingArray = phases.map(function (item, index) {
                        return false;
                    });
                    delete exercise.phases;
                    this.setState({
                        exercise: exercise,
                        savingAction: false,
                        itemsLoaded: false,
                        exerciseItems: phases,
                        itemLoadArray: loadingArray
                    });
                }
            }
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const {exercise, loaded, cleared, mainPhaseLoaded, exerciseItems, itemsLoaded} = this.state;
        window.log('CLEAR', loaded);
        if (!cleared && window.location.pathname.includes('uj')) {
            this.clearState();
        } else {
            if (exercise !== '' && !loaded) {
                if (exercise.stretch) {
                    document.getElementById('stretch').click();
                }
                document.getElementById('exerciseName').value = getTextFromHybridMemo(exercise.name);
                document.getElementById('time').value = exercise.timeRequired !== null ? exercise.timeRequired : 0.0;
                document.getElementById('exerciseVideoAttachment').value = exercise.attachmentVideo && exercise.attachmentVideo !== null ? exercise.attachmentVideo.recnum : '';
                document.getElementById('exerciseMobileVideoAttachment').value = exercise.attachmentVideoMobile && exercise.attachmentVideoMobile !== null ? exercise.attachmentVideoMobile.recnum : '';
                this.setState({
                    loaded: true,
                    savingAction: false
                });
            } else if (exerciseItems.length > 0 && !itemsLoaded) {
                for (let i = 0; i < this.state.exerciseItems.length; i++) {
                    document.getElementById('exercisePhaseText' + i).value = getTextFromHybridMemo(exerciseItems[i].text);
                }
                this.setState({
                    itemsLoaded: true
                });
            }
            if (exercise.mainPhase && exercise.mainPhase !== null && !mainPhaseLoaded) {
                document.getElementById('isMain-' + exercise.mainPhase.recnum).click();
                this.setState({
                    mainPhaseLoaded: true
                });
            }
        }
    }

    clearState() {
        window.log('CLEAR');
        document.getElementById('stretch').checked = false;
        document.getElementById('exerciseName').value = '';
        document.getElementById('time').value = 0;
        this.setState({
            exercise: '',
            exerciseItems: [],
            itemLoadArray: [],
            cleared: true,
        });
    }

    loadExercisesPhases(recnum) {
        const that = this;
        makeRequest(getExercisePhaseByExerciseRecnumQuery(recnum), function (response) {
            window.log('EXERCISES', response);
            let resultArray = response.data.ExercisePhase_getByExerciseId;
            resultArray.sort(function (a, b) {
                return a.ordernum - b.ordernum;
            });
            const loadingArray = resultArray.map(function (item, index) {
                return false;
            });
            that.setState({
                savingAction: false,
                itemsLoaded: false,
                exerciseItems: resultArray,
                itemLoadArray: loadingArray
            });
        }, function (error) {
            window.error('EXERCISES', error);
            that.setState({
                savingAction: false,
            });
        });
    }

    saveExercisePhases() {
        const {exerciseItems} = this.state;
        window.log('SAVE EXERCISEPHASES', exerciseItems);
        saveExercisePhases(exerciseItems, function (response) {

        }, function (error) {

        });
    }

    saveExercise(isLocal = false) {
        const {router} = this.props;
        const {loaded, exerciseItems} = this.state;
        window.log('SAVING EXERCISE START. . .');
        if (!loaded && !window.location.pathname.includes('uj')) {
            return;
        }
        window.log('SAVING EXERCISE . . .');
        this.setState({
            savingAction: true
        });
        const that = this;
        let exercise;
        const isStretch = document.getElementById('stretch').checked;
        const name = document.getElementById('exerciseName').value;
        const timeRequired = document.getElementById('time') ? document.getElementById('time').value : 0;
        const videoAttachmentId = document.getElementById('exerciseVideoAttachment') ? document.getElementById('exerciseVideoAttachment').value : '';
        const videoMobileAttachmentId = document.getElementById('exerciseMobileVideoAttachment') ? document.getElementById('exerciseMobileVideoAttachment').value : '';
        if (this.state.exercise === '') {
            exercise = {
                profClass: 'T',
                exerciseType: 'E',
                name: {
                    hybridMemoItems: [{textblock: name, language: 'HU'}]
                },
                stretch: isStretch
            }
        } else {
            if (!this.state.loaded) {
                return;
            }
            exercise = this.state.exercise;
            exercise.stretch = isStretch;
            exercise.timeRequired = timeRequired;
            if (isNullOrUndefined(exercise.name)) {
                exercise.name = {};
                exercise.name.hybridMemoItems = [{textblock: name, language: 'HU'}];
            } else {
                exercise.name.hybridMemoItems[0].textblock = name;
            }
            if (isNullOrUndefined(exercise.phase)) {
                exercise.phase = 'P';
            }
            if (videoAttachmentId !== '') {
                if (exercise.attachmentVideo === null) {
                    exercise.attachmentVideo = {};
                }
                exercise.attachmentVideo.recnum = videoAttachmentId;
            } else {
                exercise.attachmentVideo = null;
            }
            if (videoMobileAttachmentId !== '') {
                if (exercise.attachmentVideoMobile === null) {
                    exercise.attachmentVideoMobile = {};
                }
                exercise.attachmentVideoMobile.recnum = videoMobileAttachmentId;
            } else {
                exercise.attachmentVideoMobile = null;
            }
            delete exercise.lastHandler;
        }
        window.log('NEW EXERCISE', exercise);
        window.log('ISLOCAL', isLocal);
        if (isLocal) {
            exercise.phases = exerciseItems;
            saveLocalModifications(exercise);
        } else {
            window.log('SAVE EXERCISE', exercise);
            saveExercise(exercise, function (response) {
                const exercise = response.data.Exercise_save;
                if (window.location.pathname.includes('uj')) {
                    router.push('/admin/elemi-gyakorlatok/modosit/' + exercise.recnum);
                }
                that.saveExercisePhases();
                that.setState({
                    exercise: exercise,
                    cleared: false,
                    savingAction: false,
                    oldAttachmentVideo: null,
                    oldAttachmentVideoMobile: null
                });
            }, function (error) {
                that.setState({
                    savingAction: false
                });
            });
        }
    }

    getExercisesPhase() {
        const ordernum = this.state.exerciseItems.length;
        return {
            exercise: {
                recnum: ''
            },
            attachment: null,
            ordernum: ordernum,
            text: {
                text: ''
            }
        }
    }

    deleteExercisePhase(index) {
        const {exercise, exerciseItems, itemLoadArray} = this.state;
        window.log('DELETE', index);
        this.setState({
            savingAction: true
        });
        const that = this;
        if (exerciseItems.length > 0) {
            window.log('DELETE RECNUM', exerciseItems[index].recnum);
            if (!isNaN(exerciseItems[index].recnum)) {
                makeRequest(getDeleteExercisePhaseByObjectMutation(exerciseItems[index]), function (response) {
                    window.log('DELETE EXERCISE  PHASE response', response);
                    that.loadExercisesPhases(exercise.recnum);
                    that.setState({
                        savingAction: false
                    });
                }, function (error) {
                    that.setState({
                        savingAction: false
                    });
                    window.error('DELETE EXERCISE  PHASE response', error);
                });
            } else {
                exerciseItems.splice(index, 1);
                itemLoadArray.splice(index, 1);
                that.setState({
                    savingAction: false,
                    exerciseItems: exerciseItems,
                    itemLoadArray: itemLoadArray
                });
            }
        }
    }

    addNewExercisePhase() {
        let exerciseItems = this.state.exerciseItems;
        const exercisePhase = this.getExercisesPhase();
        exerciseItems.push(exercisePhase);
        let loadingArray = this.state.itemLoadArray;
        loadingArray.push(false);
        this.setState({
            exerciseItems: exerciseItems,
            itemLoadArray: loadingArray
        });
        this.saveExercisePhase(exercisePhase, exerciseItems.length - 1);
    }

    saveExercisePhase(exercisePhase, index, isLocal = false) {
        const {exercise} = this.state;
        window.log('SAVING EXERCISEPHASE . . .');
        this.setState({
            savingAction: true
        });
        const that = this;
        const exerciseRecnum = this.state.exercise.recnum;
        const text = document.getElementById('exercisePhaseText' + index) ? document.getElementById('exercisePhaseText' + index).value : '';
        const attachmentId = document.getElementById('exercisePhaseAttachment' + index) ? document.getElementById('exercisePhaseAttachment' + index).value : '';

        if (!exercisePhase.recnum) {
            exercisePhase = {
                exercise: {
                    recnum: exerciseRecnum
                },
                ordernum: index + 1,
            }
        } else {
            if (attachmentId !== '') {
                if (exercisePhase.attachment === null) {
                    exercisePhase.attachment = {};
                }
                exercisePhase.attachment.recnum = attachmentId;
            } else {
                exercisePhase.attachment = null;
            }
            exercisePhase.ordernum = index + 1;
        }
        if (isNullOrUndefined(exercisePhase.text)) {
            exercisePhase.text = {};
            exercisePhase.text.hybridMemoItems = [{textblock: text, language: 'HU'}];
        } else {
            exercisePhase.text.hybridMemoItems[0].textblock = text;
        }
        window.log('NEW EXERCISE PHASE', exercisePhase);
        if (isLocal) {
            exercise.phases[index] = exercisePhase;
            saveLocalModifications(exercise);
        } else {
            makeRequest(getExercisePhaseMutation(exercisePhase), function (response) {
                window.log('NEW EXERCISE  PHASE response', response);
                let exerciseItems = that.state.exerciseItems;
                exerciseItems[index] = response.data.ExercisePhase_save;
                that.setState({
                    exerciseItems: exerciseItems,
                    savingAction: false
                });
                if (exerciseItems.length === 1) {
                    exercise.mainPhase = {
                        recnum: exerciseItems[0].recnum
                    };
                    that.setState({
                        exercise: exercise
                    });
                    that.saveExercise(true);
                }
            }, function (error) {
                window.error('NEW EXERCISE  PHASE response', error);
                that.setState({
                    savingAction: false,
                });
            });
        }
    }

    onMobileVideoUpload(attachmentId) {
        window.log('attachmentId', attachmentId);
        document.getElementById('exerciseMobileVideoAttachment').value = attachmentId;
        this.saveExercise(true);
    }

    onVideoUpload(attachmentId) {
        window.log('attachmentId', attachmentId);
        document.getElementById('exerciseVideoAttachment').value = attachmentId;
        this.saveExercise(true);
    }

    getExercisePhase(exercisePhase, index) {
        const {exercise} = this.state;
        const that = this;
        const onUpload = function (attachmentId) {
            window.log('attachmentId', attachmentId);
            document.getElementById('exercisePhaseAttachment' + index).value = attachmentId;
            that.saveExercisePhase(exercisePhase, index, true);
        };
        const setMainPhase = function () {
            exercise.mainPhase = {
                recnum: exercisePhase.recnum
            };
            that.setState({
                exercise: exercise
            });
            that.saveExercise(true);
        };
        return <ExercisePhase exercisePhase={exercisePhase}
                              index={index}
                              deleteExercisePhase={that.deleteExercisePhase}
                              saveExercisePhase={that.saveExercisePhase}
                              onUpload={onUpload}
                              setMainPhase={setMainPhase} />
    }

    getExercisePhases() {
        const that = this;
        const {exerciseItems} = this.state;
        if (!isNullOrUndefined(exerciseItems)) {
            const phases = exerciseItems.map(function (item, index) {
                return that.getExercisePhase(item, index);
            });
            return (
                <div>
                    {phases}
                </div>
            );
        }
    }

    removeVideo(isMobile = false) {
        window.log('ISMOPBILE', isMobile);
        if (isMobile) {
            const attachmentVideoMobile = document.getElementById('exerciseMobileVideoAttachment').value;
            document.getElementById('exerciseMobileVideoAttachment').value = '';
            this.setState({
                oldAttachmentVideoMobile: attachmentVideoMobile
            });
        } else {
            const attachmentVideo = document.getElementById('exerciseVideoAttachment').value;
            document.getElementById('exerciseVideoAttachment').value = '';
            this.setState({
                oldAttachmentVideo: attachmentVideo
            });
        }
    }

    cancelRemoveVideo(isMobile = false) {
        const {oldAttachmentVideo, oldAttachmentVideoMobile} = this.state;
        window.log('ISMOPBILE', isMobile);
        if (isMobile) {
            document.getElementById('exerciseMobileVideoAttachment').value = oldAttachmentVideoMobile;
            this.setState({
                oldAttachmentVideoMobile: null
            });
        } else {
            document.getElementById('exerciseVideoAttachment').value = oldAttachmentVideo;
            this.setState({
                oldAttachmentVideo: null
            });
        }
    }

    getVideosComponent() {
        const {exercise, oldAttachmentVideo, oldAttachmentVideoMobile} = this.state;
        const that = this;
        return [
            <h3 className='h3' >Videók</h3>,
            <div className='clearfix' >
                <div className='col-sm-6' >
                    <h4>Mobil</h4>
                    <FileUpload onUpload={this.onMobileVideoUpload}
                                id={'mobilVideo' + exercise.recnum}
                                entityName='Exercise'
                                fileName={'ExerciseMobilVideo' + (exercise.recnum + Math.floor((Math.random() * 10000) + 1))}
                                maxFileSize={5}
                                enabledTypes={['video/mp4']}
                                entityId={exercise.recnum} />
                    <input type='hidden'
                           name={'exerciseMobileVideoAttachment'}
                           id={'exerciseMobileVideoAttachment'}
                           onChange={function(){this.saveExercise(true)}} />
                    <AttachmentVideoDownload width='400'
                                             attachmentId={exercise.attachmentVideoMobile && exercise.attachmentVideoMobile !== null ? exercise.attachmentVideoMobile.recnum : null} />
                    { exercise.attachmentVideoMobile !== null && oldAttachmentVideoMobile === null &&
                    <Button bsSize='small'
                            onClick={function () {
                                that.removeVideo(true);
                            }}
                            bsStyle='danger' >
                        Videó törlése
                    </Button>
                    }
                    { oldAttachmentVideoMobile !== null &&
                    <Button bsSize='small'
                            onClick={function () {
                                that.cancelRemoveVideo(true);
                            }}
                            bsStyle='warning' >
                        Törlés visszavonása
                    </Button>
                    }
                </div>
                <div className='col-sm-6' >
                    <h4>Nagyméretű</h4>
                    <FileUpload onUpload={this.onVideoUpload}
                                id={'video' + exercise.recnum}
                                entityName='Exercise'
                                fileName={'ExerciseVideo' + (exercise.recnum + Math.floor((Math.random() * 10000) + 1))}
                                enabledTypes={['video/mp4']}
                                entityId={exercise.recnum} />
                    <input type='hidden'
                           name={'exerciseVideoAttachment'}
                           id={'exerciseVideoAttachment'}
                           onChange={function(){this.saveExercise(true)}} />
                    <AttachmentVideoDownload width='400'
                                             attachmentId={exercise.attachmentVideo && exercise.attachmentVideo !== null ? exercise.attachmentVideo.recnum : null} />
                    { exercise.attachmentVideo !== null && oldAttachmentVideo === null &&
                    <Button bsSize='small'
                            onClick={function () {
                                that.removeVideo(false);
                            }}
                            bsStyle='danger' >
                        Videó törlése
                    </Button>
                    }
                    { oldAttachmentVideo !== null &&
                    <Button bsSize='small'
                            onClick={function () {
                                that.cancelRemoveVideo(false);
                            }}
                            bsStyle='warning' >
                        Törlés visszavonása
                    </Button>
                    }
                </div>
            </div>
        ];
    }

    render() {
        const {exercise, savingAction} = this.state;
        const {params} = this.props;
        const that = this;
        const savingButton = (
            <Button
                bsStyle='primary'
                disabled={savingAction}
                onClick={!savingAction ? function(){that.saveExercise()} : null}>
                {savingAction ? <span><img src='/img/ikonok/load-icon.svg' className='small-load'/>Mentés...</span> : 'Mentés'}
            </Button>
        );
        return (
            <div className='admin-exercise-editor' >
                <h1 className='h1' >Gyakorlat szerkesztő</h1>
                <ExerciseHeaderPanel />
                <div className='col-sm-10 nopadding' >
                    <h3 className='h3' >Alapadatok</h3>
                    <div className='clearfix' >
                        <div className='col-sm-7 nopadding' >
                            <div>
                                {exercise === '' ?
                                    <div style={{color: 'red'}} >* Először add meg a gyakorlat nevét és mentsd
                                                                 el!</div> : ''}
                                <label htmlFor='exerciseName' >Megnevezés</label>
                                <input type='text'
                                       name='exerciseName'
                                       id='exerciseName'
                                       onBlur={function(){this.saveExercise(true)}} />
                            </div>
                        </div>
                        <div className='col-sm-5 nopadding' >
                            <div className='clearfix' >
                                <div className='col-sm-4 nopadding' >
                                    <label htmlFor='' >Nyújtás?</label>
                                    <br/>
                                    <input type='checkbox'
                                           name='stretch'
                                           id='stretch'
                                           onChange={function(){this.saveExercise(true)}} />
                                    {' '}igen
                                </div>
                                <div className='col-sm-4 nopadding' >
                                    <label htmlFor='' >Izomcsoport</label>
                                    <br/>
                                    <select name=''
                                            id=''
                                            disabled >
                                        <option value='' >kar</option>
                                        <option value='' >láb</option>
                                        <option value='' >hát</option>
                                    </select>
                                </div>
                                <div className='col-sm-4 nopadding' >
                                    <label htmlFor='' >Hossz (mp)</label>
                                    <br/>
                                    <div className='col-sm-7 nopadding' >
                                        <input type='number'
                                               step='0.1'
                                               name='time'
                                               id='time'
                                               onBlur={function(){this.saveExercise(true)}} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    {!isNaN(params.recnum) ? this.getVideosComponent() : ''}
                    <hr/>
                    <h3 className='h3' >Képek és leírások</h3>
                    <div className='clearfix' >
                        {this.getExercisePhases()}
                        <div className='col-sm-6' >
                            <div className='clearfix' >
                                <div className='col-sm-6 nopadding' >
                                    <h4 className='h4 exercise-edit-h4' >Új kép és leírás
                                        {exercise === '' ?
                                            <a onClick={function () {
                                                alert('Először nevezd el, majd mentsd el a gyakorlatot!');
                                            }}
                                               className='btn-exercise-action'
                                               title='hozzáad' >+
                                            </a>
                                            :
                                            <a onClick={this.addNewExercisePhase}
                                               className='btn-exercise-action'
                                               title='hozzáad' >+
                                            </a>}
                                    </h4>
                                </div>
                                <div className='col-sm-6' >
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div className='text-right excersises-edit-save-section' >
                        {savingButton}
                    </div>
                </div>
            </div>
        );
    }
}

ExerciseEditAdmin.propTypes = {};
ExerciseEditAdmin.defaultProps = {};

export default withAuthCheck(ExerciseEditAdmin);
