import React, {
    Component,
    PropTypes,
} from 'react';
import bindAll from 'lodash.bindall';
import {Link, withRouter} from 'react-router';
import {getUserObjectProperty} from '../../utils/user-util';
import AdminSideMenu from './admin-side-menu';

class AdminIndex extends Component {
    constructor(props) {
        super(props);
        bindAll(this,
            'addListItems',
            'getList',
            'getChildItems',
            'toggleListItem',
            'getListItem',
            'filterMenuItems'
        );
        this.state = {
            list: [{children: []}],
            visibleList: [{children: []}]
        }
    }

    addListItems(menuListItems, isTree = false) {
        this.setState({
            list: menuListItems,
            visibleList: menuListItems
        });
    }

    getList(items) {
        var that = this;
        var listItems = items.map(function (item) {
            return that.getListItem(item);
        });
        return (
            <ul>
                {listItems}
            </ul>
        );
    }

    getChildItems(parentId) {
        var items = [];
        for (var i = 0; i < this.state.visibleList.length; i++) {
            var item = this.state.visibleList[i];
            if (item.parentId != '' && item.parentId == parentId) {
                var that = this;
                var toggleListItem = function () {
                    that.toggleListItem(item.recnum);
                };
                var temp = (
                    <div>
                        <li><Link to={'/admin/' + item.routePreffix + '/' + item.recnum} >{item.title}</Link></li>
                        <div>
                            {this.getChildItems(item.recnum)}
                        </div>
                    </div>
                );
                items.push(temp);
            }
        }
        return (
            <ul className='admin' >
                {items}
            </ul>
        );
    }

    toggleListItem(id) {
        const display = document.getElementById(id).style.display;
        if (display == 'none') {
            document.getElementById(id).style.display = '';
        } else {
            document.getElementById(id).style.display = 'none';
        }
    }

    getListItem(item) {
        var children = [];
        var that = this;
        var toggleListItem = function () {
            that.toggleListItem(item.recnum);
        };
        if (item.parentId == '') {
            children = this.getChildItems(item.recnum);
            return (
                <li>
                    <Link to={'/admin/' + item.routePreffix + '/' + item.recnum} >{item.title}</Link>
                    {/*<p onClick={toggleListItem}>||</p>*/}
                    <div id={item.recnum} >
                        {children}
                    </div>
                </li>
            );
        }
        return [];
    }

    filterMenuItems() {
        const searchKey = document.getElementById('searchByUserInput').value;
        window.log('searchKey', searchKey);
        var visibleList = [];
        for (var i = 0; i < this.state.list.length; i++) {
            if (this.state.list[i].title.toUpperCase().includes(searchKey.toUpperCase())) {
                var item = JSON.parse(JSON.stringify(this.state.list[i]));
                if (searchKey != '' && item.parentId != '') {
                    item.parentId = '';
                }
                visibleList.push(item);
            }
        }

        this.setState({
            visibleList: visibleList
        });
    }

    render() {
        var that = this;
        var childrenWithProps = React.Children.map(this.props.children, function (child) {
            return React.cloneElement(child, {
                addListItems: that.addListItems
            });
        });

        var menuList = this.getList(this.state.visibleList);
        return (
            <div>
                <div className='col-sm-3 admin-groups' >
                    <input type='search'
                           id='searchByUserInput'
                           name='searchByUser'
                           onKeyUp={this.filterMenuItems} />
                    {menuList}
                </div>
                <div className='col-sm-7 admin-data' >
                    {childrenWithProps}
                </div>
            </div>
        );
    }
}

AdminIndex.propTypes = {};
AdminIndex.defaultProps = {};

export default withRouter(AdminIndex);