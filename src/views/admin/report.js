import React, {
    Component,
    PropTypes,
} from 'react';
import bindAll from 'lodash.bindall';
import {Link} from 'react-router';
import withAuthCheck from '../../components/auth-check';
import {getUserObjectProperty} from '../../utils/user-util';
import ReportErrors from '../static/report-errors';

class AdminReport extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
                <div>
                  <ReportErrors baseClasses='admin-report-error' />
                </div>
        );
    }
}

AdminReport.propTypes = {};
AdminReport.defaultProps = {};

export default withAuthCheck(AdminReport, [{userType: 'A', userSubtype: '6'},{userType: 'T', userSubtype: 'T'},{userType: 'A', userSubtype: '9'}]);
