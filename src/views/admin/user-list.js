import React, {
    Component,
    PropTypes,
} from 'react';
import bindAll from 'lodash.bindall';
import moment from 'moment';
import {Link} from 'react-router';
import makeRequest from '../../utils/request-queue-handler';
import {getObjectFromLocalStore} from '../../utils/db';
import {getUserAnswersByUserRecnum} from '../../utils/admin/user-utils';
import {getUserListQuery} from '../../utils/queries/admin/user-queries';
import Pagination from '../../components/widgets/pagination';
import {getUserNameFromUserAssign} from '../../utils/user-util';
import {stringComparator} from '../../utils/helpers';

class UserListAdmin extends Component {
    constructor(props) {
        super(props);
        bindAll(this,
            'getUsers', 'getUserTable', 'getUserRows',
            'getUserRow', 'onSearch', 'setPageData',
        );
        let pageSize;
        let page;
        const savedPageSize = getObjectFromLocalStore('defaultPaginationSize');
        pageSize = savedPageSize != null && savedPageSize != '' && !isNaN(savedPageSize) ? parseInt(savedPageSize) : 10;
        page = 0;
        this.state = {
            users: [],
            shownUsers: [],
            errors: [],
            loading: false,
            totalPageNum: 1,
            pageSize: pageSize,
            page: page,
            goSearch: null
        }
    }

    getUsers() {
        const {pageSize} = this.state;
        var that = this;
        this.setState({
            loading: true
        });
        makeRequest(getUserListQuery, function (response) {
            window.log('USERS', response);
            const users = response.data.UserListWrapper_getUserListWrapperDataForTrainingPlan;
            users.sort(function (a, b) {
                const textA = getUserNameFromUserAssign(a.user.userAssigns[0]).replace(/ /g);
                const textB = getUserNameFromUserAssign(b.user.userAssigns[0]).replace(/ /g);
                return stringComparator(textA, textB);
            });
            that.setState({
                loading: false,
                users: users,
                shownUsers: users,
                totalPageNum: Math.ceil(users.length / pageSize)
            });
        }, function (error) {
            window.log('user-error', error);
        });
    }

    componentWillMount() {
        this.getUsers();
    }

    onSearch(searchKey = '') {
        const {pageSize, users, goSearch} = this.state;
        const that = this;
        if (goSearch != null) {
            clearTimeout(goSearch);
        }
        var myVar = setTimeout(function(){
            window.log('searchKey', searchKey.length);
            let shownUsers = [];
            for (var i = 0; i < users.length; i++) {
                if (searchKey.length > 0) {
                    let alreadyAdded = false;
                    if (!alreadyAdded && getUserNameFromUserAssign(users[i].user.userAssigns[0]).toUpperCase().unAccent().includes(searchKey.toUpperCase().unAccent())) {
                        shownUsers.push(JSON.parse(JSON.stringify(users[i])));
                        alreadyAdded = true;
                    }
                    if (!alreadyAdded && users[i].user.emails[0] != null && users[i].user.emails[0].email.toUpperCase().unAccent().includes(searchKey.toUpperCase().unAccent())) {
                        shownUsers.push(JSON.parse(JSON.stringify(users[i])));
                        alreadyAdded = true;
                    }
                    if (!alreadyAdded && users[i].user.userAssigns[0].mainContact != null && users[i].user.userAssigns[0].mainContact.phoneNumber != null && users[i].user.userAssigns[0].mainContact.phoneNumber.toUpperCase().unAccent().includes(searchKey.toUpperCase().unAccent())) {
                        shownUsers.push(JSON.parse(JSON.stringify(users[i])));
                        alreadyAdded = true;
                    }
                } else {
                    shownUsers.push(JSON.parse(JSON.stringify(users[i])));
                }
            }
            that.setState({
                shownUsers: shownUsers,
                totalPageNum: Math.ceil(shownUsers.length / pageSize)
            });
        }, 1000);

        this.setState({
            goSearch: myVar
        });
    }

    setPageData(page, pageSize) {
        const {shownUsers} = this.state;
        window.log('CHANGE PAGE', page + ':' + pageSize);
        this.setState({
            page: page,
            pageSize: pageSize,
            totalPageNum: Math.ceil(shownUsers.length / pageSize)
        });
    }

    getUserTable() {
        const {totalPageNum, users, shownUsers} = this.state;
        return (
            <Pagination rootContainerClassName='clearfix exercises-list-top-controls'
                        onPageChange={this.setPageData}
                        onSizeChange={this.setPageData}
                        pageCount={totalPageNum}
                        searchCallback={this.onSearch}
                        searchPlaceholder='Keresés névre/emailre/telefonszámra' >
                <div className="clearfix exercises-list-top-controls" >
                        <span>
                            {'Összes felhasználó: ' + users.length + ' / Keresésnek megfelelő felhasználó: ' + shownUsers.length}
                        </span>
                </div>

                <div className="" >
                    <table className="table table-responsive" >
                        <thead>
                        <tr>
                            <th>Név</th>
                            <th>Email cím</th>
                            <th>Felhasználónév</th>
                            <th>Telefonszám</th>
                            <th>Utolsó aktivitás</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.getUserRows()}
                        </tbody>
                    </table>

                </div>
            </Pagination>
        );
    }

    getUserRows() {
        const {users, shownUsers, page, pageSize} = this.state;
        const that = this;
        return shownUsers.map(function (user, index) {
            if (index >= page * pageSize && index < page * pageSize + pageSize) {
                return that.getUserRow(user);
            } else {
                return null;
            }
        });
    }

    getUserRow(user) {
        const name = getUserNameFromUserAssign(user.user.userAssigns[0]);
        const email = user.user.emails != null && user.user.emails[0] ? user.user.emails[0].email : '';
        const loginName = user.user.login;
        const phone = user.user.userAssigns[0].mainContact != null ? user.user.userAssigns[0].mainContact.phoneNumber : null;
        const trainingCount = user.trainingCount;
        const unprizedTrainingCount = user.unprizedTrainingCount;
        return (
            <tr>
                <td><Link to={'/admin/users/' + user.user.recnum} >{name != null ? name : '-'}</Link></td>
                <td><Link to={'/admin/users/' + user.user.recnum} >{email != null ? email : '-'}</Link></td>
                <td><Link to={'/admin/users/' + user.user.recnum} >{loginName != null ? loginName : '-'}</Link></td>
                <td><Link to={'/admin/users/' + user.user.recnum} >{phone != null ? phone : '-'}</Link></td>
                <td><Link to={'/admin/users/' + user.user.recnum} >-</Link></td>
            </tr>
        );
    }

    render() {
        if (this.state.loading) {
            return (
                <i className='loader-item' ></i>
            )
        } else {
            return (
                <div>
                    {this.getUserTable()}
                </div>
            );
        }
    }

}

UserListAdmin.propTypes = {};
UserListAdmin.defaultProps = {};

export default UserListAdmin;