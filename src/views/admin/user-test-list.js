import React, {
    Component,
    PropTypes,
} from 'react';
import bindAll from 'lodash.bindall';
import {Link} from 'react-router';
import makeRequest from '../../utils/request-queue-handler';
import FrontendPage from '../../components/frontend-page';
import getObjectFromForm from '../../utils/object-from-form-generator';
import {getUserAnswersByUserRecnum} from '../../utils/admin/user-utils';
import {generatePdfFromHTML, getTextFromHybridMemo} from '../../utils/helpers';
import AdminSideMenu from './admin-side-menu';
import UserTestAdmin from './user-test';
import TestResults from '../../views/health-test/test-results';
import {Button, ButtonGroup, ButtonToolbar, MenuItem, SplitButton} from "react-bootstrap";

class UserTestListAdmin extends Component {
    constructor(props) {
        super(props);
        bindAll(this,
            'loadSelectedUsersTests',
            'selectedUserAnswerLoaded',
            'filterTestItems',
            'selectUserTest',
        );
        this.state = {
            tests: [],
            visibleTests: [],
            showAnswers: false,
            selectedTest: null,
            errors: [],
            loading: false
        }
    }

    loadSelectedUsersTests(user) {
        window.log('TEST FOR USER ', user);
        getUserAnswersByUserRecnum(user.recnum, this.selectedUserAnswerLoaded);
    }

    selectedUserAnswerLoaded(response) {
        window.log('USERS', response);
        this.setState({
            tests: response,
            visibleTests: response
        });
    }

    componentWillMount() {
        const {user} = this.props;
        if (user) {
            this.loadSelectedUsersTests(user);
            window.log('USER UPDATE');
        }
    }

    componentWillUpdate(nextProps, nextState) {
        const {user} = this.props;
        if (nextProps.user !== user) {
            if (nextProps.user) {
                this.loadSelectedUsersTests(nextProps.user);
                window.log('USER UPDATE');
            }
        }
    }

    filterTestItems() {
        const searchKey = document.getElementById('searchByTestTypeInput').value;
        window.log('searchKey', searchKey);
        var visibleList = [];
        for (var i = 0; i < this.state.tests.length; i++) {
            if (getTextFromHybridMemo(this.state.tests[i].questionGroup.title).toUpperCase().includes(searchKey.toUpperCase()) || this.state.tests[i].fillDate.toUpperCase().includes(searchKey.toUpperCase())) {
                var item = JSON.parse(JSON.stringify(this.state.tests[i]));
                visibleList.push(item);
            }
        }

        this.setState({
            visibleTests: visibleList
        });
    }

    selectUserTest(userTest) {
        this.setState({
            selectedTest: userTest
        });
    }

    downloadPdf() {
        generatePdfFromHTML('user-test');
    }

    render() {
        const {user} = this.props;
        const {visibleTests, selectedTest, showAnswers} = this.state;
        const that = this;

        if (selectedTest !== null) {
            const buttons = (
                <ButtonGroup>
                    <Button bsStyle='warning'
                            onClick={function () {
                        that.setState({selectedTest: null});
                    }} >
                        Vissza a tesztekhez
                    </Button>
                    {showAnswers &&
                    <Button bsStyle='primary'
                            onClick={function () {
                        that.setState({showAnswers: false});
                    }} >
                        Kiértékelés
                    </Button>
                    }
                    {showAnswers &&
                    <Button bsStyle='success'
                            onClick={this.downloadPdf} >
                        Válaszok letöltése (PDF)
                    </Button>
                    }
                    {!showAnswers &&
                    <Button bsStyle='primary'
                            onClick={function () {
                        that.setState({showAnswers: true});
                    }} >
                        Válaszok
                    </Button>
                    }
                </ButtonGroup>
            );
            return (
                <div>
                    {buttons}
                    {showAnswers && <UserTestAdmin testRecnum={selectedTest.recnum} />}
                    {!showAnswers && <TestResults recnum={selectedTest.recnum} />}
                </div>
            );
        } else {
            const testList = visibleTests.map(function (item, index) {
                const selectUserTest = function () {
                    that.selectUserTest(item);
                };
                return (
                    <div>
                        <a onClick={selectUserTest} >{getTextFromHybridMemo(item.questionGroup.title)}
                            : {item.fillDate}</a>
                    </div>
                );
            });
            return (
                <div>
                    <div className='col-sm-3' >
                        <input id='searchByTestTypeInput'
                               type='search'
                               name='searchByTestType'
                               onKeyUp={this.filterTestItems} />
                        {testList}
                    </div>
                    <div className='col-sm-9' >
                        {this.props.children}
                    </div>
                </div>
            );
        }
    }

}

UserTestListAdmin.propTypes = {};
UserTestListAdmin.defaultProps = {
    user: null
};

export default UserTestListAdmin;