import React, {Component} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import {unique, getTextFromHybridMemo} from '../../utils/helpers';
import {getUserAnswerForATestByRecnum} from "../../utils/admin/user-utils";

class UserTestAdmin extends Component {
    constructor(props) {
        super(props);
        bindAll(this,
            'loadSelectedUserTest',
            'selectedUserAnswerLoaded',
            'getQuestions',
            'getAnswersForQuestion',
            'getAnswer'
        );
        this.state = {
            fillDate: '',
            questions: [],
            errors: [],
            loading: false
        }
    }

    loadSelectedUserTest(testRecnum) {
        getUserAnswerForATestByRecnum(testRecnum, this.selectedUserAnswerLoaded);
    }

    componentWillMount() {
        const {testRecnum} = this.props;
        if (testRecnum !== null) {
            this.loadSelectedUserTest(testRecnum);
        }
    }

    componentWillUpdate(nextProps, nextState) {
        const {testRecnum} = this.props;
        if (nextProps.testRecnum !== testRecnum) {
            if (nextProps.testRecnum) {
                this.loadSelectedUserTest(nextProps.testRecnum);
            }
        }
    }

    selectedUserAnswerLoaded(response) {
        let questions = [];
        let fillDate = '';
        response.forEach(function (item, index) {
            fillDate = item.fillDate;
            questions = item.userAnswerItems.map(function (questionItem, index2) {
                window.log('QUESTION_ITEM', questionItem);
                return questionItem.answer.question;
            });
        });
        questions = unique(questions);

        questions = questions.map(function (question, index) {
            question.answers = [];
            response.forEach(function (responseItem, index2) {
                responseItem.userAnswerItems.forEach(function (userAnswerItem, index3) {
                    if (userAnswerItem.answer.question.recnum === question.recnum) {
                        var answer = JSON.parse(JSON.stringify(userAnswerItem.answer));
                        answer.addValue = userAnswerItem.addValue;
                        delete answer.question;
                        question.answers.push(answer);
                    }
                });
            });
            return question;
        });

        let result = [];
        questions.forEach(function (questionItem, index) {
            window.log('QUESTION RECNUM', questionItem.recnum);
            if (!result[questionItem.recnum]) {
                result[questionItem.recnum] = questionItem;
            }
        });
        window.log('QUESTION LENGTH', questions.length);
        window.log('RESULT ', result);

        result.sort(function (a, b) {
            if (getTextFromHybridMemo(a.questionText) < getTextFromHybridMemo(b.questionText)) return -1;
            if (getTextFromHybridMemo(a.questionText) > getTextFromHybridMemo(b.questionText)) return 1;
            return 0;
        });

        window.log('PROCESSED QUESTIONS', questions);
        this.setState({
            questions: result,
            fillDate: fillDate
        });
    }

    getQuestions() {
        const {questions} = this.state;
        const that = this;
        return questions.map(function (question, index) {
            window.log('QUESTION', question);
            return (
                <div>
                    <p>
                        <strong>{getTextFromHybridMemo(question.questionText)}</strong>
                    </p>
                    {that.getAnswersForQuestion(question)}
                </div>
            );
        });
    }

    getAnswersForQuestion(question) {
        const that = this;
        const answers = question.answers.map(function (answer, index2) {
            answer.unitType = question.unitType;
            window.log('ANSWER', answer);

            return that.getAnswer(answer);
        });
        return (
            <ul>
                {answers}
            </ul>
        );
    }

    getAnswer(answer) {
        let addValue = '';
        let unitType = '';
        if (answer.addValue !== null) {
            addValue = ": " + answer.addValue;
        }
        if (answer.unitType !== null) {
            unitType = answer.unitType + '';
        }
        return (
            <li>{getTextFromHybridMemo(answer.text) + addValue + " " + unitType}</li>
        );
    }

    render() {
        const {testRecnum} = this.props;
        const {fillDate} = this.state;
        return (
            <div>
                <div id='user-test' >
                    <h1>
                        {testRecnum === null ? '' : testRecnum + ' azonosítójú egészségteszt'}
                    </h1>
                    <p>Kitöltés ideje: {fillDate}</p>
                    {this.getQuestions()}
                </div>
            </div>
        );
    }

}

UserTestAdmin.propTypes = {
    testRecnum: PropTypes.number
};
UserTestAdmin.defaultProps = {
    testRecnum: null
};

export default UserTestAdmin;