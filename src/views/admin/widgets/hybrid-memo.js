import React, {
    Component,
    PropTypes,
} from 'react';
import bindAll from 'lodash.bindall';
import FrontendPage from '../../../components/frontend-page';
import {getHybridMemoMutation} from '../../../utils/queries/admin/dictionary-queries';
import getObjectFromForm from '../../../utils/object-from-form-generator';
import makeRequest from '../../../utils/request-queue-handler';
import Message from '../../../components/message';

class HybridMemoForm extends Component {
    constructor(props) {
        super(props);
        bindAll(this,
            'handleSubmit',
        );
        this.state = {
            loading: false,
            recnum: '',
            response: '',
            errors: []
        };
    }

    componentWillMount() {
        // const recnum = this.props.recnum ? this.props.recnum : '';
        this.setState({
            recnum: ''
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        window.log('SUBMIT theader', event);
        const formArray = event.target.elements;
        let hybridMemo;
        const that = this;
        try {
            hybridMemo = getObjectFromForm(event.target.getAttribute('data-queryParamName'), event.target.elements);
            window.log('SUBMIT hybridMemo', hybridMemo);
        } catch (err) {
            window.error('VALIDATE_HYBRIDMEMO_FORM_FAILED', err);
            return false;
        }
        makeRequest(getHybridMemoMutation(hybridMemo), function (response) {
            window.log('HybridMemo ADD', response);
            that.setState({
                response: response.data
            });
        }, function (error) {
            window.error('HybridMemo ADD', response);
            var errorsArray = [];
            if (errors) {
                errorsArray = errors[0].exception.validationResult.errors.map(function (error, index) {
                    return error;
                });
            } else {
                errorsArray.push("Valami hiba történt! Kérlek próbálkozz később!");
            }
            that.setState({
                errors: errorsArray
            });
        });
    }

    render() {
        return (
            <div>
                <Message
                    type='info'
                    closable={true}
                    message={this.state.response != '' ? ['Sikeresen létrhoztad a ' + this.state.response.HybridMemo[0].recnum + ' azonosítójú, HybridMemo-t, '] : []} />
                <FrontendPage errors={this.state.errors}
                              handleSubmit={this.handleSubmit}
                              queryName='hybrid_memo_admin'
                              loading={this.state.loading} />
            </div>
        );
    }
}

HybridMemoForm.propTypes = {};
HybridMemoForm.defaultProps = {};

export default HybridMemoForm;