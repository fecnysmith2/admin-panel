import React, {
   Component,
   PropTypes,
} from 'react';
import ContactPanel from '../../components/widgets/contact-panel-pros';

class ProFeri extends Component {

   constructor(props) {
       super(props);
       this.state = {
           success: false,
           showMessage: false
       }
   }

	render() {
		return (
			<div className="page-container faq container">
              <h1 className="h1">Győri Ferenc</h1>
              <div className="clearfix">
                <div className="col-md-4">
                  <img className="img-responsive" src="/img/csapat/feri_mkisebb.jpg" alt=""/>
                </div>
                <div className="col-md-8">
                  <h3 className="h3">Bemutatkozás</h3>
                  <p>Alap végzettségemet tekintve gyógytornász vagyok. Ebből kifolyólag a legfontosabb számomra az egészség megőrzése, helyreállítása. Tanárként, edzőként, sportszeretőként egyaránt a preventív szemlélet uralkodik az életemben.</p>
                  <h3 className="h3">Sportmúlt</h3>
                  <p>Gyerekkorom óta sportolok, szinte minden sportágat kipróbáltam a labdajátékoktól a küzdősportokig, de hamar rájöttem, hogy a versenysport nem az egészségről szól.</p>
                  <ContactPanel to="ferenc.gyori85@gmail.com" successMessage="Sikeresen elküldted az üzeneted.">
                    <p>Az alábbi űrlap segítségével üzenhetsz nekem.</p>
				  </ContactPanel>
                </div>
              </div>
            </div>
		);
	}
}

ProFeri.propTypes = {};
ProFeri.defaultProps = {};

export default ProFeri;