import React, {
    Component,
    PropTypes,
} from 'react';
import { Link } from 'react-router';
import { getUserObjectProperty } from '../../utils/user-util';
import withAuthCheck from '../../components/auth-check';
import ContactPanel from '../../components/widgets/contact-panel-pros';

class AssessmentCooper extends Component {
    constructor(props) {
        super(props);
        bindAll(this,
            'setDialogVisible'
        );

        this.state = {
            dialogVisible: false
        }
    }

    setDialogVisible() {
        this.setState({
            dialogVisible: true
        });
    }

	render() {
        const {dialogVisible} = this.state;
        const dialog = (
            <ContactPanel successMessage='Sikeresen elküldted az üzeneted.' subjectTag='Felmérés, Cooper '>
                <p>Az alábbi űrlap segítségével elküldheted nekünk.</p>
            </ContactPanel>
        );
		return (
			<div className='page-container faq container'>
                <h1 className='h1'>Cooper teszt</h1>
                <div className='clearfix'>
                  <iframe className='conditional-test-landing-video pull-left' width='390' height='219' src='https://www.youtube.com/embed/YCg4luoiNAc?autoplay=1&loop=1' frameborder='0' allowfullscreen/>
                  <div className=''>
                      <p>A Cooper teszt elvégzésére azért van szükség, mert segítségével pontos képet kapunk fittségi állapotodról, pulzusod alakulásáról. Ezek az eredmények elengedhetetlenek a céljaidnak megfelelő, személyre szabott edzésterv elkészítéséhez.</p>
                      <p>A teszt teljesíthető futással és kerékpározással. Elvégzéséhez megfelelő sportcipőre, kényelmes sportruházatra, pulzusmérő jeladóra és a mobilodon futó OnlineTrainer alkalmazásra van szükség.</p>
                  </div>
                  <div className='text-center'>
                    <p className='text-center'>Szerezd meg: Letölthető a Google Play-ből és az App Store-ból</p>
                    <a className='cooper-download-play' href='https://play.google.com/store/apps/details?id=hu.innotrain.onlinetrainer' target='_blank'><img src='/img/ikonok/letolt-google-play.png' alt='Szerezd meg: Google Play'/></a>
                    <a className='cooper-download-appstore' href='https://itunes.apple.com/hu/app/online-trainer/id868134198?mt=8' target='_blank'><img src='/img/ikonok/letolt-app-store.png' alt='Letölthető az App Store-ból'/></a>
                  </div>
                </div>
                <p>Maga a Cooper teszt 12 perc folyamatos futást vagy kerékpározást jelent. A cél az, hogy a lehető legnagyobb távolságot tedd meg 12 perc alatt. Ezt követően az eredményt rendszerünk automatikusan kiértékeli számodra.</p>
                <p className='pull-right'><img src='/img/felmeres/kondicionalis_teszt-mockup.jpg' className='conditional-test-img' alt=''/></p>
                <p>Amennyiben nincs jeladód, vagy még nem rendeltél, javasoljuk, hogy szerezd be minél előbb. Webshopunkban a mellkasi jeladótól az óráig többféle kiváló minőségű eszközből választhatsz. Használhatsz más BLE technológiás jeladót is, de a kínálatunkban nem megtalálható eszközök kompatibilitását nem garantáljuk.</p>
                <p>A cooper tesztet jeladó nélkül is elvégezheted, viszont ez esetben az edzések személyre szabhatósága nagymértékben csökken és nem tudunk megfelelő támogatást biztosítani a kardióedzések biztonságos és hatékony kivitelezéséhez.</p>
                <p>Ha részletesebb tájékoztatást szeretnél kapni a Cooper teszttel kapcsolatban, akkor nézd meg mellékelt videónkat.</p>
                <p>
                    <strong>Van még kérdésed?</strong>
                    {/*<Link className='link' to='/szakember-valaszto'>Tedd fel szakértőinknek.</Link>*/}
                </p>
                <p className='width-one-third'>
                    {dialogVisible ? dialog : <a className='link' onClick={this.setDialogVisible}>Írj nekünk</a>}
                </p>
            </div>
		);
	}
}
AssessmentCooper.propTypes = {};
AssessmentCooper.defaultProps = {};

export default withAuthCheck(AssessmentCooper);
