import React, {
   Component,
   PropTypes,
} from 'react';
import { Link } from 'react-router';
import { webshopUrl } from '../../../config';
import BBEmailCheckModal from '../../components/widgets/bb-email-check-modal';

class BBMoreShorterEdit extends Component {
   constructor(props) {
       super(props);
   }

	render() {
		return (
			<div className="">
				<BBEmailCheckModal />
	      <section className="landing-fitness-intro-bb-short">
	        <div className="container">
	          <div className="text-center">
	            <h2 className="h2 hidden-xs">A Budapest Bank és az Online Trainer közös Egészség programja</h2>
	            <h3 className="h3 visible-xs-block">A Budapest Bank és az Online Trainer közös egészségmegőrző és életmódváltó programja</h3>
	            <h3 className="h3">Változtass életeden <em>most</em>, legyél tudatosabb és egészségesebb már <em>holnap</em>!</h3>
	          </div>
	          <div className="clearfix">
	            <div className="">
	            <p>Életmód változtatáson többet értünk, mint módosítani az étkezéseken, illetve egy kicsivel többet mozogni a nap során. Az egészséges életmód kialakítása nem megy egyik napról a másikra, egy folyamat eredményeként jön létre. </p>
	            <div className="text-center">
	              <h3 className="h3">Hogyan támogatunk téged?</h3>
	            </div>
	            <p>A Budapest Bank évek óta elkötelezett dolgozói egészsége iránt, ezért az Online Trainer csapatával együtt kidolgoztunk egy olyan programot neked, mely hosszú távon támogatja egészséged és hozzásegít életminőséged javulásához.</p>
	            <p>Az Online Trainer egy olyan komplex megoldást kínál számodra, mellyel könnyedén elsajátíthatod az egészséges életmód alapjait és kiegyensúlyozottabbá válhatsz, valamint olyan tudásra is szert tehetsz, melyet egész életed során hasznosítani tudsz!</p>
	            </div>
	          </div>
	        </div>
	      </section>
	      <section className="landing-bb-short-program-points">
	        <div className="container clearfix">
	          <div className="col-xs-12">
	            <h2 className="h2">A program részletei</h2>
	            <div className="clearfix">
	              <img className="img" src="img/bb-rovid-program-reszletek.jpg" alt="Program részletek"/>
                  <p>A Budapest Bank Egészség Programján belül kétféle lehetőség közül választhatsz: <strong>Életmódváltó Program</strong> és <strong>Online Trainer Klubtagság</strong>.</p>
                  <p><strong>A program 2016. októberében indul, de regisztrálni már most tudsz rá.</strong></p>
	              <p>Ha csatlakozol a programhoz a Budapest Bank jelentős anyagi hozzájárulással segít céljaid elérésében. </p>
	              <p>Komplex életmódváltó programokra <strong>könnyen elkölthetsz 50-100.000 Ft-ot</strong>, de akár ennek többszörösét is. Te most ennek töredékéért vehetsz részt egy 6 hónapos programban a <strong>Budapest Bank és az Online Trainer támogatásával mindössze 7.000 Ft-ért</strong>, melyet adómentesen elszámolhatsz egészségpénztári számlád terhére vagy fizetheted a díjat 6 havi részletben is (1 x 2.000 Ft + 5 x 1.000 Ft).</p>
	              <p>A program részeként az alábbi szolgáltatásokhoz jutsz hozzá:</p>
	              <ul>
	                <li><strong>célkitűző munkafüzet</strong>, mely segít céljaid pontos megfogalmazásában, elhatározásod megerősítésében és fenntartásában,</li>
	                <li><strong>aktivitás napló</strong> és folyamatos motiváció, hogy ne csak egy múló elhatározás legyen az életmódváltás,</li>
	                <li><strong>széleskörű tudásanyag és szakmai támogatás</strong> mozgás, táplálkozás, regeneráció és stresszkezelés témákban,</li>
	                <li>kondícionális felmérésekre alapuló, <strong>személyreszabott</strong>, aktivitásodhoz igazodó, dinamikus <strong>edzésterv</strong>,</li>
	                <li><strong>interaktív edzéstámogatás</strong> és rögzítés, mobilalkalmazásban,</li>
	                <li><strong>közösségi felületek</strong>, hogy ne érezed magad egyedül és legyen aki példát mutat,</li>
	                <li><strong>jelentős kedvezmények</strong>, egyes sporteszközökre.</li>
	              </ul>
	              <p>Ha most belevágsz, egyből az Online Trainer klubtagjává is válsz, melynek részeként folyamatosan bővülő szolgáltatásokhoz jutsz hozzá, sőt, ez a tagság a program befejezését követően is fenntartható!</p>
	              <p>A szakszerű kivitelezés, egészséged biztonsága és az edzések maximális hatékonysága érdekében a program végrehajtásához egy Bluetooth pulzusmérő jeladóra lesz szükséged, melyet <strong>szintén kiemelt kedvezménnyel biztosítunk a program keretében</strong>. A választékról <Link to="/jelado-valaszto">itt tájékozódhatsz</Link>.</p>
	              <p>A program végrehajtásához szintén elengedhetelen egy Android 4.3 vagy iOs 8.4 verziójú okostelefon. <strong>Webshopunkból kiváló <a href="http://webshop.onlinetrainer.hu/kiegeszitok/karpantok-es-telefontokok" target="_blank">sport karpántokat</a> tudunk biztosítani szinte fillérekért</strong>, hogy mindig veled lehessen kedvenc készüléked.</p>
	              <div className="button-container text-center">
	                <Link to="/jelado-valaszto/eletmod" className="ot-btn btn-yellow">Csatlakozom az életmódváltó programhoz!</Link>
	                <p className="choose-beacon-notification">(a következő oldalon egy pulzusmérő jeladót választhatsz a mobilodhoz)</p>
	              </div>
	            </div>
	          </div>
	        </div>
	      </section>
	      <section className="landing-bb-short-6-months">
	        <div className="container">
	          <div className="col-sm-7">
	           <img src="img/bb-eletmod-landing.jpg" className="img-responsive" alt=""/>
	          </div>
	          <div className="col-sm-5">
	            <h2 className="h2">6 hónapos Online Trainer Klubtagság</h2>
	            <p>Számos nagyszerű szolgáltatást érhetnek el azok is, akik most úgy érzik nincs szükségük életmódváltásra, csak fejlődni akarnak vagy kontrolláltabban megfelelő támogatás mellett szeretnék edzéseiket megtartani, vagy szakmai (sport-, táplálkozási tanácsadás stb.) segítségre van szükségük. Az életmódváltó program mellett lehetőség van klubtagságunkra előfizetni, mely <strong>rengeteg hasznos funkciót tartalmaz, ám közel sem olyan komplex, mint az életmódváltó program</strong>.</p>
	            <p>A 6 hónapos időszakra szóló Online Trainer Klubtagságra – melyet szintén támogat a Budapest Bank - összesen 6.000 Ft-ért fizethetsz elő, tehát <strong>havi 1.000 Ft-ért megkapod</strong>, mint Budapest Bank dolgozó!</p>
	            <Link to="/jelado-valaszto/klubtagsag" className="ot-btn btn-yellow">Klubtagságot szeretnék!</Link><br/>
	            <span className="next-pulse-beacon">(a következő oldalon egy pulzusmérő jeladót választhatsz a mobilodhoz)</span>
	          </div>
	        </div>
	      </section>
	      <section className="landing-bb-running-field short">
	        <div className="container">
	          <div className="text-center">
	            <h2 className="h2">Mi a különbség <br/> a két program között?</h2>
	          </div>
	          <div className="clearfix">
	            <div className="col-xs-12 col-sm-12">
	              <p>Az Életmódváltó Program egy teljes körű program, ami az életed sok területén kínál fejlődési lehetőséget. Köztudott, hogy például egy diéta mozgás nélkül, vagy egy könnyű sportprogram étrendváltoztatás nélkül ritkán vezet sikerre. <strong>Az Életmódváltó Program komplex megoldást ad</strong>, hogy könnyebben érhess el eredményt.
	              </p>
	              <p>
	              Az Életmódváltó Program ezen felül tartalmazza az Online Trainer Klubtagság minden szolgáltatását. <strong>Az Életmódváltó Program díja a 6 hónapos Online Trainer Klubtagsággal együtt összesen 7.000 Ft</strong> (részletfizetés esetén: 2.000 Ft kezdő részlet + 5 x 1.000 Ft). <span className="no-instalments">(Tájékoztatunk, hogy egészségpénztári fizetés esetén nincs lehetőség részletfizetésre.)</span></p>

	              <p>Az Online Trainer Klubtagság eszközöket ad a kezedbe, hogy hatékonyabban tudj formába jönni és formában maradni, segít a testre szabott edzéseidet kontrollált, támogatott formában megtartani. Szakemberek segítségét is megkapod online formában és alapvetően a sporthoz, edzéshez és egészséges életmódhoz kapcsolódó tudásanyagokhoz férhetsz hozzá. A 6 hónapos Online Trainer Klubtagság díja most összesen 6.000 Ft (részletfizetés esetén: 6 x 1.000 Ft).</p>
	              <p><strong>Kinek melyik ajánlott?</strong></p>
	              <p>Az <strong>Életmódváltó Programot azoknak ajánljuk, akik szeretnének javítani az életminőségükön, szeretnének egészségesebbek, sportosabbak, kiegyensúlyozottabbak lenni</strong>, jobb közérzettel élni és ehhez átfogó támogatást kapni.</p>
                  <p>Az <strong>Online Trainer Klubtagságot azoknak ajánljuk</strong>, akik már aktívan sportolnak és hatékony segítségre vágynak.</p>
	              <div className="clearfix">
	                <div className="col-xs-12 text-center">
	                  <Link to="/jelado-valaszto/eletmod" className="ot-btn btn-yellow">Kérem a teljes programot!</Link>
	                </div>
	              </div>
	               <p className="text-center"><strong><Link to="/bb-osszehasonlito" className="check-bb-compare">Megnézem az Összehasonlító táblázatot!</Link></strong></p>
                </div>
	          </div>
	        </div>
	      </section>
			</div>
		);
	}
}

BBMoreShorterEdit.propTypes = {};
BBMoreShorterEdit.defaultProps = {};

export default BBMoreShorterEdit;