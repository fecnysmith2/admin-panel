import React, {Component} from 'react';
import {getUserObjectProperty} from '../utils/user-util';
import withAuthCheck from '../components/auth-check';
import {observ} from '../utils/observables';

class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
        }
    }
}

Dashboard.propTypes = {};
Dashboard.defaultProps = {};

export default withAuthCheck(Dashboard);