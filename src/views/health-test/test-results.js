import React, {
    Component,
    PropTypes,
} from 'react';
import bindAll from 'lodash.bindall';
import {withRouter, Link} from 'react-router';
import makeRequest from '../../utils/request-queue-handler';
import {getTestResults} from '../../utils/queries/queries';
import ContactPanel from '../../components/widgets/contact-panel';
import ModalDialog from '../../components/modal';

class TestResults extends Component {
    constructor(props) {
        super(props);
        bindAll(this,
            'getTextualNode',
            'onDataReceived',
            'onError',
            'openCoachContactModal',
            'closeCoachContactModal',
            'onCoachContactCallback'
        );
        this.state = {
            data: [],
            modalIsOpen: false,
            messageSended: false
        };
    }

    getTextualNode(text, index) {
        var data = text.evaluationText;
        switch (data.type) {
            case 'C':
                return <div className={'health-test-result-title-container'} >
                    <h1 className={'health-test-result-title ' + (index == 0 ? 'h1' : 'h3')}
                        key={index} >{data.text}</h1>
                </div>;
            case 'T':
                return <div className={'health-test-result-text-container'} >
                    <p className={'health-test-result-text'}
                       key={index} >{data.text}</p>
                </div>;
            case 'L':
                var text = data.text;
                text = text.replace('°', '');
                return <div className={'health-test-result-list-container'} >
                    <li className={'health-test-result-list'}
                        key={index} >{text}</li>
                </div>;
            case 'V':
                return <div className={'health-test-result-variable-text-container'} >
                    <p className={'health-test-result-variable-text'}
                       key={index} >
                        <strong>{data.text}</strong>
                    </p>
                </div>;
            case 'F':
                var lights = [];
                var greens = data.text.split('/')[0];
                for (var i = 1; i <= 3; i++) {
                    if (i <= greens) {
                        lights.push(<div key={i + 100}
                                         className='result-circle good' ></div>);
                    } else {
                        lights.push(<div key={i + 100}
                                         className='result-circle bad' ></div>);
                    }
                }
                return <div>{lights}</div>;
            default:
                return <p key={index} >
                    <small>{data.text}</small>
                </p>
        }
    }

    onDataReceived(response) {
        const {recnum} = this.props;
        /*@TODO hiba kezelés finomítása szükséges, hibaüzenet megjelenítése hibától függően.*/
        if (response.errors.length > 0 && recnum == null) {
            var redirectURL = btoa('test_eval/' + this.props.params.recnum);
            this.props.router.push('/belepes/' + redirectURL);
            return;
        }
        if (response.data) {
            const data = response.data.UserAnswerEvaluationText.map(this.getTextualNode);
            this.setState({
                data: data
            });
        }
    }

    onError(error) {
        /*@TODO hiba kezelés finomítása szükséges, hibaüzenet megjelenítése hibától függően.*/
        if (error.length > 0 && recnum == null) {
            var redirectURL = btoa('test_eval/' + this.props.params.recnum);
            this.props.router.push('/belepes/' + redirectURL);

        }
    }

    componentWillMount() {
        const {recnum, params} = this.props;
        const testRecnum = recnum != null ? recnum : params.recnum;
        makeRequest(getTestResults(testRecnum), this.onDataReceived, this.onError);
    }

    openCoachContactModal() {
        this.setState({
            modalIsOpen: true
        });
    }

    closeCoachContactModal() {
        this.setState({
            modalIsOpen: false
        });
    }

    onCoachContactCallback(response) {
        this.setState({
            messageSended: true
        });
    }

    render() {
        const {recnum} = this.props;
        const okButton = <input type="submit"
                              value="Oké"
                              onClick={this.closeCoachContactModal} />;
        return (
            <div className='page-container container' >
                <div className='result-content' >
                    {this.state.data}
                </div>
                <ModalDialog open={this.state.modalIsOpen}
                             close={this.closeCoachContactModal}
                             modalClose={true}
                             enableClose={true} >
                    <p>Sorold fel mivel kapcsolatban szeretnél tanácsot kapni!</p>
                    <ContactPanel
                        hideSendButtonOnSuccess={true}
                        email={false}
                        phone={true}
                        successMessage='Kérésedet továbbítottuk tanácsadód felé!'
                        to='lilla@onlinetrainer.hu'
                        callBack={this.onCoachContactCallback} />
                    {this.state.messageSended ? okButton : ''}
                </ModalDialog>
                <p>
                    Amennyiben szeretnél személyre szabott tanácsadást kérni, megteheted
                    <span className='cursor-pointer'
                          onClick={this.openCoachContactModal} ><strong> <u>itt.</u></strong></span>
                </p>
                {recnum == null ? <Link to='/dashboard'
                      className='ot-btn btn-yellow'
                      style={{marginLeft: "200px", float: "left"}} >Vissza a kezdőlapra</Link> : '' }
            </div>
        );
    }

}

TestResults.propTypes = {};
TestResults.defaultProps = {
    recnum: null
};

export default withRouter(TestResults);