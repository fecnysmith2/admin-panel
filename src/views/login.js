import React, {Component} from "react";
import {bindAll} from "lodash";
import makeRequest from '../utils/request-queue-handler';
import {encodeAndSaveObjectToLocalStore, saveObjectToLocalStore, saveOauthToken} from '../utils/db';
import passwordHash from "password-hash";
import axios from 'axios';
import {SHOP_KEY, SHOP_PASS, API_URL} from '../../config';

import xhr from 'xhr';
import {saveAccountObject, saveAccountStores, setSelectedStore} from "../utils/user-util";
import Loading from "../components/loading";


class Login extends Component {
    constructor(props) {
        super(props);

        bindAll(this,
            'loginAccount'
        );

        this.state = {
            'loadingSignup': false,
            'loginError': false
        };

    }

    loginAccount(event){
        event.preventDefault();
        let formData = $('#form-one').serializeObject();
        window.log(formData);
        let that = this;
        that.setState({loadingSignup: true});

        axios.post(API_URL+'/api/oAuth/login', formData)
            .then((response) => {
                window.log("login response", response)
                saveOauthToken("oAuth_token", response.data.token);
                saveAccountObject(response.data.account);

                // get stores if login is valid

                makeRequest("getStores", {account_id: response.data.account._id}, function (ret) {
                    window.log("getStores return", ret);
                    saveAccountStores(ret);
                    setSelectedStore(ret[0]);
                    window.location.href="/dashboard";
                });


            })
            .catch((error) => {
                window.error("login response", error)
                that.setState({loginError: true, loadingSignup: false})
            });
    }

    render() {
        return (<main id="main-container" style={{"minHeight": "370px"}}><div className="bg-gd-dusk">
            <div className="hero-static content content-full bg-white js-appear-enabled animated fadeIn" data-toggle="appear">
                <div className="py-30 px-5 text-center">
                    <a className="link-effect font-w700" href="https://shopycloud.org">
                        <span className="font-size-xl text-primary-dark">Shopy</span><span className="font-size-xl">Clouds</span>
                    </a>
                    <h2 className="h4 font-w400 text-muted mb-0">Please sign in</h2>
                    <h5 className="h4 font-w400 text-muted mb-0">
                        Legyen részese valami újnak! Próbálja ki ShopyCloud webáruház hálózatunkat.
                    </h5>
                </div>
                <div className="row justify-content-center px-5">
                    <div className="col-sm-8 col-md-6 col-xl-4">

                        {this.state.loadingSignup ? <div className="spinner"/> : (<form className={this.state.loginError ? "shake animated js-validation-signin" : "js-validation-signin"} autoComplete="off" role="presentation" id="form-one" onSubmit={this.loginAccount}>
                            <div className={this.state.loginError ? "row is-invalid form-group" : " row form-group"}>
                                <div className="col-12">
                                    <div className="form-material floating">
                                        <input type="email"
                                               className="form-control" id="email"
                                               autoComplete={"false"}
                                               name="email"
                                        />
                                            <label htmlFor="email">E-mail címed</label>
                                    </div>
                                </div>
                            </div>
                            <div className={this.state.loginError ? "row is-invalid form-group" : " row form-group"}>
                                <div className="col-12">
                                    <div className="form-material floating">
                                        <input type="password" className="form-control" autoComplete={"new-password"} id="" name="password"/>
                                            <label htmlFor="password">Jelszavad</label>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group row gutters-tiny">
                                <div className="col-12 mb-10">
                                    <button type="submit" className="btn btn-block btn-hero btn-noborder btn-rounded btn-alt-primary">
                                        <i className="si si-login mr-10"></i> Bejelentkezés
                                    </button>
                                </div>
                            </div>
                        </form>)}
                    </div>
                </div>
            </div>
        </div>
        </main>);

        return (<div className="login-container">
            <h2 className="m-n fw-thk"><span className="text-success">SHOPY</span>CLOUD</h2>
            <h3>Legyen részese valami újnak! Próbálja ki ShopyCloud webáruház hálózatunkat.</h3>
            <p></p>

            <div className="social-sign-up text-center">
                <p className="header text-uppercase text-center">Hitelesítse magáta következők egyikével</p>
                <ul className="list-inline m-md-t m-md-b">
                    <li className="w-75"><a className="text-grey" href="javascript:void(0)"><i className="fa fa-2x fa-facebook fa-fw"></i></a></li>
                    <li className="w-75"><a className="text-grey" href="javascript:void(0)"><i className="fa fa-2x fa-google-plus fa-fw"></i></a></li>
                    <li className="w-75"><a className="text-grey" href="javascript:void(0)"><i className="fa fa-2x fa-twitter fa-fw"></i></a></li>
                </ul>
            </div>

            <p className="m-md text-center text-uppercase"><small>vagy</small></p>

            {this.state.loadingSignup ? <div className="spinner"/> : <form id="form-one" onSubmit={this.loginAccount}>
                <p className="header text-uppercase text-center m-md-b">Jelentkezzen be ShopyCloud fiókjával</p>
                {this.state.loginError && <div className="text-center alert alert-danger">
                    <i className="fa fa-warning pull-left" style={{"marginTop": "8px"}}/>
                    <p className="uk-margin-small-top"><strong>Hiba,</strong> nem megfelelő bejelentkezési adatok!</p>
                </div>}
                <div className="form-group">
                    <label for="email"><i className="fa fa-user m-xs-r"></i>E-mail cím</label>
                    <input type="text" className="form-control" id="email" name="email"/>
                </div>

                <div className="form-group">
                    <label for="password"><i className="fa fa-lock m-xs-r"></i>Jelszó</label>
                    <input type="password" className="form-control" id="password" name="password"/>
                </div>

                <p className="m-xl-t m-md-b"><button type="submit" className="btn btn-primary w-150">Bejelentkezés</button></p>

                <div className="row">
                    <div className="col-xs-6">
                        <p><a href="pages.forgot-password.html" className="text-grey">Elfelejtette jelszavát?</a></p>
                    </div>
                </div>
            </form>}

        </div>);
    }
}

Login.propTypes = {};
Login.defaultProps = {};

export default Login;