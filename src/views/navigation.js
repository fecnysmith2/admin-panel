import React, {Component} from "react";
import {bindAll} from "lodash";
import {Link} from 'react-router';
import {getAccountName, getAccountObject} from "../utils/user-util";

class Navigation extends Component {
    constructor(props) {
        super(props);
        // bindAll(this,
        //     'searchQueryEntered'
        // );

        this.state = {
            accountName: getAccountName()
        };
    }

    render() {

        const Logout = function(event){
            localStorage.removeItem("account_object");
            localStorage.removeItem("account_stores");
            localStorage.removeItem("oAuth_token");
            localStorage.removeItem("selected_store");
        };

        return (<nav id="sidebar">
            <div id="sidebar-scroll">
                <div className="sidebar-content">
                    <div className="content-header content-header-fullrow px-15">
                        <div className="content-header-section sidebar-mini-visible-b">
<span className="content-header-item font-w700 font-size-xl float-left animated fadeIn">
<span className="text-dual-primary-dark">s</span><span className="text-primary">c</span>
</span>
                        </div>
                        <div className="content-header-section text-center align-parent sidebar-mini-hidden">
                            <button type="button" className="btn btn-circle btn-dual-secondary d-lg-none align-v-r"
                                    data-toggle="layout" data-action="sidebar_close">
                                <i className="fa fa-times text-danger"></i>
                            </button>
                            <div className="content-header-item">
                                <a className="link-effect font-w700" href="index.php">
                                    <span className="font-size-xl text-dual-primary-dark">Shopy</span><span
                                    className="font-size-xl text-primary">Clouds</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="content-side content-side-full content-side-user px-10 align-parent">
                        <div className="sidebar-mini-visible-b align-v animated fadeIn">
                            <img className="img-avatar img-avatar32" src="https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-9/22045749_1688380197848150_3869550302776614939_n.jpg?oh=beefe613c61b3fc1a480f204490243eb&oe=5A86080A" alt=""/>
                        </div>
                        <div className="sidebar-mini-hidden-b text-center">
                            <a className="img-link" href="be_pages_generic_profile.php">
                                <img className="img-avatar" src="https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-9/22045749_1688380197848150_3869550302776614939_n.jpg?oh=beefe613c61b3fc1a480f204490243eb&oe=5A86080A" alt=""/>
                            </a>
                            <ul className="list-inline mt-10">
                                <li className="list-inline-item">
                                    <Link className="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase"
                                       to="/profile">F. Ferenc</Link>
                                </li>
                                <li className="list-inline-item">
                                    <a className="link-effect text-dual-primary-dark has-notification" href="javascript:void(0)">
                                        <i className="si si-drop"></i>
                                    </a>
                                </li>
                                <li className="list-inline-item">
                                    <Link className="link-effect text-dual-primary-dark" onClick={Logout} to={"/login"}>
                                        <i className="si si-logout"></i>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="content-side content-side-full">
                        <ul className="nav-main">
                            <li>
                                <Link to="/dashboard"><i className="si si-speedometer"></i><span
                                    className="sidebar-mini-hide">Vezérlőpult</span></Link>
                            </li>
                            <li>
                                <Link to="/insider"><i className="si si-shield"></i><span
                                    className="sidebar-mini-hide">Insider központ</span></Link>
                            </li>
                            <li className="nav-main-heading"><span className="sidebar-mini-visible">ÁR</span><span
                                className="sidebar-mini-hidden">Áruház</span></li>
                            <li>
                                <a className="nav-submenu" data-toggle="nav-submenu" href="#"><i
                                    className="fa fa-shopping-bag"></i><span
                                    className="sidebar-mini-hide">Rendelések</span></a>
                                <ul>
                                    <li>
                                        <Link to="/order/list">Nyitott rendelések (0)</Link>
                                    </li>
                                    <li>
                                        <Link to="/order/archive">Archívum</Link>
                                    </li>
                                    <li>
                                        <Link to="/order/export">Exportálás</Link>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a className="nav-submenu" data-toggle="nav-submenu" href="#"><i
                                    className="fa fa-hashtag"></i><span
                                    className="sidebar-mini-hide">Csoportosítások</span></a>
                                <ul>
                                    <li>
                                        <Link to="/category/list">Termékkategóriák</Link>
                                    </li>
                                    <li>
                                        <Link to="/tags/list">Cimkék</Link>
                                    </li>
                                    <li>
                                        <Link to="/placements/list">Kiemelések, elhelyezések</Link>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <Link className="nav-menu" to="/product/list"><i
                                    className="fa fa-cubes"></i><span
                                    className="sidebar-mini-hide">Termékek</span></Link>
                            </li>




                            <li className="nav-main-heading"><span className="sidebar-mini-visible">BE</span><span
                                className="sidebar-mini-hidden">Tartalmak</span></li>
                            <li>
                                <a className="nav-submenu" data-toggle="nav-submenu" href="#"><i
                                    className="si si-flag"></i><span
                                    className="sidebar-mini-hide">Oldalak</span></a>
                                <ul>
                                    <li>
                                        <a href="be_pages_error_all.php">Aktív oldalak (5)</a>
                                    </li>
                                    <li>
                                        <a href="be_pages_error_all.php">Landing-oldalak</a>
                                    </li>
                                </ul>
                            </li>

                            <li className={"hidden"}>
                                <a className="nav-submenu" data-toggle="nav-submenu" href="#"><i
                                    className="si si-bubbles"></i><span
                                    className="sidebar-mini-hide">Hozzászólások</span></a>
                                <ul>
                                    <li>
                                        <a href="be_pages_forum_categories.php">Moderálandó (4)</a>
                                    </li>
                                    <li>
                                        <a href="be_pages_forum_topics.php">Idővonal</a>
                                    </li>
                                    <li>
                                        <a href="be_pages_forum_topics.php">Arhív</a>
                                    </li>
                                </ul>
                            </li>
                            <li className="nav-main-heading"><span className="sidebar-mini-visible">BE</span><span
                                className="sidebar-mini-hidden">Beállítások</span></li>
                            <li>
                                <a className="nav-submenu" data-toggle="nav-submenu" href="#"><i
                                    className="fa fa-wrench"></i><span
                                    className="sidebar-mini-hide">Általános</span></a>
                                <ul>
                                    <li>
                                        <Link to="/store/settings/basic">Általános beállítások</Link>
                                    </li>
                                    <li>
                                        <Link to="/store/settings/payment">Fizetés & szállítás</Link>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a className="nav-submenu" data-toggle="nav-submenu" href="#"><i
                                    className="si si-vector"></i><span
                                    className="sidebar-mini-hide">Megjelenés</span></a>
                                <ul>
                                    <li>
                                        <Link to="/store/settings/appearance">Általános</Link>
                                    </li>
                                    <li>
                                        <Link to="/store/settings/layout">Elrendezések</Link>
                                    </li>
                                    <li>
                                        <Link to="/store/settings/emails">E-mail események</Link>
                                    </li>
                                </ul>
                            </li>
                            <li className="open">
                                <a className="nav-submenu" data-toggle="nav-submenu" href="#"><i
                                    className="si si-globe-alt"></i><span
                                    className="sidebar-mini-hide">Lokalizáció</span></a>
                                <ul>
                                    <li>
                                        <a href="be_pages_error_all.php">Nyelvek</a>
                                    </li>
                                    <li>
                                        <a href="be_pages_error_all.php">Statikus fordítások</a>
                                    </li>
                                    <li>
                                        <a href="be_pages_error_all.php">Devizák</a>
                                    </li>
                                    <li>
                                        <a href="be_pages_error_all.php">Árfolyamok</a>
                                    </li>
                                    <li>
                                        <a href="be_pages_error_all.php">Korlátozások</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>);
    }
}

Navigation.propTypes = {};
Navigation.defaultProps = {};

export default Navigation;