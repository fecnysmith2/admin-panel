import React, {
    Component,
} from 'react';
import makeRequest from '../../../utils/request-queue-handler'
import Nestable from 'react-nestable';
import {bindAll} from "lodash";
import {Modal} from "react-bootstrap";
import {Slug} from '../../../utils/url'
import {TITLE_BASE} from "../../../../config";


class Category extends Component {

    constructor(props) {
        super(props);

        bindAll(this,
            'loadCategoryData', 'toggleCategoryModal', 'generateURL',
            'saveCategory', 'removeCategory'
        );

        this.state = {
            'loading': false,
            'baseLoading': false,
            'categoryData': [],
            'keyUpTimer': null,
            'modalOpened': false,
            'modalSettings': {headerString: 'Új kategória hozzáadása', subString: ''},
            'editCategory': null,
            'validateCategoryName': false,
            'categorySuccessSave': false
        };
        this.loadCategoryData();
    }

    componentWillMount() {
        document.title = "Kategóriák" + " | "+TITLE_BASE;
    }

    validateCategory(data, callback){
        let that = this;
        makeRequest("validateCategoryURL", {"url_string": data.url_string}, function (ret) {
            let status = 'success';
            if(data.url_string !== ret.nextValidURL){
                status = 'error';
            }
            callback({status: status, newURL: ret.nextValidURL});
        });
    }

    saveCategory(event){
        event.preventDefault();
        let that = this;

        const categoryName = document.getElementById("properties.name").value;
        const categoryURL = document.getElementById("url_string").value;

        if(!this.state.editCategory) {
            makeRequest("createCategory", {
                "url_string": {"hu": categoryURL, "en": categoryURL},
                "properties": {"name": {"hu": categoryName, "en": categoryName}},
                "productCount": 0, "parent": null
            }, function (ret) {
                that.setState({categorySuccessSave: true}, that.toggleCategoryModal);
                that.loadCategoryData();
            });
        }else{
            makeRequest("editCategory", {ObjectID: this.state.editCategory.id, changes: {
                "url_string": {"hu": categoryURL, "en": categoryURL},
                "properties": {"name": {"hu": categoryName, "en": categoryName}}
            }}, function (ret) {
                that.setState({categorySuccessSave: true}, that.toggleCategoryModal);

                that.loadCategoryData();
            });
        }
        return false;
    }

    loadCategoryData() {
        let that = this;
        makeRequest("getCategories", {}, function (ret) {
            window.log("getCategories return", ret);
            that.setState({categoryData: ret, baseLoading: false});
        });
    }

    menuReOrder(items, editedItem){
        window.log("menuReOrder items:", items);
        window.log("menuReOrder editedItem:", editedItem);
        let that = this;
        makeRequest("reorderCategories", {"reorderData": items}, function (ret) {
            window.log("reorderCategories return", ret);
            that.loadCategoryData();
        });
    }

    generateURL(event){
        let that = this;
        clearTimeout(that.state.keyUpTimer);
        that.setState({keyUpTimer:null});

        const keyUpTimer = setTimeout(function(){
            const categoryName = document.getElementById("properties.name").value;
            that.setState({validateCategoryName: true});

            that.validateCategory({'url_string': Slug(categoryName)}, function(returnAns){
                if(returnAns.status === "error") {
                    document.getElementById("url_string").value = returnAns.newURL;
                }else{
                    document.getElementById("url_string").value = Slug(categoryName);
                }
                that.setState({validateCategoryName: false});
            });
        }, 400);

        that.setState({keyUpTimer:keyUpTimer});
    }

    removeCategory(event){
        event.preventDefault();
        let that = this;
        if(this.state.editCategory){
            if(window.confirm("Biztosan törölni szeretnéd ezt a kategóriát?")){
                makeRequest("removeCategory", {"ObjectID": this.state.editCategory.id}, function (ret) {
                    window.log("removeCategory return", ret);
                    that.toggleCategoryModal();
                    that.loadCategoryData();
                });
            }
        }
        return false;
    }
    
    renderModalContent(){
        return (<Modal show={this.state.modalOpened} keyboard={true} onHide={this.toggleCategoryModal}>
                    <Modal.Header closeButton>
                        <p className="header text-uppercase">{this.state.modalSettings.headerString}</p>
                        <p>{this.state.modalSettings.subString}</p>

                    </Modal.Header>
                    <Modal.Body>
                        {this.state.categorySuccessSave ? (
                            <div className="alert alert-success">Sikeresen elmentettük a kategóriát!</div>
                        ) : (<form id="form-one" className="m-md-t" onSubmit={function(){return false;}}>
                            <div className="form-group form-group-default">
                                <label htmlFor="properties.name"><i className="fa fa-user m-xs-r"/>Kategória neve</label>
                                <input onKeyUp={this.generateURL} type="text" className="form-control" id="properties.name" name="properties.name"/>
                            </div>

                            <div className="form-group form-group">
                                <label htmlFor="url_string"><i className="fa fa-at m-xs-r"/>URL</label>
                                <input type="text" className="form-control" id="url_string" name="url_string" readOnly={true}/>
                                <div className={"loading"+(this.state.validateCategoryName ? "" : " hidden")} style={{'position':'absolute', 'marginTop':'-42px', 'right':'35px'}}/>
                            </div>

                            {this.state.editCategory ? (<button className="btn btn-danger w-150 m-sm-t pull-left" onClick={this.removeCategory}>Törlés</button>) : ""}

                            <button className="btn btn-primary w-150 m-sm-t pull-right" onClick={this.saveCategory}>Mentés</button>
                            <div className="clearfix"/>
                        </form>)}
                    </Modal.Body>
                </Modal>);
    }

    toggleCategoryModal(){
        // if close modal, empty states
        if(this.state.modalOpened){
            this.setState({editCategory: null});
        }
        this.setState({categorySuccessSave: false});
        this.setState({modalOpened:!this.state.modalOpened});

        if(this.state.editCategory) {
            this.setState({modalSettings: {headerString: this.state.editCategory.text + ' szerkesztése', subString: ''}});
        }else{
            this.setState({modalSettings: {headerString: 'Új kategória hozzáadása', subString: ''}});
        }

        let that = this;

        const setModalFormData = function(){
            if(that.state.editCategory) {
                if (document.getElementById("properties.name")) {
                    document.getElementById("properties.name").value = that.state.editCategory.text;
                    document.getElementById("url_string").value = that.state.editCategory.url_string;
                    document.getElementById("properties.name").focus();
                } else {
                    setTimeout(setModalFormData, 200);
                }
            }
        };
        if(that.state.editCategory) {
            setModalFormData();
        }
    }

    render() {
        if(this.state.baseLoading){ return (<div className="spinner"/>); }
        let that = this;
        const renderItem = ({item}) => {
            const editItem = function(){
                that.setState({editCategory:item}, function(){that.toggleCategoryModal()});
            };
            return (<div>
                        <span className="categoryName">{item.text}</span>
                        <span className="productsCount">
                            (<strong>{item.productsCount}</strong> terméket tartalmaz,
                            <strong>n/A Ft</strong> átlagos havi profit)   </span>
                        <button onClick={editItem} className="pull-right btn btn-small btn-primary">
                            <i className="fa fa-pencil"/>
                        </button><button onClick={editItem} className="pull-right btn btn-small btn-secondary">
                            <i className="fa fa-bar-chart"/>
                        </button>
                    </div>);
        };

        function generateCategoryArray(fromArray = []){
            window.log("fromObject",fromArray);
            let returnArray = [];
            fromArray.forEach(function(rootCategory){
                if(rootCategory.children.length > 0){
                    returnArray.push({id: rootCategory._id, url_string: rootCategory.url_string.hu, productsCount: rootCategory.productCount, text: rootCategory.properties.name.hu, children: generateCategoryArray(rootCategory.children)});
                }else{
                    returnArray.push({id: rootCategory._id, url_string: rootCategory.url_string.hu, productsCount: rootCategory.productCount, text: rootCategory.properties.name.hu});
                }
            });
            return returnArray;
        }

        let categories = generateCategoryArray(this.state.categoryData);
        window.log(categories);

        return (<div className="content">
            <h2 className="content-heading mb-0">
                Kategóriák

            </h2>
            {this.renderModalContent()}
            <div className="row">
                <div className="col-md-8">
                    <div className="block pb-3">
                        <div className="block-content">
                            <div className="row">
                                <div className="col-md-12">
                                    {this.state.categoryData.length > 0 ? (<Nestable
                                        onChange={this.menuReOrder}
                                        items={categories}
                                        renderItem={renderItem}
                                        collapseIcon={true}
                                    />) : (<div>
                                        <h3 className="fw-nml text-center">Úgy tűnik, még nincs egyetlen kategória sem rögzítve!</h3>
                                        <hr/>
                                        <h4 className="fw-lgt text-justify">A jobb oldali panelben található <button onClick={this.toggleCategoryModal} type="button" className="btn btn-primary">
                                            <i className="fa fa-plus fa-fw m-r-xs"/> Új kategória</button> gomb segítségével hozza létre első kategóriáját a folytatáshoz!</h4>
                                    </div>)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4">
                    <div className="panel-x panel-transparent">
                        <div className="panel-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <button onClick={this.toggleCategoryModal} data-toggle="modal" data-target=".category-modal" type="button" className="btn btn-primary btn-block">
                                        <i className="fa fa-plus fa-fw m-r-xs"/> Új kategória</button>
                                    <hr/>
                                    <p className="text-justify">
                                        A kategória kezelővel könnyedén rendezheti kategóriáit, módosíthatja az alábontásokat.
                                        <br/>Használja grafikonjainkat a <code>részletes analitikákhoz</code>, valamint <code>hatékonyságot növelő inteligens tippjeinkhez</code>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>);
    }
}

Category.propTypes = {};
Category.defaultProps = {};

export default Category;