import React, {
    Component,
} from 'react';
import makeRequest from '../../../utils/request-queue-handler'
import {bindAll} from "lodash";
import Dropzone from 'react-dropzone'
import {Modal} from "react-bootstrap";
import {SHOP_KEY, SHOP_PASS, API_URL, TITLE_BASE} from '../../../../config';
var sendPostRequest = require("../../../utils/xhr-request.js").sendPostRequest;


class Media extends Component {

    constructor(props) {
        super(props);

        bindAll(this,
            'loadMediaData', 'onMediaUpload', 'mediaPanel',
            'renderModalContent', 'removeMedia', 'saveMedia',
            'toggleModal'
        );

        this.state = {
            loading: false,
            baseLoading: false,
            mediaData: [],
            dropzoneRef: null,
            modalOpened: false,
            editedMedia: false
        };
        this.loadMediaData();
    }

    componentWillMount() {
        document.title = "Médiaelemek" + " | "+TITLE_BASE;
    }


    loadMediaData() {
        let that = this;
        makeRequest("getMedias", {}, function (ret) {
            window.log("getCategories return", ret);
            that.setState({mediaData: ret, baseLoading: false});
        });
    }

    onMediaUpload(files) {
        window.log("onMediaUpload files", files);
        let mediaData = this.state.mediaData;
        let that = this;
        files.forEach(function (thisFile) {
            thisFile.url = thisFile.preview;
            thisFile.onUpload = true;
            mediaData.push(thisFile);
        });
        this.setState({
            mediaData: mediaData
        });

        let uploadedMedias = [];
        files.forEach(function (thisFile) {
            let formData = new FormData();
            formData.append('fileData', thisFile);
            sendPostRequest(API_URL + '/api/upload', formData).then(function (mediaId) {
                uploadedMedias.push(mediaId.body);
                if (files.length === uploadedMedias.length) {
                    that.loadMediaData();
                }
            });
        });
        return false;
    }

    mediaPanel() {
        const dropFileString = (<h4 style={{"marginTop": "40px"}} className="text-center">
            Húzza ide képeit, vagy kattintson az
            <button className="btn btn-primary" onClick={() => {
                this.state.dropzoneRef.open()
            }}>
                <i className="fa fa-plus fa-fw"/>
                Új médiaelem
            </button>
            gombra...
        </h4>);
        const overlayStyle = {
            width: "100%",
            height: "auto",
            minHeight: "200px"
        };
        let that = this;
        const showImageBox = function (f) {
            const openMediaModal = function (event) {
                event.preventDefault();
                that.setState({editedMedia: f}, that.toggleModal);
                return false;
            };
            return (<div
                className={f.onUpload ? "product_edit-filePreview text-center waiting-for-upload" : "product_edit-filePreview text-center"}>
                <div className="imageData" style={{"backgroundImage": "url(" + f.url + ")"}}/>
                {!f.onUpload ? (<span className="fa-stack fa-lg" onClick={openMediaModal}>
                  <i className="fa fa-circle fa-stack-2x"/>
                  <i className="fa fa-eye fa-stack-1x fa-inverse"/>
                </span>) : ""}
            </div>);
        };

        return (<section>
            <div className="dropzone">
                <Dropzone disableClick={true} onDrop={this.onMediaUpload} style={overlayStyle} ref={(node) => {
                    this.state.dropzoneRef = node;
                }}>
                    {this.state.mediaData.length > 0 ? this.state.mediaData.map(f => showImageBox(f)) :
                        dropFileString}
                    <div className="clearfix"/>
                </Dropzone>
            </div>
        </section>);
    }

    toggleModal() {
        // if close modal, empty states
        if (this.state.modalOpened) {
            this.setState({editedMedia: null});
        }

        this.setState({modalOpened: !this.state.modalOpened});

        let that = this;

        const setModalFormData = function () {
            if (that.state.editedMedia) {
                if (document.getElementById("name")) {
                    document.getElementById("name").value = that.state.editedMedia.name;
                    document.getElementById("url").value = that.state.editedMedia.url;
                    document.getElementById("previewImage-modal").src = that.state.editedMedia.url;
                    document.getElementById("name").focus();
                } else {
                    setTimeout(setModalFormData, 200);
                }
            }
        };
        if (that.state.editedMedia) {
            setModalFormData();
        }
    }

    removeMedia(event) {
        let that = this;
        event.preventDefault();
        if(window.confirm("Biztosan törölni szeretnéd ezt a médiaelemet? Ezzel megszűnik minden kapcsolat (Termék, oldal, blog, stb)")){
            makeRequest("removeMedia", {"ObjectID": this.state.editedMedia._id}, function (ret) {
                window.log("removeMedia return", ret);
                that.loadMediaData();
                that.toggleModal();
            });
        }
        return false;
    }

    saveMedia(event) {
        let that = this;
        event.preventDefault();
        makeRequest("editMedia", {ObjectID: this.state.editedMedia._id, changes: {
            "name": document.getElementById("name").value
        }}, function (ret) {
            that.loadMediaData();
            that.toggleModal();
        });
        return false;
    }

    renderModalContent() {
        if (this.state.editedMedia) {
            return (<Modal show={this.state.modalOpened} keyboard={true} bsSize="large" onHide={this.toggleModal}>
                <Modal.Header closeButton>
                    <p className="header text-uppercase">Médiaelem szerkesztése</p>

                </Modal.Header>
                <Modal.Body>
                    <div className="row">
                        <div className="col-md-7">
                            <form id="form-one" className="m-md-t" onSubmit={function (){return false;}}>
                                <div className="form-group form-group-default">
                                    <label htmlFor="name"><i className="fa fa-image m-xs-r"/>Média
                                        neve</label>
                                    <input type="text" className="form-control" id="name" name="name"/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="url"><i className="fa fa-chain m-xs-r"/>Média hivatkozása</label>
                                    <input type="text" className="form-control" id="url" name="url" readOnly={true}/>
                                </div>


                                <button className="btn btn-danger w-150 m-sm-t pull-left" onClick={this.removeMedia}>
                                    Törlés
                                </button>
                                <button className="btn btn-primary w-150 m-sm-t pull-right" onClick={this.saveMedia}>
                                    Mentés
                                </button>
                                <div className="clearfix"/>
                            </form>
                        </div>
                        <div className="col-md-5">
                            <img width="100%" id="previewImage-modal" src=""/>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>);
        }
    }

    render() {
        if (this.state.baseLoading) {
            return (<section className="uk-padding">
                <div className="uk-container uk-container-center">
                    <div className="loading"/>
                </div>
            </section>);
        }
        let that = this;

        return (<section>
            {this.renderModalContent()}
            <div className="page-title">
                <h3>
                    Médiatár
                    <button className="btn btn-primary pull-right" onClick={() => {
                        this.state.dropzoneRef.open()
                    }}>
                        <i className="fa fa-plus fa-fw"/>
                        Új médiaelem
                    </button>
                </h3>
                <ol className="breadcrumb">
                    <li>
                        <small><i className="fa fa-home fa-fw m-xs-r"/>Vezérlőpúlt</small>
                    </li>
                    <li><a href="javascript:void(0)" className="text-info">
                        <small>Médiatár</small>
                    </a></li>
                </ol>
            </div>
            <div className="row row-xl">
                <div className="col-md-12">
                    <div className="panel-x">
                        <div className="panel-body">
                            {this.mediaPanel()}
                        </div>
                    </div>
                </div>
            </div>
        </section>);
    }
}

Media.propTypes = {};
Media.defaultProps = {};

export default Media;