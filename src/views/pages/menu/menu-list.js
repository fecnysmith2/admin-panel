import React, {
    Component,
} from 'react';
import makeRequest from '../../../utils/request-queue-handler'
import {bindAll} from "lodash";
import {priceFormat} from "../../../utils/number";
import Pagination from "../../../components/widgets/pagination";

import Scroll from "react-scroll";
var Element = Scroll.Element;
var scroller = Scroll.scroller;

class MenuList extends Component {

    constructor(props) {
        super(props);

        bindAll(this,
            'loadPages',
            'sortData', 'setPage'
        );

        this.state = {
            'loading': true,
            'baseLoading': true,
            'categoryID': this.props.params.categoryID,
            'pages': [],
            'categoryData': {'rowsCount': 1000},
            'productsCount': 0,
            'currentPage': 1,
            'productPerPage': 25,
            'orderBy': {'name': -1},
            'filter': {}
        };
    }

    componentWillMount() {
        this.loadPages();
    }


    sortData() {
        function getSelectedOptions(select) {
            let result = [];
            const options = select.getElementsByTagName('option');
            for (let i = 0; i < options.length; i++) {
                if (options[i].selected)
                    return options[i];
            };
        }


        let sortObject = {};
        const byType = document.getElementById("sortSelect").value;
        const sortKey = getSelectedOptions(document.getElementById("sortSelect")).getAttribute("data-sortKey");
        sortObject[byType] = parseInt(sortKey);
        window.log("sortObject", sortObject);
        this.setState({orderBy: sortObject}, this.loadCategoryData);
    }

    setPage(page) {
        scroller.scrollTo('categoryTopScroller', {
            duration: 400,
            delay: 0,
            offset: -23,
            smooth: true
        });
        this.setState({currentPage: page}, this.loadCategoryProducts);
    }

    loadPages() {
        let that = this;
        let searchString = "";
        this.setState({loading: true});
        if(document.getElementById("search-pages-list")) {
            searchString = document.getElementById("search-pages-list").value;
        }
        const params = {
            "filter": {},
            "searchString": searchString,
            "CurrentPage": this.state.currentPage,
            "ppp": this.state.productPerPage,
            "orderBy": this.state.orderBy
        };
        window.log("loadCategoryProducts params", params);
        makeRequest("getMenus", params, function (ret) {
            window.log("getMenus return", ret);
            that.setState({pages: ret.menus});
            that.setState({pagesCount: ret.menusCount});
            that.setState({loading: false});
        });
    }


    render() {

        return (<section>
            <div className="page-title">
                <h3>
                    Menük

                    <a href="/menu/add"><button className="btn btn-primary pull-right w-150 m-sm-r m-sm-b">
                        <i className="fa fa-plus fa-fw"/>
                        Új menücsoport
                    </button></a>
                </h3>
                <ol className="breadcrumb">
                    <li>
                        <small><i className="fa fa-home fa-fw m-xs-r"/>Vezérlőpúlt</small>
                    </li>
                    <li>
                        <small><i className="fa fa-home fa-fw m-xs-r"/>Tartalmak</small>
                    </li>
                    <li><a href="javascript:void(0)" className="text-info">
                        <small>Menük</small>
                    </a></li>
                </ol>
            </div>
            <div className="row row-xl">
                <div className="col-md-12">
                    <div className="panel-x">
                        <div className="panel-body">
                            <div className="row">
                                <p className="col-md-7"></p>
                                <div className="col-md-5 ">
                                    <input type="text" id="search-pages-list" onKeyUp={this.loadPages} placeholder="Keresés..." className="form-control pull-right"/>
                                </div>
                            </div>
                            <div className="table-responsive">
                                <table className="table table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Oldal neve</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.state.loading ? (<tr><td colSpan={4} align="center"><div className="spinner"/></td></tr>) : (this.state.pages.map(function (pageData) {
                                        const openPageEdit = function(){
                                            window.location.href = "/menu/"+pageData._id+"/edit";
                                        };
                                        let pageStatus = "Publikus";
                                        switch(pageData.status){
                                            case'public': pageStatus = 'Publikus'; break;
                                            case'draft': pageStatus = 'Piszkozat'; break;
                                        }
                                        return (<tr className="clickable-row" key={pageData._id} onClick={openPageEdit}>
                                            <td className="nopadding-bottom">
                                                <p className="header text-uppercase">{pageData.name}</p>
                                            </td>
                                        </tr>);
                                    })
                                    )}
                                    </tbody>
                                </table>
                                <Pagination productsPerPage={this.state.productPerPage} productsFound={this.state.pagesCount} currentPage={this.state.currentPage} onPagination={this.setPage}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>);

}
}

MenuList.propTypes = {};
MenuList.defaultProps = {};

export default MenuList;