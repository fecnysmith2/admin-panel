import React, {Component} from "react";
import {bindAll} from "lodash";
import { Link } from 'react-router';
import makeRequest from '../../../utils/request-queue-handler';
import {getVat, priceFormat} from "../../../utils/number";
import {TITLE_BASE} from "../../../../config";


class OrderDetails extends Component {
    constructor(props) {
        super(props);

        bindAll(this,
            'loadOrder', 'showAddressBlock'
        );

        this.state = {
            orderData: {},
            orderID: this.props.params.orderID,
            loading: true
        };
    }

    componentWillMount() {
        this.loadOrder();
    }


    loadOrder() {
        let that = this;
        makeRequest("getOrder", {"ObjectID": this.state.orderID}, function (ret) {
            window.log("getOrder return", ret);

            document.title = "Rendelés részletei / "+that.state.orderID+" | " + TITLE_BASE;
            that.setState({loading: false, orderData: ret});
        });
    }

    showAddressBlock(type) {
        let baseObject = this.state.orderData.billing;

        if (type !== "billing" && this.state.orderData.hasOwnProperty("shipping")) {
            baseObject = this.state.orderData.shipping;
        }


        return <div className="block block-rounded">
            <div className="block-header block-header-default">
                <h3 className="block-title">{type === "billing" ? "Számlázási adatok" : "Szállítási adatok"}</h3>
            </div>
            <div className="block-content">
                <div className="font-size-lg text-black mb-5">{baseObject.personalData.name.firstName} {baseObject.personalData.name.lastName}</div>
                <address>
                    {baseObject.address.address} {baseObject.address.building_door && "("+baseObject.address.building_door+")"}<br/>
                    {baseObject.address.city} {baseObject.address.zip}<br/>
                    {baseObject.address.country}<br/><br/>
                    <i className="fa fa-phone mr-5"></i> {baseObject.contact.phone}<br/>
                    <i className="fa fa-envelope-o mr-5"></i> <a href="javascript:void(0)">{baseObject.contact.email}</a>
                </address>
            </div>
        </div>;
    }

    render() {
        let that = this;
        let cartSubPrice = 0;

        return (<div className="content">
            <h2 className="content-heading">
                <button type="button" className="btn btn-sm btn-secondary float-right">
                    <i className="fa fa-check text-success mr-5"></i>Complete
                </button>
                <button type="button" className="btn btn-sm btn-secondary float-right mr-5">
                    <i className="fa fa-times text-danger mr-5"></i>Cancel
                </button>
                #{this.state.orderID}
            </h2>
            <div className="row gutters-tiny">
                <div className="col-md-6 col-xl-3">
                    <div className="block block-rounded">
                        <div className="block-content block-content-full">
                            <div className="py-20 text-center">
                                <div className="mb-20">
                                    <i className="fa fa-check fa-3x text-success"></i>
                                </div>
                                <div className="font-size-sm font-w600 text-uppercase text-success">1. Visszaigazolva</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-6 col-xl-3">
                    <div className="block block-rounded">
                        <div className="block-content block-content-full">
                            <div className="py-20 text-center">
                                <div className="mb-20">
                                    <i className="fa fa-check fa-3x text-success"></i>
                                </div>
                                <div className="font-size-sm font-w600 text-uppercase text-success">2. Fizetve</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-6 col-xl-3">
                    <div className="block block-rounded">
                        <div className="block-content block-content-full">
                            <div className="py-20 text-center">
                                <div className="mb-20">
                                    <i className="fa fa-spinner fa-3x fa-spin text-warning"></i>
                                </div>
                                <div className="font-size-sm font-w600 text-uppercase text-warning">3. Szállítás alatt</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-6 col-xl-3">
                    <div className="block block-rounded">
                        <div className="block-content block-content-full">
                            <div className="py-20 text-center">
                                <div className="mb-20">
                                    <i className="fa fa-times fa-3x text-muted"></i>
                                </div>
                                <div className="font-size-sm font-w600 text-uppercase text-muted">4. Lezárva</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h2 className="content-heading">
                <button type="button" className="btn btn-sm btn-secondary float-right">
                    <i className="fa fa-envelope-o text-info mr-5"></i>E-mail küldése
                </button>
                Vásárló adatai
            </h2>
            <div className="row row-deck">
                <div className="col-lg-4">
                    <a className="block block-rounded block-link-shadow text-center" href="be_pages_ecom_customer.php">
                        <div className="block-content bg-gd-dusk">
                            <div className="push">
                                <img className="img-avatar img-avatar-thumb" src="assets/img/avatars/avatar15.jpg" alt=""/>
                            </div>
                            <div className="pull-r-l pull-b py-10 bg-black-op-25">
                                <div className="font-w600 mb-5 text-white">
                                    {this.state.orderData.billing.personalData.name.firstName} {this.state.orderData.billing.personalData.name.lastName} <i className="fa fa-star text-warning"></i>
                                </div>
                                {/*<div className="font-size-sm text-white-op">Premium Member</div>*/}
                            </div>
                        </div>
                        <div className="block-content">
                            <div className="row items-push text-center">
                                <div className="col-6">
                                    <div className="mb-5"><i className="si si-bag fa-2x"></i></div>
                                    <div className="font-size-sm text-muted">6 Orders</div>
                                </div>
                                <div className="col-6">
                                    <div className="mb-5"><i className="si si-basket-loaded fa-2x"></i></div>
                                    <div className="font-size-sm text-muted">15 Products</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div className="col-lg-8">
                    <div className="block block-rounded">
                        <div className="block-header block-header-default">
                            <h3 className="block-title">Vásárló további rendelései</h3>
                        </div>
                        <div className="block-content">
                            <table className="table table-borderless table-sm table-striped">
                                <tbody>

                                <tr>
                                    <td>
                                        <Link className="font-w600" to={"/order/598060f9672d8fbe32919a39"}>#598060f9672d8fbe32919a39</Link>
                                    </td>
                                    <td>
                                        <span className="badge badge-success">Lezárva</span>
                                    </td>
                                    <td className="d-none d-sm-table-cell">
                                        2017. 10. 05.</td>
                                    <td className="text-right">
                                        <span className="text-black">{priceFormat(35600)} Ft</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <h2 className="content-heading">Elérhetőségek</h2>
            <div className="row row-deck gutters-tiny">
                <div className="col-md-6">
                    {this.showAddressBlock('billing')}
                </div>
                <div className="col-md-6">
                    {this.showAddressBlock('shipping')}
                </div>
            </div>
            <h2 className="content-heading">Termékek</h2>
            <div className="block block-rounded">
                <div className="block-content">
                    <div className="table-responsive">
                        <table className="table table-borderless table-striped">
                            <thead>
                            <tr>
                                <th style={{"width":"100px"}}>Cikkszám</th>
                                <th>Product</th>
                                <th className="text-center">Megnevezés</th>
                                <th className="text-center">Mennyiség</th>
                                <th className="text-right" style={{"width":"10%"}}>Egységár</th>
                                <th className="text-right" style={{"width":"10%"}}>Összesen</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <a className="font-w600" href="be_pages_ecom_product_edit.php">PID.258</a>
                                </td>
                                <td>
                                    <a href="be_pages_ecom_product_edit.php">Dark Souls III</a>
                                </td>
                                <td className="text-center">92</td>
                                <td className="text-center font-w600">1</td>
                                <td className="text-right">$69,00</td>
                                <td className="text-right">$69,00</td>
                            </tr>
                            <tr>
                                <td>
                                    <a className="font-w600" href="be_pages_ecom_product_edit.php">PID.263</a>
                                </td>
                                <td>
                                    <a href="be_pages_ecom_product_edit.php">Bloodborne</a>
                                </td>
                                <td className="text-center">32</td>
                                <td className="text-center font-w600">1</td>
                                <td className="text-right">$29,00</td>
                                <td className="text-right">$29,00</td>
                            </tr>
                            <tr>
                                <td>
                                    <a className="font-w600" href="be_pages_ecom_product_edit.php">PID.214</a>
                                </td>
                                <td>
                                    <a href="be_pages_ecom_product_edit.php">The Surge</a>
                                </td>
                                <td className="text-center">15</td>
                                <td className="text-center font-w600">1</td>
                                <td className="text-right">$59,00</td>
                                <td className="text-right">$59,00</td>
                            </tr>
                            <tr>
                                <td>
                                    <a className="font-w600" href="be_pages_ecom_product_edit.php">PID.358</a>
                                </td>
                                <td>
                                    <a href="be_pages_ecom_product_edit.php">Bioshock Collection</a>
                                </td>
                                <td className="text-center">77</td>
                                <td className="text-center font-w600">1</td>
                                <td className="text-right">$39,00</td>
                                <td className="text-right">$39,00</td>
                            </tr>
                            <tr>
                                <td>
                                    <a className="font-w600" href="be_pages_ecom_product_edit.php">PID.425</a>
                                </td>
                                <td>
                                    <a href="be_pages_ecom_product_edit.php">Until Dawn</a>
                                </td>
                                <td className="text-center">25</td>
                                <td className="text-center font-w600">1</td>
                                <td className="text-right">$49,00</td>
                                <td className="text-right">$49,00</td>
                            </tr>
                            <tr>
                                <td colSpan="5" className="text-right font-w600">Total Price:</td>
                                <td className="text-right">$245,00</td>
                            </tr>
                            <tr>
                                <td colSpan="5" className="text-right font-w600">Total Paid:</td>
                                <td className="text-right">$245,00</td>
                            </tr>
                            <tr className="table-success">
                                <td colSpan="5" className="text-right font-w600 text-uppercase">Total Due:</td>
                                <td className="text-right font-w600">$0,00</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>);

        return (<div className="invoice-container bg-white p-md">
            <div className="invoice-header">
                <div className="row">
                    <div className="col-md-6"><h1 className="text-left fw-lgt text-uppercase">Rendelés részletei</h1></div>
                    <div className="col-md-6">
                        <address>
                            <h3 className="text-right fw-thkr"></h3>
                        </address>
                    </div>
                </div>
            </div>

            <hr/>

                {this.state.loading ? (<div className="spinner"></div>) : <section><div className="invoice-summary">
                <div className="row">
                    <div className="col-md-6">
                        {this.showAddressBlock('billing')}
                    </div>

                    <div className="col-md-offset-2 col-md-4">
                        <div className="invoice-details-container text-right">
                            <p className="text-uppercase fw-lgt">rendelés ideje: <span
                                className="fw-thkr text-capitalize">{this.state.orderData.date}</span></p>

                            <p className="text-uppercase fw-lgt">fizetési mód: <span className="fw-thkr">{this.state.orderData.payment_method}</span></p>
                            <p className="text-uppercase fw-lgt">szállítási mód: <span className="fw-thkr">{this.state.orderData.shipment_method}</span></p>

                            <h4 className="text-uppercase fw-lgt">Teljes végösszege</h4>
                            <h3 className="fw-thkr m-n">{priceFormat(this.state.orderData.fullPrice)} Ft</h3>
                        </div>
                    </div>
                </div>
            </div>

            <hr/>

            <div className="invoice-description">
                <div className="table-responsive">
                    <table className="table">
                        <thead>
                        <tr>
                            <th className="col-sm-6">
                                <h4 className="m-b-lg text-uppercase fw-thrk">Termék megnevezése</h4>
                                <p className="m-t-lg fw-lgt">Termék részletes megnevezeése & vásárlási opciók</p>
                            </th>
                            <th className="col-sm-1">
                                <h4 className="m-b-lg text-uppercase text-center fw-thrk">Egységár</h4>
                                <p className="m-t-lg fw-lgt text-center">Vásárláskori ár</p>
                            </th>
                            <th className="col-sm-1">
                                <h4 className="m-b-lg text-uppercase text-center fw-thrk">Mennyiség</h4>
                                <p className="m-t-lg fw-lgt text-center">Rendelt mennyiség</p>
                            </th>
                            <th className="col-sm-1">
                                <h4 className="m-b-lg text-uppercase text-center fw-thrk">Összesen</h4>
                                <p className="m-t-lg fw-lgt text-center">Termékek összes ára</p>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        {Object.keys(this.state.orderData.cartItems).map(function(thisCartId){
                            const cartItem = that.state.orderData.cartItems[thisCartId];
                            cartSubPrice += cartItem.qty * (cartItem.product.prices.huf.actionprice ? cartItem.product.prices.huf.actionprice : cartItem.product.prices.huf.price);
                            return <tr>
                                <td>
                                    <h4 className="fw-thkr">{cartItem.product.properties.name.hu}</h4>
                                    <small className="fw-lgt">{cartItem.product.properties.description.hu}
                                    </small>
                                </td>
                                <td><h4 className="fw-lgt text-center">{priceFormat(cartItem.product.prices.huf.actionprice ? cartItem.product.prices.huf.actionprice : cartItem.product.prices.huf.price)} Ft</h4></td>
                                <td><h4 className="fw-lgt text-center">{cartItem.qty}</h4></td>
                                <td><h4 className="fw-thkr text-center">{priceFormat(cartItem.qty * (cartItem.product.prices.huf.actionprice ? cartItem.product.prices.huf.actionprice : cartItem.product.prices.huf.price))} Ft</h4></td>
                            </tr>;
                        })}
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colSpan="3"><p className="fw-lgt text-right">Bruttó végösszeg</p></td>
                            <td><p className="fw-lgt text-center">{priceFormat(cartSubPrice)} Ft</p></td>
                        </tr>
                        <tr>
                            <td colSpan="3"><p className="fw-lgt text-right">Ebből ÁFA: (27%)</p></td>
                            <td><p className="fw-lgt text-center">{priceFormat(getVat(cartSubPrice))} Ft</p></td>
                        </tr>
                        <tr>
                            <td colSpan="3"><p className="fw-lgt text-right">Kedvezmény (0%)</p></td>
                            <td><p className="fw-lgt text-center">0 Ft</p></td>
                        </tr>
                        <tr>
                            <td colSpan="3"><h3 className="fw-thkr text-uppercase text-right">Teljes végösszeg</h3></td>
                            <td><h3 className="fw-thkr text-center">{priceFormat(this.state.orderData.fullPrice)} Ft</h3></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div></section>}
        </div>
    );
    }
    }

    OrderDetails.propTypes = {};
    OrderDetails.defaultProps = {};

    export default OrderDetails;