import React, {
    Component,
} from 'react';
import makeRequest from '../../../utils/request-queue-handler'
import { Link } from 'react-router';
import {bindAll} from "lodash";
import {priceFormat} from "../../../utils/number";
import Pagination from "../../../components/widgets/pagination";
import {Modal} from "react-bootstrap";

import Scroll from "react-scroll";
var Element = Scroll.Element;
var scroller = Scroll.scroller;

class OrdersPanel extends Component {

    constructor(props) {
        super(props);

        bindAll(this,
            'loadOrders',
            'sortData', 'setPage', 'renderModalContent', 'deleteOrder', 'setOrderStatus'
        );

        this.state = {
            'loading': true,
            'baseLoading': true,
            'orders': [],
            'categoryData': {'rowsCount': 1000},
            'ordersCount': 0,
            'currentPage': 1,
            'productPerPage': 25,
            'orderBy': {'name.hu': -1},
            'filter': {'status': {$in: this.props.statuses}},
            'modalOpened': false,
            deleteOrderId: null,
            panelsLoaded: true
        };
    }

    componentWillMount() {
        this.loadOrders();
    }

    componentDidUpdate(prevProps, prevState, prevContext) {
        // this.loadOrders();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.panelsLoaded !== this.state.panelsLoaded){
            this.loadOrders(); this.setState({panelsLoaded: nextProps.panelsLoaded});
        }
    }


    sortData() {
        function getSelectedOptions(select) {
            let result = [];
            const options = select.getElementsByTagName('option');
            for (let i = 0; i < options.length; i++) {
                if (options[i].selected)
                    return options[i];
            };
        }


        let sortObject = {};
        const byType = document.getElementById("sortSelect").value;
        const sortKey = getSelectedOptions(document.getElementById("sortSelect")).getAttribute("data-sortKey");
        sortObject[byType] = parseInt(sortKey);
        window.log("sortObject", sortObject);
        this.setState({orderBy: sortObject}, this.loadCategoryData);
    }

    setPage(page) {
        scroller.scrollTo('categoryTopScroller', {
            duration: 400,
            delay: 0,
            offset: -23,
            smooth: true
        });
        this.setState({currentPage: page}, this.loadCategoryProducts);
    }

    loadOrders() {
        // this.props.reloadPanels();
        let that = this;
        let searchString = "";
        this.setState({loading: true});
        if (document.getElementById("search-pages-list")) {
            searchString = document.getElementById("search-pages-list").value;
        }
        const params = {
            "filter": this.state.filter,
            "searchString": searchString,
            "CurrentPage": this.state.currentPage,
            "ppp": this.state.productPerPage,
            "orderBy": this.state.orderBy
        };
        window.log("loadCategoryProducts params", params);
        makeRequest("getOrders", params, function (ret) {
            window.log("getOrders return", ret);
            that.setState({orders: ret.orders});
            that.setState({pagesCount: ret.ordersCount});
            that.setState({loading: false});
        });
    }

    getOrderStatus(orderStatus) {
        let percentage = 0;
        switch (orderStatus) {
            case'pending':
                percentage = 0;
                break;
            case'payed':
                percentage = 25;
                break;
            case'confirmed':
                percentage = 50;
                break;
            case'shipped':
                percentage = 75;
                break;
            case'completed':
                percentage = 100;
                break;
            case'cancelled':
                percentage = 100;
                break;
        }
        const divClassName = "c100 p" + percentage + " small";
        return (<div className={divClassName}>
            <span>{orderStatus !== "cancelled" ? percentage+"%" : <i className="fa fa-close"/>}</span>
            <div className="slice">
                <div className="bar"/>
                <div className="fill"/>
            </div>
        </div>);
    }

    getOrderButtons(orderStatus, orderId, orderLastStatus) {
        let that = this;
        let clickBtnEvent = function(){};

        switch (orderStatus) {
            case'pending':
                clickBtnEvent = function(event){event.preventDefault();that.setOrderStatus(orderId, "payed");return false;};
                return (<button className="btn btn-sm btn-primary btn-block" onClick={clickBtnEvent}>Rendelés fizetve</button>);
                break;
            case'payed':
                clickBtnEvent = function(event){event.preventDefault();that.setOrderStatus(orderId, "confirmed");return false;};
                return (<button className="btn btn-sm btn-primary btn-block" onClick={clickBtnEvent}>Visszaigazolás</button>);
                break;
            case'confirmed':
                clickBtnEvent = function(event){event.preventDefault();that.setOrderStatus(orderId, "shipped");return false;};
                return (<button className="btn btn-sm btn-primary btn-block" onClick={clickBtnEvent}>Szállítás alatt</button>);
                break;
            case'shipped':
                clickBtnEvent = function(event){event.preventDefault();that.setOrderStatus(orderId, "completed");return false;};
                return (<button className="btn btn-sm btn-success btn-block" onClick={clickBtnEvent}>Rendelés lezárása</button>);
                break;
            case'cancelled':
                clickBtnEvent = function(event){event.preventDefault();that.setOrderStatus(orderId, orderLastStatus);return false;};
                return (<button className="btn btn-sm btn-primary btn-block" onClick={clickBtnEvent}>Rendelés újranyitása</button>);
                break;
        }
    }


    getProductsString(productsObject) {
        let products = [];
        {
            Object.keys(productsObject).map(function (thisItemId) {
                products.push(productsObject[thisItemId].qty + "x " + productsObject[thisItemId].product.properties.name.hu);
            })
        }

        return products.join(", ");
    }

    deleteOrder(event){
        event.preventDefault()
        let that = this;
        makeRequest("setOrderStatus", {"ObjectID": this.state.deleteOrderId, "changes": {"status": "cancelled", "cause": document.getElementById('delete_cause').value}}, function (ret) {
            that.setState({deleteOrderId: null, modalOpened: false});
            that.loadOrders();
            that.props.reloadPanels()
        });
        return false;
    }

    setOrderStatus(OrderID, status){
        let that = this;
        makeRequest("setOrderStatus", {"ObjectID": OrderID, "changes": {"status": status}}, function (ret) {
            that.loadOrders();
            that.props.reloadPanels()
        });
    }

    renderModalContent(){
        let that = this;
        return (<Modal show={this.state.modalOpened} keyboard={true} onHide={function(){that.setState({modalOpened: false, deleteOrderId: null})}}>
            <Modal.Header closeButton>
                <p className="header text-uppercase">Rendelés visszamondása / törlése</p>
                <p>Biztosan törölni szeretné ezt a rendelést?</p>

            </Modal.Header>
            <Modal.Body>
                <form id="form-one" className="m-md-t" onSubmit={function(){return false;}}>
                    <div className="form-group form-group">
                        <label htmlFor="delete_cause"><i className="fa fa-times-circle-o m-xs-r"/>Törlés indoklása</label>
                        <select className="form-control" id="delete_cause">
                            <option value={"invertory_shortage"}>Készlethiány</option>
                            <option value={"wrong"}>Téves rendelés</option>
                            <option value={"fraud"}>Csaló</option>
                            <option value={"other"}>Egyéb</option>
                        </select>
                    </div>
                    <button className="btn btn-danger w-150 m-sm-t pull-right" onClick={this.deleteOrder}>Visszamondás</button>
                    <div className="clearfix"/>
                </form>
            </Modal.Body>
        </Modal>);
    }

    render() {
        let that = this;
        return (<section>
            {this.renderModalContent()}
            <div className="row row-xl">
                <div className="col-md-12">
                    <div className="panel-x">
                        <div className="panel-body">
                            <h2 className="content-heading mb-0">
                                    {this.props.title}
                                    <small className="ml-1">(0 új rendelés)</small>

                            </h2>
                            <div className="row mb-2">
                                <div className={"col-4"}>
                                <div className="form-material floating ">
                                    <input type="text" className="form-control" id="material-text2" onKeyUp={this.loadOrders} name="material-text2"/>
                                    <label htmlFor="material-text2">Keresés...</label>
                                </div>
                                </div>
                            </div>
                                {this.state.loading ? (<div className="spinner"></div>) : (this.state.orders.length > 0 ? this.state.orders.map(function (orderData) {
                                        const deleteOrder = function (event) {
                                            event.preventDefault();
                                            that.setState({modalOpened: true, deleteOrderId: orderData._id});

                                            return false;
                                        };
                                        const orderViewLink = "/order/"+orderData._id;
                                        return (<div className="row gutters-tiny js-appear-enabled animated fadeIn" data-toggle="appear">
                                            <div className="col-md-12">
                                                <Link to={orderViewLink} className="block block-link-shadow overflow-hidden">
                                                    <div className="block-content block-content-full">
                                                        <div className="row">
                                                            <div className={"col-1"}>
                                                                {that.getOrderStatus(orderData.status)}
                                                            </div>
                                                            <div className="col-3 text-right border-r">
                                                                <div className="js-appear-enabled animated fadeInLeft" data-toggle="appear" data-className="animated fadeInLeft">
                                                                    <div className="font-size-h3 font-w600">{orderData.billing.personalData.name.firstName} {orderData.billing.personalData.name.lastName}</div>
                                                                    <div className="font-size-sm font-w600 text-uppercase text-muted">#{orderData._id}</div>
                                                                </div>
                                                            </div>
                                                            <div className="col-6 text-center border-r">
                                                                <div className="js-appear-enabled animated fadeInLeft" data-toggle="appear" data-className="animated fadeInLeft">
                                                                    <div className="font-size-xs font-w300 text-uppercase text-muted text-justify">{that.getProductsString(orderData.cartItems)}</div>
                                                                </div>
                                                            </div>
                                                            <div className="col-2 border-l">
                                                                <div className="js-appear-enabled animated fadeInRight" data-toggle="appear" data-className="animated fadeInRight">
                                                                    {that.getOrderButtons(orderData.status, orderData._id, orderData.last_status)}
                                                                    {orderData.status !== "cancelled" && orderData.status !== "completed" && <button className="btn btn-danger btn-block btn-sm" onClick={deleteOrder}>Visszamondás</button>}

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Link>
                                            </div>
                                        </div>);



                                        return (<div className="media p-md mail-item order-item"
                                                     data-toggle-class="mail-message-visible">
                                            <div className="media-left">
                                                {that.getOrderStatus(orderData.status)}
                                            </div>
                                            <div className="media-body">
                                                <Link to={orderViewLink}>
                                                    <div className="col-md-9">
                                                        <h5 className="fw-thk m-n mail-list-subject">
                                                            {orderData._id}
                                                            {" - "}
                                                            {orderData.billing.personalData.name.firstName} {orderData.billing.personalData.name.lastName}
                                                        </h5>
                                                        <p className="mail-list-content">
                                                            {that.getProductsString(orderData.cartItems)}
                                                        </p>
                                                        <p className="fs-sm text-grey">
                                                            <span className="pull-left" style={{"marginRight": "15px"}}><i
                                                                className="fa fa-money fa-fw m-xs-r"></i>Rendelés összértéke: <strong>{priceFormat(orderData.fullPrice)}
                                                                Ft</strong></span>
                                                            <i className="fa fa-clock-o fa-fw m-xs-r"></i><strong>4 órával
                                                            ezelőtt</strong>
                                                        </p>
                                                    </div>
                                                </Link>
                                                <div className="col-md-3">
                                                    {that.getOrderButtons(orderData.status, orderData._id, orderData.last_status)}
                                                    {orderData.status !== "cancelled" && orderData.status !== "completed" && <button className="btn btn-danger btn-block" onClick={deleteOrder}>Visszamondás</button>}
                                                </div>
                                            </div>
                                        </div>);
                                    }) : (<h2 className="text-center">Nincs egyetlen {this.props.titleAlt} rendelés sem!</h2> )
                                )}
                                <div className={"pull-right"}>
                                <Pagination productsPerPage={this.state.productPerPage}
                                            productsFound={this.state.pagesCount} currentPage={this.state.currentPage}
                                            onPagination={this.setPage}/>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>);

    }
}

OrdersPanel.propTypes = {};
OrdersPanel.defaultProps = {};

export default OrdersPanel;