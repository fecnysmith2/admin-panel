import React, {
    Component,
} from 'react';
import {Link} from 'react-router';
import {bindAll} from "lodash";
import {Modal} from "react-bootstrap";

import Scroll from "react-scroll";
import OrdersPanel from "./orders-panel";
import {TITLE_BASE} from "../../../../config";

class Orders extends Component {

    constructor(props) {
        super(props);

        bindAll(this,
            'reloadPanels', 'loadPanels'
        );
        this.state = {
            panelsLoaded: true
        };
    }

    componentWillMount() {
        document.title = "Rendelések" + " | " + TITLE_BASE;
    }


    reloadPanels() {
        this.setState({panelsLoaded: Math.random()});
    }



    loadPanels(closed = false) {
        if(!closed){
        return <section>
                    <OrdersPanel panelsLoaded={this.state.panelsLoaded} reloadPanels={this.reloadPanels}
                                 title="Nyitott rendelések" statuses={["pending", "payed", "confirmed", "shipped"]}
                                 titleAlt="nyitott"/>
        </section>;
        }else{
            return <OrdersPanel panelsLoaded={this.state.panelsLoaded} reloadPanels={this.reloadPanels}
                                title="Lezárt rendelések" statuses={["completed", "cancelled"]} titleAlt="lezárt"/>;
        }
    }

    render() {
        let that = this;
        window.log("thisparamss", this.props.location.pathname);
        return (<div className="content">
                        {this.loadPanels(this.props.location.pathname == "/order/archive")}
        </div>);

    }
}

Orders.propTypes = {};
Orders.defaultProps = {};

export default Orders;