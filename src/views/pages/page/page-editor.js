import React, {
    Component,
} from 'react';
import makeRequest from '../../../utils/request-queue-handler'
import $ from 'jquery';
import {bindAll} from "lodash";
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css';

import 'bootstrap/js/modal';
import 'bootstrap/js/dropdown';
import 'bootstrap/js/tooltip';
import 'bootstrap/dist/css/bootstrap.css';

import {Slug} from "../../../utils/url";
var sendPostRequest = require("../../../utils/xhr-request.js").sendPostRequest;


class PageEditor extends Component {

    constructor(props) {
        super(props);

        bindAll(this,
            'generateURL', 'savePage', 'loadPageData',
            'fillPageData', 'contentChanged'
        );

        this.state = {
            'loading': true,
            'pageID': this.props.params.pageID,
            'pageData': {},
            'findInput': [],
            'contentOfPage': 'Itt szerkesztheted az oldalad tartalmát...'
        };
    }

    loadPageData(){
        let that = this;
        if(this.state.pageID){
            makeRequest("getPage", {ObjectID: this.state.pageID}, function (ret) {
                window.log("getPage return", ret);
                that.setState({pageData: ret},
                    function(){ that.fillPageData(ret); }
                );
            });
        }
    }

    componentDidMount() {
        this.loadPageData();
    }


    fillPageData(object, findInput = ""){
        let that = this;

        Object.keys(object).forEach(function (thisIndex) {
            const thisValue = object[thisIndex];
            const newIndex = (findInput && findInput+".")+thisIndex;
            if(typeof thisValue === 'object') {
                that.fillPageData(thisValue, newIndex);
            }else{
                window.log("Attach element", newIndex);
                if(newIndex.indexOf('content') > -1){
                    that.setState({'contentOfPage': thisValue});
                }else {
                    if (document.getElementById(newIndex)) {
                        document.getElementById(newIndex).value = thisValue;
                    }
                }
            }
        });
    }

    generateURL(productName){
        document.getElementById("url_string.hu").value = Slug(document.getElementById("name.hu").value)
    }

    pageHeader() {
        return (<div className="page-title">
            <h3>Oldal létrehozása</h3>
            <ol className="breadcrumb">
                <li>
                    <small><i className="fa fa-home fa-fw m-xs-r"></i>Vezérlőpúlt</small>
                </li>
                <li>
                    <small><i className="fa fa-clone fa-fw m-xs-r"></i>Oldalak</small>
                </li>
                <li><a href="javascript:void(0)" className="text-info">
                    <small>Új oldal</small>
                </a></li>
            </ol>
        </div>);
    }
    
    pageBasicDetails(){
        return (<div className="row row-xl">
            <div className="col-md-5">
                <div className="panel-x panel-transparent">
                    <div className="panel-body">
                        <h3>Alapadatok</h3>
                        <p>Itt adhatok meg az oldalhoz szükséges alapvető inormációkat.</p>
                    </div>
                </div>
            </div>

            <div className="col-md-7">
                <div className="panel-x panel-transparent">
                    <div className="panel-body">
                        <div className="form-group">
                            <label htmlFor="status"><i className="fa fa-send m-xs-r"/>Státusz</label>
                            <select className="form-control" id="status" name="status">
                                <option value={"draft"}>Piszkozat</option>
                                <option value={"public"}>Publikus</option>
                            </select>
                        </div>

                        <div className="form-group">
                            <label htmlFor="name.hu"><i className="fa fa-hashtag m-xs-r"/>Oldal megnevezése</label>
                            <input type="text" className="form-control" id="name.hu" name="name[hu]" onKeyUp={this.generateURL}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="url_string.hu"><i className="fa fa-chain m-xs-r"/>URL végződés</label>
                            <input type="text" className="form-control" id="url_string.hu" name="url_string[hu]"/>
                        </div>

                        <div className="form-group">
                            <label htmlFor="seo.description"><i className="fa fa-feed m-xs-r"/>SEO leírás</label>
                            <input type="text" className="form-control" id="seo.description" name="seo[description]"/>
                        </div>
                        <button type="submit" className="btn btn-primary btn-block">Mentés</button>
                    </div>
                </div>
            </div>
        </div>);
    }

    contentChanged(content){
        this.setState({contentOfPage: content});
    }

    pageEditContent(){
        return (<ReactSummernote
                    value={this.state.contentOfPage}
                    onChange={this.contentChanged}
                    options={{
                        height: 350,
                        // dialogsInBody: true,
                        airMode: true,
                        popover: {
                            air: [
                                ['color', ['color']],
                                ['font', ['bold', 'underline', 'clear']],
                                ['para', ['ul', 'paragraph']],
                                ['table', ['table']],
                                ['insert', ['link', 'picture']],
                                ['link', ['linkDialogShow', 'unlink']],
                                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                                ['remove', ['removeMedia']]
                            ]
                        },
                        toolbar: [
                            ['style', ['style']],
                            ['font', ['bold', 'underline', 'clear']],
                            ['fontname', ['fontname']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['table', ['table']],
                            ['insert', ['link', 'picture', 'video']],
                            ['view', ['fullscreen', 'codeview']]
                        ]
                    }}
                />);
    }

    savePage(event){
        event.preventDefault();
        let that = this;
        that.setState({loading: true});

        let formData = $('#page-editor-form').serializeObject();
        // add content to formData
        formData['content'] = {'hu': that.state.contentOfPage};

        window.log("savePage formData", formData);


        makeRequest("createPage", formData, function (ret) {
            window.log("createPage return", ret);
            window.location.href = '/page/'+ret.returnID+'/edit';
            that.setState({loading: false});
        });
        return false;
    }

    render() {
        return (<section>
            {this.pageHeader()}
            <div className="row row-xl">
                <div className="col-md-12">
                    <div className="panel-x panel-transparent">
                        <div className="panel-body">
                            <form name="product-editor-form" id="page-editor-form" onSubmit={this.savePage}>
                                {this.pageBasicDetails()}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-md-12">
                    <h3>Oldal tartalma</h3>
                    <div className="panel-x panel">
                        <div className="panel-body">
                            {this.pageEditContent()}
                        </div>
                    </div>
                </div>
            </div>
        </section>);
    }

}

PageEditor.propTypes = {};
PageEditor.defaultProps = {};

export default PageEditor;