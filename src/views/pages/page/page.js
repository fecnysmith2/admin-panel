import React, {
    Component,
} from 'react';
import makeRequest from '../../../utils/request-queue-handler'
import {bindAll} from "lodash";
import PageNotFound from "../../404";

class Product extends Component {

    constructor(props) {
        super(props);

        bindAll(this,
            'loadPageData'
        );

        this.state = {
            'loading': true,
            'pageName': this.props.params.pageName,
            'pageData': null
        };
        this.loadPageData();
    }

    componentWillMount(){

    }

    loadPageData(){
        let that = this;
        makeRequest("getPage", {"pageName":this.state.pageName}, function(ret){
            window.log("loadPageData return", ret);
            that.setState({loading:false, pageData: ret});
        });
    }

    render() {
        if(this.state.loading){ return (<section className="uk-padding"><div className="uk-container uk-container-center"><span className="loading"/></div></section>); }
        if(!this.state.pageData){ return (<PageNotFound/>); }
        return (<div>
			<section>
				<div className="uk-container uk-container-center">
					<div id="pagetitle">
						<div className="uk-grid">
							<div className="uk-width-3-10">
								<h2>{this.state.pageData.name.hu}</h2>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section className="uk-padding">
				<div className="uk-container uk-container-center">
					<div className="uk-grid uk-margin-large-bottom">
						<div className="uk-width-1-1">
                            {this.state.pageData.content.hu}
						</div>
					</div>
				</div>
			</section>
		</div>);
    }
}

Product.propTypes = {};
Product.defaultProps = {};

export default Product;