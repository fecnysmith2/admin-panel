import React, {
    Component,
} from 'react';
import makeRequest from '../../../utils/request-queue-handler'
import {Tab, Nav, NavItem, Col, Row, Modal} from "react-bootstrap";
import {bindAll} from "lodash";
import PageNotFound from "../../404";
import {getSelectedStore} from "../../../utils/user-util";
import {TITLE_BASE} from "../../../../config";
import {fillDataObject, storeEvents} from "../../../utils/helpers";
import {priceFormat} from "../../../utils/number";
import $ from 'jquery';
import renderHTML from 'react-render-html';
const SweetAlert = require('react-swal');




class StoreMailSettings extends Component {

    constructor(props) {
        super(props);
        //
        bindAll(this,
            'getEmailTemplate', 'loadEmailEditPage', 'saveEvent'
        );

        this.state = {
            'loading': true,
            'currentEvent': false,
            'events': storeEvents,
            'contentChanged': false,
            'saveInProgress': false,
            'showSuccessMessage': true
        };
    }

    componentWillReceiveProps(nextProps, nextContext) {
        window.log("this.props.params.MailName", this.props.params.MailName);
        window.log("nextProps.params.MailName", nextProps.params.MailName);
        if(this.props.params.MailName && nextProps.params.MailName !== this.props.params.MailName){
            this.props.history.push({
                pathname: "/store/settings/emails/"+storeEvents[nextProps.params.MailName].name
            });
        }else if(this.props.params.MailName && nextProps.params.MailName === this.props.params.MailName){
            this.getEmailTemplate();
        }
    }

    componentWillMount() {
        if(!this.props.params.MailName){
            window.log("mailEditor componentWillMount", storeEvents);
            this.props.history.push({
                pathname: "/store/settings/emails/ACCOUNT_CREATE"
            });
        }
    }


    loadEmailEditPage(email_name){
        this.props.history.push({
            pathname: "/store/settings/emails/"+email_name
        });
    }

    getEmailTemplate(){
        let that = this;
        makeRequest('loadEmailTemplate', {'name': this.props.params.MailName}, function(returnData){
            window.log("getEmailTemplate return", returnData);
            that.setState({currentEvent: returnData, loading: false});
        })
    }

    saveEvent(btnEvent){
        btnEvent.preventDefault();
        let that = this;
        const formData = {contents: {"subject": document.getElementById("content.subject").innerText, "template": document.getElementById("content.template").innerText}};

        makeRequest("editEvent", {eventName: this.props.params.MailName, changes: formData}, function(returnData){
            window.log("saveEvent returnData", returnData);
            that.setState({saveInProgress: false, contentChanged: false, showSuccessMessage: true});



            $('body').notify({
                type: "thumb-circle",
                message: "Sikeres mentés!",
                position: "top-right",
                style: "success",
                autoClose: true,
                imgSource: "https://scontent-frt3-2.xx.fbcdn.net/v/t1.0-9/17952695_1519815814704590_3073324586972207007_n.jpg?oh=ced1ff8b8aee5cf1bd91a738b24f8c07&oe=5A0F476B"
            });
        });

        return false;
    }

    render() {
        let that = this;

        const contentSetChanged = function(event){
            if(!that.state.contentChanged) {
                that.setState({contentChanged: true});
            }
            return false;
        };

        return (<div className="mail-container">
            <SweetAlert isOpen={that.state.showSuccessMessage}
                        type="warning"
                        confirmButtonText="Yup"
                        cancelButtonText="Nope"
                        callback={function(){that.setState({showSuccessMessage: false});}} />
            <div className="mail-list">
                <div className="ioslist-wrapper" data-ios="false">
                    <div className="ioslist-group-container">
                        <p className="header text-uppercase ioslist-group-header">Események</p>

                        {Object.keys(this.state.events).map(function(thisEventName){
                            const thisEvent = that.state.events[thisEventName];
                            const loadEditPage = function(event){
                                event.preventDefault();
                                if(that.state.contentChanged){
                                    if(window.confirm("Az oldalon nem mentett módosítások vannak, melyek elvesznek! Biztosan folytatja?")){
                                        that.loadEmailEditPage(thisEventName);
                                    }
                                }else{
                                    that.loadEmailEditPage(thisEventName);
                                }

                                return false;
                            };
                            let itemClass = "media p-md mail-item";

                            if(that.props.params.MailName === thisEventName){
                                itemClass += " active";
                            }

                            return <div className={itemClass} onClick={loadEditPage} data-target={thisEventName}>
                                <div className="media-body">
                                    <h5 className="fw-thk m-n mail-list-subject">{thisEvent.properties.name.hu}</h5>
                                    <p className="mail-list-content">{thisEvent.properties.description.hu}</p>
                                </div>
                            </div>;
                        })}
                    </div>
                </div>
            </div>

            {this.state.loading ? <div className="mail-message text-center"><div className="spinner"/></div> :
                <div className="mail-message">
                    <form id={"event-edit-form"} onSubmit={function(event){event.preventDefault();return false;}}>
                    <div className="mail-header">
                        <div className="media p-md">
                            <div className="media-body">
                                <p className="fs-sm m-n">Feladó :</p>
                                <h4 className="media-heading text-uppercase fw-thk">ShopyCloud webáruház &#60;info@shopycloud.org&#62;</h4>
                            </div>
                        </div>
                    </div>
                    <div className="mail-subject p-md">
                        <p>Tárgy :</p>
                        <h4 className="m-n fw-thk" name={"content[subject]"} id={"content.subject"} onKeyDown={contentSetChanged} contentEditable={true}>{this.state.currentEvent.contents.subject}</h4>
                    </div>

                    <div className="mail-body p-md">
                        <div className="mail-body-content m-lg-t" name={"content[template]"} id={"content.template"} contentEditable={true} onKeyDown={contentSetChanged}>
                            {renderHTML(this.state.currentEvent.contents.template)}
                            </div>
                        <hr/>
                        {that.state.saveInProgress ? <div className="spinner pull-right"/> : <button onClick={that.saveEvent} className="btn btn-primary pull-right" style={{"display": this.state.contentChanged ? "block" : "none"}} name="saveMailEvent_btn">Mentés!</button>}
                        </div>
                    </form>
                </div>}
                </div>);
    }
}
StoreMailSettings.propTypes = {};
StoreMailSettings.defaultProps = {};

export default StoreMailSettings;