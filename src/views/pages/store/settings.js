import React, {
    Component,
} from 'react';
import makeRequest from '../../../utils/request-queue-handler'
import {Tab, Nav, NavItem, Col, Row, Modal} from "react-bootstrap";
import {bindAll} from "lodash";
import PageNotFound from "../../404";
import {getSelectedStore} from "../../../utils/user-util";
import {TITLE_BASE} from "../../../../config";
import {fillDataObject} from "../../../utils/helpers";
import {priceFormat} from "../../../utils/number";

class StoreSettings extends Component {

    constructor(props) {
        super(props);

        bindAll(this,
            'loadStoreSettings', 'getBasicSettings', 'loadPaymentAndShipmentMethods', 'hideModalEditor', 'renderModalContent'
        );

        this.state = {
            'loading': true,
            'storeData': false,
            'storeOrigins': [],
            'menus': [],

            'enabledPaymentMethods': [],
            'enabledShipmentMethods': [],

            'paymentPriceEdit': false,
            'shipmentPriceEdit': false
        };
    }

    loadMenus(callback) {
        let that = this;
        const params = {
            "filter": {},
            "searchString": "",

            "CurrentPage": 1,
            "ppp": 9999
        };
        makeRequest("getMenus", params, function (ret) {
            window.log("getMenus return", ret);
            that.setState({menus: ret.menus}, function(){
                that.loadPaymentAndShipmentMethods(callback);
            });
        });
    }

    loadPaymentAndShipmentMethods(callback) {
        let that = this;
        makeRequest("getPaymentMethods", {}, function (returnArray) {
            that.setState({enabledPaymentMethods: returnArray}, function(){
                makeRequest("getShipmentMethods", {}, function (returnArray) {
                    that.setState({enabledShipmentMethods: returnArray}, callback);
                });
            });
        });
    }

    loadStoreSettings(){
        let that = this;
        this.loadMenus(function(){
            const selectedStore = getSelectedStore();
            that.setState({storeData: selectedStore, storeOrigins: selectedStore.origins},
                function(){
                    window.log("Store data loaded", selectedStore);
                    that.setState({loading: false});
                }
            );
        });
    }

    componentWillMount(){
        this.loadStoreSettings();
    }

    componentDidUpdate(prevProps, prevState, prevContext) {
        if(!this.state.loading && prevState.loading) {
            window.log("Store data fill start");
            fillDataObject(this.state.storeData)
        }
    }


    saveSettings(event){
        event.preventDefault();

        return false;
    }

    pageHeader() {
        if (this.state.storeData) {
            if (!this.state.loading) {
                document.title = "Bolt beállításai / " + this.state.storeData.name+ " | " + TITLE_BASE;
            }
        } else {
            document.title = "Bolt beállításai" + " | " + TITLE_BASE;
        }


        return (
            <h2 className={"content-heading"}>{this.state.storeData ?
                (this.state.loading ? "Bolt beállításai" : "Bolt beállításai / " + this.state.storeData.name)
                : "Új bolt létrehozása"}

                {this.state.storeData &&
                <button className="btn btn-danger pull-right m-sm-r m-sm-b">
                    <i className="fa fa-trash fa-fw"/>
                    Bolt bezárása
                </button>}
                <button onClick={this.saveSettings} className="btn btn-primary pull-right w-200 m-sm-r m-sm-b">
                    <i className="fa fa-cloud-upload fa-fw"/>
                    Beállítások mentése
                </button>
            </h2>);
    }

    getLayoutSettings(){
        return <div className="row">
            <div className="col-md-4">
                <img src="https://api.thumbnail.ws/api/abeee0b66b46a350e90979c47e8796b6983c8b4f9e80/thumbnail/get?url=https://devshop.shopycloud.org%2F&width=700&delay=5000" width="100%"/>
            </div>
            <div className="col-md-8">

                <div className="form-group">
                    <label htmlFor="layout.template" className="control-label">Választott sablon</label>
                    <select className="form-control" id="layout.template">
                        <option value="basic">ShopyCloud #1</option>
                    </select>
                    <p className="m-n-b m-xs-t fs-sm text-grey">Szerezzen be több sablont a <a href="https://market.shopycloud.org" target="_blank">Marketplace</a>-ből</p>
                </div>

                <section className="layout-settings-block" style={{"marginTop":"30px"}}>
                    <h4>Sablon testreszabása</h4><hr/>
                    <div className="spinner"/>
                </section>
            </div>
        </div>;
    }

    hideModalEditor(){
        this.setState({paymentPriceEdit: null, shipmentPriceEdit: null});
    }


    renderModalContent(){
        let that = this;
        let headerString = this.state.paymentPriceEdit ? "Fizetési mód szerkesztése" : "Szállítási mód szerkesztése";
        const baseObject = this.state.paymentPriceEdit ? this.state.paymentPriceEdit : this.state.shipmentPriceEdit;

        const saveObject = function(event){
            event.preventDefault();
            let formData = $('#shipment_payment-modal-form').serializeObject()["modal"];
            const storeDataObject = that.state.storeData;
            storeDataObject[that.state.paymentPriceEdit ? "payments" : "shipments"].forEach(function(thisObject, thisIndex){
                if(thisObject._id == baseObject._id){
                    formData["_id"] = baseObject._id;

                    let pricesObject = {};
                    Object.keys(formData.prices).forEach(function (key) {
                        pricesObject[key] = {
                            "price": parseFloat(formData.prices[key].price),
                            "freefrom": formData.prices[key].freefrom ? parseFloat(formData.prices[key].freefrom) : ""
                        }
                    });

                    formData.prices = pricesObject;

                    storeDataObject[that.state.paymentPriceEdit ? "payments" : "shipments"][thisIndex] = formData;
                }
            });
            that.setState({storeData: storeDataObject}, that.hideModalEditor);
            return false;
        };

        if(baseObject){setTimeout(function(){fillDataObject(baseObject, "modal");},300)}
        let icon = "";
        if(baseObject && baseObject.properties.icon){
            let iconClass = "fa "+baseObject.properties.icon;
            icon = <i className={iconClass}/>;
        }
        return (<Modal show={this.state.paymentPriceEdit || this.state.shipmentPriceEdit} keyboard={true} onHide={this.hideModalEditor}>
            <Modal.Header closeButton>
                <p className="header text-uppercase">{headerString}</p>
                <p>
                    {icon}
                    {baseObject && baseObject.properties.name.hu}
                </p>

            </Modal.Header>
            <Modal.Body><form id="shipment_payment-modal-form" className="m-md-t" onSubmit={function(){return false;}}>

                    <div className="form-group">
                        <label htmlFor="modal.properties.name.hu">Megnevezés</label>
                        <input type="text" className="form-control input-sm" id="modal.properties.name.hu" name="modal[properties][name][hu]"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="modal.properties.description.hu">Leírás</label>
                        <textarea type="text" className="form-control input-sm" style={{"height":"100px"}} id="modal.properties.description.hu" name="modal[properties][description][hu]"/>
                    </div>

                <div className="row">
                    <div className="form-group col-md-6">
                        <label htmlFor="modal.prices.huf.price">Ára</label>
                        <input type="text" className="form-control input-sm" id="modal.prices.huf.price" name="modal[prices][huf][price]"/>
                    </div>
                    <div className="form-group col-md-6">
                        <label htmlFor="modal.prices.huf.freefrom">Ingyenes ettől az értéktől</label>
                        <input type="text" className="form-control input-sm" id="modal.prices.huf.freefrom" name="modal[prices][huf][freefrom]"/>
                    </div>
                </div>

                    <button className="btn btn-primary w-150 m-sm-t pull-right" onClick={saveObject}>Mentés & bezárás</button>
                    <div className="clearfix"/>
                </form>
            </Modal.Body>
        </Modal>);
    }

    getPaymentShipmentMethods(){
        let that = this;
        const inArray = function(key, value, array){
            let inArrayRet = false;
            array.forEach(function(thisArray){
                if(thisArray[key] && thisArray[key] == value){
                    inArrayRet = true;
                    return inArrayRet;
                }
            });
            return inArrayRet;
        };

        const addPaymentMethod = function(event){
            if(event.target.value){
                const storeDataPayments = that.state.storeData;
                const addPayment = that.state.enabledPaymentMethods[event.target.value];
                storeDataPayments.payments.push(addPayment);
                that.setState({storeData: storeDataPayments},function(){
                    document.getElementById("addPaymentMethod").value = "";
                    that.setState({paymentPriceEdit: addPayment});
                });
            }
        };
        const addShipmentMethod = function(event){
            if(event.target.value){
                const storeDataShipments = that.state.storeData;
                const addShipment = that.state.enabledShipmentMethods[event.target.value];
                storeDataShipments.shipments.push(addShipment);
                that.setState({storeData: storeDataShipments},function(){
                    document.getElementById("addShipmentMethod").value = "";
                    that.setState({shipmentPriceEdit: addShipment});
                });
            }
        };

        return <div className="row">
            <div className="col-md-6">
                <h4>Fizetési módok</h4><hr/>
                <div className="form-group">
                    <select className="form-control" onChange={addPaymentMethod} id="addPaymentMethod">
                        <option value="">Itt adhat hozzá fizetési módot</option>
                        {this.state.enabledPaymentMethods.map(function(thisData, thisIndex){
                            return <option value={thisIndex} disabled={inArray("_id", thisData._id, that.state.storeData.payments)}>{thisData.properties.name.hu}</option>
                        })}
                    </select>
                    <div className="mini-calendar">
                        <div className="mini-calendar-body">
                            {this.state.storeData.payments.length > 0 ?
                                <ul className="ul-clear">
                                    {this.state.storeData.payments.map(function (catId, index) {
                                        const removeOrigin = function () {
                                            const storeData = that.state.storeData;
                                            storeData.payments.splice(index, 1);
                                            that.setState({storeData: storeData});
                                        };
                                        const editPaymentPrice = function (event) {
                                            if(event.target.className != "fa fa-close") {
                                                that.setState({paymentPriceEdit: catId});
                                            }
                                        };
                                        return <li onClick={editPaymentPrice}>{catId.properties.name.hu}<small style={{"marginLeft": "10px"}}>({priceFormat(catId.prices.huf.price)} Ft)</small>
                                                <i className="fa fa-close" onClick={removeOrigin}/>
                                        </li>
                                    })}
                                </ul> : <p className="text-center">Nincs még egyetlen fizetési mód sem engedélyezve!</p>}
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-6">
                <h4>Szállítási módok</h4><hr/>
                <div className="form-group">
                    <select className="form-control" onChange={addShipmentMethod} id="addShipmentMethod">
                        <option value="">Itt adhat hozzá szállítási módot</option>
                        {this.state.enabledShipmentMethods.map(function(thisData, thisIndex){
                            return <option value={thisIndex} disabled={inArray("_id", thisData._id, that.state.storeData.shipments)}>{thisData.properties.name.hu}</option>
                        })}
                    </select>
                    <div className="mini-calendar">
                        <div className="mini-calendar-body">
                            {this.state.storeData.shipments.length > 0 ?
                                <ul className="ul-clear">
                                    {this.state.storeData.shipments.map(function (catId, index) {
                                        const removeOrigin = function () {
                                            const storeData = that.state.storeData;
                                            storeData.shipments.splice(index, 1);
                                            that.setState({storeData: storeData});
                                        };
                                        const editPaymentPrice = function (event) {
                                            if(event.target.className != "fa fa-close") {
                                                that.setState({shipmentPriceEdit: catId});
                                            }
                                        };
                                        return <li onClick={editPaymentPrice}>{catId.properties.name.hu}<small style={{"marginLeft": "10px"}}>({priceFormat(catId.prices.huf.price)} Ft)</small>
                                            <i className="fa fa-close" onClick={removeOrigin}/>
                                        </li>
                                    })}
                                </ul> : <p className="text-center">Nincs még egyetlen szállítási mód sem engedélyezve!</p>}
                        </div>
                    </div>
                </div>
            </div>
        </div>;
    }

    getBasicSettings(){
        let that = this;
        const addOrigin = function(event){
            if(event.key == "Enter") {
                let origins = that.state.storeOrigins;
                origins.push(event.target.value);
                that.setState({storeOrigins: origins});
                document.getElementById('addOrigin').value='';
            }
        };
        return <div className="row">
            <div className="col-md-12">

                <div className="form-group">
                    <label htmlFor="name" className="control-label">Bolt teljes neve</label>
                    <input type="name" className="form-control" id="name" placeholder="Ez a név fog megjelenni a címsorban is."/>
                </div>

                <div className="form-group">
                    <label htmlFor="store_key" className="control-label">Biztonsági kulcs</label>
                    <input type="text" className="form-control" id="store_key" readOnly={true}/>
                </div>

                <div className="form-group">
                    <label htmlFor="addOrigin" className="control-label">Bolt domainneve(i)</label>
                    <input type="text" className="form-control" id="addOrigin"
                           placeholder="Itt adhat hozzá új domainneveket. Például: mintabolt.hu"
                           onKeyPress={addOrigin}
                    />
                    <div className="mini-calendar">
                        <div className="mini-calendar-body">
                            {this.state.storeOrigins.length > 0 ?
                                <ul className="ul-clear">
                                    {this.state.storeOrigins.map(function (catId, index) {
                                        const removeOrigin = function () {
                                            const origins = that.state.storeOrigins;
                                            origins.splice(index, 1);
                                            that.setState({storeOrigins: origins});
                                        };
                                        return <li>{catId}<i className="fa fa-close"
                                                                                        onClick={removeOrigin}/></li>
                                    })}
                                </ul> : <p className="text-center">Nincs még egyetlen webcím sem hozzárendelve!</p>}
                        </div>
                    </div>
                </div>

            </div>
        </div>;
    }

    render() {
        let loadSection = this.getBasicSettings();
        switch(this.props.location.pathname){
            case'/store/settings/basic': loadSection = this.getBasicSettings(); break;
            case'/store/settings/payment': loadSection = this.getPaymentShipmentMethods(); break;
            case'/store/settings/appearance': loadSection = this.getLayoutSettings(); break;
            case'/store/settings/layout': loadSection = this.getLayoutSettings(); break;
        }


        return (<div className="content">{this.pageHeader()}{this.renderModalContent()}
            <div className="block"><div className="block-content"><form name="store-editor-form" id="store-editor-form">
                {this.state.loading ? <div className="text-center col-md-12" align="center">
                    <div className="spinner"/>
                </div> : loadSection}
            </form></div></div></div>);
    }
}

StoreSettings.propTypes = {};
StoreSettings.defaultProps = {};

export default StoreSettings;