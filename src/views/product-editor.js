import React, {
    Component,
} from 'react';
import makeRequest from '../utils/request-queue-handler'
import {bindAll} from "lodash";
import {Tab, Nav, NavItem, Col, Row} from "react-bootstrap";
import $ from 'jquery';
import Dropzone from 'react-dropzone'
import {Slug} from "../utils/url";
import ProductBox from "../components/widgets/product-box"; // ES2015
import NVD3Chart from "react-nvd3/dist/react-nvd3";
const sendPostRequest = require("../utils/xhr-request.js").sendPostRequest;

import {TITLE_BASE, API_URL} from '../../config';
import ProductList from "./productlist";
import {context, getMonthName} from "../utils/chart";
import PageNotFound from "./404";
import {getAccountStores} from "../utils/user-util";


class ProductEditor extends Component {

    constructor(props) {
        super(props);

        bindAll(this,
            'loadProductData', 'selectTab', 'loadProductSchemes',
            'productSchemeSettings', 'setScheme', 'mediaPanel', 'onMediaUpload',
            'removeMediaUpload', 'saveProduct', 'processMedias', 'addCategoryToProduct',
            'reloadData', 'createEmptyShadow', 'addStoreToProduct'
        );

        this.state = {
            'loading': true,
            'productID': this.props.params.productID,
            'productData': {},
            'selectedData': 'product-analytics',
            'productSchemes': [],
            'activeScheme': null,
            'productFiles': [],
            'productMediaIds': [],
            'productCategories': [],
            'productStores': [],
            'categoriesById': [],
            'storesById': [],
            'categories': [],
            'connectedProducts': [],
            'connectedProductIDs': []
        };
    }

    storesById(){
        const stores = getAccountStores();
        let stateObject = {};
        stores.forEach(function(thisStore){
           stateObject[thisStore._id] = thisStore.name;
        });

        this.setState({storesById: stateObject});
    }

    resizeTabNavs() {
        const tabsInUl = this.state.productID ? 6 : 5;
        return 100 / parseInt(tabsInUl) + '%';
    }

    getCategories() {
        let that = this;
        let categories = {};
        makeRequest('getCategories', {}, function (categoryData) {
            const returnOptions = function (object, lastState = "") {
                object.forEach(function (cat) {
                    const newName = (lastState && lastState + " / ") + cat.properties.name.hu;
                    categories[cat._id] = newName;
                    if (cat.children) {
                        returnOptions(cat.children, newName);
                    }
                });
            };
            returnOptions(categoryData);
            that.setState({categories: categoryData, categoriesById: categories});
        });
    }

    componentWillMount() {
        this.loadProductSchemes();
        this.getCategories();
        this.storesById();
    }

    componentDidUpdate(prevProps, prevState, prevContext) {
        let that = this;
        if (prevProps.params.productID !== this.props.params.productID) {
            this.setState({productID: this.props.params.productID}, function () {
                that.loadProductSchemes();
                that.getCategories();
            });
        }
    }


    componentDidMount() {
        // this.resizeTabNavs();
    }

    selectTab(key) {
        this.setState({selectedData: key});
    }

    generateURL(productName) {
        return Slug(productName)
    }

    setScheme(event) {
        let that = this;
        const selectedScheme = event.target.value;
        window.log("selectedScheme", selectedScheme);
        this.state.productSchemes.forEach(function (scheme) {
            if (scheme._id === selectedScheme) {
                that.setState({activeScheme: scheme});
            }
        });
    }

    productSchemeSettings() {
        window.log("this.state.activeScheme", this.state.activeScheme);
        let that = this;
        if (this.state.activeScheme) {
            return Object.keys(this.state.activeScheme.properties).map(function (propKey) {
                const propValue = that.state.activeScheme.properties[propKey];
                switch (propValue.type) {
                    case'TEXT':
                        return (<div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor={"properties." + propKey}>{propValue.name}</label>
                                <input id={"properties." + propKey} name={"properties[" + propKey + "]"} type="text"
                                       className="form-control"/>
                            </div>
                        </div>);
                        break;
                    case'NUMBER':
                        return (<div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor={"properties." + propKey}>{propValue.name}</label>
                                <input id={"properties." + propKey} name={"properties[" + propKey + "]"} type="number"
                                       className="form-control"/>
                            </div>
                        </div>);
                        break;
                    case'SELECT':
                        return (<div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor={"properties." + propKey}>{propValue.name}</label>
                                <select multiple={propValue.multiple} className="form-control"
                                        name={"properties[" + propKey + "]"} id={"properties." + propKey}>
                                    <option>Kérem, válasszon</option>
                                    {propValue.values.map(function (property) {
                                        return (<option value={property}>{property}</option>);
                                    })}
                                </select>
                            </div>
                        </div>);
                        break;
                    case'TEXTBOX':
                        return (<div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor={"properties." + propKey}>{propValue.name}</label>
                                <textarea id={"properties." + propKey} name={"properties[" + propKey + "]"}
                                          className="form-control"/>
                            </div>
                        </div>);
                        break;
                }
            });
        } else {
            return (<h3 style={{"textAlign": "center"}}><i className="fa fa-angle-double-left"/>

                Kérem, előbb válasszon egy termék sémát a folytatáshoz!</h3>);
        }
    }

    productBasicDetails() {
        return (<div>
            <h4>Itt adhatja meg terméke legfontosabb paramétereit</h4>
            <hr/>
            <div className="row">
                <div className="col-md-5">
                    <div className="form-group">
                        <label htmlFor="product_scheme"><i className="fa fa-money fa-fw m-xs-r"/>Termék séma</label>
                        <select className="form-control" name="product_scheme" id="product_scheme"
                                onChange={this.setScheme}>
                            <option>Kérem, válasszon</option>
                            {this.state.productSchemes.map(function (scheme) {
                                return (<option value={scheme._id}>{scheme.name}</option>);
                            })}
                        </select>
                    </div>

                    <div className="form-group">
                        <label htmlFor="product_no"><i className="fa fa-at fa-fw m-xs-r"/>Cikkszám *</label>
                        <input id="product_no" name="product_no" type="text" className="required form-control"
                               aria-required="true"/>
                    </div>


                    <div className="form-group">
                        <label htmlFor="properties.name.hu"><i className="fa fa-user fa-fw m-xs-r"/>Megnevezés *</label>
                        <input id="properties.name.hu" name="properties[name][hu]" type="text"
                               className="required form-control" aria-required="true"/>
                    </div>

                    <div className="form-group w-right-button">
                        <label htmlFor="prices.huf.price"><i className="fa fa-money fa-fw m-xs-r"/>Termék ára *</label>
                        <input id="prices.huf.price" name="prices[huf][price]" type="text"
                               className="required form-control" aria-required="true"/>
                        <button className="btn btn-primary holder-btn"><i className="fa fa-globe"/></button>
                        <div className="clearfix"/>
                    </div>

                </div>
                <div className="col-md-7">
                    {/*<h3>Részletes leírás</h3>*/}
                    <textarea name="properties[description][hu]" className="summernote-editor"/>
                </div>
            </div>
        </div>);
    }

    loadProductSchemes() {
        let that = this;
        makeRequest("getProductSchemes", {}, function (ret) {
            window.log("getProductSchemes return", ret);
            that.setState({productSchemes: ret});
            that.loadProductData();
        });
    }

    fillProductData(object) {
        let that = this;

        const fillDataObject = function (object, findInput = "") {
            Object.keys(object).forEach(function (thisIndex) {
                const thisValue = object[thisIndex];
                const newIndex = (findInput && findInput + ".") + thisIndex;
                if (typeof thisValue === 'object') {
                    fillDataObject(thisValue, newIndex);
                } else {
                    window.log("Attach element", newIndex);
                    if (newIndex.indexOf('content') > -1) {
                        that.setState({'contentOfPage': thisValue});
                    } else {
                        if (document.getElementById(newIndex)) {
                            document.getElementById(newIndex).value = thisValue;
                        }
                    }
                }
            });
        };

        // fill product_scheme then delete from object
        if (object.hasOwnProperty("product_scheme") && document.getElementById("product_scheme")) {
            document.getElementById("product_scheme").value = object.product_scheme;
            this.setScheme({target: {value: object.product_scheme}});
            delete object.product_scheme;
        }

        if (object.hasOwnProperty("medias")) {
            this.setState({productFiles: object.medias});
            delete object.medias;
        }
        if (object.hasOwnProperty("categoryIDs")) {
            this.setState({productCategories: object.categoryIDs});
            delete object.categoryIDs;
        }
        if (object.hasOwnProperty("store_id")) {
            this.setState({productStores: object.store_id});
            delete object.store_id;
        }
        if (object.hasOwnProperty("connectedProductsIDs")) {
            this.setState({connectedProductIDs: object.connectedProductsIDs});
            delete object.connectedProductsIDs;
        }

        setTimeout(function () {
            fillDataObject(object);
        }, 1000);
    }


    loadProductData() {
        let that = this;
        makeRequest("getProductData", {"ObjectID": this.state.productID}, function (ret) {
            window.log("loadProductData return", ret);
            that.setState({loading: false, productData: ret}, function () {
                if(ret) {
                    that.fillProductData(ret);
                }
            });
        });
    }

    pageHeader() {
        if (this.state.productID) {
            if (!this.state.loading) {
                document.title = "Termék módosítása / " + this.state.productData.product_no + " | " + TITLE_BASE;
            }
        } else {
            document.title = "Termék létrehozása" + " | " + TITLE_BASE;
        }


        return (<h2 className="content-heading">
            {this.state.productID ?
                (this.state.loading ? "Termék módosítása" : "Termék módosítása / " + this.state.productData.product_no)
                : "Termék létrehozása"}

                {this.state.productID &&
                <button onClick={this.saveProduct} className="btn btn-danger pull-right btn-sm ml-1">
                    <i className="fa fa-trash fa-fw"/>
                    Törlés
                </button>}
                <button onClick={this.saveProduct} className="btn btn-primary pull-right btn-sm">
                    <i className="fa fa-cloud-upload fa-fw"/>
                    Termék mentése!
                </button>
        </h2>);
    }

    removeMediaUpload(file) {
        let productFiles = this.state.productFiles;
        productFiles.forEach(function (thisFile, index) {
            if (file &&
                ((file.hasOwnProperty("preview") && thisFile.preview === file.preview)
                ||
                (file.hasOwnProperty("url") && thisFile.url === file.url))
            ) {
                productFiles.splice(index, 1);
            }
        });
        this.setState({
            productFiles: productFiles
        });
    }

    onMediaUpload(files) {
        window.log("onMediaUpload files", files);
        let productFiles = this.state.productFiles;
        files.forEach(function (thisFile) {
            productFiles.push(thisFile);
        });
        this.setState({
            productFiles: productFiles
        });
        return false;
    }

    // TODO: Add Media attach panel from Media class without processor (just attach)
    mediaPanel() {
        const dropFileString = "Húzza ide képeit, vagy kattintson a tallózáshoz...";
        const overlayStyle = {
            width: "100%",
            height: "auto",
            minHeight: "200px"
        };
        let that = this;
        const showImageBox = function (f) {
            if (!f) {
                return false;
            }
            const removeMediaAttachment = function (event) {
                event.preventDefault();
                that.removeMediaUpload(f);
                return false;
            };
            let productImage = '';
            if (f.hasOwnProperty("url")) {
                productImage = f.url;
            } else if (f.hasOwnProperty("preview")) {
                productImage = f.preview;
            }
            return (<div className="product_edit-filePreview text-center">
                <div className="imageData" style={{"backgroundImage": "url(" + productImage + ")"}}/>
                <span className="fa-stack fa-lg" onClick={removeMediaAttachment}>
                  <i className="fa fa-circle fa-stack-2x"/>
                  <i className="fa fa-trash-o fa-stack-1x fa-inverse"/>
                </span>
            </div>);
        };

        return (<section>
            <div className="dropzone">
                <Dropzone onDrop={this.onMediaUpload} style={overlayStyle}>
                    {this.state.productFiles.length > 0 ? this.state.productFiles.map(f => showImageBox(f)) :
                        <h4 style={{"marginTop": "40px"}} className="text-center">{dropFileString}</h4>}
                    <div className="clearfix"/>
                </Dropzone>
            </div>
        </section>);
    }

    invertoryAndShipping() {
        const toggleShippingBlock = function () {
            const shippingSwitch = document.getElementById("shipping.enabled").checked;
            if (shippingSwitch) {
                document.getElementById("shippingBlock").style.display = "block";
            } else {
                document.getElementById("shippingBlock").style.display = "none";
            }
        };
        return (<div>
            <div className="row">
                <div className="col-md-6">
                    <h4>Készlet</h4>
                    <hr/>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="invertory.sku"><i className="fa fa-cube fa-fw m-xs-r"/>SKU (Stock
                                    Keeping Unit)</label>
                                <input id="invertory.sku" name="invertory[sku]" type="text" className="form-control"/>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="invertory.barcode"><i className="fa fa-barcode fa-fw m-xs-r"/>Vonalkód
                                    (SBN, UPC, GTIN, stb)</label>
                                <input id="invertory.barcode" name="invertory[barcode]" type="text"
                                       className="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="invertory.method"><i className="fa fa-exchange fa-fw m-xs-r"/>Készletkezelés
                                    módja</label>
                                <select id="invertory.method" name="invertory[method]" className="form-control">
                                    <option value="none">Ne legyen készletkezelés</option>
                                    <option value="shopycloud">ShopyCloud kezelje a készletet</option>
                                </select>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="invertory.stock"><i className="fa fa-cubes fa-fw m-xs-r"/>Jelenlegi
                                    készlet</label>
                                <input id="invertory.stock" name="invertory[stock]" type="number"
                                       className="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-6">
                    <h4>Szállítás</h4>
                    <hr/>
                    <div className="row">
                        <div className="col-md-12 text-center" style={{"height": "63px", "marginTop": "20px"}}>
                            <div className="switch primary">
                                <label>
                                    <input type="checkbox" id="shipping.enabled" name="shipping[enabled]"
                                           onChange={toggleShippingBlock}/>
                                    <span className="lever"/>
                                    Ez a termék kiszállítást igényel
                                </label>
                            </div>
                        </div>
                    </div>
                    <div className="row" id="shippingBlock" style={{"display": "none"}}>
                        <div className="col-md-6">
                            <div className="form-group w-right-button">
                                <label htmlFor="shipping.weight"><i className="fa fa-balance-scale fa-fw m-xs-r"/>Termék
                                    súlya *</label>
                                <input id="shipping.weight" name="shipping[weight]" type="number" min={0.1} step={0.2}
                                       className="required form-control" aria-required="true"/>
                                <button className="btn btn-primary holder-btn">kg</button>
                                <div className="clearfix"/>
                            </div>
                        </div>

                        <div className="col-md-6">
                            <div className="form-group">
                                <label htmlFor="shipping.type"><i className="fa fa-truck fa-fw m-xs-r"/>Szállítólevél
                                    típusa</label>
                                <select id="shipping.type" name="shipping[type]" className="form-control">
                                    <option value="manual">Kézi</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>);
    }

    getAnalytics() {
        let that = this;
        const datum = [{
            key: "Cumulative Return",
            values: []
        }];
        const pieCountry = [];
        const pieCity = [];

        Object.keys(that.state.productData.orderStats.byMonths).map(function (thisMonthStat) {
            datum[0].values.push({
                "label": getMonthName(thisMonthStat),
                "value": that.state.productData.orderStats.byMonths[thisMonthStat]
            });
        });

        Object.keys(that.state.productData.orderStats.byCountries).map(function (thisMonthStat) {
            pieCountry.push({
                "label": thisMonthStat,
                "value": that.state.productData.orderStats.byCountries[thisMonthStat]
            });
        });

        Object.keys(that.state.productData.orderStats.byCities).map(function (thisMonthStat) {
            pieCity.push({
                "label": thisMonthStat,
                "value": that.state.productData.orderStats.byCities[thisMonthStat]
            });
        });

        return (<sectioin>

            <div className="row">
                <div className="col-md-12">
                    <h4>Eladások hónapok szerint</h4>
                    <hr/>
                    <NVD3Chart id="barChart"
                               containerStyle={{width: "auto", height: 90}}
                               type="discreteBarChart" datum={datum} x="label"
                               y={function getX(d) {
                                   return parseInt(d.value);
                               }}
                               showValues="false"
                               context={context}
                               color={{name: 'getColor', type: 'function'}}
                               margin={{left: 0, right: 0, bottom: 0, top: 2}}/>
                </div>
            </div>

            <div className="row" style={{"marginTop": "70px"}}>
                <div className="col-md-6">
                    <h4>Vásárlási régiók (Országok szerint)</h4>
                    <hr/>
                    <NVD3Chart id="barChart"
                               type="pieChart" datum={pieCountry} x="label"
                               y="value"
                               width={600}
                               height={370}/>
                </div>
                <div className="col-md-6">
                    <h4>Vásárlási régiók (Városok szerint)</h4>
                    <hr/>
                    <NVD3Chart id="barChart"
                               type="pieChart" datum={pieCity} x="label"
                               y="value"
                               width={600}
                               height={370}/>
                </div>
            </div>
        </sectioin>);
    }

    addCategoryToProduct(event) {
        const addValue = event.target.value;
        const productCategories = this.state.productCategories;
        productCategories.push(addValue);
        this.setState({productCategories: productCategories});
    }

    addStoreToProduct(event) {
        const addValue = event.target.value;
        const productStores = this.state.productStores;
        productStores.push(addValue);
        this.setState({productStores: productStores});
    }

    categoryRecursiveOptions() {
        let categoryData = [];
        const productCategories = this.state.productCategories;
        const returnOptions = function (object, lastState = "") {
            object.forEach(function (cat) {
                const newName = (lastState && lastState + " / ") + cat.properties.name.hu;
                categoryData.push(<option value={cat._id}
                                          disabled={productCategories.indexOf(cat._id) > -1}>{newName}</option>);
                if (cat.children) {
                    returnOptions(cat.children, newName);
                }
            });
        };
        returnOptions(this.state.categories);
        return <select className="form-control" onChange={this.addCategoryToProduct}>
            <option value={0}>Válassz kategóriát...</option>
            {categoryData}</select>;
    }

    storesOptions() {
        let productStores = this.state.productStores;
        return <select className="form-control" onChange={this.addStoreToProduct}>
            <option value={0}>Válassz hálózatot...</option>
            {getAccountStores().map(function(thisStore){
                return <option value={thisStore._id} disabled={productStores.indexOf(thisStore._id) > -1}>{thisStore.name}</option>
            })}</select>;
    }

    connectionsPanel() {
        let that = this;
        return (<div className="row">
            <div className="col-md-3">
                <h4>Kategóriák</h4>
                <hr/>
                {this.categoryRecursiveOptions()}
                <div className="mini-calendar">
                    <div className="mini-calendar-body">
                        {this.state.productCategories.length > 0 ?
                            <ul className="ul-clear">
                                {this.state.productCategories.map(function (catId, index) {
                                    const removeCategory = function () {
                                        const productCategories = that.state.productCategories;
                                        productCategories.splice(index, 1);
                                        that.setState({productCategories: productCategories});
                                    };
                                    return <li>{that.state.categoriesById[catId]}<i className="fa fa-close"
                                                                                    onClick={removeCategory}/></li>
                                })}
                            </ul> : <p className="text-center">Nincs még kategória hozzárendelve!</p>}
                    </div>
                </div>
            </div>
            {/* Todo: Add networks from stores*/}
            <div className="col-md-3">
                <h4>Hálózatok</h4>
                <hr/>

                {this.storesOptions()}
                <div className="mini-calendar">
                    <div className="mini-calendar-body">
                        {this.state.productStores.length > 0 ?
                            <ul className="ul-clear">
                                {this.state.productStores.map(function (catId, index) {
                                    const removeCategory = function () {
                                        const productStores = that.state.productStores;
                                        productStores.splice(index, 1);
                                        that.setState({productStores: productStores});
                                    };
                                    return <li>{that.state.storesById[catId]}<i className="fa fa-close"
                                                                                    onClick={removeCategory}/></li>
                                })}
                            </ul> : <p className="text-center">Nincs még kategória hozzárendelve!</p>}
                    </div>
                </div>
            </div>
            <div className="col-md-6">
                <h4>Kapcsolt termékek</h4>
                <hr/>
                <div className="row">
                    {this.state.connectedProductIDs.map(function (connectedProductData) {
                        return <div className="col-md-6"><ProductBox productID={connectedProductData}/></div>
                    })}
                </div>
            </div>
        </div>);
    }

    processMedias(callback) {
        let productMediaIds = [];
        let that = this;
        if (this.state.productFiles.length > 0) {
            this.state.productFiles.forEach(function (thisFile) {
                if (thisFile.hasOwnProperty('preview')) {
                    let formData = new FormData();
                    formData.append('fileData', thisFile);
                    sendPostRequest(API_URL + '/api/upload', formData).then(function (mediaId) {
                        productMediaIds.push(mediaId.body);
                        if (that.state.productFiles.length === productMediaIds.length) {
                            that.setState({productMediaIds: productMediaIds}, function () {
                                callback(productMediaIds);
                            });
                        }
                    });
                } else {
                    productMediaIds.push(thisFile._id);
                    if (that.state.productFiles.length === productMediaIds.length) {
                        that.setState({productMediaIds: productMediaIds}, function () {
                            callback(productMediaIds);
                        });
                    }
                }
            });
        } else {
            callback([]);
        }
    }

    saveProduct(event) {
        event.preventDefault();
        let that = this;
        let formData = $('#product-editor-form').serializeObject();

        window.log("saveProduct", true);
        this.setState({loading: true});
        this.processMedias(function (returnMediaIds) {
            window.log("returnMediaIds", returnMediaIds);
            window.log("formData", formData);

            let pricesObject = {};
            Object.keys(formData.prices).forEach(function (key) {
                pricesObject[key] = {
                    "price": parseFloat(formData.prices[key].price),
                    "actionprice": formData.prices[key].actionprice ? parseFloat(formData.prices[key].actionprice) : 0
                }
            });
            formData.categories = that.state.productCategories;
            formData.store_id = that.state.productStores;
            formData.prices = pricesObject;

            // generate url string
            // TODO: Validate url string
            let url_stringObject = {};
            Object.keys(formData.properties.name).forEach(function (key) {
                url_stringObject[key] = that.generateURL(formData.properties.name[key]);
            });
            formData["url_string"] = url_stringObject;
            formData["medias"] = that.state.productMediaIds;
            if (that.state.productMediaIds[0]) {
                formData["default_media"] = that.state.productMediaIds[0];
            }

            if (!that.state.productID) {
                makeRequest("createProduct", formData, function (ret) {
                    window.log("createProduct return", ret);
                    that.setState({loading: false});
                    that.props.history.push({
                        pathname: "/product/" + ret.returnID + "/edit",
                        state: {response: 'success'}
                    });
                });
            } else {
                makeRequest("editProduct", {ObjectID: that.state.productID, changes: formData}, function (ret) {
                    window.log("editProduct return", ret);
                    that.setState({loading: false});
                    that.props.history.push({
                        pathname: '/product/list',
                        state: {response: 'success'}
                    });
                    that.props.history.push('/product/list');
                });
            }

            window.log("saveProduct formData", formData);
        });
        return false;
    }

    reloadData() {
        this.setState({loading: true});
    }

    createEmptyShadow() {
        let formData = {};
        let that = this;
        formData['shadow_id'] = (that.state.productData.shadow_id && that.state.productData.shadow_id !== "" ? that.state.productData.shadow_id : that.state.productID);
        formData['product_no'] = that.state.productData.product_no + "-" + Math.floor(Math.random() * 999) + 111;

        makeRequest("createProduct", formData, function (ret) {
            window.log("createProduct return", ret);
            that.setState({loading: false});
            window.location.href = "/product/" + ret.returnID + "/edit";
        });
    }

    render() {
        let shadowProductsHolder;
        if (this.state.productData) {
            shadowProductsHolder = <ProductList mainId={this.state.productData.shadow_id} clickBefore={this.reloadData}
                                                createProductFunction={this.createEmptyShadow}
                                                shadowProducts={this.state.productData.shadowProducts}/>;
        }
        if(this.state.productData) {
            return (<section><div className="content">
                {this.pageHeader()}
                <div className="row">
                    <div className="col-md-12">
                        <div className="panel-x panel-transparent">
                            <div className="block" >
                                <form name="product-editor-form" id="product-editor-form" onSubmit={this.saveProduct}>
                                    {this.state.loading ? <div className="text-center col-md-12" align="center">
                                        <div className="spinner"/>
                                    </div> : <Tab.Container id="product-edit-tabs"
                                                            defaultActiveKey={this.state.productID ? "analytics" : "details"}>
                                        <Row className="clearfix">
                                            <Col sm={12}>
                                                <Nav className={"nav nav-tabs nav-tabs-alt nav-fill nav-tabs-block"}>
                                                    {this.state.productID &&
                                                    <NavItem eventKey="analytics" className="dynamic-width nav-item"
                                                             style={{width: this.resizeTabNavs()}}>
                                                        Analytika
                                                    </NavItem>}
                                                    <NavItem eventKey="details" className="dynamic-width nav-item"
                                                             style={{width: this.resizeTabNavs()}}>
                                                        Alapadatok
                                                    </NavItem>
                                                    <NavItem eventKey="properties" className="dynamic-width nav-item"
                                                             style={{width: this.resizeTabNavs()}}>
                                                        Paraméterek
                                                    </NavItem>
                                                    <NavItem eventKey="medias" className="dynamic-width nav-item"
                                                             style={{width: this.resizeTabNavs()}}>
                                                        Képek
                                                    </NavItem>
                                                    <NavItem eventKey="connections" className="dynamic-width nav-item"
                                                             style={{width: this.resizeTabNavs()}}>
                                                        Kapcsolatok
                                                    </NavItem>
                                                    <NavItem eventKey="stock" className="dynamic-width nav-item"
                                                             style={{width: this.resizeTabNavs()}}>
                                                        Készlet, szállítás
                                                    </NavItem>
                                                </Nav>
                                            </Col>
                                            <Col sm={12}>
                                                <div>
                                                <Tab.Content animation className={"block-content"}>
                                                    {this.state.productID &&
                                                    <Tab.Pane eventKey="analytics">
                                                        {this.getAnalytics()}
                                                        <div className="clearfix"/>
                                                    </Tab.Pane>}
                                                    <Tab.Pane eventKey="details">
                                                        {this.productBasicDetails()}
                                                        <div className="clearfix"/>
                                                    </Tab.Pane>
                                                    <Tab.Pane eventKey="properties">
                                                        {this.productSchemeSettings()}
                                                        <div className="clearfix"/>
                                                    </Tab.Pane>
                                                    <Tab.Pane eventKey="medias">
                                                        {this.mediaPanel()}
                                                        <div className="clearfix"/>
                                                    </Tab.Pane>
                                                    <Tab.Pane eventKey="connections">
                                                        {this.connectionsPanel()}
                                                        <div className="clearfix"/>
                                                    </Tab.Pane>
                                                    <Tab.Pane eventKey="stock">
                                                        {this.invertoryAndShipping()}
                                                        <div className="clearfix"/>
                                                    </Tab.Pane>
                                                </Tab.Content>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Tab.Container>}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>{!this.state.loading && shadowProductsHolder}</section>);
        }else{
            return (<PageNotFound/>);
        }
    }

}

ProductEditor.propTypes = {};
ProductEditor.defaultProps = {};

export default ProductEditor;