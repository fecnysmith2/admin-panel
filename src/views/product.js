import React, {
   Component,
} from 'react';
import makeRequest from '../utils/request-queue-handler'
import {bindAll} from "lodash";
import ProductBox from "../components/widgets/product-box";
import Cart from "./cart";

class Product extends Component {

   constructor(props) {
       super(props);

       bindAll(this,
           'loadProductData'
       );

       this.state = {
           'loading': true,
		   'productID': this.props.params.productID,
           'productData': {},
           'productAddedToCart': false
       };
       this.loadProductData();
   }

    componentWillMount(){

    }
    componentDidMount(){
    	const setDefaultQty = function(){
            if(document.getElementById("quantity-value")){
                document.getElementById("quantity-value").value = 1;
            }else{
            	setTimeout(setDefaultQty, 100);
			}
		};
        setDefaultQty();
    }

    loadProductData(){
        let that = this;
        makeRequest("getProductData", {"ObjectID":this.state.productID}, function(ret){
            window.log("loadProductData return", ret);
            that.setState({loading:false, productData: ret});
        });
    }
	render() {
		return (<main id="page-content">
			<div className="page-title">
				<h3>Validation</h3>
				<ol className="breadcrumb">
					<li><small><i className="fa fa-home fa-fw m-xs-r"></i>Home</small></li>
					<li><small><i className="fa fa-desktop fa-fw m-xs-r"></i>Forms</small></li>
					<li><a href="javascript:void(0)" className="text-info"><small>Validation</small></a></li>
				</ol>
			</div>
			<div className="row row-xl">
				<div className="col-md-12">
					<div className="panel-x panel-transparent">
						<div className="panel-body">
							<h3>Form Wizards helps you make the old school lenghty forms in tidy and robust way.</h3>
							<p>We do use forms a lot of times and having a form wizard helps you get this cleaner, more compact and very robust. We offer jQuery Steps and Bootstrap Wizard. We've made sure these wizards are not just clean, they look nice as well.</p>
						</div>
					</div>
				</div>
			</div>
			<div className="row row-xl">
				<div className="col-md-12">
					<div className="panel-x panel-transparent">
						<div className="panel-body">
							<p className="header text-uppercase">jQuery Steps</p>
							<p>The below form uses jQuery steps and along with validation. Till the criterias are met for the first tab, you cannot advance to the next tab.</p>
							<form id="example-advanced-form" action="#" role="application" className="wizard clearfix" novalidate="novalidate"><div className="steps clearfix"><ul role="tablist"><li role="tab" className="first current error" aria-disabled="false" aria-selected="true"><a id="example-advanced-form-t-0" href="#example-advanced-form-h-0" aria-controls="example-advanced-form-p-0"><span className="current-info audible">current step: </span><span className="number">1.</span> Account</a></li><li role="tab" className="disabled" aria-disabled="true"><a id="example-advanced-form-t-1" href="#example-advanced-form-h-1" aria-controls="example-advanced-form-p-1"><span className="number">2.</span> Profile</a></li><li role="tab" className="disabled" aria-disabled="true"><a id="example-advanced-form-t-2" href="#example-advanced-form-h-2" aria-controls="example-advanced-form-p-2"><span className="number">3.</span> Warning</a></li><li role="tab" className="disabled last" aria-disabled="true"><a id="example-advanced-form-t-3" href="#example-advanced-form-h-3" aria-controls="example-advanced-form-p-3"><span className="number">4.</span> Finish</a></li></ul></div><div className="content clearfix">
								<h3 id="example-advanced-form-h-0" tabindex="-1" className="title current">Account</h3>
								<fieldset id="example-advanced-form-p-0" role="tabpanel" aria-labelledby="example-advanced-form-h-0" className="body current" aria-hidden="false">
									<div className="row">
										<div className="col-md-5">
											<h3>Get started with the account information!</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur in nihil dignissimos reprehenderit! Praesentium omnis iste, nam fugit eveniet quasi sint, et veniam ab veritatis quam animi consequuntur quaerat saepe.</p>
										</div>

										<div className="col-md-7">
											<h3>Account Information</h3>

											<div className="form-group form-group-default">
												<label htmlFor="userName-2"><i className="fa fa-user fa-fw m-xs-r"></i>User name *</label>
												<input id="userName-2" name="userName" type="text" className="required form-control error" aria-required="true"/><label id="userName-2-error" className="error" htmlFor="userName-2">This field is required.</label>
											</div>

											<div className="form-group form-group-default">
												<label htmlFor="password-2"><i className="fa fa-lock fa-fw m-xs-r"></i>Password *</label>
												<input id="password-2" name="password" type="text" className="required form-control error" aria-required="true"/><label id="password-2-error" className="error" htmlFor="password-2">This field is required.</label>
											</div>

											<div className="form-group form-group-default">
												<label htmlFor="confirm-2"><i className="fa fa-lock fa-fw m-xs-r"></i>Confirm Password *</label>
												<input id="confirm-2" name="confirm" type="text" className="required form-control error" aria-required="true"/><label id="confirm-2-error" className="error" htmlFor="confirm-2">This field is required.</label>
											</div>

											<p>(*) Mandatory</p>
										</div>
									</div>
								</fieldset>
							</div><div className="actions clearfix"><ul role="menu" aria-label="Pagination"><li className="disabled" aria-disabled="true"><a href="#previous" role="menuitem">Previous</a></li><li aria-hidden="false" aria-disabled="false"><a href="#next" role="menuitem">Next</a></li><li aria-hidden="true"><a href="#finish" role="menuitem">Finish</a></li></ul></div></form>
						</div>
					</div>
				</div>
			</div>
		</main>);
	}
}

Product.propTypes = {};
Product.defaultProps = {};

export default Product;