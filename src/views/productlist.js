import React, {
    Component,
} from 'react';
import makeRequest from '../utils/request-queue-handler'
import {bindAll} from "lodash";
import {priceFormat} from "../utils/number";
import Pagination from "../components/widgets/pagination";

import Scroll from "react-scroll";
import {Link} from 'react-router';
import NVD3Chart from "react-nvd3/dist/react-nvd3";
import {TITLE_BASE} from "../../config";
import $ from 'jquery';
import {context, getMonthName} from "../utils/chart";


var Element = Scroll.Element;
var scroller = Scroll.scroller;

class ProductList extends Component {

    constructor(props) {
        super(props);

        bindAll(this,
            'loadCategoryProducts',
            'sortData', 'setPage'
        );

        this.state = {
            'loading': this.props.shadowProducts ? false : true,
            'baseLoading': this.props.shadowProducts ? false : true,
            'categoryID': this.props.shadowProducts ? null : this.props.params.categoryID,
            'categoryProducts': this.props.shadowProducts ? this.props.shadowProducts : {},
            'categoryData': {'productCount': 1000},
            'productsCount': 0,
            'currentPage': 1,
            'productPerPage': this.props.shadowProducts ? 999999 : 25,
            'orderBy': {'properties.name.hu': -1},
            'filter': {},
            'showShadows': false
        };
    }


    componentWillMount() {
        if (!this.props.shadowProducts) {
            this.loadCategoryProducts();
        }
        document.title = "Termékek" + " | " + TITLE_BASE;
    }


    componentDidMount() {
        if (this.props.location && this.props.location.state && this.props.location.state.response === 'success') {
            $('body').notify({
                type: "thumb-circle",
                message: "Sikeres mentés!",
                position: "top-right",
                style: "success",
                autoClose: true,
                imgSource: "https://scontent-frt3-2.xx.fbcdn.net/v/t1.0-9/17952695_1519815814704590_3073324586972207007_n.jpg?oh=ced1ff8b8aee5cf1bd91a738b24f8c07&oe=5A0F476B"
            });
        }
    }


    sortData() {
        function getSelectedOptions(select) {
            let result = [];
            const options = select.getElementsByTagName('option');
            for (let i = 0; i < options.length; i++) {
                if (options[i].selected)
                    return options[i];
            }
            ;
        }


        let sortObject = {};
        const byType = document.getElementById("sortSelect").value;
        const sortKey = getSelectedOptions(document.getElementById("sortSelect")).getAttribute("data-sortKey");
        sortObject[byType] = parseInt(sortKey);
        window.log("sortObject", sortObject);
        this.setState({orderBy: sortObject}, this.loadCategoryData);
    }

    setPage(page) {
        scroller.scrollTo('categoryTopScroller', {
            duration: 400,
            delay: 0,
            offset: -23,
            smooth: true
        });
        this.setState({currentPage: page}, this.loadCategoryProducts);
    }

    loadCategoryProducts() {
        let that = this;
        let searchString = "";
        this.setState({loading: true});
        if (document.getElementById("search-product-list")) {
            searchString = document.getElementById("search-product-list").value;
        }
        let params = {
            "filter": {},
            "searchString": searchString,
            "CurrentPage": this.state.currentPage,
            "ppp": this.state.productPerPage,
            "orderBy": this.state.orderBy
        };

        params.filter = {'shadow_id': null};

        window.log("loadCategoryProducts params", params);
        makeRequest("getProducts", params, function (ret) {
            window.log("searchProduct return", ret);
            that.setState({categoryProducts: ret.products});
            that.setState({productsCount: ret.productCount});
            that.setState({loading: false});
        });
    }

    getCategoryString(categoryArray){
        let categoryNameArray = [];
        categoryArray.map(function(thisCategory){
           categoryNameArray.push(thisCategory.properties.name.hu);
        });

        return categoryNameArray.join(' / ');
    }

    render() {


        let that = this;


        return (<div className="content">
            <div className="page-title">
                <h2 className="content-heading mb-0">
                    {this.props.shadowProducts ? "Termék további változatai" : "Termékek"}


                    {this.props.shadowProducts ? <button onClick={this.props.createProductFunction}
                                                         className="btn btn-primary pull-right btn-sm">
                        <i className="fa fa-cloud fa-fw"/>
                        Új termékváltozat
                    </button> : <a href="/product/add">
                        <button className="btn btn-primary pull-right btn-sm">
                            <i className="fa fa-plus fa-fw"/>
                            Új termék
                        </button>
                    </a>}
                </h2>
            </div>

            <div className="row row-xl">
                <div className="col-md-12">
                    <div className="panel-x">
                        <div className="panel-body">
                            {!this.props.shadowProducts && <div className="row">
                                <p className="col-md-7">A szabadszavas kereső csak a termék (összes nyelvű) nevében és
                                    leírásában keres, használja a termék szűrőt a még pontosabb találatokhoz.
                                    Szűrővarázslónkról részletesen <code>itt olvashat.</code></p>
                                <div className="col-md-5 ">
                                    <div className="form-material floating" style={{"marginTop":"-20px"}}>
                                        <input type="text" className="form-control" onKeyUp={this.loadCategoryProducts} id="search-product-list" name="material-text2"/>
                                        <label htmlFor="search-product-list">Keresés...</label>
                                    </div>
                                </div>
                            </div>}
                                {this.state.loading ? (<div className="text-center col-md-12" align="center">
                                    <div className="spinner"/>
                                </div>) : (this.state.categoryProducts.map(function (productData) {

                                        const datum = [{
                                            key: "Cumulative Return",
                                            values: []
                                        }];

                                        Object.keys(productData.orderStats.byMonths).map(function(thisMonthStat){
                                            datum[0].values.push({
                                                "label": getMonthName(thisMonthStat),
                                                "value": productData.orderStats.byMonths[thisMonthStat]
                                            });
                                        });

                                        window.log("datumdatum", datum);


                                        const orderViewLink = "/product/" + productData._id + "/edit";
                                        const mediaDivId = "row gutters-tiny js-appear-enabled animated fadeIn" + (that.props.mainId && that.props.mainId === productData._id ? " main-list-item" : "");
                                        return (<div className="row gutters-tiny js-appear-enabled animated fadeIn" data-toggle="appear">
                                            <div className="col-md-12">
                                                <Link to={orderViewLink} className="block block-link-shadow overflow-hidden">
                                                    <div className="block-content block-content-full">
                                                        <div className="row">
                                                            <div className={"col-1"}>
                                                                <img src={productData.default_media.url}
                                                                     alt={productData.properties.name.hu}
                                                                     className="img-circle" width="57px" height="57px" style={{"border":"1.5px solid #b5b5b5", "marginTop": "5px"}}/>
                                                            </div>
                                                            <div className="col-4 text-right border-r">
                                                                <div className="js-appear-enabled animated fadeInLeft" data-toggle="appear" data-className="animated fadeInLeft">
                                                                    <div className="font-size-h6 font-w600">{productData.properties.name.hu} {(productData.shadowProducts && productData.shadowProducts.length > 0) && "(" + (productData.shadowProducts.length + 1) + ")"}</div>
                                                                    <div className="font-size-sm font-w300 text-uppercase text-muted">
                                                                        {that.getCategoryString(productData.categories)}
                                                                    </div><div className="font-size-sm font-w400 text-uppercase text-muted">
                                                                        <span className="pull-left" style={{"marginRight": "15px"}}><i
                                                                            className="fa fa-money fa-fw m-xs-r"></i>Jelenlegi ára: {productData.prices.huf.actionprice ? (
                                                                            <stong><span
                                                                                className="cm-strikethrough">{priceFormat(productData.prices.huf.price)}
                                                                                Ft</span>
                                                                                | {priceFormat(productData.prices.huf.price)} Ft
                                                                            </stong>) : (
                                                                            <strong>{priceFormat(productData.prices.huf.price)}
                                                                                Ft</strong>)}</span>
                                                                    <i className="fa fa-stars-o fa-fw m-xs-r"></i>Értékelések:
                                                                    <strong>4.3</strong>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-3 text-left">
                                                                <div className="js-appear-enabled animated fadeInLeft" data-toggle="appear" data-className="animated fadeInLeft">
                                                                    <div className="font-size-xs font-w200 text-uppercase text-muted">
                                                                Összes eladás:<strong>{productData.orderStats.allOrders}</strong>
                                                                        <br/>
                                                                        Nettó
                                                                        profit:<strong> n/A</strong>

                                                                        <br/>
                                                                        Megtekintések:
                                                                        <strong> n/A</strong>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-4 border-l">
                                                                <div className="js-appear-enabled animated fadeInRight" data-toggle="appear" data-className="animated fadeInRight">
                                                                    <NVD3Chart id="barChart"
                                                                               containerStyle={{width: "auto", height: 70}}
                                                                               type="discreteBarChart" datum={datum} x="label"
                                                                               y={function getX(d) {
                                                                                   return parseInt(d.value);
                                                                               }}
                                                                               showValues="false"
                                                                               context={context}
                                                                               color={{name: 'getColor', type: 'function'}}
                                                                               margin={{left: 0, right: 0, bottom: 0, top: 2}}/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Link>
                                            </div>
                                        </div>);




                                        return [<div className={mediaDivId}
                                                     data-toggle-class="mail-message-visible">
                                            <div className="media-left">
                                                <img src={productData.default_media.url}
                                                     alt={productData.properties.name.hu}
                                                     className="img-circle mCS_img_loaded" height="75" width="75"/>
                                            </div>
                                            <div className="media-body">
                                                <Link to={orderViewLink} onClick={function (event) {
                                                    if (that.props.clickBefore) {
                                                        that.props.clickBefore();
                                                    }
                                                }}>
                                                    <div className="col-md-4">
                                                        <h5 className="fw-thk m-n mail-list-subject">
                                                            {productData.properties.name.hu} {(productData.shadowProducts && productData.shadowProducts.length > 0) && "(" + (productData.shadowProducts.length + 1) + ")"}
                                                        </h5>
                                                        <p className="mail-list-content">
                                                            {that.getCategoryString(productData.categories)}
                                                        </p>
                                                        <p className="fs-sm text-grey">
                                                            <span className="pull-left" style={{"marginRight": "15px"}}><i
                                                                className="fa fa-money fa-fw m-xs-r"></i>Jelenlegi ára: {productData.prices.huf.actionprice ? (
                                                                <stong><span
                                                                    className="cm-strikethrough">{priceFormat(productData.prices.huf.price)}
                                                                    Ft</span>
                                                                    | {priceFormat(productData.prices.huf.price)} Ft
                                                                </stong>) : (
                                                                <strong>{priceFormat(productData.prices.huf.price)}
                                                                    Ft</strong>)}</span>
                                                            <i className="fa fa-stars-o fa-fw m-xs-r"></i><strong>Értékelések:
                                                            4.3</strong>
                                                        </p>
                                                    </div>
                                                    <div className="col-md-3">
                                                        <p className="fs-sm text-grey">
                                                            <span className="pull-left" style={{"marginRight": "15px"}}>
                                                                <i className="fa fa-stars-o fa-fw m-xs-r"></i><strong>Összes eladás: {productData.orderStats.allOrders}</strong>
                                                            </span>
                                                            <br/>
                                                            <i className="fa fa-stars-o fa-fw m-xs-r"></i><strong>Nettó
                                                            profit: n/A</strong>

                                                            <br/>
                                                            <i className="fa fa-stars-o fa-fw m-xs-r"></i><strong>Megtekintések:
                                                            n/A</strong>
                                                        </p>

                                                    </div>
                                                </Link>
                                                <div className="col-md-5">


                                                </div>
                                            </div>
                                        </div>];
                                    })
                                )}
                                {!this.props.shadowProducts && <div className={"pull-right"}><Pagination productsPerPage={this.state.productPerPage}
                                                                           productsFound={this.state.productsCount}
                                                                           currentPage={this.state.currentPage}
                                                                                                         onPagination={this.setPage}/></div>}
                        </div>
                    </div>
                </div>
            </div>
        </div>);

    }
}

ProductList.propTypes = {};
ProductList.defaultProps = {};

export default ProductList;