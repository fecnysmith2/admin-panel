import React, {Component} from 'react';
import bindAll from 'lodash.bindall';
import {Link, withRouter} from 'react-router';
import FrontendPage from '../../components/frontend-page';
import makeRequest from '../../utils/request-queue-handler';
import Message from '../../components/message';
import getObjectFromForm from '../../utils/object-from-form-generator';
import PageIntro from '../../components/page-intro';
import {validateConfirmFormInputs} from '../../utils/helpers';
import {
    saveObjectToLocalStore,
    removeObjectFromLocalStore,
    saveOauthTokenObject
} from '../../utils/db';
import {
    getLoginTicketQuery,
    getRegisterUserMutation,
    getAuthenticateUserByFacebookMutation,
    getUserQuery,
    getUserFromInviteKeyQuery
} from '../../utils/queries/queries';
import {saveUserObject, getProfilePicture} from '../../utils/user-util';
import ProfileImagePanel from '../../components/widgets/profile-image-panel';
import FacebookRegisterButton from '../../components/widgets/facebook/facebook-register-button';

/*
 * @TODO refact this class*/
class Register extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'getRegisterUserMutation', 'handleSubmit', 'continueRegistration',
            'toogleConfrimDialog', 'handleUserLoginTicket', 'checkDependentPartner',
            'onQueryError', 'getRegistrationSendedContent', 'getRegistrationPageContent',
            'getRegistrationFormContent', 'getRegistrationEmailConfirmedContent', 'getRegistrationErrorContent',
            'getContent', 'handleFacebookLogin', 'handleFacebookRegSuccess'
        );

        this.state = {
            dependentPartner: '',
            continuousRegistration: false,
            loading: false,
            errors: [],
            inviteData: null
        };
    }

    handleFacebookRegSuccess(userWrapper) {
        window.log('FB USER WRAPPER', userWrapper);
        this.handleUserLoginTicket();
    }

    handleFacebookLogin(facebookUserData) {
        window.log('FB USER DATA', facebookUserData);
    }

    getRegisterUserMutation(event) {
        let userData;
        const that = this;
        try {
            userData = getObjectFromForm(event.target.getAttribute('data-queryParamName'), event.target.elements);
        } catch (err) {
            // window.error('VALIDATE_REGISTER_FORM_FAILED', err);
            return false;
        }

        let partnerGroupInput = '-1';
        if (document.getElementById('partnerGroup')) {
            partnerGroupInput = document.getElementById('partnerGroup').value;
        }
        window.log('PARTNERINPUT', partnerGroupInput);
        let inviteKey = '';
        const {params} = this.props;
        if (params && params.inviteData) {
            inviteKey = params.inviteData;
        }
        return getRegisterUserMutation(userData, partnerGroupInput, inviteKey);
    }

    handleSubmit(event) {
        const {router, callbackFunction, params} = this.props;
        const {errors, continuousRegistration, inviteData} = this.state;
        event.preventDefault();
        window.log('Contion', continuousRegistration);
        if (!continuousRegistration
            && window.location.pathname.includes('jelado-valaszto/eletmod')
            && !event.target.elements.email.value.includes('@budapestbank.hu')
            && !event.target.elements.email.value.includes('@lillafitt.hu')) {
            this.toogleConfrimDialog(true);
            return;
        }
        this.setState({
            loading: true
        });
        if (!validateConfirmFormInputs(event.target.elements)) {
            errors.push('Nem megegyező adatok!');
            this.setState({
                errors: errors,
                loading: false
            });
            return;
        } else {
            this.setState({
                errors: []
            });
        }

        const that = this;
        const registerUserMutation = this.getRegisterUserMutation(event);
        window.log('LOG REGISTER', registerUserMutation);
        if (registerUserMutation) {
            removeObjectFromLocalStore('email-verified');
            makeRequest(registerUserMutation, function (response) {
                if (callbackFunction !== undefined) {
                    callbackFunction();
                }
                const user = response.data.User_register.user;
                if (params && params.inviteData && (inviteData !== null && user.userAssigns[0].mainEmail.email === inviteData.userEmail)) {
                    saveOauthTokenObject(response.data.User_register.initialOAuth2Token);
                    saveUserObject(user);
                    that.handleUserLoginTicket();
                } else {
                    that.setState({
                        loading: false
                    });
                    router.push('/regisztracio/elkuldve');
                }
            }, function (error) {
                that.onQueryError(error);
            });
        } else {
            errors.push('Hibás adatok!');
            this.setState({
                errors: errors,
                loading: false
            });
        }
    }

    continueRegistration() {
        this.setState({
            continuousRegistration: true
        });
        this.toogleConfrimDialog(false);

        let inputs = document.getElementsByTagName('input');

        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i].type.toLowerCase() === 'submit' && inputs[i].value.toLowerCase() === 'regisztráció') {
                inputs[i].click();
            }
        }
    }

    toogleConfrimDialog(visible) {
        this.setState({
            confirmDialogIsOpen: visible
        });
    }

    handleUserLoginTicket() {
        const that = this;
        makeRequest(getLoginTicketQuery, function (response) {
            window.log('LOGIN TICKET', response);
            saveObjectToLocalStore('login_ticket', response.data.LoginTicket_generateForCurrentUser.ticketKey);
            saveObjectToLocalStore('firstLogin', true);
            that.checkDependentPartner();
        }, function (error) {
            that.onQueryError(error);
        });
    }

    checkDependentPartner() {
        const {router, params, callbackFunction} = this.props;
        const that = this;
        makeRequest(getUserQuery, function (response) {
            const userObject = response.data.User;
            window.log('USER OBJECT SAVE', userObject);
            saveUserObject(userObject);
            if (!window.location.pathname.includes('jelado-valaszto/eletmod') && !window.location.pathname.includes('egeszsegteszt')) {
                if (userObject.dependentPartner) {
                    if (callbackFunction !== undefined) {
                        callbackFunction();
                    }
                    router.push('/dashboard');
                } else {
                    if (params !== undefined) {
                        event = params.event;
                        if (event === 'webshop') {
                            router.push('/regisztracio/elkuldve');
                        } else {
                            router.push('/dashboard');
                        }
                    } else {
                        if (callbackFunction !== undefined) {
                            callbackFunction();
                        }
                        router.push('/dashboard');
                    }
                }
            } else {
                callbackFunction();
            }
            that.setState({
                loading: false
            });
        }, function (error) {
            window.error('REG', error);
            that.setState({
                errors: ['Valami hiba történt! Kérlek próbálkozz később!'],
                loading: false
            });
        });
    }

    onQueryError(errors) {
        window.error('REG', errors);
        let errorsArray = [];
        if (errors) {
            errorsArray = errors[0].exception.validationResult.errors.map(function (error, index) {
                return error;
            });
        } else {
            errorsArray.push('Valami hiba történt! Kérlek próbálkozz később!');
        }
        this.setState({
            errors: errorsArray,
            loading: false
        });
    }

    getRegistrationSendedContent() {
        return (
            <PageIntro
                title='Regisztráció'
                text='Sikeresen regisztráltál az Online Trainer oldalára. Regisztrációdat a megadott email címre küldött levélben található linkre kattintva aktiválhatod.' />
        );
    }

    getRegistrationPageContent() {
        const {loading, errors, inviteData} = this.state;
        let inviteComponent = '';
        if (inviteData !== null) {
            const {profilePicture, defaultImage} = getProfilePicture(inviteData.trainer);
            inviteComponent = (
                <div className='user-stats col-sm-12' >
                    <div className='col-sm-5' >
                        <ProfileImagePanel defaultImage={defaultImage}
                                           className='profile-figure-img'
                                           profilePicture={profilePicture}
                                           id='kepEdzo'
                        />
                    </div>
                    <div className='col-sm-7' >
                        <h2>{inviteData.trainer.userAssigns[0].lastName + ' ' + inviteData.trainer.userAssigns[0].firstName}</h2>
                        <p>Edző</p>
                    </div>
                </div>
            );
        }
        return (
            <div className='page-container container' >
                <div className='page-wrapper' >
                    <div className='container register-page' >
                        {inviteComponent}
                        <div className='col-sm-12 text-center' >
                            <p>
                                A regisztrációval elfogadod a
                                <a target='_blank' href='/aszf'
                                   style={{color: '#577ebe'}} > Felhasználási feltételeket.
                                </a>
                            </p>
                        </div>
                        <div className='col-sm-12 text-center' >
                            {this.getInputButtonFb()}
                        </div>
                        <div className='col-sm-12 text-center' >
                            <p>A regisztrációhoz kérlek töltsd ki a szükséges adatokat!</p>
                        </div>
                        <div className='col-sm-12 text-center' >
                        <Message type='danger'
                                 message={errors} />
                        </div>

                        {this.getRegistrationFormContent()}
                        <div className='col-sm-12' >
                            <p>
                                A regisztrációval elfogadod a
                                <a target='_blank' href='/aszf'
                                   style={{color: '#577ebe'}} > Felhasználási feltételeket.
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    componentWillMount() {
        const {params} = this.props;
        const that = this;
        if (params && params.inviteData) {

            makeRequest(getUserFromInviteKeyQuery(params.inviteData), function (response) {
                const inviteData = response.data.UserInviteWrapper_getUserFromInviteKey;
                saveObjectToLocalStore('email-verified', inviteData.userEmail);
                that.setState({
                    inviteData: inviteData
                });
            }, function (error) {
                window.error('INVITE DATA EXCEPTION', error);
            });
        }

    }

    getRegistrationFormContent() {
        const {loading, errors} = this.state;
        return (
            <div>
                <FrontendPage errors={errors}
                              handleSubmit={this.handleSubmit}
                              queryName='register'
                              loading={loading} />
            </div>
        );
    }

    getRegistrationEmailConfirmedContent() {
        return (
            <PageIntro
                title='Regisztráció'
                text='Sikeresen regisztráltál az Online Trainer oldalára. Kérlek aktiváld a fiókodat és jelentkezz be, hogy a vásárolhass webshopunkban.' />
        );
    }

    getRegistrationErrorContent() {
        return (
            <PageIntro
                title='Regisztráció' >
                <p>Hiba a regisztráció során. Kérlek a hiba elhárításához lépj kapcsolatba{' '}
                    <Link style={{color: 'white'}}
                          to={'kapcsolat'} >
                        <b>
                            <u>velünk</u>
                        </b>
                    </Link></p>
            </PageIntro>
        );
    }

    getContent() {
        const {params} = this.props;
        let event;
        if (params !== undefined) {
            event = params.event;
        } else {
            return this.getRegistrationPageContent();
        }
        switch (event) {
            case 'elkuldve':
                return this.getRegistrationSendedContent();
                break;
            case 'sikeres':
                return this.getRegistrationEmailConfirmedContent();
                break;
            case 'hiba':
                return this.getRegistrationErrorContent();
                break;
            default:
                return this.getRegistrationPageContent();
        }
    }

    getInputButtonFb() {
        if (window.location.pathname.includes('regisztracio/elkuldve')) {
            return null;
        } else {
            return (
                <FacebookRegisterButton onRegisterSuccess={this.handleFacebookRegSuccess}
                                        onFaceBookLogin={this.handleFacebookLogin}
                                        onRegisterError={this.onQueryError}/>
            );
        }
    }

    render() {
        const {confirmDialogIsOpen, errors} = this.state;
        const that = this;
        return (
            <div className='bb-email-register-dialog' >
                {/*<ModalDialog*/}
                    {/*open={confirmDialogIsOpen}*/}
                    {/*modalClose={true}*/}
                    {/*title='Figyelmeztetés'*/}
                    {/*enableClose={false} >*/}
                    {/*<div>*/}
						{/*<span>*/}
							{/*A kedvezményes árakat csak Budapest Bankos email címmel tudod igénybe venni.*/}
						{/*</span>*/}
                    {/*</div>*/}
                    {/*<div className='text-center' >*/}
                        {/*<button className='modify'*/}
                                {/*onClick={function () {*/}
                                    {/*that.toogleConfrimDialog(false)*/}
                                {/*}} >Módosítom*/}
                        {/*</button>*/}
                        {/*<button className='understood'*/}
                                {/*onClick={function () {*/}
                                    {/*that.continueRegistration()*/}
                                {/*}} >Megértettem*/}
                        {/*</button>*/}
                    {/*</div>*/}
                {/*</ModalDialog>*/}
                { window.location.pathname.includes('jelado-valaszto/eletmod') ? 'A kedvezményes vásárlás folytatásához, kérjük add meg adataidat.' : '' }
                {this.getContent()}
            </div>
        );
    }
}

Register.propTypes = {};
Register.defaultProps = {};

export default withRouter(Register);
