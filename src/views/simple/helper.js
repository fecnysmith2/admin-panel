var Crypto = require('crypto');

var Helper = {
 /**
  * Get the value of a querystring
  *
  * @param  {String} field The field to get the value of
  * @param  {String} url   The URL to get the value from (optional)
  * @return {String}       The field value
  */
  getParam: function (field, url) {
    var href   = url ? url : window.location.href;
    var reg    = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    window.log('PARAM',string);
    return string ? string[1] : null;
  },

  getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  },

  /**
   * Create a decimal hash from an email address
   *
   * @param String
   * @return Int
   */
  makeEmailHash: function (email) {
    if (typeof(email) === "undefined") {
      return "";
    }
    var hexHash = Crypto
      .createHmac('md5', new Buffer(email, 'utf-8'))
      .digest('hex');
    var decHash = parseInt(hexHash, 16);
    return decHash;
  },
  /**
   * Strip a hash to the first six characters
   *
   * @param String
   * @return String
   */
  stripHash: function (hash) {
    if (typeof(hash) === "undefined") {
      return "";
    }
    // We need to strip the period because of floating point number format
    return hash.toString().replace('.', '').substr(0, 6);
  }
};

module.exports = Helper;
