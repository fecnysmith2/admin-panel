var PayConfig = require('./pay-config');
var Pay = require('./pay');
var fs = require('fs');

var Recurring = {
  /**
   * Append object to JSON file.
   *
   * @param String
   * @param Object
   */
  appendObject: function(file, val) {
    try {
      var configFile = fs.readFileSync(file);
      var config     = JSON.parse(configFile);
      config.tokens.push(val);
      var configJSON = JSON.stringify(config);
      fs.writeFileSync(file, configJSON);
    } catch (err) {
      fs.writeFileSync(file, JSON.stringify({ tokens: [ val ]}), { flag: 'wx' });
    }
  },

  /**
   * Delete specific object in JSON file.
   *
   * @param String
   * @param Object
   */
  deleteObject: function(file, val) {
    try {
      var configFile = fs.readFileSync(file);
      var config     = JSON.parse(configFile);

      for (var i; i < config.tokens.length; i++) {
        if (config.tokens[i] === val) {
          delete config.tokens[i];
        }
      }

      var configJSON = JSON.stringify(config);
      fs.writeFileSync(file, configJSON);
    } catch (err) {
    }
  },

  /**
   * Get all tokens from JSON file.
   *
   * @param String
   * @return Object
   */
  getTokens: function(file) {
    try {
      var configFile = fs.readFileSync(file);
      return JSON.parse(configFile);
    } catch (err) {
      window.log(err);
      return null;
    }
  },

  /**
   * Return new recurring sale object data using token.
   *
   * @param String
   * @return Object
   */
  getTokenNewSale: function(event) {
    var elements = event.target.elements;
    var mainRequest = {
      'AMOUNT':           elements.AMOUNT.value,
      'BILL_ADDRESS':     elements.BILL_ADDRESS.value,
      'BILL_CITY':        elements.BILL_CITY.value,
      'BILL_EMAIL':       elements.BILL_EMAIL.value,
      'BILL_FNAME':       elements.BILL_FNAME.value,
      'BILL_LNAME':       elements.BILL_LNAME.value,
      'BILL_PHONE':       elements.BILL_PHONE.value,
      'CANCEL_REASON':    elements.CANCEL_REASON.value,
      'CURRENCY':         elements.CURRENCY.value,
      'DELIVERY_ADDRESS': elements.DELIVERY_ADDRESS.value,
      'DELIVERY_CITY':    elements.DELIVERY_CITY.value,
      'DELIVERY_FNAME':   elements.DELIVERY_FNAME.value,
      'DELIVERY_LNAME':   elements.DELIVERY_LNAME.value,
      'DELIVERY_PHONE':   elements.DELIVERY_PHONE.value,
      'EXTERNAL_REF':     elements.EXTERNAL_REF.value,
      'MERCHANT':         elements.MERCHANT.value,
      'METHOD':           elements.METHOD.value,
      'REF_NO':           elements.REF_NO.value,
      'TIMESTAMP':        elements.TIMESTAMP.value
    };
    if (elements.METHOD.value != 'TOKEN_CANCEL') {
      delete mainRequest.CANCEL_REASON;
    }
    var sourceString = Pay.makeSourceStringForRecurring(mainRequest);
    var secretKey    = PayConfig.getConfig(1).secretKey;
    var hashToAdd    = {
      'SIGN': Pay.makeHash(sourceString, secretKey)
    };
    var o = {};
    // return united object
    return Object.assign(o, mainRequest, hashToAdd);
  }
};

module.exports = Recurring;
