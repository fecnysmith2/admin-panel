import React, {
    Component,
} from 'react';
import bindAll from 'lodash.bindall';
import moment from 'moment';
import PayConfig from './pay-config';
import Pay from './pay';
import Helper from './helper';
import ModalDialog from '../../components/modal';
import {
    getCreditCardProcessMutation,
    getCreditCardProcessEventMutation,
    getProductQuery,
    getUserSubscriptionMutation
} from '../../utils/queries/queries';
import makeRequest from '../../utils/request-queue-handler';
import {simple} from '../../../config';
import Recurring from './recurring';
import {sendTokenPayment} from '../../utils/helpers';
import {getUserAssignId} from '../../utils/user-util';

class SimpleIndex extends Component {
    constructor(props) {
        super(props);
        bindAll(this,
            'buildProductList', 'buildRow', 'buildProductsForm',
            'buildInput', 'showAgreePayment', 'showInformation',
            'modalClose', 'getAgreePayment', 'getInformation',
            'toggleAgreePayment', 'toggleInformationChecked', 'agreePaymentDataForward',
            'getOrderObjectFromUrl', 'getOrderObjectFromObject', 'onCreditCardProcessEventCreated',
            'productsReceived', 'onUserSubscriptionCreated', 'onCreditCardProcessCreated',
            'onTokenReceived', 'getMainForm', 'makeTokensArray',
            'getRecurringForm', 'getTokenSubmitText', 'submitTokenOperation',
            'onSuccess', 'onError', 'getFormForTokenOperation',
        );

        this.state = {
            tokens: [],
            checkMark: '',
            disablePay: true,
            isAgreeChecked: false,
            isInformationChecked: false,
            modalOpen: false,
            showAgreePayment: false,
            sourceString: '',
            secretKey: '3_[21~9Z5~3%I&O2#J63',
            orderHash: '',
            orderRef: '',
            tokenMessage: '',
            isRecurring: false,
            creditCardProcessObject: {},
            creditCardProcessEventObject: {},
            subscriptionObject: {},
            orderObject: {},
            order: '',
            fullPrice: 0,
            priceWithoutTax: 0,
        }
    }

    buildProductList(dataObject) {
        if (Array.isArray(dataObject.orderPname)) {
            let rows = [];
            for (let i = 0; i < dataObject.orderPname.length; i++) {
                rows.push(
                    this.buildRow(
                        dataObject.orderQty[i],
                        dataObject.orderPname[i],
                        dataObject.orderPrice[i],
                        dataObject.orderPcode[i]
                    )
                );
            }
            return rows;
        } else {
            return this.buildRow(
                dataObject.orderQty,
                dataObject.orderPname,
                dataObject.orderPrice,
                dataObject.orderPcode
            );
        }
    }

    buildRow(rowQty, rowPname, rowPrice, orderPcode) {
        const price = rowPrice * rowQty;
        return (
            <tr key={orderPcode} >
                <td>{rowQty}</td>
                <td>{rowPname}</td>
                <td>
                    <div className='text-right' >{price.toFixed(0)} Ft</div>
                </td>
            </tr>
        );
    }

    /**
     * Make form handling multiple products.
     *
     * @param Object
     * @return JSX
     */
    buildProductsForm(products) {
        if (Array.isArray(products.orderPname)) {
            var pnames = [];
            var pcode = [];
            var pinfo = [];
            var price = [];
            var qty = [];
            var vat = [];
            for (let i = 0; i < products.orderPname.length; i++) {
                pnames.push(this.buildInput('ORDER_PNAME', products.orderPname[i]));
            }
            for (let i = 0; i < products.orderPcode.length; i++) {
                pnames.push(this.buildInput('ORDER_PCODE', products.orderPcode[i]));
            }
            for (let i = 0; i < products.orderPinfo.length; i++) {
                pnames.push(this.buildInput('ORDER_PINFO', products.orderPinfo[i]));
            }
            for (let i = 0; i < products.orderPrice.length; i++) {
                pnames.push(this.buildInput('ORDER_PRICE', products.orderPrice[i]));
            }
            for (let i = 0; i < products.orderQty.length; i++) {
                pnames.push(this.buildInput('ORDER_QTY', products.orderQty[i]));
            }
            for (let i = 0; i < products.orderVat.length; i++) {
                pnames.push(this.buildInput('ORDER_VAT', products.orderVat[i]));
            }
            return [pnames, pcode, pinfo, price, qty, vat];
        } else {
            return (
                <div>
                    <input type='hidden'
                           name='ORDER_PNAME[]'
                           id='ORDER_PNAME'
                           value={products.orderPname} />
                    <input type='hidden'
                           name='ORDER_PCODE[]'
                           id='ORDER_PCODE'
                           value={products.orderPcode} />
                    <input type='hidden'
                           name='ORDER_PINFO[]'
                           id='ORDER_PINFO'
                           value={products.orderPinfo} />
                    <input type='hidden'
                           name='ORDER_PRICE[]'
                           id='ORDER_PRICE'
                           value={products.orderPrice} />
                    <input type='hidden'
                           name='ORDER_QTY[]'
                           id='ORDER_QTY'
                           value={products.orderQty} />
                    <input type='hidden'
                           name='ORDER_VAT[]'
                           id='ORDER_VAT'
                           value={products.orderVat} />
                </div>
            );
        }
    }

    buildInput(inputName, inputValue) {
        return (
            <input type='hidden'
                   name={inputName + '[]'}
                   id={inputName}
                   value={inputValue} />
        );
    }

    showAgreePayment(e) {
        this.setState({
            modalOpen: true,
            showAgreePayment: true
        });
    }

    showInformation(e) {
        this.setState({
            modalOpen: true,
            showAgreePayment: false
        });
    }

    modalClose() {
        this.setState({
            modalOpen: false
        });
    }

    getAgreePayment() {
        return (
            <div className='text-left' >
                <h3 className='h3' >Adattovábítási nyilatkozat</h3>
                <p>Elfogadom, hogy a(z)
                    <strong>Mindset Nonprofit Kft.</strong>
                   (Magyarország, 2049 Diósd, Tündér utca 6.) által a(z)
                    <strong>www.onlinetrainer.hu</strong>
                   felhasználói adatbázisában tárolt alábbi
                   személyes adataim átadásra kerüljenek az
                    <strong>OTP Mobil Kft.</strong>
                   (1093 Budapest, Közraktár
                   u. 30-32.), mint adatkezelő részére. A továbbított adatok köre: vezetéknév, keresztnév, ország,
                   telefonszám, e-mail cím.
                </p>
                <p>Az adattovábbítás célja: a felhasználók részére történő ügyfélszolgálati segítségnyújtás, a
                   tranzakciók visszaigazolása és a felhasználók védelme érdekében végzett fraud-monitoring.
                </p>
            </div>
        );
    }

    getInformation() {
        return (
            <div className='text-left' >
                <h3 className='h3' >Tájékoztató az ismétlődő bankkártyás fizetésről</h3>
                <p>Az ismétlődő bankkártyás fizetés (továbbiakban: „Ismétlődő fizetés”) egy, a SimplePay által
                   biztosított bankkártya elfogadáshoz tartozó funkció, mely azt jelenti, hogy a Vásárló által a
                   regisztrációs tranzakció során megadott bankkártyaadatokkal a jövőben újabb fizetéseket lehet
                   kezdeményezni a bankkártyaadatok újbóli megadása nélkül.
                </p>
                <p>Az Ismétlődő fizetés igénybevételéhez jelen nyilatkozat elfogadásával Ön hozzájárul, hogy a sikeres
                   regisztrációs tranzakciót követően jelen webshopban (www.onlinetrainer.hu) kezdeményezett későbbi
                   fizetések a bankkártyaadatok újbóli megadása és az Ön tranzakciónként hozzájárulása nélkül a
                   Kereskedő által
                </p>
                <p>kezdeményezve történjenek.</p>
                <p>Figyelem(!): a bankkártyaadatok kezelése a kártyatársasági szabályoknak megfelelően történik. A
                   bankkártyaadatokhoz sem a Kereskedő, sem a SimplePay nem fér hozzá.
                </p>
                <p>A Kereskedő által tévesen vagy jogtalanul kezdeményezett ismétlődő fizetéses tranzakciókért
                   közvetlenül a Kereskedő felel, Kereskedő fizetési szolgáltatójával (SimplePay) szemben bármilyen
                   igényérvényesítés kizárt.
                </p>
                <p>Jelen tájékoztatót átolvastam, annak tartalmát tudomásul veszem és elfogadom.</p>
            </div>
        );
    }

    toggleAgreePayment(e) {
        this.setState({isAgreeChecked: !this.state.isAgreeChecked});
        window.log('agree', this.state.isInformationChecked, this.state.isAgreeChecked);
        if (!this.state.isAgreeChecked && (this.state.isInformationChecked || !this.state.isRecurring)) {
            this.setState({disablePay: false});
            this.setState({checkMark: ' ✔'});
        } else {
            this.setState({disablePay: true});
            this.setState({checkMark: ''});
        }
    }

    toggleInformationChecked(e) {
        this.setState({isInformationChecked: !this.state.isInformationChecked});
        window.log('info', this.state.isInformationChecked, this.state.isAgreeChecked);
        if (!this.state.isInformationChecked && this.state.isAgreeChecked && this.state.isRecurring) {
            this.setState({disablePay: false});
            this.setState({checkMark: ' ✔'});
        } else {
            this.setState({disablePay: true});
            this.setState({checkMark: ''});
        }
    }

    agreePaymentDataForward() {
        const {disablePay, isRecurring, checkMark, isInformationChecked, isAgreeChecked, modalOpen, showAgreePayment} = this.state;
        const submitButton = disablePay ? <a className='ot-btn btn-yellow btn-simple'
                                             onClick={function () {
                                                 const recurringMessage = isRecurring ? ' és az ismétlődő bankártyás fizetésról szóló tájékoztatót' : '';
                                                 alert('A folytatáshoz előbb el kell fogadnod az Adattovábítási nyilatkozatot' + recurringMessage + '!');
                                             }} >
            Fizetés
        </a>
            : <button className='ot-btn btn-yellow btn-simple'
                      type='submit' >
                Fizetés{checkMark}
            </button>;
        const agreeRecurring = (
            <div>
                <input type='checkbox'
                       name='agreePaymentDataForward'
                       checked={isInformationChecked}
                       onChange={this.toggleInformationChecked} />
                Elfogadom a
                <span
                    className='cursor-pointer' />
                <span className='cursor-pointer'
                      onClick={this.showInformation} ><strong><u>Tájékoztatót az ismétlődő bankkártyás fizetésről</u>.</strong></span>
            </div>);
        return (
            <div>
                <input type='checkbox'
                       name='agreePaymentDataForward'
                       checked={isAgreeChecked}
                       onChange={this.toggleAgreePayment}
                       id='' />
                Elfogadom az
                <span className='cursor-pointer' />
                <span
                    className='cursor-pointer'
                    onClick={this.showAgreePayment} ><strong><u>Adattovábítási nyilatkozatot</u>.</strong></span>
                <br/>
                {isRecurring ? agreeRecurring : ''}
                <ModalDialog open={modalOpen}
                             modalClose={true}
                             close={this.modalClose}
                             title='Adattovábbítási Nyilatkozat'
                             enableClose={true} >
                    {showAgreePayment ? this.getAgreePayment() : this.getInformation()}
                </ModalDialog>
                <div>
                    {submitButton}
                </div>
            </div>
        );
    }

    getOrderObjectFromUrl() {
        const order = {
            ORDER_REF: Helper.getParameterByName('ORDER_REF'),
            ORDER_DATE: Helper.getParameterByName('ORDER_DATE'),
            PRICES_CURRENCY: Helper.getParameterByName('PRICES_CURRENCY'),
            ORDER_SHIPPING: Helper.getParameterByName('ORDER_SHIPPING') ? Helper.getParameterByName('ORDER_SHIPPING') : 0,
            DISCOUNT: Helper.getParameterByName('DISCOUNT'),
            BILL_EMAIL: Helper.getParameterByName('BILL_EMAIL'),
            BILL_FNAME: Helper.getParameterByName('BILL_FNAME'),
            BILL_LNAME: Helper.getParameterByName('BILL_LNAME')
        };
        let hasMore = true;
        let productCount = 1;
        while (hasMore) {
            let productName = Helper.getParameterByName('ORDER_PNAME%5B' + productCount + '%5D');
            if (productName != null) {
                productCount++;
            } else {
                hasMore = false;
            }
        }
        window.log('PRODUCT COUNT', productCount);
        let products = [];
        let price = 0;
        let fullPrice = 0;
        let hasRecurring = false;
        for (let i = 1; i < productCount; i++) {
            let product = {
                ORDER_PNAME: Helper.getParameterByName('ORDER_PNAME%5B' + i + '%5D'),
                ORDER_PCODE: Helper.getParameterByName('ORDER_PCODE%5B' + i + '%5D'),
                ORDER_PINFO: Helper.getParameterByName('ORDER_PINFO%5B' + i + '%5D') != '' ? Helper.getParameterByName('ORDER_PINFO%5B' + i + '%5D') : 'info',
                ORDER_PRICE: Helper.getParameterByName('ORDER_PRICE%5B' + i + '%5D'),
                ORDER_PAYMENT_TYPE: Helper.getParameterByName('ORDER_PAYMENT_TYPE%5B' + i + '%5D'),
                ORDER_QTY: Helper.getParameterByName('ORDER_QTY%5B' + i + '%5D'),
                ORDER_VAT: Helper.getParameterByName('ORDER_VAT%5B' + i + '%5D') ? parseInt(Helper.getParameterByName('ORDER_VAT%5B' + i + '%5D')) : 0
            };
            fullPrice += product.ORDER_QTY * product.ORDER_PRICE * (product.ORDER_VAT / 100 + 1);
            price += (product.ORDER_PRICE * product.ORDER_QTY);
            if (product.ORDER_PAYMENT_TYPE == 'R') {
                hasRecurring = true;
            }
            products.push(product);
        }
        window.log('PRODUCTS', products);
        window.log('PRODUCTS HAS RECURRING', hasRecurring);
        fullPrice += parseInt(order.ORDER_SHIPPING);
        price += parseInt(order.ORDER_SHIPPING);
        this.setState({
            fullPrice: parseInt(fullPrice),
            priceWithoutTax: parseInt(price),
            isRecurring: hasRecurring
        });
        order.products = products;
        return order;
    }

    getOrderObjectFromObject(order) {
        let hasMore = true;
        let price = 0;
        let fullPrice = 0;
        let hasRecurring = false;
        for (let i = 0; i < order.products.length; i++) {
            let product = order.products[i];
            fullPrice += product.ORDER_QTY * product.ORDER_PRICE * (product.ORDER_VAT / 100 + 1);
            price += (product.ORDER_PRICE * product.ORDER_QTY);
            if (product.ORDER_PAYMENT_TYPE == 'R') {
                hasRecurring = true;
            }
        }
        window.log('PRODUCTS', order.products);
        window.log('PRODUCTS HAS RECURRING', hasRecurring);
        fullPrice += parseInt(order.ORDER_SHIPPING);
        price += parseInt(order.ORDER_SHIPPING);

        window.log('PRODUCTS PRICE', fullPrice);
        window.log('PRODUCTS PRICE 2', price);
        this.setState({
            fullPrice: parseInt(fullPrice),
            priceWithoutTax: parseInt(price),
            isRecurring: hasRecurring
        });
        return order;
    }

    componentWillMount() {
        const {orderFromUrl, order} = this.props;
        let orderObject;
        if (orderFromUrl) {
            orderObject = this.getOrderObjectFromUrl();
        } else {
            orderObject = this.getOrderObjectFromObject(order);
        }
        this.setState({
            orderObject: orderObject
        });
        makeRequest(getProductQuery, this.productsReceived, this.onError);
    }

    onCreditCardProcessEventCreated(response) {
        var orderObject = this.state.orderObject;
        var orderRef = response.data.CreditCardProcessEvent_add.recnum;
        orderObject.orderRef = orderRef;
        var order = PayConfig.getConfig(orderObject);
        var sourceString = Pay.makeSourceString(order);
        var orderHash = Pay.makeHash(sourceString, this.state.secretKey);
        window.log('ORDER HASH', orderHash);
        this.setState({
            order: order,
            sourceString: sourceString,
            orderRef: orderRef,
            orderHash: orderHash
        });
        window.log('PAYMENT AVAILABLE');
    }

    productsReceived(response) {
        var orderObject = this.state.orderObject;
        var products = response.data.Product_getAll;
        var selectedProduct;
        window.log('PRODUCTS', products);
        products.forEach(function (product, index) {
            orderObject.products.forEach(function (orderProduct, index2) {
                if (product.productCode == orderProduct.ORDER_PCODE) {
                    selectedProduct = product;
                }
            });
        });
        window.log('SELECTED PRODUCT', selectedProduct);
        if (selectedProduct) {
            let userSubscription = {
                product: selectedProduct,
                subCycle: 30
            };
            makeRequest(getUserSubscriptionMutation(userSubscription), this.onUserSubscriptionCreated, this.onError);
        } else {
            const userAssignId = getUserAssignId();
            const fullPrice = this.state.fullPrice;
            let creditCardProcess = {
                currency: orderObject.PRICES_CURRENCY,
                userAssign: {
                    recnum: userAssignId
                },
                subscriptionId: orderObject.ORDER_REF,
                firstAmount: fullPrice,
                recurringAmount: fullPrice
            };
            makeRequest(getCreditCardProcessMutation(creditCardProcess), this.onCreditCardProcessCreated, this.onError);
        }
    }

    onUserSubscriptionCreated(response) {
        var orderObject = this.state.orderObject;
        if (response.data.UserSubscription_add != null) {
            this.setState({
                subscriptionObject: response.data.UserSubscription_add
            });
            const userAssignId = getUserAssignId();
            const fullPrice = this.state.fullPrice;
            var creditCardProcess = {
                currency: orderObject.PRICES_CURRENCY,
                userAssign: {
                    recnum: userAssignId
                },
                firstAmount: fullPrice,
                recurringAmount: 0,
                simpleUserid: PayConfig.merchant,
                salesUserAssign: {
                    recnum: '106463'
                },
                subscriptionId: orderObject.ORDER_REF
            };
            const subscription = this.state.subscriptionObject.recnum ? this.state.subscriptionObject : null;
            if (subscription != null) {
                creditCardProcess.subId = subscription;
            }
            makeRequest(getCreditCardProcessMutation(creditCardProcess), this.onCreditCardProcessCreated, this.onError);
        }
    }

    onCreditCardProcessCreated(response) {
        var creditCardProcessEvent = {
            process: response.data.CreditCardProcess_add,
            tokenType: 'SIMPLE_NEW'
        };
        makeRequest(getCreditCardProcessEventMutation(creditCardProcessEvent), this.onCreditCardProcessEventCreated, this.onError);
    }

    onTokenReceived(response) {
        window.log('sendQuery response', response);
        var tokens = [];
        response.data.CreditCardProcess_get.forEach(function (item, index) {
            if (item.token != null) {
                tokens.push(item.token);
            }
        });
        this.setState({
            tokens: tokens
        });
    }

    /**
     * Create form for basic payment.
     *
     * @return JSX
     */
    getMainForm() {
        var inputsForToken = (
            <div>
                <input type='hidden'
                       name='LU_ENABLE_TOKEN'
                       id='LU_ENABLE_TOKEN'
                       value='1' />
                <input type='hidden'
                       name='LU_TOKEN_TYPE'
                       id='LU_TOKEN_TYPE'
                       value='PAY_BY_CLICK' />
            </div>
        );
        return (
            <form action={simple}
                  method='POST'
                  id='SimpleForm'
                  acceptCharset='UTF-8' >
                {this.state.isRecurring ? inputsForToken : ''}
                <input type='hidden'
                       name='MERCHANT'
                       id='MERCHANT'
                       value={this.state.order.merchant} />
                <input type='hidden'
                       name='ORDER_REF'
                       id='ORDER_REF'
                       value={this.state.orderRef} />
                <input type='hidden'
                       name='ORDER_DATE'
                       id='ORDER_DATE'
                       value={this.state.order.orderDate} />
                {this.buildProductsForm(this.state.order)}
                <input type='hidden'
                       name='PRICES_CURRENCY'
                       id='PRICES_CURRENCY'
                       value={this.state.order.currency} />
                <input type='hidden'
                       name='ORDER_SHIPPING'
                       id='ORDER_SHIPPING'
                       value={this.state.order.orderShipping} />
                <input type='hidden'
                       name='DISCOUNT'
                       id='DISCOUNT'
                       value={this.state.order.discount} />
                <input type='hidden'
                       name='PAY_METHOD'
                       id='PAY_METHOD'
                       value={this.state.order.payMethod} />
                <input type='hidden'
                       name='LANGUAGE'
                       id='LANGUAGE'
                       value={this.state.order.language} />
                <input type='hidden'
                       name='ORDER_TIMEOUT'
                       id='ORDER_TIMEOUT'
                       value={this.state.order.orderTimeout} />
                <input type='hidden'
                       name='TIMEOUT_URL'
                       id='TIMEOUT_URL'
                       value={this.state.order.timeOutUrl} />
                <input type='hidden'
                       name='BACK_REF'
                       id='BACK_REF'
                       value={this.state.order.backRefUrl} />
                <input type='hidden'
                       name='BILL_EMAIL'
                       id='BILL_EMAIL'
                       value={this.state.order.billEmail} />
                <input type='hidden'
                       name='BILL_FNAME'
                       id='BILL_FNAME'
                       value={this.state.order.billFname} />
                <input type='hidden'
                       name='BILL_LNAME'
                       id='BILL_LNAME'
                       value={this.state.order.billLname} />
                <input type='hidden'
                       name='BILL_ADDRESS'
                       id='BILL_ADDRESS'
                       value={this.state.order.billAddress} />
                <input type='hidden'
                       name='BILL_CITY'
                       id='BILL_CITY'
                       value={this.state.order.billCity} />
                <input type='hidden'
                       name='ORDER_HASH'
                       id='ORDER_HASH'
                       value={this.state.orderHash} />
                <div className='text-right img-simple-container' >
                    <img src='/img/simple/bankcards_simplepay.png'
                         alt='Simple Pay fizetés'
                         className='img-responsive' />
                    <p>{' '}</p>
                    {this.agreePaymentDataForward()}
                </div>
            </form>
        );
    }

    /**
     * Create array from tokens string.
     *
     * @param String
     * @return Array
     */
    makeTokensArray(tokenObject) {
        var tokens = [];
        var obj = JSON.parse(tokenObject);
        for (let i = 0; i < obj.tokens.length; i++) {
            tokens.push(obj.tokens[i]);
        }
        return tokens;
    }

    /**
     * Create form for recurring payment.
     * types can be: TOKEN_NEWSALE, TOKEN_CANCEL, TOKEN_GETINFO
     *
     * @param String
     * @return JSX
     */
    getRecurringForm(type) {
        var forms = [];
        var that = this;
        this.state.tokens.map(function (token) {
            forms.push(that.getFormForTokenOperation(token, type));
        });
        return forms;
    }

    /**
     * Get text for token submit button.
     *
     * @param String
     * @return String
     */
    getTokenSubmitText(type) {
        var text = '';
        switch (type) {
            case 'TOKEN_NEWSALE' :
                text = 'Fizetés ✔';
                break;
            case 'TOKEN_CANCEL'  :
                text = 'Törlés (x)';
                break;
            case 'TOKEN_GETINFO' :
                text = 'Info (i)';
                break;
        }
        return text;
    }

    submitTokenOperation(event, type) {
        window.log('submit type', type);
        window.log('submit event', event.target.elements);
        sendTokenPayment(Recurring.getTokenNewSale(event), this.onSuccess, this.onError);
    }

    onSuccess(response) {
        window.log('TOKEN response', response);
        this.setState({
            tokenMessage: response
        });
    }

    onError(error) {
        window.error('simplepay error', error);
    }

    /**
     * Helper function for making forms for TOKEN operations.
     *
     * @param String
     * @param String
     * @return JSX
     */
    getFormForTokenOperation(token, type) {
        var amount = Math.floor(Math.random() * 10 + 5);
        var random = Math.floor(Math.random() * 10000000 + 1000000);
        var that = this;
        var submit = function (event) {
            event.preventDefault();
            that.submitTokenOperation(event, type);
        };
        return (
            <form onSubmit={submit}
                  id='SimpleForm'
                  acceptCharset='UTF-8' >
                <input type='hidden'
                       name='AMOUNT'
                       id='AMOUNT'
                       value={amount} />
                <input type='hidden'
                       name='BILL_ADDRESS'
                       id='BILL_ADDRESS'
                       value={this.state.order.billAddress} />
                <input type='hidden'
                       name='BILL_CITY'
                       id='BILL_CITY'
                       value={this.state.order.billCity} />
                <input type='hidden'
                       name='BILL_EMAIL'
                       id='BILL_EMAIL'
                       value={this.state.order.billEmail} />
                <input type='hidden'
                       name='BILL_FNAME'
                       id='BILL_FNAME'
                       value={this.state.order.billFname} />
                <input type='hidden'
                       name='BILL_LNAME'
                       id='BILL_LNAME'
                       value={this.state.order.billLname} />
                <input type='hidden'
                       name='BILL_PHONE'
                       id='BILL_PHONE'
                       value={this.state.order.billPhone} />
                <input type={type == 'TOKEN_CANCEL' ? 'text' : 'hidden'}
                       name='CANCEL_REASON'
                       id='CANCEL_REASON'
                       placeholder='törlés indoklása' />
                <input type='hidden'
                       name='CURRENCY'
                       id='CURRENCY'
                       value={this.state.order.currency} />
                <input type='hidden'
                       name='DELIVERY_ADDRESS'
                       id='DELIVERY_ADDRESS'
                       value={this.state.order.deliveryAddress} />
                <input type='hidden'
                       name='DELIVERY_CITY'
                       id='DELIVERY_CITY'
                       value={this.state.order.deliveryCity} />
                <input type='hidden'
                       name='DELIVERY_FNAME'
                       id='DELIVERY_FNAME'
                       value={this.state.order.deliveryFname} />
                <input type='hidden'
                       name='DELIVERY_LNAME'
                       id='DELIVERY_LNAME'
                       value={this.state.order.deliveryLname} />
                <input type='hidden'
                       name='DELIVERY_PHONE'
                       id='DELIVERY_PHONE'
                       value={this.state.order.deliveryPhone} />
                <input type='hidden'
                       name='EXTERNAL_REF'
                       id='EXTERNAL_REF'
                       value={random} />
                <input type='hidden'
                       name='MERCHANT'
                       id='MERCHANT'
                       value={this.state.order.merchant} />
                <input type='hidden'
                       name='METHOD'
                       id='METHOD'
                       value={type} />
                <input type='hidden'
                       name='REF_NO'
                       id='REF_NO'
                       value={token} />
                <input type='hidden'
                       name='TIMESTAMP'
                       id='TIMESTAMP'
                       value={moment(new Date()).format('yyyymmddHHMMss')} />
                <div className='container' >
                    <div className='col-sm-6' >
                        <p>
                            <strong>&bull; {token}</strong>
                        </p>
                    </div>
                    <div className='col-sm-6 text-right' >
                        <button className='ot-btn btn-yellow'
                                type='submit' >
                            Recurring {this.getTokenSubmitText(type)}</button>
                    </div>
                </div>
            </form>
        );
    }

    /**
     * Main page.
     */
    render() {
        const {orderFromUrl} = this.props;
        return (
            <div className={orderFromUrl ? 'page-container container' : ''} >
                <h1 className='h1' >Kártyás fizetés</h1>
                <h3 className='h3' >Vásárló</h3>
                <p>&nbsp;</p>
                <table className='table table-responsive' >
                    <tbody>
                    <tr>
                        <td>Vezetéknév:</td>
                        <td>{this.state.order != '' ? this.state.order.billLname : ''}</td>
                    </tr>
                    <tr>
                        <td>Keresztnév:</td>
                        <td>{this.state.order != '' ? this.state.order.billFname : ''}</td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td>{this.state.order != '' ? this.state.order.billEmail : ''}</td>
                    </tr>
                    </tbody>
                </table>
                <h2 className='h2' >
                    <span className='basket-cart-icon' />
                    Kosár
                </h2>
                <table className='table table-responsive' >
                    <thead>
                    <tr>
                        <th>Mennyiség</th>
                        <th>Termék</th>
                        <th>
                            <div className='text-right' >Ár</div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.buildProductList(this.state.order)}
                    <tr>
                        <td>&nbsp;</td>
                        <td>Szállítási költség</td>
                        <td>
                            <div className='text-right' >{this.state.order.orderShipping} Ft</div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>Áfa</td>
                        <td>
                            <div className='text-right' >{this.state.fullPrice - this.state.priceWithoutTax} Ft</div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <strong>Összesen</strong>
                        </td>
                        <td>
                            <div className='text-right' >
                                <strong>{this.state.fullPrice} Ft</strong>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                {this.state.order != '' ? this.getMainForm() : ''}
                {/*
                 <p>Token események üzenetei</p>
                 <p>{this.state.tokenMessage}</p>
                 <h2 className='h2'>&#8608; Recurring fizetés</h2>
                 <p>Ha itt küldöd el, akkor meglévő recurringet tesztel a weblap.</p>
                 {this.getRecurringForm('TOKEN_NEWSALE')}
                 <h2 className='h2'>&#10007; Recurring törlés</h2>
                 <p>Meglévő recurring (token) törlése.</p>
                 {this.getRecurringForm('TOKEN_CANCEL')}
                 <h2 className='h2'>&#8520; Recurring info</h2>
                 <p>Meglévő recurring (token) információk.</p>
                 {this.getRecurringForm('TOKEN_GETINFO')}
                 */}
            </div>
        );
    }
}

SimpleIndex.propTypes = {};
SimpleIndex.defaultProps = {
    orderFromUrl: true
};

export default SimpleIndex;