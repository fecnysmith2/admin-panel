import React, {
   Component,
} from 'react';

class SimpleIpn extends Component {
   constructor(props) {
       super(props);
   }

	render() {
		return (
          <div className="page-container container">

            <h1 className="h1">Sikeres vásárlás</h1>
            <h2 className="h2">Minden rendben történt a fizetés során</h2>

            <p>Köszönjük, hogy nálunk vásároltál! Hamarosan jelzünk emailben.</p>

          </div>
		);
	}
}

SimpleIpn.propTypes = {};
SimpleIpn.defaultProps = {};

export default SimpleIpn;