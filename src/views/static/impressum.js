import React, {
   Component,
} from 'react';
import PropTypes from 'prop-types';

class Impressum extends Component {

   constructor(props) {
       super(props);
   }

	render() {
		return (
			<div className="page-container container">
              <div className="container">
                <section className="impressum">

                  <h2 className="h2">Impresszum</h2>

                  <p>A weboldal üzemeltetője:</p>
                  <ul className="fix-list">
                    <li>Cégnév: Innotrain Hungary Kft.</li>
                    <li>Székhely: 1096 Budapest, Thaly Kálmán utca 39.</li>
                    <li>Adószám: 14623501-2-13</li>
                    <li>Cégjegyzékszám: 13-09-125953</li>
                    <li>Telefon: +36 1 789 5185</li>
                    <li>E-mail: info(kukac)innotrain.hu</li>
                  </ul>

                  <p>A hosting szolgáltatás biztosítója:</p>
                  <ul className="fix-list">
                    <li>Cégnév: Hetzner Online GmbH.</li>
                    <li>Székhely: Industriestr. 25, 91710 Gunzenhausen, Germany</li>
                    <li>Adószám: DE 812871812</li>
                    <li>Telefonszám: +49 (0)9831 505-0</li>
                    <li>Fax: +49 (0)9831 505-3</li>
                  </ul>

                </section>
              </div>
			</div>
		);
	}
}

Impressum.propTypes = {};
Impressum.defaultProps = {};

export default Impressum;