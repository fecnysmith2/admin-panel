import React, {
   Component,
} from 'react';
import PropTypes from 'prop-types';
import PageIntro from '../../components/page-intro.js';
import { Link } from 'react-router';

class Membership extends Component {
   constructor(props) {
     super(props);
   }

	render() {
		return (
			<div className="page-container2">
               <PageIntro title="Az Online Trainer klubtagság előnyei" text="">
                  <p>Válaszd te is a személyre szabottságot és a hatékony edzéseket, hozd ki magadból a maximumot!</p>
                  <p>A klubtagsággal mindössze havi 3.000 Ft-ért biztosíthatod, hogy az edzéseid valóban csak neked szóljanak, profi szakemberek felügyeljék és célodnak megfelelően haladhass. A klubtagsággal minden edzésfunkciót használhatsz, míg a próbaverzióban ezek közül a legtöbb csak korlátozottan használható.</p>
                  <p>Próbáld ki a 2 hetes ingyenes verziót!</p>
                  <p>Az alábbiakban összefoglaltuk neked, hogy mit tartalmaz a 2 hetes ingyenes próbaverzió illetve mennyivel többet kapsz majd klubtagként!</p>
               </PageIntro>
               <div className="container membership-explained">
               </div>
               <section className="membership">
                 <div className="container">
                   <table className="hidden-xs">
                     <thead>
                       <tr>
                         <th></th>
                         <th></th>
                         <th className="paid-top"></th>
                       </tr>
                       <tr>
                         <th></th>
                         <th className="free-name">Próbaváltozat</th>
                         <th className="paid-name">Klubtagság</th>
                       </tr>
                       <tr>
                         <th><h2 className="h2">Mit tartalmaz?</h2></th>
                         <th className="free-price">INGYENES</th>
                         <th className="paid-price">3.000 <span>Ft/hó</span></th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr>
                         <td><strong>Egészségügyi és életmód tanácsadás</strong></td>
                         <td><span className="checkmark-no"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Kardio kontrollált edzéstervezés</strong></td>
                         <td><span className="checkmark-no"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Pulzusmérő jeladó (BLE)</strong></td>
                         <td>Normál áron</td>
                         <td>Kedvezményes áron</td>
                       </tr>
                       <tr>
                         <td><strong>Motivációs csomag</strong></td>
                         <td><span className="checkmark-no"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Személyre szabott, interaktív heti edzésterv</strong></td>
                         <td><span className="checkmark-no"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Edzéscéloknak megfelelő edzéstervek</strong></td>
                         <td><span className="checkmark-no"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Interaktív edzésvezetés</strong></td>
                         <td><span className="checkmark-no"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Otthon vagy edzőteremben elvégezhető gyakorlatsorok</strong></td>
                         <td><span className="checkmark-no"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Állapotfelmérés</strong></td>
                         <td><span className="checkmark-no"></span></td>
                         <td><span className="checkmark-yes"></span> Jeladó szükséges</td>
                       </tr>
                       <tr>
                         <td><strong>Intervall edzések</strong></td>
                         <td><span className="checkmark-yes"></span> Jeladó szükséges</td>
                         <td><span className="checkmark-yes"></span> Jeladó szükséges</td>
                       </tr>
                       <tr>
                         <td>Magyar nyelvű mobilalkalmazás</td>
                         <td><span className="checkmark-yes"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Edzésstatisztika, elemzés</td>
                         <td><span className="checkmark-no"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Online egészségteszt</td>
                         <td><span className="checkmark-no"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Bemelegítés</td>
                         <td><span className="checkmark-no"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Edzésnaptár</td>
                         <td><span className="checkmark-no"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Edzés utáni nyújtás</td>
                         <td><span className="checkmark-no"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Automatizált fitneszteszt</td>
                         <td><span className="checkmark-yes"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Edzésnaplózás</td>
                         <td><span className="checkmark-yes"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Pulzus adatok rögzítése</td>
                         <td><span className="checkmark-yes"></span> Jeladó szükséges</td>
                         <td><span className="checkmark-yes"></span> Jeladó szükséges</td>
                       </tr>
                       <tr>
                         <td>Sebességmérés és rögzítés</td>
                         <td><span className="checkmark-yes"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Szabadedzés</td>
                         <td><span className="checkmark-yes"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Mobilos és webes fiók</td>
                         <td><span className="checkmark-yes"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Bónuszpontok (coinok)</td>
                         <td><span className="checkmark-yes"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Videós tartalmak</td>
                         <td><span className="checkmark-yes"></span></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr className="last-row">
                         <td></td>
                         <td></td>
                         <td><Link to="/klubtagsag#vasarol" className="choose-paid"><span className="top">Klubtag</span><span className="bottom">szeretnék lenni</span></Link></td>
                       </tr>
                     </tbody>
                   </table>
                   <table className="mobile-free visible-xs-block">
                     <thead>
                       <tr>
                         <th colSpan="2" className="free-name text-center">Próbaváltozat</th>
                       </tr>
                       <tr>
                         <th colSpan="2" className="free-price text-center">INGYENES</th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr>
                         <td><strong>Egészségügyi és életmód tanácsadás</strong></td>
                         <td><span className="checkmark-no"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Kardio kontrollált edzéstervezés</strong></td>
                         <td><span className="checkmark-no"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Pulzusmérő jeladó (BLE)</strong></td>
                         <td>Normál áron</td>
                       </tr>
                       <tr>
                         <td><strong>Motivációs csomag</strong></td>
                         <td><span className="checkmark-no"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Személyre szabott, interaktív heti edzésterv</strong></td>
                         <td><span className="checkmark-no"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Edzéscéloknak megfelelő edzéstervek</strong></td>
                         <td><span className="checkmark-no"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Interaktív edzésvezetés</strong></td>
                         <td><span className="checkmark-no"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Otthon vagy edzőteremben elvégezhető gyakorlatsorok</strong></td>
                         <td><span className="checkmark-no"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Állapotfelmérés</strong></td>
                         <td><span className="checkmark-no"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Intervall edzések</strong></td>
                         <td><span className="checkmark-yes"></span> JELADÓ SZÜKSÉGES</td>
                       </tr>
                       <tr>
                         <td>Magyar nyelvű mobilalkalmazás</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Edzésstatisztika, elemzés</td>
                         <td><span className="checkmark-no"></span></td>
                       </tr>
                       <tr>
                         <td>Online egészségteszt</td>
                         <td><span className="checkmark-no"></span></td>
                       </tr>
                       <tr>
                         <td>Bemelegítés</td>
                         <td><span className="checkmark-no"></span></td>
                       </tr>
                       <tr>
                         <td>Edzésnaptár</td>
                         <td><span className="checkmark-no"></span></td>
                       </tr>
                       <tr>
                         <td>Edzés utáni nyújtás</td>
                         <td><span className="checkmark-no"></span></td>
                       </tr>
                       <tr>
                         <td>Automatizált fitneszteszt</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Edzésnapló</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Pulzus adatok rögzítése</td>
                         <td><span className="checkmark-yes"></span> Jeladó szükséges</td>
                       </tr>
                       <tr>
                         <td>Sebességmérés és rögzítés</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Szabadedzés</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Mobilos és webes fiók</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Bónuszpontok (coinok)</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Videós tartalmak</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                     </tbody>
                   </table>
                   <div className="clearfix">
                     <table className="mobile-free visible-xs-block">
                       <thead>
                         <tr>
                           <th colSpan="2" className="paid-name text-center">Klubtagság</th>
                         </tr>
                         <tr>
                           <th colSpan="2" className="paid-price text-center">3.000 <span>Ft/hó</span></th>
                         </tr>
                       </thead>
                       <tbody>
                       <tr>
                         <td><strong>Egészségügyi és életmód tanácsadás</strong></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Kardio kontrollált edzéstervezés</strong></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Pulzusmérő jeladó (BLE)</strong></td>
                         <td>Kedvezményes áron</td>
                       </tr>
                       <tr>
                         <td><strong>Motivációs csomag</strong></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Személyre szabott, interaktív heti edzésterv</strong></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Edzéscéloknak megfelelő edzéstervek</strong></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Interaktív edzésvezetés</strong></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Otthon vagy edzőteremben elvégezhető gyakorlatsorok</strong></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Állapotfelmérés</strong></td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td><strong>Intervall edzések</strong></td>
                         <td><span className="checkmark-yes"></span> JELADÓ SZÜKSÉGES</td>
                       </tr>
                       <tr>
                         <td>Magyar nyelvű mobilalkalmazás</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Edzésstatisztika, elemzés</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Online egészségteszt</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Bemelegítés</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Edzésnaptár</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Edzés utáni nyújtás</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Automatizált fitneszteszt</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Edzésnapló</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Pulzus adatok rögzítése</td>
                         <td><span className="checkmark-yes"></span> Jeladó szükséges</td>
                       </tr>
                       <tr>
                         <td>Sebességmérés és rögzítés</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Szabadedzés</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Mobilos és webes fiók</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Bónuszpontok (coinok)</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                       <tr>
                         <td>Videós tartalmak</td>
                         <td><span className="checkmark-yes"></span></td>
                       </tr>
                         <tr>
                           <td colSpan="2" className="text-center">
                             <Link to="/klubtagsag#vasarol" className="choose-paid"><span className="top">Klubtag</span><span className="bottom">szeretnék lenni</span></Link>
                           </td>
                         </tr>
                       </tbody>
                     </table>
                   </div>
                 </div>
               </section>
			</div>
		);
	}
}

Membership.propTypes = {};
Membership.defaultProps = {};

export default Membership;