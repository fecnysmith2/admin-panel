import React, {
    Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import ModalDialog from '../../components/modal';
import ContactPanelOffer from '../../components/widgets/contact-panel-offer';

class Offer extends Component {

    constructor(props) {
        super(props);
        bindAll(this,
            'closeModal', 'openModal',
        );

        this.state = {
            modalIsOpen: false
        };
    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        });
    }

    openModal() {
        this.setState({
            modalIsOpen: true
        });
    }

    render() {
        return (
            <div className="offer-corporate">
                <ModalDialog open={this.state.modalIsOpen} close={this.closeModal} modalClose={true} enableClose={true}>
                    <ContactPanelOffer callBack={this.closeModal}></ContactPanelOffer>
                </ModalDialog>
                <section className="landing-fitness-intro office-corporate-intro">
                    <div className="container">
                        <div className="text-center">
                            <h2 className="h2">Corporate <span className="offer-365-title-yellow">365</span></h2>
                            <h3 className="h3">Sport- és egészségtámogatás az év minden napjára céged minden
                                dolgozójának.</h3>
                        </div>
                        <div className="clearfix">
                            <div className="">
                                <p className="text-center">Milyen előnyöket jelent egy jól összeállított
                                    egészségtámogató program cégednek?</p>
                                <div className="clearfix">
                                    <div className="col-xs-12 col-sm-2"></div>
                                    <ul className="col-xs-12 col-sm-4">
                                        <li>nő a dolgozók produktivitása,</li>
                                        <li>csökken a táppénzes napok száma,</li>
                                        <li>jelentős a CSR érték és a PR tartalom,</li>
                                        <li>erősödik a munkavállalók lojalitása,</li>
                                        <li>javul a munkaerőpiaci vonzerő,</li>
                                    </ul>
                                    <ul className="col-xs-12 col-sm-4">
                                        <li>adókedvezmények vehetőek igénybe,</li>
                                        <li>megnő a tényleges munkaórák száma,</li>
                                        <li>csapatépítési eszköz: javul a belső kommunikáció és a csapatszellem,</li>
                                        <li>mérséklődik a munkaerő elvándorlás,</li>
                                        <li>bármely dolgozó részt vehet rajta.</li>
                                    </ul>
                                </div>
                            </div>
                            <div className="text-center">
                                <div onClick={this.openModal} className="ot-btn btn-yellow interested-btn">Érdekel!
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="mega-double-section">
                    <article className="article">
                        <div className="left">
                            <h2 className="h2">Mit kínál az Online Trainer? Egészséges életmódot az év 365 napján!</h2>
                            <p></p>
                            <p><strong>Egyre több vállalat ismeri fel, hogy megéri foglalkozni az alkalmazottak
                                egészségével,</strong> hiszen többszörösen is megtérül. Nem csupán a táppénz, a munkából
                                való kiesés költségei csökkennek egy egészséges munkavállalónál, de jobb, energikusabb,
                                lendületesebb munkát végez egy fitt alkalmazott. Mindemellett <strong>a munkavállalók
                                    sokra értékelik, hogy a cég érdemben törődik az egészségükkel,</strong> így
                                rendkívül jó ösztönző hatása van és növeli a vállalat imázsát.
                                Az Online Trainer egy egyedi és innovatív megoldást kínál a dolgozók egészségmegőrzésére
                                és életminőségük javítására:</p>
                            <ul>
                                <li>a legkiválóbb szakemberek támogatásával összeállított programok,</li>
                                <li>magas fokú automatizáltsága révén verhetetlen ár-érték arányú szolgáltatás,</li>
                                <li>komplex és valós segítséget nyújtó megoldások,</li>
                                <li>a teljesen online-tól a személyes szolgáltatásokig terjedő skála,</li>
                                <li>saját fejlesztőcsapat révén egyedi megoldások is lehetségesek,</li>
                                <li>egyedülálló, világújdonságnak számító rendszer,</li>
                                <li>akár egészségpénztári kártyára is igénybe vehető szolgáltatások,</li>
                                <li>modulárisan felépíthető program.</li>
                            </ul>
                            <p>Mivel alkalmazásunk egy rugalmasan kialakított, alapvetően online elérhető platformon
                                működik, így <strong>akár az év minden napjára tudunk változatos és egyedi támogatást
                                    biztosítani a dolgozóknak.</strong></p>
                            <div className="text-center">
                                <div onClick={this.openModal} className="ot-btn btn-yellow interested-btn">Érdekel!
                                </div>
                            </div>
                        </div>
                    </article>
                    <div className="picture body-measure"></div>
                </section>
                <section className="mega-double-section">
                    <div className="picture doctor-action"></div>
                    <article className="article">
                        <div className="right">
                            <h2 className="h2">Felmérés és tanácsadás minden dolgozónak <span className="hidden-xs">az Egészségprogram alapköve</span>
                            </h2>
                            <p><strong>Egészség és vitalitás teszt + kiértékelés és tanácsadás</strong>. Komplex online
                                kérdőíves egészségügyi és életmód felmérés teljeskörű személyes kiértékeléssel és
                                tanácsadással minden dolgozónak.
                                A kérdőív opcionálisan, munkahely specifikus kérdésekkel bővíthető.</p>

                            <p><strong>Egészségtérkép a munkaadónak</strong>, hogy átfogó képe legyen a dolgozók
                                egészségi állapotáról, célzott
                                megoldásokat tudjon keresni a munkakörülmények javítására.</p>
                            <div className="text-center">
                                <div onClick={this.openModal} className="ot-btn btn-yellow interested-btn">Érdekel!
                                </div>
                            </div>
                        </div>
                    </article>
                </section>
                <section className="mega-double-section">
                    <article className="article">
                        <div className="left">
                            <h2 className="h2 text-center">Ösztönző és motivációs programok</h2>
                            <p>Különböző programjainkon belül számtalan aktivitásserkentő és mozgásra ösztönző programot
                                nyújtunk. Egyedi igényektől függően nyereményjátékokat, kihívás programokat szervezünk a
                                dolgozóknak, hogy még jobban ösztönözzük őket egy aktivabb, tudatosabb, egészségesebb
                                élet elérésére.</p>
                            <p>A kihívás programok előnyei:</p>
                            <ul>
                                <li>erősíti a csapatszellemet,</li>
                                <li>növekedik a közösségi aktivitás,</li>
                                <li>jó csapatban eredményesebb a munkavégzés,</li>
                                <li>a jó munkahelyi légkör hatékony eszköz a teljesítmény fokozására,</li>
                                <li>fejlődik a szervezeti egységek közötti együttműködés.</li>
                            </ul>
                            <h3 className="h3">Nyereményjátékok szervezése</h3>
                            <p>A nyereményjátékok indítása az egyik legkönyebben megszervezhető és talán a
                                leglátványosabban kommunikálható ösztönzőelem lehet egy cég egészségprogramjában. Célja
                                éreztetni a dolgozóval, hogy értékelik erőfeszítéseit és bíztatást kap a folytatására,
                                hiszen ez csak érte van.</p>
                            <p>A nyereményjátékok előnyei:</p>
                            <ul>
                                <li>a dolgozó motiválása,</li>
                                <li>erőfeszítéseinek értékelése, odafigyelés,</li>
                                <li>az egyéni kitartás honorálása.</li>
                            </ul>
                            <div className="text-center">
                                <div onClick={this.openModal} className="ot-btn btn-yellow interested-btn">Érdekel!
                                </div>
                            </div>
                        </div>
                    </article>
                    <div className="picture motivation"></div>
                </section>

                <section className="offer-features">
                    <ul>
                        <li>
                            <span className="icon daily-routine"></span>
                            <h3 className="h3">Napi rutin torna</h3>
                            <p>Gyakorlatok a mindennapokra alkalmazásban és intraneten vagy mikrositeon.</p>
                        </li>
                        <li>
                            <span className="icon spine"></span>
                            <h3 className="h3">Gerinc program</h3>
                            <p>A fájdalommentes gerincért.</p>
                        </li>
                        <li>
                            <span className="icon pulse-cardio"></span>
                            <h3 className="h3">Pulzuskontrollált kardio edzésterv és edzéstámogatás</h3>
                            <p>Az egészséges keringési rendszerért.</p>
                        </li>
                        <li>
                            <span className="icon lose-weight"></span>
                            <h3 className="h3">Életmódváltó program</h3>
                            <p>...</p>
                        </li>
                        <li>
                            <span className="icon activity"></span>
                            <h3 className="h3">Activity</h3>
                            <p>Mozgás ösztönző kihívás program és nyereményjáték szervezése a dolgozók számára.</p>
                        </li>
                    </ul>
                </section>

                <section className="mega-double-section">
                    <div className="picture personal-services"></div>
                    <article className="article">
                        <div className="right">
                            <h2 className="h2 personal-services-h2">Személyes szolgáltatások</h2>
                            <ul>
                                <li>Fizikai és kondícionális állapotfelmérés.</li>
                                <li>Egyes munkakörök speciális terheléseinek és kondícionális igényeinek felmérése.</li>
                                <li>Személyi edzés az Online Trainer segítségével, havi egy személyes konzultációval
                                    (javasolt vezető beosztásúak számára egy szűkebb körben elérhetővé tenni).
                                </li>
                            </ul>
                            <div className="text-center">
                                <div onClick={this.openModal} className="ot-btn btn-yellow interested-btn">Érdekel!
                                </div>
                            </div>
                        </div>
                    </article>
                </section>

                <section className="mega-double-section">
                    <article className="article">
                        <div className="left">
                            <h2 className="h2 online-edu-h2">Online oktatás és képzés</h2>
                            <ul>
                                <li>Ismeretterjesztő cikksorozatok mozgás, táplálkozás, stresszkezelés témákban,</li>
                                <li>oktatóvideók mozgás és munkahelyi ergonómia vonatkozásában,</li>
                                <li>munkahelyi átmozgatógyakorlatok videón és mobilalkalmazásban,</li>
                                <li>oktató videók fizikai munkát végzők számára,</li>
                                <li>munkahelyi egészségmegőrző eszközök és használatuk bemutatása (air disc, fitnesz
                                    labda) felhasználói oktató videókkal,
                                </li>
                                <li>megfelelő folyadékbevitelre figyelmeztető program a mobilalkalmazásba építve.</li>
                            </ul>
                            <div className="text-center">
                                <div onClick={this.openModal} className="ot-btn btn-yellow interested-btn">Érdekel!
                                </div>
                            </div>
                        </div>
                    </article>
                    <div className="picture online-edu"></div>
                </section>
            </div>
        );
    }
}

Offer.propTypes = {};
Offer.defaultProps = {};

export default Offer;
