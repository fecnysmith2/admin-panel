import React, {
   Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';

class ReportErrors extends Component {
    constructor(props) {
      super(props);
      bindAll(this,
          'openModal',
          'hideModal'
      );
      this.state = {
        isOpen: true
      }

    }

    componentDidMount() {
        this.openModal();
        window.log('wtf');
    }

    openModal() {
        this.setState({
          isOpen: true
        });
    }

    hideModal() {
        this.setState({
          isOpen: false
        });
    }

	render() {
        const { baseClasses } = this.props;
		return (
			<div className={baseClasses}>
  	          <h1 className="h1"></h1>
  	          {/*
  	          <ModalDialog enableClose={true} open={this.state.isOpen} close={this.hideModal}>
                <h2 className="h2">Aktiméter csatlakozási probléma!</h2>
                <p>Jelenleg a készüléknek rendszerátállás miatt nincs támogatása.
                Dolgozunk az Actimeter újbóli integrációján, és igyekszünk minél előbb legalább az alap funkciókat elérhetővé tenni.</p>
                <p>A megoldásig türelmet kérünk!</p>
                <p>Köszönettel: az OT csapata</p>
                <div className="text-center">
                  <button onClick={this.hideModal}>Értem</button>
                </div>
              </ModalDialog>
              */}
  	          <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfRZE1NcXYrjq36LUJ2WeFp4JFqzWa9L3JvGpBcTFH666jy1g/viewform?embedded=true#responses" frameBorder="0" marginHeight="0" marginWidth="0">Betöltés...</iframe>
			</div>
		);
	}
}

ReportErrors.propTypes = {};
ReportErrors.defaultProps = {
  baseClasses: 'page-container container report-errors-container'
};

export default ReportErrors;