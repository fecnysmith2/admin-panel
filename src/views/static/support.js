import React, {
   Component,
} from 'react';
import PropTypes from 'prop-types';
import bindAll from 'lodash.bindall';
import ReactDOM from 'react-dom';
import ContactPanel from '../../components/widgets/contact-panel';

class Support extends Component {
    constructor(props) {
      super(props);
    }

	render() {
		return (
			<div className="page-container container">
				<h1 className="h1">Ügyélszolgálatunk elérhetőségei</h1>
                <p>Cégünkkel, termékeinkkel, szolgáltatásainkkal kapcsolatos további információkért az alábbi elérhetőségeken fordulhatsz hozzánk: <br/>
                Telefon: +36 1 789 5185 (munkanapokon 10.00 - 16.00 óra között) <br/></p>
                <ContactPanel>
                    <p>Online üzenet:</p>
                </ContactPanel>
			</div>
		);
	}
}

Support.propTypes = {};
Support.defaultProps = {};

export default Support;