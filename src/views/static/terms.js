import React, {Component} from 'react';

class Terms extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="page-container container">
                <section className="tos">
                    <h2 className="h2">ÁSZF / Általános szerződési feltételek</h2>

                    <p>Jelen Általános Szerződési Feltétek (továbbiakban: ÁSZF) hatálya a www.onlinetrainer.hu oldalon
                        elérhető, és letölthető „OnlineTrainer” (<em>a továbbiakban: OT</em>) virtuális személyi edző
                        rendszer, és fitnesz platform felhasználói – ideértve a regisztráció nélküli, a regisztrált és
                        az előfizetéssel rendelkező felhasználókat – részére a Mindset Nonprofit Kft. és az Innotrain Hungary Kft. (<em>továbbiakban: Szolgáltató</em>) által nyújtott szolgáltatásokra és adatkezelésekre terjed ki.
                            </p>

                    <h3 className="h3">1. A Szolgáltató adatai</h3>
                    <div className="clearfix">
                        <div className="col-sm-6 col-xs-12">
                            <ul className="fix-list">
                                <li>Mindset Nonprofit Kft.</li>
                                <li>Székhely: 2049 Diósd, Tündér utca 6.</li>
                                <li>Képviselő neve: Gády-Kayser Tamás</li>
                                <li>Cégjegyzékszám: 13-09-143599</li>
                                <li>Adószám: 14215087-2-13</li>
                                <li>Számlavezető pénzintézet: UniCredit Bank Hungary Zrt.</li>
                                <li>Bankszámlaszám: 10918001-00000057-69950009</li>
                                <li>Statisztikai számjel: 14215087-9329-572-13</li>
                                <li>E-mail: info@mindset.hu</li>
                            </ul>
                        </div>
                        <div className="col-sm-6 col-xs-12">
                            <ul className="fix-list">
                                <li>Innotrain Hungary Kft.</li>
                                <li>Székhely: 1096 Budapest, Thaly K. u. 39.</li>
                                <li>Képviselő neve: Szabó Rudi</li>
                                <li>Cégjegyzékszám. 01-09-292277</li>
                                <li>Adószám: 14623501-2-43</li>
                                <li>Számlavezető pénzintézet: Erste Bank Hungary Zrt.</li>
                                <li>Bankszámlaszám: 11600006-00000000-32628021</li>
                                <li>Statisztikai számjel: 14623501-5829-113-01</li>
                                <li>E-mail: info@innotrain.hu</li>
                            </ul>
                        </div>
                    </div>

                    <h3 className="h3">2. Irányadó jog</h3>
                    <p>A Szolgáltató Magyarországon bejegyzett Gazdasági Társaság, amely tevékenységét elsősorban a
                        magyar jogszabályok hatálya alatt végzi. Amennyiben a Szolgáltató tevékenysége – ide értve a
                        reklámozási tevékenységet is – Magyarország határain túlra irányul, a célországnak megfelelő
                        jogok az irányadóak.</p>

                    <h3 className="h3">3. A Felhasználó</h3>
                    <p>Felhasználónak minősül a www.onlinetrainer.hu (<em>továbbiakban: Oldal</em>) weboldalra belépő látogató akár rendelkezik regisztrációval az Oldalon, akár nem. </p>

                    <h3 className="h3">4. Regisztráció</h3>
                    <p>A regisztrációhoz a regisztrációs űrlap kitöltése szükséges. Ezen a "név" és "E-mail" mezőkben a
                        Felhasználó a saját, teljes (vezetéknév és keresztnév) valós nevét, illetve saját, létező
                        (érvényes) email címét adja meg. Ellenkező esetben a regisztráció törlésre kerül.</p>
                    <p>Regisztrált felhasználó kizárólag az lehet, aki</p>
                    <ul className="fix-list">
                        <li>18. életévét betöltött, cselekvőképes</li>
                        <li>14. életévét betöltött kiskorú, akinek törvényes képviselője hozzájárult a Szolgáltatás
                            használatához és a regisztrációhoz és ezt kérésre bármikor bizonyítani is tudja
                        </li>
                        <li>jogi személy vagy jogi személyiség nélküli gazdálkodó szervezet nevében jár el és
                            szavatolja, hogy jogosult az adott szervezet képviseletére
                        </li>
                    </ul>

                    <p>Felhasználó a regisztráció során választhat magának felhasználónevet és profil képet.</p>
                    <p>Tilos olyan fantázianevet választani, amely</p>
                    <ul className="fix-list">
                        <li>közszereplő, közismert személy neve, kivéve, ha az egyben a Felhasználó saját,
                            anyakönyvezett neve is
                        </li>
                        <li>obszcén, ízléstelen vagy trágár kifejezést tartalmaz;</li>
                        <li>valamilyen törvénysértő kifejezést tartalmaz.</li>
                    </ul>

                    <p>Tilos olyan profil képet elhelyezni az Oldalon, amely</p>
                    <ul className="fix-list">
                        <li>közerkölcsöt sért;</li>
                        <li>a Felhasználási Feltételekkel vagy hatályos jogszabállyal ütközik.</li>
                    </ul>
                    <p>Felhasználó maga dönt arról, hogy fantázianevén kívül milyen adatokat, információkat kíván
                        megjeleníteni adatlapján (profilján). Tilos az adatlapon a jogsértő, az akár nonprofit, akár
                        profit szerzésére irányuló, burkolt vagy nyílt reklámhordozónak minősülő, a harmadik személy
                        jogait vagy jogos érdekeit sértő, az obszcén vagy trágár tartalom megjelenítése.</p>
                    <p>Felhasználó tudomásul veszi, hogy a Felhasználási Feltételek, hatályos jogszabályok vagy harmadik
                        személy jogainak és jogos érdekeinek megsértése esetén Felhasználó regisztrációja külön
                        értesítés és indoklás nélkül törlésre kerül.</p>

                    <h3 className="h3">5. Felelősség</h3>
                    <p>A Szolgáltató és felhasználó között a regisztrációt és az előfizetési díj megfizetését
                        követően határozatlan időtartamú szolgáltatási szerződés jön létre, melyet a felhasználó az
                        alábbi feltételek mellett vehet igénybe:</p>

                    <h4 className="h4">5.1. A Felhasználó felelőssége</h4>
                    <p>A Felhasználó az OT programot csak és kizárólag saját felelősségére használhatja. A Szolgáltató
                        nem vállal felelősséget az OT használata során a Felhasználót ért olyan károkért,
                        kellemetlenségekért, amelyek abból adódnak, hogy a Felhasználó az OT használata során nem a
                        kellő körültekintéssel járt el.</p>
                    <p>A felhasználó az OT program használata előtt</p>
                    <ul className="fix-list">
                        <li>vagy a Szolgáltató által elfogadott állapotfelmérésen vesz részt, mely során a rendszer
                            használatának kezdő időpontjában fennálló egészségi állapotát méri fel a Szolgáltató,
                        </li>
                        <li>vagy saját, korábban kiválasztott személyi edzőjének útmutatása szerint csatlakozik a
                            rendszerhez, amely esetben a felhasználó egészségi állapotának a felmérését a személyi edző
                            végzi.
                        </li>
                    </ul>
                    <p>Amennyiben a felhasználó a Szolgáltató által meghatározott állapotfelmérésen nem vesz részt, a
                        Szolgáltató az állapotfelmerés elmaradásából eredően bekövetkező károk vonatkozásában
                        felelősséget nem vállal. A felhasználó a Szolgáltató által meghatározott állapotfelmérés során
                        is saját felelősségére nyilatkozik fizikai és egészségi állapotáról. Az állapotfelmérés során
                        kiterjedt orvosi kivizsgálásra, laboratóriumi vizsgálatokra nem kerül sor, a felhasználó ennek
                        tükrében dönt az OT igénybevételéről és annak használatáról.</p>
                    <p>A felhasználó tudomásul veszi, hogy az OT rendszer használata során a felhasználó pulzus
                        intenzitását figyelembe véve visszajelzést és iránymutatást ad az OT rendszer optimális
                        használatára, és a legmegfelelőbb edzésintenzitásra vonatkozóan, azonban nem követi figyelemmel
                        a felhasználó aktuális egészségi állapotát, így esetleges rosszullét, túledzettség, kimerülés
                        esetén semmilyen külső segítséget nem tud nyújtani a felhasználó részére.</p>
                    <p>A felhasználó köteles az OT használatának szabályait tiszteletben tartani.</p>
                    <p>Amennyiben a szolgáltatás használatával összefüggő bármely tevékenység a Felhasználó államának
                        joga szerint nem megengedett, a használatért kizárólag Felhasználót terheli a felelősség.</p>

                    <h4 className="h4">5.2. A Szolgáltató felelőssége</h4>
                    <p>A Szolgáltató vállalja, hogy minden tőle elvárható intézkedést megtesz a szolgáltatás
                        zavartalansága és folyamatossága érdekében, azonban a kiesésmentes működésért nem vállal
                        felelősséget.</p>
                    <p>A Szolgáltató minden szükséges intézkedést megtesz a felhasználók adatainak biztonsága és annak
                        érdekében, hogy a felhasználók fizikai, és egészségi állapotát a szolgáltatás megkezdésekor és
                        azt követően folyamatosan a legmegfelelőbben mérje fel. Nem vállal felelősséget a Felhasználó
                        magatartásáért, azaz minden Felhasználó a saját felelősségére veszi igénybe az OT
                        szolgáltatásait. Valamennyi, a felhasználó magatartására visszavezethető kár vonatkozásában a
                        Szolgáltató a felelősségét kizárja.</p>

                    <h3 className="h3">6. Szerzői jogok</h3>
                    <p>A Honlapon megjelenített tartalom (ideértve többek között valamennyi, a szolgáltatás keretében
                        elérhető grafikát és egyéb anyagokat, a honlap felületének elrendezését, szerkesztését, a
                        használt szoftveres és egyéb megoldásokat, ötletet, megvalósítást) a Szolgáltató szellemi
                        alkotása. Kivételt képeznek ez alól a honlapokon feltüntetett egyes gyártók, forgalmazók
                        védjegyei. A szolgáltatás rendeltetésszerű használatával járó megjelenítésen, az ehhez szükséges
                        ideiglenes többszörözésen és a magáncélú másolatkészítésen túl ezen szellemi alkotások a
                        Szolgáltató előzetes írásbeli engedélye nélkül semmilyen formában nem használható fel és nem
                        hasznosítható.</p>
                    <p>A partnerszolgáltatók által megadott tartalom (ideértve többek között valamennyi, a szolgáltatás
                        keretében elérhető fotó, videó, grafika és egyéb anyagok) – eltérő megjelölés hiányában – a
                        partnerszolgáltató szellemi alkotásának tekintendő, és a szolgáltatás rendeltetésszerű
                        használatával járó megjelenítésen, az ehhez szükséges ideiglenes többszörözésen és a magáncélú
                        másolatkészítésen túl a partnerszolgáltató előzetes írásbeli engedélye nélkül semmilyen formában
                        nem használható fel és nem hasznosítható.</p>

                    <h3 className="h3">7. Vásárlási feltételek</h3>

                    <h4 className="h4">7.1. A szolgáltatásért járó ellenérték, előfizetési díjak:</h4>
                    <p>A Szolgáltató a rendszer használata során több különböző szolgáltatási csomagot és tartalmat dolgoz ki
                        a felhasználók részére, azok igényeihez igazodva és a visszajelzések alapján.</p>
                    <p>A tárgyuknál fogva nem egyszeri szolgáltatásra irányuló szolgáltatások vonatkozásában a
                        Szolgáltató és a Felhasználó közötti szerződések határozatlan időtartamra jönnek létre. A
                        Felhasználó a szolgáltatási szerződést az alábbi esetekben, és módon szüntetheti meg:</p>
                    <p>A Felhasználó a szolgáltatást írásbeli felmondás útján (elektronikusan, vagy postai úton)
                        szüntetheti meg.</p>
                    <ul className="fix-list">
                        <li>postai úton ajánlott levél formájában;</li>
                        <li>elektronikusan: ügyfélszolgálat menüpont → szolgáltatás szüneteltetése/megszüntetése →
                            regisztrációs adatok megadása és a kérdőív kitöltése.
                        </li>
                    </ul>
                    <p>A szolgáltatási szerződést minden esetben kizárólag az előfizetői díjjal nem rendezett
                        szerződéses időtartam vonatkozásában lehet megszüntetni. A Felhasználó mindezekre tekintettel
                        tudomásul veszi, hogy amennyiben felmondása az előfizetői díjjal rendezett szolgáltatási
                        időtartamra esik, előfizetői díj visszatérítésére – az előfizetői időszakból még hátralévő időre
                        – nem jogosult. A felhasználó felmondása nem érinti a Szolgáltató előfizetői díjjal rendezett, a
                        szolgáltatási szerződésből még hátralévő időtartamra vonatkozó szolgáltatási kötelezettségét.
                        Amennyiben a szolgáltatási szerződés felmondására vagy megszüntetésére a szolgáltatás
                        megkezdésétől számított fél éves tartamon belül kerül sor, Szolgáltatónak jogában áll a
                        szolgáltatás keretében a felhasználónak juttatott eszközök (különös tekintettel a mellkasi
                        jeladó) önköltségi árát a felhasználóra terhelni.</p>
                    <p>A Szolgáltató által nyújtott csomagok pontos tartalma és az ahhoz kapcsolódó előfizetési díjak a
                        szolgáltató marketing felületein kerülnek feltüntetésre. A Felhasználók a Szolgáltató marketing
                        felületeiről, illetve az OT keretein belül működtetett webshopban rendelhetik meg a
                        szolgáltatási csomagokat valamint a szolgáltatás nyújtásához szükséges termékeket. Ezen
                        webshopban a Szolgáltató a szolgáltatás során igénybe vehető ún. kiegészítő termékeket is
                        értékesít a Felhasználók számára (pl. táplálékkiegészítők, sporteszközök, telefon- és
                        tabletkiegészítők). Ezen termékek árait és a részletes termékleírást a Felhasználók a webshopban
                        ismerhetik meg. A Szolgáltató ezúton tájékoztatja a Felhasználót, hogy a Szolgáltató által az
                        egyes szolgáltatási csomagokra megállapított időszakos előfizetési díjakat az adott
                        szolgáltatási csomagra állapította meg és az arra az időszakra járó valamennyi költséget
                        tartalmazza.</p>

                    <h4 className="h4">7.1.1 Automatikus megújulás</h4>
                    <p>Az Előfizetések legrövidebb igénybe vehető időtartama 31 nap, azonban felmondás hiányában minden előfizetés automatikusan megújul az aktuális előfizetési időszak utolsó napján. A vonatkozó díjak azon a fizetési módozaton kerülnek kiszabásra, amelyet az előfizető az eredeti tranzakciónál használtál. Abban az esetben, ha csökkentett előfizetési díjú, Kedvezményes Ajánlattal élt az automatikus megújítás alapáron történik.</p>
                    <p>Az automatikus megújítás addig tart, amíg valamelyik fél fel nem mondja azt. A felmondás a következő előfizetési időszak indulása előtt legalább 1 nappal történhet meg a ”Profil / Személyes adatok / Előfizetések” menüpontban alatt.</p>

                    <h4 className="h4">7.2. Szállítási és fizetési feltételek</h4>
                    <h4 className="h4">7.2.1 Megrendelés módja és visszaigazolás</h4>
                    <p>A megrendeléseket az oldalon és a webáruházon keresztül lehet leadni a Szolgáltató felé. A
                        megrendelések feldolgozása és visszaigazolása, számítógépes rendszeren keresztül történik.</p>
                    <h4 className="h4">7.2.2 Szállítás</h4>
                    <p>A megrendelt árukat Magyarország területén a DPD futárszolgálat közreműködésével szállítjuk, vagy
                        átvehetik személyesen telephelyünkön (1096 Budapest, Thaly Kálmán u. 39). A szállítási díj
                        bruttó 1.250.- Ft, amely speciális esetekben (súlytól és mérettől függően) módosulhat. </p>
                    <h4 className="h4">7.2.3 Fizetési lehetőségek</h4>
                    <p><em>Utánvét</em></p>
                    <p><em>Átutalás</em></p>
                    <p><em>Bankkártya</em></p>
                    <p>A bankkártyás fizetést az OTP Mobil (Simple) oldalán keresztül biztosítjuk, melyet a fizetés
                        megkezdésekor az Adattovábbítási nyilatkozat elfogadásával lehet indítani. A nyilatkozat
                        elfogadásának hiánya a bankkártyást fizetés lehetőségét kizárja.</p>
                    <p>A Szolgáltató a regisztrált Felhasználók számára ismétlődő bankkártyás fizetési lehetőséget
                        biztosít a felhasználók számára. Ezen ismétlődő fizetés alapján a felhasználónak lehetősége
                        nyílik arra, hogy az első vásárlás – ún. regisztrációs vásárlás – során megadott bankkártya
                        adatok ismételt megadása nélkül fizessen a Szolgáltató szolgáltatásaiért. Az ismétlődő fizetés
                        esetén a felhasználó minden egyes tranzakcióról a bankkártyás fizetéssel megegyező csatornákon
                        értesítést kap. Az ismétlődő fizetés történhet a felhasználó aktív közreműködése nélkül (ebben
                        az esetben a regisztrációs tranzakció során a felhasználó hozzájárul az ismétlődő fizetésekhez)
                        vagy történhet a felhasználó eseti jóváhagyásával.</p>
                    <p>Egyszeri hozzájárulás: a felhasználó ennél a módozatnál a regisztrációs tranzakció alkalmával
                        hozzájárul, hogy a Szolgáltató a jövőben esedékessé váló összegekre ismétlődő fizetési
                        lehetőséget kezdeményezzen és elfogadja, hogy ezzel előre meghatározott időközönként, előre
                        meghatározott összegekkel beterhelésre kerül majd a bankkártyájához tartozó bankszámlaszám.</p>
                    <p>Eseti hozzájárulás: a felhasználó valamennyi jövőbeni fizetésnél jóvá kell, hogy hagyja az
                        ismétlődő tranzakciót.</p>
                    <p>Ebben az esetben a felhasználó számára az ismétlődő fizetés tulajdonképpen egy kényelmi funkció,
                        melynek segítségével nem kell minden fizetés során újból megadnia a bankkártya adatait. A
                        Szolgáltató kifejezetten felhívja a felhasználó figyelmét arra, hogy az ismétlődő fizetés esetén
                        a bankkártya adatok kezelése a bankkártya kibocsátó társaságok által meghatározott előírásoknak
                        megfelelően történik. A megadott bankkártya adatokhoz sem a Szolgáltató, sem a Szolgáltató által
                        megbízott fizetési szolgáltató (PayU) nem fér hozzá. A Szolgáltató tájékoztatja a felhasználót,
                        hogy az általa esetlegesen tévesen kezdeményezett ismétlődő fizetéses tranzakciók vonatkozásában
                        a fizetési szolgáltató (PayU) mindenfajta felelősségét kizárta. A felhasználó a fent ismertetett
                        fizetési feltételeket a jelen felhasználási feltételek elfogadásával kifejezetten és
                        visszavonhatatlanul tudomásul vette.</p>

                    <h4 className="h4">7.3 A szolgáltatás igénybevétele során felmerülő szavatossági kérdések:</h4>
                    <p>A Felhasználót a Szolgáltatónál történt regisztrációját követően a szolgáltatás igénybevételétől
                        kezdődően – függetlenül attól, hogy a felhasználó regisztrációjára, illetve a szolgáltatási
                        szerződés megkötésére interneten került sor – a 45/2014. (II/26.) Korm. rendelet alapján, a 14
                        munkanapos elállási jog – tekintettel a szolgáltatás jellegére – nem illeti meg.</p>
                    <p>A Szolgáltatónál a webshopban megvásárolt termékek vonatkozásában a felhasználót a szerződés
                        megkötésétől – írásbeli visszaigazolásától – számított 14 munkanapon belül elállási jog illeti
                        meg. Ezen elállási jog azonban nem illeti meg a felhasználót az olyan zárt csomagolású termék
                        tekintetében, amely egészségvédelmi vagy higiéniai okokból az átadást követő felbontása után nem
                        küldhető vissza. A Szolgáltató bizonyos feltételek teljesítése esetén pénzvisszatérítési
                        lehetőséget biztosít a felhasználó számára, erről a “100% garancia” menüpont alatt
                        tájékozódhatnak részletesen.</p>

                    <p><strong><em>Kellékszavatosság</em></strong></p>

                    <p>A Felhasználó a Szolgáltató hibás teljesítése esetén a vállalkozással szemben kellékszavatossági
                        igényt érvényesíthet. A Polgári Törvénykönyv szabályai szerint a felhasználó – választása
                        szerint – az alábbi kellékszavatossági igénnyel élhet:</p>
                    <p>Javítást vagy csere: kivéve, ha ezek közül a választott igény teljesítése lehetetlen vagy a
                        vállalkozás számára más igénye teljesítéséhez képest aránytalan többletköltséggel járna.</p>
                    <p>Ha a javítást vagy a cserét nem kérte, illetve nem kérhette, úgy igényelheti</p>
                    <ul className="fix-list">
                        <li>arányos ellenszolgáltatás leszállítását,</li>
                        <li>a Szolgáltató költségére a hibát kijavíthatja, illetve mással kijavíttathatja vagy</li>
                        <li>végső esetben – a szerződéstől elállhat.</li>
                    </ul>
                    <p>A választott kellékszavatossági jogáról egy másikra áttérhet, ez esetben azonban az áttérés
                        költségét a Felhasználó viseli, kivéve, ha az indokolt volt, vagy arra a Szolgáltató okot
                        adott.</p>
                    <p>A Felhasználó köteles a hibát annak felfedezése után haladéktalanul, de nem később, mint a hiba
                        felfedezésétől számított kettő hónapon belül közölni, azonban a felhasználó a szerződés
                        teljesítésétől számított két éves elévülési határidőn túl kellékszavatossági jogait már nem
                        érvényesítheti.</p>
                    <p>A teljesítéstől számított hat hónapon belül a kellékszavatossági igény érvényesítésének a hiba
                        közlésén túl nincs egyéb feltétele, ha a Felhasználó igazolja, hogy a terméket, illetve a
                        szolgáltatást a Szolgáltató nyújtotta. A teljesítéstől számított hat hónap eltelte után azonban
                        Felhasználónak bizonyítani kell, hogy a felismert hiba már a teljesítés időpontjában is
                        megvolt.</p>
                    <h4 className="h4">7.4. Termékszavatosság</h4>
                    <p>Ingó dolog (termék) hibája esetén a Felhasználó – választása szerint – kellékszavatossági jogot
                        vagy termékszavatossági igényt érvényesíthet. Termékszavatossági igényként a felhasználó
                        kizárólag a hibás termék javítását vagy cseréjét kérheti.</p>
                    <p>A termék akkor hibás, ha az nem felel meg a forgalomba hozatalakor hatályos minőségi
                        követelményeknek vagy pedig, ha nem rendelkezik a gyártó által adott leírásban szereplő
                        tulajdonságokkal.</p>
                    <p>Termékszavatossági igényét a felhasználó a termékgyártó általi forgalomba hozatalától számított
                        két éven belül érvényesítheti. E határidő elteltével e jogosultságát elveszti.</p>
                    <p>A termék hibáját termékszavatossági igény érvényesítése esetén a felhasználónak kell
                        bizonyítania.</p>
                    <p>A felhasználó ugyanazon hiba miatt kellékszavatossági és termékszavatossági igényt egyszerre,
                        egymással párhuzamosan nem érvényesíthet. Termékszavatossági igényének eredményes érvényesítése
                        esetén azonban a kicserélt termékre, illetve a kijavított részre vonatkozó kellékszavatossági
                        igényét a gyártóval szemben érvényesítheti.</p>

                    <h4 className="h4">7.5 A felhasználói minőségi kifogások rendezésének módja:</h4>
                    <p>A felhasználó bármely panaszával vagy minőségi kifogásával köteles elsőként a Szolgáltatóhoz
                        fordulni, a fenti elérhetőségeken. A Szolgáltató a minőségi kifogást 15 napon belül kivizsgálja
                        és a kivizsgálás eredményéről a felhasználót írásban (elektronikus úton) tájékoztatja, amely
                        tájékoztatással egyidejűleg a minőségi kifogás orvoslására is konkrét javaslatot tesz a
                        felhasználó részére. Amennyiben a felhasználó a minőségi kifogás rendezésének módjával nem
                        elégedett, vagy minőségi kifogását a Szolgáltató elutasította, a felhasználó a területileg
                        illetékes Békéltető Testület eljárását kezdeményezheti az alábbi elérhetőségeken:</p>
                    <ul className="fix-list">
                        <li>Pest Megyei Békéltető Testület</li>
                        <li>Címe: 1119 Budapest, Etele út 59-61. 2. em. 240.</li>
                        <li>Telefonszáma: (1)-269-0703; E-mail cím: pmbekelteto@pmkik.hu</li>
                        <li>
                            Honlap cím: <a href="http://www.panaszrendezes.hu/"
                                           target="_blank">www.panaszrendezes.hu</a>
                        </li>
                    </ul>

                    <h3 className="h3">8. Adatkezelés</h3>

                    <h4 className="h4">8.1. Az adatkezelés célja, jogalapja és elvei</h4>
                    <p>A jelen szabályzatban használt „személyes adatok” kifejezés összhangban az információs
                        önrendelkezési jogról és az információszabadságról szóló 2011. évi CXII. törvényben foglaltakkal
                        olyan adatokra vonatkozik, amelyek a Felhasználó személyéhez kapcsolódnak, továbbá amelyeket a
                        Felhasználó önkéntesen adott meg a Szolgáltató részére, és amelyekre a Szolgáltatónak a
                        Felhasználó részére nyújtott szolgáltatásaink biztosításához (és fejlesztéséhez), továbbá a
                        jelen szabályzatban megfogalmazott célok eléréséhez van szükség. Ezen adatok közé az alábbiak
                        tartoznak: név, cím, e-mail cím, egészségügyi adatok. A Szolgáltató a Felhasználó kifejezett
                        hozzájárulása nélkül nem kezeli a személyes adatait.</p>
                    <p>A Szolgáltatónak a Felhasználó által megadott személyes adatait saját adatbázisában tárolja
                        addig, ameddig az feltétlenül szükséges a szolgáltatás nyújtása érdekében.</p>
                    <p>A Szolgáltató a Felhasználók által megadott összes információt személyre szólóan nem
                        beazonosítható formában őrzi és az adatkezelést kizárólag a Felhasználó önkéntes, előzetes
                        tájékoztatáson alapuló hozzájárulása alapján végezi.</p>

                    <h4 className="h4">8.2. Az adatkezelés helye, irányadó jog</h4>
                    <p>A személyes adatok kezelése a Szolgáltató székhelyén, fióktelepén, telephelyén, vagy a
                        Szolgáltató által megbízott adatfeldolgozó helyiségében történik. Az adatkezeléssel kapcsolatos
                        döntéseket a Szolgáltató hozza meg.</p>
                    <p>A Szolgáltató magyar jogi személy, az adatkezelés helye Magyarország. Mindezek alapján a
                        személyes adatok kezelésére a magyar jogszabályok, elsősorban az információs önrendelkezési
                        jogról és az információszabadságról szóló 2011. évi CXII. törvény szabályai az irányadók.</p>

                    <h4 className="h4">8.3. Hozzáférés a kezelt adatokhoz, adatfeldolgozó igénybevétele</h4>
                    <p>A kezelt adatokat a Szolgáltató és az Adatfeldolgozó csak az érintett hozzájárulása, vagy
                        törvényi rendelkezés alapján teszi hozzáférhetővé harmadik személy(ek) számára. Amennyiben a
                        felhasználó a szolgáltatás használata során maga vesz igénybe kívülálló harmadik személyt (pl.
                        személyi edzőt) – a szolgáltatás hatékonysága és céljának elérése érdekében – úgy a felhasználó
                        hozzájárulását az általa kiválasztott kívülálló harmadik személy részére a szolgáltatás
                        igénybevételekor megadott és kezelt adatok megismeréséhez megadottnak kell tekinteni.</p>

                    <h4 className="h4">8.4. Az adatok Szolgáltató általi felhasználása</h4>
                    <p>A Szolgáltató a Felhasználó által megadott adatokat elsősorban a szolgáltatás nyújtása céljából
                        használja fel. A szolgáltatás igénybevétele során az elsődleges adatkezelés automatizált,
                        számítástechnikai rendszeren keresztül, emberi beavatkozás nélkül történik.</p>
                    <p>Egyes szolgáltatások esetében vagy a felhasználó egyedi kéréseinek, panaszainak vizsgálata során
                        a Szolgáltató a felhasználó adatait az általa ismert és jóváhagyott módon használja fel.</p>
                    <p>A Szolgáltató jogosult az adatok felhasználására, különösen, ha a felhasználó jogellenes
                        magatartást tanúsít.</p>
                    <p>A Szolgáltató jogosult arra, hogy a felhasználó regisztráció során megadott e-mail címére a
                        szolgáltatással összefüggő hírlevelet és rendszerüzeneteket küldjön.</p>

                    <h4 className="h4">8.5. Reklámozási célú adatkezelés</h4>
                    <p>A Felhasználó a regisztrációval hozzájárul ahhoz, hogy a Szolgáltató a hírlevél, illetve az egyéb
                        értesítő levél alján saját szolgáltatásaival és termékeivel kapcsolatos, illetve vele
                        szerződéses kapcsolatban álló partnereinek termékeivel kapcsolatos reklámokat helyezzen el. A
                        Felhasználó a Szolgáltató által küldött hírlevélről bármikor leiratkozhat.</p>

                    <h4 className="h4">8.6. A Felhasználó jogai, a magánszféra védelme</h4>
                    <p>A Felhasználónak jogában áll, hogy adatait helyesbítse vagy a tévesen szereplő, és általa nem
                        helyesbíthető adatok javítását a Szolgáltatótól kérje. A Felhasználó tájékoztatást kérhet adatai
                        kezeléséről és a Szolgáltató köteles a kérelem benyújtásától számított legrövidebb idő alatt, de
                        legfeljebb 30 napon belül közérthető formában az Felhasználónak írásban tájékoztatást adni. A
                        tájékoztatás teljes körű, az adatkezelés minden részletére kiterjed.</p>

                    <h4 className="h4">8.7. Cookie-k</h4>
                    <p>Az ún. „cookie”-k a Felhasználó merevlemezén tárolódó kis fájlok, amelyeket a Szolgáltató igénybe
                        vesz annak érdekében, hogy összegyűjtsön és rögzítsen olyan információkat mint például a
                        Felhasználó IP-címe, látogatásának időpontja, számítógépe operációs rendszerének, esetlegesen
                        böngészőprogramjának típusa.</p>
                    <p>Ezen információk tájékoztatást nyújtanak a Szolgáltató részére arról, hogy a felhasználók milyen
                        böngészővel használják internetes oldalakat, a Szolgáltató mely oldalain haladnak át a látogatás
                        alkalmával és segítséget nyújtanak weboldal fejlesztéséhez.</p>
                    <p>A legtöbb böngésző automatikusan rögzít cookie-kat, azonban a Felhasználó saját böngészője
                        beállításainak módosításával deaktiválhatja a cookie-k tárolását avagy előzetes figyelmeztetést
                        kérhet a cookie-k rögzítéséről. A cookie-k el nem fogadásakor előfordulhat, hogy egyes
                        internetes oldalak nem működnek tökéletesen. Ne feledje, ha más és más számítógépet használ
                        különböző helyeken, biztosítania kell, hogy mindegyik böngésző igazodjon az Ön által előnyben
                        részesített cookie beállításokhoz.</p>
                    <p>Továbbá a cookie lehet elsődleges vagy harmadik fél cookie. Az elsődleges cookie az internetes
                        oldal tulajdonosa javára kerül rögzítésre. Egyes esetekben bizonyos cookie-kat harmadik fél –
                        általában aki az internetes oldal tulajdonosának nyújt szolgáltatásokat – kezel számunkra, jelen
                        esetben az SWNet.</p>
                    <p>Fentiek alapján a honlap használatával hozzájárulását adja a cookie-k fentiek szerinti igénybe
                        vételéhez és rögzítéséhez.</p>

                    <h4 className="h4">8.8. Adatvédelmi nyilvántartás</h4>
                    <p>A Szolgáltató az adatkezelést a Nemzeti Adatvédelmi és Információszabadság Hatóság által vezetett
                        nyilvántartásba bejelentette. Az adatkezelés nyilvántartási száma: NAIH-76926/2014.</p>

                    <h3 className="h3">9. Az ÁSZF egyoldalú módosítása</h3>
                    <p>Jelen Felhasználási Feltételek módosításának jogát - a felhasználók előzetes e-mailes
                        tájékoztatása mellett - a Szolgáltató fenntartja. A módosított rendelkezések a közzétételt
                        követően válnak hatályossá.</p>
                    <p>Az Általános Felhasználási Feltételek jelenlegi változatának hatályba lépési dátuma:</p>
                    <p>Jelen Szabályzatban nem szabályozott kérdésekben a Polgári Törvénykönyv, a távollévők között
                        kötött szerződésekről szóló 45/2014. (II.26) Kormányrendelet és az elektronikus kereskedelmi
                        szolgáltatások valamint az információs társadalommal összefüggő szolgáltatások egyes kérdéseiről
                        szóló 2001. évi CVIII. törvény rendelkezései az irányadók.</p>

                </section>
            </div>
        );
    }
}

Terms.propTypes = {};
Terms.defaultProps = {};

export default Terms;